﻿package 
{
	import com.cupcake.Images;
	
	public final class Page16 extends WBZ_BookPage
	{
		public function Page16()
		{
			super();
			imageFolder = GlobalData.pagesFolder;
			Images.Add( "BG", { imageName: "p16", fromFile: true, extension: ".jpg" } );
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic6";
			
			
			super.Init();
			Reset();
		}		
	}		
}