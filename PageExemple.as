﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.BinaryDataLoader;
	import org.as3wavsound.WavSound;
	import flash.utils.ByteArray;
	import org.as3wavsound.WavSoundChannel;
	
	public final class Page01 extends WBZ_BookPage
	{
		
		private var door:TapSpritesheet;
		private var dog:TapSpritesheet;
		private var waterfall:TapSpritesheet;
		private var butterfly:TapSpritesheet;
		
		
		public var doorSounds:SoundCollection;
		public var cherrySounds:SoundCollection;
		public var giggleSounds:SoundCollection;
		
		public var snd:WavSound;
		public var channel:WavSoundChannel
		
		public var sndButterfly:SoundEx			= null;
		public var sndDog:SoundEx			= null;
		public var sndFlower1:SoundEx			= null;
		public var sndFlower2:SoundEx			= null;
		public var sndBush1:SoundEx			= null;
		public var sndBush2:SoundEx			= null;
		
		private var Particles:SB_Particles			= null;
		
		private var dogOut:Boolean = false;
		private var dogBarkCounter:int = 0;
		
		private var firstTurn:Boolean = true;
		
		
		private var tossableArray:Array;
		
		private var itemsArray:Array;
		private var itemsXArray:Array;
		private var itemsYArray:Array;
		
		
		//Core.
		public function Page01(){
			super();
			
			Particles = new SB_Particles(false);
			
			
			imageFolder = CONFIG::DATA + "images/pages/page01/";
			Images.Add( "P01_BG", { container: "BG", fromFile: true } );
			Images.Add( "P01_HOUSE1_OL", { container: "strawberryHouse_mc", fromFile: true } );
			Images.Add( "P01_HOUSE2_OL", { container: "orangeHouse_mc", fromFile: true } );
			Images.Add( "P01_HOUSE3_OL", { container: "lemonHouse_mc", fromFile: true } );
			Images.Add( "P01_STRAWBERRY3", { container: "strawberryBush3_mc", fromFile: true } );
			Images.Add( "P01_FLOWER2", { container: scene_mc.flower1_mc.flower1_mc.flower1_mc, fromFile: true } );
			Images.Add( "P01_FLOWER1", { container: scene_mc.flower2_mc.flower2_mc.flower2_mc, fromFile: true } );
			Images.Add( "P01_STRAWBERRY1", { container: scene_mc.strawberryBush1_mc.strawberryBush1_mc.strawberryBush1_mc, fromFile: true } );
			Images.Add( "P01_STRAWBERRY2", { container: scene_mc.strawberryBush2_mc.strawberryBush2_mc.strawberryBush2_mc, fromFile: true } );
			Images.Add( "P01_PURPLE", { container: scene_mc.purpleFlower_mc.purpleFlower_mc.purpleFlower_mc, fromFile: true } );
			
			
			/*
			
		P1_giggle
			*/
			
			W_Loader.Add( "P08_Kid_2", null, null, "kid2_mc");
			
			
			var ldr:BinaryDataLoader = new BinaryDataLoader( CONFIG::DATA + "sounds/Animation_Sounds/P1_waterfall.wav", { onComplete: WaterfallLoaded } );
   			ldr.load();
			
			sndButterfly = new SoundEx( "sndButterfly", CONFIG::DATA + "sounds/Animation_Sounds/P1_butterfly/P1_butterfly.mp3", App.soundVolume );
			sndDog = new SoundEx( "sndDog", CONFIG::DATA + "sounds/Animation_Sounds/P1_dog_bark/P1_dog_bark.mp3", App.soundVolume );
			sndFlower1 = new SoundEx( "sndFlower1", CONFIG::DATA + "sounds/Animation_Sounds/P1_flower1.mp3", App.soundVolume );
			sndFlower2 = new SoundEx( "sndFlower2", CONFIG::DATA + "sounds/Animation_Sounds/P1_flower2.mp3", App.soundVolume );
			sndBush1 = new SoundEx( "sndBush1", CONFIG::DATA + "sounds/Animation_Sounds/P1_strawberry_patch1.mp3", App.soundVolume );
			sndBush2 = new SoundEx( "sndBush2", CONFIG::DATA + "sounds/Animation_Sounds/P1_strawberry_patch2.mp3", App.soundVolume );
			
			
			cherrySounds = SoundCollection.GetAnimSounds( "P1_Cherry", [1] );
			TapMultiLite.Add( "cherry", { imageName: "P01_CHERRY_00", sounds: cherrySounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			giggleSounds = SoundCollection.GetAnimSounds( "P1_giggle", [1,2] );
			TapMultiLite.Add( "strawberry", { imageName: "P01_STRAWBERRY_00", sounds: giggleSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "raspberry", { imageName: "P01_RASPBERRY_00", sounds: giggleSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "lemon", { imageName: "P01_LEMON_00", sounds: giggleSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			var bugSeq:SpriteSheetSequence = new SpriteSheetSequence( "bug", [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,
			51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70], true, 60 );
			
			var waterfallSeq:SpriteSheetSequence = new SpriteSheetSequence( "waterfall", SpriteSheetSequence.createRange( 0, 9, false, 0 ), true, 60 );
			waterfall = spriteSheetLoader.Add( "P01_WATERFALL", 60, null, null, false, false, new <SpriteSheetSequence>[waterfallSeq] );
			
			doorSounds = SoundCollection.GetAnimSounds( "P1_door", [1] );
			door = spriteSheetLoader.Add( "P01_DOOR", 60, doorSounds, null, false, false);
			
			butterfly = spriteSheetLoader.Add( "P01_BUTTERFLY", 60, null, null, false, false, null, null, scene_mc.butterfly_mc.butterfly_mc.P01_BUTTERFLY );
			
			var dog1Seq:SpriteSheetSequence = new SpriteSheetSequence( "dog1", SpriteSheetSequence.createRange( 0, 23, false, 23 ), false, 60 );
			var dog2Seq:SpriteSheetSequence = new SpriteSheetSequence( "dog2", SpriteSheetSequence.createRange( 24, 33, false, 0 ), false, 60 );
			dog = spriteSheetLoader.Add( "P01_PUPCAKE", 60, null, null, false, false, new <SpriteSheetSequence>[dog1Seq,dog2Seq] );
			
			sugar = spriteSheetLoader.Add( "SS_P10_SUGAR", 60, null, null, false, false, null, null, scene_mc.sugar_mc.sugar_mc.sugar_mc);
			sugar.onComplete = sugarComplete;
			
			
			
			
			tossableArray = new Array();
			
			itemsArray = new Array;
			itemsArray.push(scene_mc.tossable1_mc,
						   scene_mc.tossable2_mc,
						   scene_mc.tossable3_mc,
						   scene_mc.tossable4_mc);
			
			itemsXArray = new Array;
			itemsYArray = new Array;
			for(var X:int = 0; X < itemsArray.length; X++)
			{
				itemsXArray.push(itemsArray[X].x);
				itemsYArray.push(itemsArray[X].y);
				Spawn_Tossable(itemsArray[X]);
			}
			initItems();
			
			//for doing something after the VO is done
			if ( this.step == BookPage.STEP_DONE )
			{
				
			}
			
			scene_mc.targetZone_mc.hitTestPoint(mouseX, mouseY, true)
			scene_mc.targetZone_mc.hitTestObject(scene_mc.character_mc)
			(controls[0] as TapMultiLite).doTap();
			var newVar = Math.floor(Math.random() * 4) + 1;
			var index:int = scene_mc.getChildIndex(scene_mc.carriage_mc);
			scene_mc.setChildIndex(tossableArray[X][0], index);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic1";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			Particles.Clear_Particles_Que();         //Clear Call.	
			Particles.Clear_All_Particles(this);	 //Clear Call.	
			
			dogOut = false;
			scene_mc.P01_DOOR_BTN.mouseEnabled = true;
			scene_mc.dogBark_BTN.mouseEnabled = false;
			dogBarkCounter = 0;
			firstTurn = true;
			
			
			initItems();
			
			
			if(channel != null)
			{
				channel.stop();
			}
			
			sndButterfly.Stop();
			
			
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			if(snd != null)
			{
				snd = null;
			}
			if(channel != null)
			{
				channel.stop();
				channel = null;
			}
			
			Particles.Clear_Particles_Que();        //Clear Call.
			Particles.Clear_All_Particles(this);	//Clear Call.	
			
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			Select_Tossable(mouseX, mouseY);
			
			switch(e.target.name)
			{
				case "P01_DOOR_BTN":
				{
					scene_mc.P01_DOOR_BTN.mouseEnabled = false;
					dogOut = true;
				}
				break;
				
				
				case "dogBark_BTN":
				{
					if(dogBarkCounter == 0)
					{
						dogBarkCounter = 1;
						sndDog.Start();
						dog.playSequence( "dog2" );
					}
				}
				break;
				
				case "butterflyGo_BTN":
				{
					if(scene_mc.butterfly_mc.currentFrame == 1 ||
					   scene_mc.butterfly_mc.currentFrame == 101 ||
					   scene_mc.butterfly_mc.currentFrame == 201 ||
					   scene_mc.butterfly_mc.currentFrame == 301)
					{
						sndButterfly.Start();
						scene_mc.butterfly_mc.play();
					}
				}
				break;
				
				case "flower1_BTN":
				{
					if(scene_mc.flower1_mc.currentFrame == 1)
					{
						sndFlower1.Start();
						Particles.Q_Particles(1, 12, 370, 100, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 315, 15, 0, 90, 1, 1);
						
						scene_mc.flower1_mc.play();
					}
				}
				break;
				
				case "flower2_BTN":
				{
					if(scene_mc.flower2_mc.currentFrame == 1)
					{
						sndFlower2.Start();
						Particles.Q_Particles(1, 12, 880, 130, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 15, 0, 90, 1, 1);
						
						scene_mc.flower2_mc.play();
					}
				}
				break;
				
				case "strawberryBush1_BTN":
				{
					if(scene_mc.strawberryBush1_mc.currentFrame == 1)
					{
						sndBush1.Start();
						scene_mc.strawberryBush1_mc.play();
					}
				}
				break;
				
				case "strawberryBush2_BTN":
				{
					if(scene_mc.strawberryBush2_mc.currentFrame == 1)
					{
						sndBush2.Start();
						scene_mc.strawberryBush2_mc.play();
					}
				}
				break;
				
				case "purpleFlower_BTN":
				{
					if(scene_mc.purpleFlower_mc.currentFrame == 1)
					{
						sndFlower1.Start();
						Particles.Q_Particles(1, 8, 770, 450, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 270, 20, 20, 90, 1, 1);
						
						scene_mc.purpleFlower_mc.play();
					}
				}
				break;
				
				
			}
		}		
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			UnSelect_Tossable();			
			
			
		}
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Particles.SB_Update(this);
				Update_Tossable();
				
				if(dogBarkCounter > 0)
				{
					dogBarkCounter++;
				}
				if(dogBarkCounter >= 10)
				{
					dogBarkCounter = 0;
				}
				
				if(firstTurn)
				{
					channel = snd.play( 0, int.MAX_VALUE, App.soundVolume.sndTransform );
					firstTurn = false;
					waterfall.doTap();
				}
				
				if((scene_mc.butterfly_mc.currentFrame > 1 && scene_mc.butterfly_mc.currentFrame < 101) ||
				(scene_mc.butterfly_mc.currentFrame > 101 && scene_mc.butterfly_mc.currentFrame < 201) ||
				(scene_mc.butterfly_mc.currentFrame > 201 && scene_mc.butterfly_mc.currentFrame < 301) ||
				(scene_mc.butterfly_mc.currentFrame > 301 && scene_mc.butterfly_mc.currentFrame < 400))
				{
					butterfly.doTap();
				}
				
				dogComesOut();
			}
			super.handleFrame( e );
		}		
		
		
		private function dogComesOut():void
		{
			if(dogOut && !door.isPlaying)
			{
				dogOut = false;
				dog.doTap();
				scene_mc.dogBark_BTN.mouseEnabled = true;
				
			}
		}
		
		
		private function WaterfallLoaded( e:LoaderEvent ):void
		{
			snd = new WavSound( e.target.content as ByteArray );
			( e.target as BinaryDataLoader ).dispose( true );
			
		}
		
		
		private function initItems():void 
		{
			for(var X:int = 0; X < itemsArray.length; X++)
			{
				itemsArray[X].x = itemsXArray[X];
				itemsArray[X].y = itemsYArray[X];
				itemsArray[X].rotation = 0;
			}
			
		}
		
		
		private function Spawn_Tossable(clip:Placeholder):void
		{
			
			var tossable_Props:Array = new Array();
			
			
			var Phase:int = 1;			
			var Force_X:Number = ((Math.random() * 40) - 20);
			var Force_Y:Number = -40.0;
			
			tossable_Props.push(clip);
			tossable_Props.push(Phase);
			tossable_Props.push(Force_X);
			tossable_Props.push(Force_Y);
			tossable_Props.push(0);
			
			tossableArray.push(tossable_Props);
			
		}
		
		private function Select_Tossable(pX:int, pY:int):void
		{
			for(var X:int = 0; X < tossableArray.length; X++){
				if(tossableArray[X][0].hitTestPoint(mouseX, mouseY, true)){
					tossableArray[X][1] = 2;
					tossableArray[X][4] = 0;
					scene_mc.setChildIndex(tossableArray[X][0], (scene_mc.numChildren - 1));
					return;
				}
			}
		}
		private function UnSelect_Tossable():void
		{
			for(var X:int = 0; X < tossableArray.length; X++)
			{
				switch(tossableArray[X][1])
				{
					case 2:
					{
						var Angle:Number = Math.atan2((mouseY - tossableArray[X][0].y), (mouseX - tossableArray[X][0].x));
						var P1:Point = new Point(mouseX, mouseY);
						var P2:Point = new Point(tossableArray[X][0].x, tossableArray[X][0].y);
						var Speed:Number = Point.distance(P1, P2);
						if(Speed > 30){Speed = 30;}
						tossableArray[X][2] = (0 + (Speed * Math.cos(Angle)));
						tossableArray[X][3] = (0 + (Speed * Math.sin(Angle)));
						tossableArray[X][1] = 0;
					}break;
				}
			}
		}
		private function Update_Tossable():void
		{			
		
		
			for(var X:int = 0; X < tossableArray.length; X++){
				
				//scene_mc.setChildIndex(tossableArray[X][0], (scene_mc.numChildren - 1));
				
				switch(tossableArray[X][1]){
					case 0:{					
						
						tossableArray[X][0].x = (tossableArray[X][0].x + tossableArray[X][2]);
						tossableArray[X][0].y = (tossableArray[X][0].y + tossableArray[X][3]);		
						
						tossableArray[X][0].rotation = (tossableArray[X][0].rotation + tossableArray[X][2]);
						
						//Y Force.
						tossableArray[X][3] = Math.floor(tossableArray[X][3] + 3.0);
						
						
						if(tossableArray[X][0].y >= 550){
							tossableArray[X][3] = -Math.floor(tossableArray[X][3] - (tossableArray[X][3] / 2)); 							
							if(tossableArray[X][3] > -4){
								tossableArray[X][3] = 0;
							}
							tossableArray[X][0].y = 550;
							tossableArray[X][4] = (tossableArray[X][4] + 1);
							if(tossableArray[X][4] > 2){tossableArray[X][3] = 0; tossableArray[X][2] = 0;}
							if(tossableArray[X][3] != 0)
							{
								//sndPizza.Start();
							}
						}		
						//X Force.										
						if(tossableArray[X][0].x >= 1024 + clsMain.offsetX){
							tossableArray[X][2] = -Math.floor(tossableArray[X][2] - (tossableArray[X][2] / 3)); 
							tossableArray[X][0].x = 1023 + clsMain.offsetX;
							if(tossableArray[X][2] != 0)
							{
								//sndPizza.Start();
							}
							
						}
						if(tossableArray[X][0].x <= 0 - clsMain.offsetX){
							tossableArray[X][2] = -Math.floor(tossableArray[X][2] - (tossableArray[X][2] / 3)); 
							tossableArray[X][0].x = 1 - clsMain.offsetX;
							if(tossableArray[X][2] != 0)
							{
								//sndPizza.Start();
							}
							
						}		
						if(tossableArray[X][2] < 0){
							tossableArray[X][2] = (tossableArray[X][2] + 0.1);
							if(tossableArray[X][2] >= 0){
								tossableArray[X][2] = 0;
							}
						}
						else if(tossableArray[X][2] > 0){
							tossableArray[X][2] = (tossableArray[X][2] - 0.1);
							if(tossableArray[X][2] <= 0){
								tossableArray[X][2] = 0;
							}
						}
									
					}break;
					case 1:{
						
					}break;		
					case 2:{
						tossableArray[X][0].x = mouseX;
						tossableArray[X][0].y = mouseY;
					}break;		
				}
			}
		}
		
		
		
		
		
		private function sugarComplete(e:TapSpritesheet):void
		{
			//scene_mc.sugar_BTN.mouseEnabled = true;
			scene_mc.sugar_mc.play();
		}
		
		
		
	}
}