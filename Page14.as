﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page14 extends WBZ_BookPage
	{
		public var cherrySounds:SoundCollection;
		public var lemonSounds:SoundCollection;
		public var orangeSounds:SoundCollection;
		public var strawberrySounds:SoundCollection;
		public var berrykinsLeftSounds:SoundCollection;
		public var berrykinsRightSounds:SoundCollection;
		
		public var sndBoombox1:SoundEx			= null;
		public var sndBoombox2:SoundEx			= null;
		public var sndBoombox3:SoundEx			= null;
		public var sndBalloon:SoundEx			= null;
		public var sndConfetti:SoundEx			= null;
		
		
		
		
		private var Particles1:SB_Particles			= null;
		private var Particles2:SB_Particles			= null;
		private var Particles3:SB_Particles			= null;
		
		private var notesToPlay:Boolean = false;
		private var noteSpeed:int = 5;
		
		private var boomboxCounter:int = 0;
		private var confettiCounter:int = 1000;
		
		
		
		//Core.
		public function Page14(){
			super();
			
			Particles1 = new SB_Particles(false);
			Particles2 = new SB_Particles(false);
			Particles3 = new SB_Particles(false);
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page14/";
			Images.Add( "SS_P14_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_BOOMBOX", { container: scene_mc.boombox_mc.actor.boombox_mc, fromFile: true } );
			Images.Add( "SS_P14_STRAWBERRY_BALLOON_01", { container: scene_mc.balloon1_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P14_BLUE_BALLOON", { container: scene_mc.balloon2_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P14_ORANGE_BALLOON", { container: scene_mc.balloon3_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P14_STRAWBERRY_BALLOON_02", { container: scene_mc.balloon4_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P14_MUSIC_NOTE_01", { container: scene_mc.note1_mc.note_mc.note_mc, fromFile: true } );
			Images.Add( "SS_P14_MUSIC_NOTE_02", { container: scene_mc.note2_mc.note_mc.note_mc, fromFile: true } );
			Images.Add( "SS_P14_MUSIC_NOTE_03", { container: scene_mc.note3_mc.note_mc.note_mc, fromFile: true } );
			Images.Add( "SS_P14_MUSIC_NOTE_04", { container: scene_mc.note4_mc.note_mc.note_mc, fromFile: true } );
			
			
			/*
			P14_berrykins_1
			
		public var berrykinsLeftSounds:SoundCollection;
		public var berrykinsRightSounds:SoundCollection;
			240
			
			
			*/
			
			sndBoombox1 = new SoundEx( "sndBoombox1", CONFIG::DATA + "sounds/Animation_Sounds/P14_boombox_1.mp3", App.soundVolume );
			sndBoombox2 = new SoundEx( "sndBoombox2", CONFIG::DATA + "sounds/Animation_Sounds/P14_boombox_2.mp3", App.soundVolume );
			sndBoombox3 = new SoundEx( "sndBoombox3", CONFIG::DATA + "sounds/Animation_Sounds/P14_boombox_3.mp3", App.soundVolume );
			sndBalloon = new SoundEx( "sndBalloon", CONFIG::DATA + "sounds/Animation_Sounds/P14_balloon.mp3", App.soundVolume );
			sndConfetti = new SoundEx( "sndConfetti", CONFIG::DATA + "sounds/Animation_Sounds/P14_confetti.mp3", App.soundVolume );
			
			
			
			cherrySounds = SoundCollection.GetAnimSounds( "P14_CHERRY", [1] );
			TapMultiLite.Add( "cherry", { imageName: "SS_P14_CHERRY_00", sounds: cherrySounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			lemonSounds = SoundCollection.GetAnimSounds( "P14_LEMON", [1] );
			TapMultiLite.Add( "lemon", { imageName: "SS_P14_LEMON_00", sounds: lemonSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			orangeSounds = SoundCollection.GetAnimSounds( "P14_Orange", [1] );
			TapMultiLite.Add( "orange", { imageName: "SS_P14_ORANGE_00", sounds: orangeSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			
			
			strawberrySounds = SoundCollection.GetAnimSounds( "P14_Strawberry", [1,2] );
			spriteSheetLoader.Add( "SS_P14_STRAWBERRY_ANIM", 60, strawberrySounds, null, false, false);
			
			
			berrykinsRightSounds = SoundCollection.GetAnimSounds( "P14_berrykins_2", [1] );
			spriteSheetLoader.Add( "SS_P14_BLUE_BERRY_ANIM", 60, berrykinsRightSounds, null, false, false, null, null, null, scene_mc.berrykinsRight_BTN );
			spriteSheetLoader.Add( "SS_P14_PINK_BERRY_ANIM", 60, null, null, false, false, null, null, null, scene_mc.berrykinsRight_BTN );
			
			
			berrykinsLeftSounds = SoundCollection.GetAnimSounds( "P14_berrykins_1", [1,2] );
			var berrykinPurple1Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykinPurple1", SpriteSheetSequence.createRange( 0, 34, false, 34 ), false, 60 );
			var berrykinPurple2Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykinPurple2", SpriteSheetSequence.createRange( 35, 75, false, 0 ), false, 60 );
			spriteSheetLoader.Add( "SS_P14_PURPLE_BERRY_ANIM", 60, berrykinsLeftSounds, null, false, false, new <SpriteSheetSequence>[berrykinPurple1Seq,berrykinPurple2Seq], null, null, scene_mc.berrykinsLeft_BTN );
			spriteSheetLoader.Add( "SS_P14_YELLOW_BERRY_ANIM", 60, null, null, false, false, null, null, null, scene_mc.berrykinsLeft_BTN );
			
			sparklers.Add(scene_mc.SS_P14_STRAWBERRY_ANIM_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.orange_btn, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.cherry_btn, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.lemon_btn, scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.berrykinsRight_BTN, scene_mc.Sparkler5_sprkl);
			sparklers.Add(scene_mc.balloons_BTN, scene_mc.Sparkler6_sprkl);
			sparklers.Add(scene_mc.berrykinsLeft_BTN, scene_mc.Sparkler7_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic3";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			Particles1.Clear_Particles_Que();         //Clear Call.	
			Particles1.Clear_All_Particles(this);	 //Clear Call.	
			Particles2.Clear_Particles_Que();         //Clear Call.	
			Particles2.Clear_All_Particles(this);	 //Clear Call.	
			Particles3.Clear_Particles_Que();         //Clear Call.	
			Particles3.Clear_All_Particles(this);	 //Clear Call.	
			
			
			scene_mc.note1_mc.gotoAndStop(1);
			scene_mc.note2_mc.gotoAndStop(1);
			scene_mc.note3_mc.gotoAndStop(1);
			scene_mc.note4_mc.gotoAndStop(1);
			
			scene_mc.note1_mc.x = 1500;
			scene_mc.note2_mc.x = 1500;
			scene_mc.note3_mc.x = 1500;
			scene_mc.note4_mc.x = 1500;
			
			notesToPlay = false;
			
			confettiCounter = 1000;
			
			boomboxCounter = 0;
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			Particles1.Clear_Particles_Que();        //Clear Call.
			Particles1.Clear_All_Particles(this);	//Clear Call.	
			Particles2.Clear_Particles_Que();        //Clear Call.
			Particles2.Clear_All_Particles(this);	//Clear Call.	
			Particles3.Clear_Particles_Que();        //Clear Call.
			Particles3.Clear_All_Particles(this);	//Clear Call.	
			
			sndConfetti.Stop();
			stopBoombox();
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "boombox_BTN":
				{
					if(scene_mc.boombox_mc.currentFrame == 1)
					{
						if(scene_mc.note1_mc.x > 1400 &&
						   scene_mc.note2_mc.x > 1400 &&
						   scene_mc.note3_mc.x > 1400 &&
						   scene_mc.note4_mc.x > 1400)
						{
							notesToPlay = true;
						}
						
						stopBoombox();
						boomboxCounter++;
						
						if(boomboxCounter == 1 && this.step == BookPage.STEP_DONE)
						{
							sndBoombox1.Start();
						}
						if(boomboxCounter == 2 && this.step == BookPage.STEP_DONE)
						{
							sndBoombox2.Start();
						}
						if(boomboxCounter >= 3 && this.step == BookPage.STEP_DONE)
						{
							sndBoombox3.Start();
							boomboxCounter = 0;
						}
						
						scene_mc.boombox_mc.play();
					}
				}
				break;
				
				case "balloons_BTN":
				{
					if(scene_mc.balloon1_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBalloon.Start();
						}
						
						scene_mc.balloon1_mc.play();
						scene_mc.balloon2_mc.play();
						scene_mc.balloon3_mc.play();
						scene_mc.balloon4_mc.play();
					}
				}
				break;
				
				case "gems_BTN":
				{
					if(confettiCounter >= 240)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndConfetti.Start();
							confettiCounter = 0;
						}
						
					}
					
					Particles1.Q_Particles(1, 12, 229, -50, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
					Particles2.Q_Particles(1, 12, 579, -50, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
					Particles3.Q_Particles(1, 12, 829, -50, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
				}
				break;
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Particles1.SB_Update(this);
				Particles2.SB_Update(this);
				Particles3.SB_Update(this);
				playNotes();
				
				
				if(confettiCounter < 1000)
				{
					confettiCounter++;
				}
			}
			super.handleFrame( e );
		}		
		
		
		private function playNotes():void
		{
			if(notesToPlay)
			{
				
				notesToPlay = false;
				
				var whichNoteNotPlayed:int = Math.floor(Math.random() * 4) + 1;
				
				if(whichNoteNotPlayed != 1)
				{
					scene_mc.note1_mc.x = Math.floor(Math.random() * 75) + 154;
					scene_mc.note1_mc.y = Math.floor(Math.random() * 50) + 175;
					scene_mc.note1_mc.play();
				}
				
				if(whichNoteNotPlayed != 2)
				{
					scene_mc.note2_mc.x = Math.floor(Math.random() * 75) + 154;
					scene_mc.note2_mc.y = Math.floor(Math.random() * 50) + 175;
					scene_mc.note2_mc.play();
				}
				
				if(whichNoteNotPlayed != 3)
				{
					scene_mc.note3_mc.x = Math.floor(Math.random() * 75) + 154;
					scene_mc.note3_mc.y = Math.floor(Math.random() * 50) + 175;
					scene_mc.note3_mc.play();
				}
				
				if(whichNoteNotPlayed != 4)
				{
					scene_mc.note4_mc.x = Math.floor(Math.random() * 75) + 154;
					scene_mc.note4_mc.y = Math.floor(Math.random() * 50) + 175;
					scene_mc.note4_mc.play();
				}
			}
			
			if(scene_mc.note1_mc.x < 1400)
			{
				scene_mc.note1_mc.y -= noteSpeed;
				
				if(scene_mc.note1_mc.currentFrame == scene_mc.note1_mc.totalFrames)
				{
					scene_mc.note1_mc.gotoAndStop(1);
					scene_mc.note1_mc.x = 1500;
				}
			}
			
			if(scene_mc.note2_mc.x < 1400)
			{
				scene_mc.note2_mc.y -= noteSpeed;
				
				if(scene_mc.note2_mc.currentFrame == scene_mc.note2_mc.totalFrames)
				{
					scene_mc.note2_mc.gotoAndStop(1);
					scene_mc.note2_mc.x = 1500;
				}
			}
			
			if(scene_mc.note3_mc.x < 1400)
			{
				scene_mc.note3_mc.y -= noteSpeed;
				
				if(scene_mc.note3_mc.currentFrame == scene_mc.note3_mc.totalFrames)
				{
					scene_mc.note3_mc.gotoAndStop(1);
					scene_mc.note3_mc.x = 1500;
				}
			}
			
			if(scene_mc.note4_mc.x < 1400)
			{
				scene_mc.note4_mc.y -= noteSpeed;
				
				if(scene_mc.note4_mc.currentFrame == scene_mc.note4_mc.totalFrames)
				{
					scene_mc.note4_mc.gotoAndStop(1);
					scene_mc.note4_mc.x = 1500;
				}
			}
		}
		
		
		private function stopBoombox():void
		{
			sndBoombox1.Stop();
			sndBoombox2.Stop();
			sndBoombox3.Stop();
		}
		
		
		
		
		
		
	}
}