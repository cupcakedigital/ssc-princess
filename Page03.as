﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page03 extends WBZ_BookPage
	{
		
		private var pupcake:TapSpritesheet;
		private var custard:TapSpritesheet;
		private var berrykins:TapSpritesheet;
		private var strawberry:TapSpritesheet;
		private var cherry:TapSpritesheet;
		
		public var sndStrawberry1:SoundEx			= null;
		public var sndStrawberry2:SoundEx			= null;
		public var sndStrawberry3:SoundEx			= null;
		public var sndStrawberryFound:SoundEx			= null;
		public var sndCherry1:SoundEx			= null;
		public var sndCherry2:SoundEx			= null;
		public var sndCherry3:SoundEx			= null;
		public var sndCherryFound:SoundEx			= null;
		public var sndCrown:SoundEx			= null;
		public var sndCat:SoundEx			= null;
		public var sndDog:SoundEx			= null;
		public var sndBerrykin1:SoundEx			= null;
		public var sndBerrykin2:SoundEx			= null;
		public var sndBerrykin3:SoundEx			= null;
		
		
		
		private var Particles:SB_Particles			= null;
		
		private var crownCounter:int = 0;
		private var berrykinCounter:int = 0;
		
		private var strawberryCounter:int = 1;
		private var cherryCounter:int = 1;
		private var girlsTurn:Boolean = true;
		
		
		
		//Core.
		public function Page03(){
			super();
			
			Particles = new SB_Particles(false);
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page03/";
			Images.Add( "SS_P03_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_P03_FLOWER_OL", { container: "yellowFlower_mc", fromFile: true } );
			Images.Add( "SS_P03_CROWN", { container: "crown_mc", fromFile: true } );
			
			
			/*
			P3_berrykin
			P3_cat
			P3_dog
			
			
		public var sndCat:SoundEx			= null;
		public var sndDog:SoundEx			= null;
		public var sndBerrykin1:SoundEx			= null;
		public var sndBerrykin2:SoundEx			= null;
		public var sndBerrykin3:SoundEx			= null;
			*/
			
			sndStrawberry1 = new SoundEx( "sndStrawberry1", CONFIG::DATA + "sounds/Animation_Sounds/P03_STRAWBERRYFLAP_1.mp3", App.soundVolume );
			sndStrawberry2 = new SoundEx( "sndStrawberry2", CONFIG::DATA + "sounds/Animation_Sounds/P03_STRAWBERRYFLAP_2.mp3", App.soundVolume );
			sndStrawberry3 = new SoundEx( "sndStrawberry3", CONFIG::DATA + "sounds/Animation_Sounds/P03_STRAWBERRYFLAP_3.mp3", App.soundVolume );
			sndStrawberryFound = new SoundEx( "sndStrawberryFound", CONFIG::DATA + "sounds/Animation_Sounds/P03_STRAWBERRYFLAP_FOUND.mp3", App.soundVolume );
			sndCherry1 = new SoundEx( "sndCherry1", CONFIG::DATA + "sounds/Animation_Sounds/P03_CHERRYFLAP_1.mp3", App.soundVolume );
			sndCherry2 = new SoundEx( "sndCherry2", CONFIG::DATA + "sounds/Animation_Sounds/P03_CHERRYFLAP_2.mp3", App.soundVolume );
			sndCherry3 = new SoundEx( "sndCherry3", CONFIG::DATA + "sounds/Animation_Sounds/P03_CHERRYFLAP_3.mp3", App.soundVolume );
			sndCherryFound = new SoundEx( "sndCherryFound", CONFIG::DATA + "sounds/Animation_Sounds/P03_CHERRYFLAP_FOUND.mp3", App.soundVolume );
			sndCrown = new SoundEx( "sndCrown", CONFIG::DATA + "sounds/Animation_Sounds/P3_crown.mp3", App.soundVolume );
			sndCat = new SoundEx( "sndCat", CONFIG::DATA + "sounds/Animation_Sounds/P3_cat_1.mp3", App.soundVolume );
			sndDog = new SoundEx( "sndDog", CONFIG::DATA + "sounds/Animation_Sounds/P3_dog_1.mp3", App.soundVolume );
			sndBerrykin1 = new SoundEx( "sndBerrykin1", CONFIG::DATA + "sounds/Animation_Sounds/P3_berrykin_1.mp3", App.soundVolume );
			sndBerrykin2 = new SoundEx( "sndBerrykin2", CONFIG::DATA + "sounds/Animation_Sounds/P3_berrykin_2.mp3", App.soundVolume );
			sndBerrykin3 = new SoundEx( "sndBerrykin3", CONFIG::DATA + "sounds/Animation_Sounds/P3_berrykin_3.mp3", App.soundVolume );
			
			
			
			TapMultiLite.Add( "flowers", { imageName: "SS_P03_FLOWER_FLAP_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "bush1", { imageName: "SS_P03_STRAWBERRY_FLAP_UPPER_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "bush2", { imageName: "SS_P03_STRAWBERRY_FLAP_SIDE_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "bush3", { imageName: "SS_P03_STRAWBERRY_FLAP_BACK_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			
			
			pupcake = spriteSheetLoader.Add( "SS_P03_PUPCAKE_ANIM", 60, null, null, false, false);
			pupcake.onComplete = pupcakeComplete;
			
			custard = spriteSheetLoader.Add( "SS_P03_CUSTARD_ANIM", 60, null, null, true, false );
			custard.onComplete = custardComplete;
			
			var berrykins1Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykins1", SpriteSheetSequence.createRange( 0, 0, false, 0 ), false, 60 );
			var berrykins2Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykins2", SpriteSheetSequence.createRange( 1, 1, false, 1 ), false, 60 );
			var berrykins3Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykins3", SpriteSheetSequence.createRange( 2, 2, false, 2 ), false, 60 );
			var berrykins4Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykins4", SpriteSheetSequence.createRange( 3, 3, false, 3 ), false, 60 );
			var berrykins5Seq:SpriteSheetSequence = new SpriteSheetSequence( "berrykins5", SpriteSheetSequence.createRange( 4, 4, false, 4 ), false, 60 );
			berrykins = spriteSheetLoader.Add( "SS_P03_BERRYKINS", 60, null, null, false, false, new <SpriteSheetSequence>[berrykins1Seq,berrykins2Seq,berrykins3Seq,berrykins4Seq,berrykins5Seq] );
			
			
			var strawberry1Seq:SpriteSheetSequence = new SpriteSheetSequence( "strawberry1", SpriteSheetSequence.createRange( 0, 28, false, 28 ), false, 60 );
			var strawberry2Seq:SpriteSheetSequence = new SpriteSheetSequence( "strawberry2", SpriteSheetSequence.createRange( 28, 50, false, 0 ), false, 60 );
			strawberry = spriteSheetLoader.Add( "SS_P03_STRAWBERRY_ANIM", 60, null, null, false, false, new <SpriteSheetSequence>[strawberry1Seq,strawberry2Seq] );
			
			
			var cherry1Seq:SpriteSheetSequence = new SpriteSheetSequence( "cherry1", SpriteSheetSequence.createRange( 0, 24, false, 24 ), false, 60 );
			var cherry2Seq:SpriteSheetSequence = new SpriteSheetSequence( "cherry2", SpriteSheetSequence.createRange( 24, 47, false, 0 ), false, 60 );
			cherry = spriteSheetLoader.Add( "SS_P03_CHERRY_ANIM", 60, null, null, false, false, new <SpriteSheetSequence>[cherry1Seq,cherry2Seq] );
			
			sparklers.Add(scene_mc.strawberry_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.cherry_BTN, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.flowers_btn, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.bush3_btn, scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.bush2_btn, scene_mc.Sparkler6_sprkl);
			sparklers.Add(scene_mc.bush1_btn, scene_mc.Sparkler5_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic4";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			Particles.Clear_Particles_Que();         //Clear Call.	
			Particles.Clear_All_Particles(this);	 //Clear Call.	
			
			scene_mc.flowers_btn.mouseEnabled = true;
			scene_mc.bush1_btn.mouseEnabled = true;
			scene_mc.bush2_btn.mouseEnabled = true;
			scene_mc.bush3_btn.mouseEnabled = true;
			scene_mc.strawberry_BTN.mouseEnabled = true;
			scene_mc.cherry_BTN.mouseEnabled = true;
			
			girlsTurn = true;
			
			crownCounter = 0;
			berrykinCounter = 0;
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			
			Particles.Clear_Particles_Que();        //Clear Call.
			Particles.Clear_All_Particles(this);	//Clear Call.	
			
			
			stopVoices();
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "strawberry_BTN":
				{
					searchVoiceStrawberry();
					strawberry.playSequence( "strawberry1" );
				}
				break;
				
				case "cherry_BTN":
				{
					searchVoiceCherry();
					cherry.playSequence( "cherry1" );
				}
				break;
				
				case "flowers_btn":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndDog.Start();
					}
					
					searchVoiceStrawberry();
					scene_mc.flowers_btn.mouseEnabled = false;
				}
				break;
				
				case "bush1_btn":
				{
					scene_mc.strawberry_BTN.mouseEnabled = false;
					scene_mc.cherry_BTN.mouseEnabled = false;
					scene_mc.bush1_btn.mouseEnabled = false;
					
					if ( this.step == BookPage.STEP_DONE )
					{
						sndCrown.Start();
					}
					
					stopVoices();
					if(girlsTurn)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndStrawberryFound.Start();
						}
						
						girlsTurn = false;
					}
					else
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndCherryFound.Start();
						}
						
						girlsTurn = true;
					}
					
					
				}
				break;
				
				case "bush2_btn":
				{
					searchVoiceCherry();
					var newVar = Math.floor(Math.random() * 5) + 1;
					
					if(newVar == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin1.Start();
						}
						
						berrykins.playSequence( "berrykins1" );
					}
					if(newVar == 2)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin2.Start();
						}
						
						berrykins.playSequence( "berrykins2" );
					}
					if(newVar == 3)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin3.Start();
						}
						
						berrykins.playSequence( "berrykins3" );
					}
					if(newVar == 4)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin1.Start();
						}
						
						berrykins.playSequence( "berrykins4" );
					}
					if(newVar == 5)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin2.Start();
						}
						
						berrykins.playSequence( "berrykins5" );
					}
					
					scene_mc.bush2_btn.mouseEnabled = false;
				}
				break;
				
				case "bush3_btn":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndCat.Start();
					}
					
					searchVoiceStrawberry();
					scene_mc.bush3_btn.mouseEnabled = false;
				}
				break;
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void{
		   if(isRunning && !isPaused)
		   {
			   Particles.SB_Update(this);
			   middleBushAnims();
			   
			   
		   }
		   super.handleFrame( e );
		}		
		
		
		
		
		private function middleBushAnims():void
		{
			if(scene_mc.flowers.currentFrame == 24)
			{
				pupcake.doTap();
			}
			if(scene_mc.flowers.currentFrame == scene_mc.flowers.totalFrames)
			{
				scene_mc.flowers_btn.mouseEnabled = true;
			}
			
			
			if(scene_mc.bush1.currentFrame == 24)
			{
				//stopVoices();
				crownCounter++;
				strawberry.playSequence( "strawberry2" );
				cherry.playSequence( "cherry2" );
				Particles.Q_Particles(1, 12, 803, 146, "Fade_Out", "flowerSparkle_mc", 0.03, 270, 20, 20, 90, 1, 1);
					
			}
			if(crownCounter > 0)
			{
			   crownCounter++;
			}
			if(crownCounter >= 30)
			{
				(controls[1] as TapMultiLite).doTap();
				crownCounter = 0;
			}
			if(scene_mc.bush1.currentFrame == scene_mc.bush1.totalFrames)
			{
				scene_mc.bush1_btn.mouseEnabled = true;
				scene_mc.strawberry_BTN.mouseEnabled = true;
				scene_mc.cherry_BTN.mouseEnabled = true;
			}
			
			
			if(scene_mc.bush2.currentFrame == 24)
			{
				berrykinCounter++;
			}
			if(berrykinCounter > 0)
			{
			   berrykinCounter++;
			}
			if(berrykinCounter >= 30)
			{
				(controls[2] as TapMultiLite).doTap();
				berrykinCounter = 0;
			}
			if(scene_mc.bush2.currentFrame == scene_mc.bush2.totalFrames)
			{
				scene_mc.bush2_btn.mouseEnabled = true;
			}
			
			
			if(scene_mc.bush3.currentFrame == 24)
			{
				custard.doTap();
			}
			if(scene_mc.bush3.currentFrame == scene_mc.bush3.totalFrames)
			{
				
				scene_mc.bush3_btn.mouseEnabled = true;
			}
		}
		
		
		private function pupcakeComplete(e:TapSpritesheet):void
		{
			(controls[0] as TapMultiLite).doTap();
		}
		
		
		private function custardComplete(e:TapSpritesheet):void
		{
			(controls[3] as TapMultiLite).doTap();
		}
		
		
		private function searchVoiceStrawberry():void
		{
			stopVoices();
			if(strawberryCounter == 1 && this.step == BookPage.STEP_DONE)
			{
				sndStrawberry1.Start();
			}
			if(strawberryCounter == 2 && this.step == BookPage.STEP_DONE)
			{
				sndStrawberry2.Start();
			}
			if(strawberryCounter == 3 && this.step == BookPage.STEP_DONE)
			{
				sndStrawberry3.Start();
			}
			
			strawberryCounter++;
			if(strawberryCounter == 4)
			{
				strawberryCounter = 1;
			}
		}
		
		
		private function searchVoiceCherry():void
		{
			stopVoices();
			
			if(cherryCounter == 1 && this.step == BookPage.STEP_DONE)
			{
				sndCherry1.Start();
			}
			if(cherryCounter == 2 && this.step == BookPage.STEP_DONE)
			{
				sndCherry2.Start();
			}
			if(cherryCounter == 3 && this.step == BookPage.STEP_DONE)
			{
				sndCherry3.Start();
			}
			
			cherryCounter++;
			if(cherryCounter == 4)
			{
				cherryCounter = 1;
			}
			
		}
		
		
		private function stopVoices():void
		{
			sndStrawberry1.Stop();
			sndStrawberry2.Stop();
			sndStrawberry3.Stop();
			sndStrawberryFound.Stop();
			sndCherry1.Stop();
			sndCherry2.Stop();
			sndCherry3.Stop();
			sndCherryFound.Stop();
		}
		
		
		
		
	}
}