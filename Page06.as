﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page06 extends WBZ_BookPage
	{
		private var flowersTop:TapSpritesheet;
		private var wheelBig:TapSpritesheet;
		private var wheelSmall:TapSpritesheet;
		private var wheelBack:TapSpritesheet;
		private var lemon:TapSpritesheet;
		private var orange:TapSpritesheet;
		
		
		public var lemonSounds:SoundCollection;
		public var orangeSounds:SoundCollection;
		public var hennaSounds:SoundCollection;
		
		public var sndDone:SoundEx			= null;
		public var sndBerrykin1:SoundEx			= null;
		public var sndBerrykin2:SoundEx			= null;
		public var sndBerrykin3:SoundEx			= null;
		public var sndlemon:SoundEx			= null;
		public var sndFlowers:SoundEx			= null;
		public var sndWheel:SoundEx			= null;
		public var sndHammer:SoundEx			= null;
		public var sndDress:SoundEx			= null;
		
		
		
		private var Particles:SB_Particles			= null;
		
		
		private var tossableArray:Array;
		
		private var itemsArray:Array;
		private var itemsXArray:Array;
		private var itemsYArray:Array;
		
		private var berrykinsCounter:int = 1;
		private var lemonCounter:int = 0;
		private var wheelCounter:int = 0;
		
		
		
		//Core.
		public function Page06(){
			super();
			
			Particles = new SB_Particles(false);
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page06/";
			Images.Add( "SS_P06_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_P06_ORANGE_SAWHORSE", { container: "bench_mc", fromFile: true } );
			Images.Add( "SS_P06_CARRIAGE_BASE", { container: "carriage_mc", fromFile: true } );
			Images.Add( "SS_P06_HOUSE", { container: "houseOL_mc", fromFile: true } );
			
			Images.Add( "SS_P06_FLOWERS", { container: "flowersTossable_mc", align: Align.CENTER, fromFile: true } );
			Images.Add( "SS_P06_CARRIAGE_WHEEL_BIG", { container: "bigTossable_mc", align: Align.CENTER, fromFile: true } );
			Images.Add( "SS_P06_CARRIAGE_WHEEL_SMALL", { container: "smallTossable_mc", align: Align.CENTER, fromFile: true } );
			Images.Add( "SS_P06_CARRIAGE_WHEEL_BACK", { container: "backTossable_mc", align: Align.CENTER, fromFile: true } );
			
			Images.Add( "SS_P06_BERRYKIN_BLUE", { container: scene_mc.berrykin1.berrykin.berrykin, fromFile: true } );
			Images.Add( "SS_P06_BERRYKIN_PURPLE", { container: scene_mc.berrykin2.berrykin.berrykin, fromFile: true } );
			Images.Add( "SS_P06_BERRYKIN_YELLOW", { container: scene_mc.berrykin3.berrykin.berrykin, fromFile: true } );
			
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle1.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle2.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle3.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle4.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle5.bigSparkle.bigSparkle, fromFile: true } );
			
			
			/*
			SS_P06_HENNA
			
			
			*/
			
			sndDone = new SoundEx( "sndDone", CONFIG::DATA + "sounds/Animation_Sounds/P06_DONE.mp3", App.soundVolume );
			sndBerrykin1 = new SoundEx( "sndBerrykin1", CONFIG::DATA + "sounds/Animation_Sounds/P06_berrykin_1.mp3", App.soundVolume );
			sndBerrykin2 = new SoundEx( "sndBerrykin2", CONFIG::DATA + "sounds/Animation_Sounds/P06_berrykin_2.mp3", App.soundVolume );
			sndBerrykin3 = new SoundEx( "sndBerrykin3", CONFIG::DATA + "sounds/Animation_Sounds/P06_berrykin_3.mp3", App.soundVolume );
			sndlemon = new SoundEx( "sndlemon", CONFIG::DATA + "sounds/Animation_Sounds/P06_lemon.mp3", App.soundVolume );
			sndFlowers = new SoundEx( "sndFlowers", CONFIG::DATA + "sounds/Animation_Sounds/P06_flowers.mp3", App.soundVolume );
			sndWheel = new SoundEx( "sndWheel", CONFIG::DATA + "sounds/Animation_Sounds/P06_wheel.mp3", App.soundVolume );
			sndHammer = new SoundEx( "sndHammer", CONFIG::DATA + "sounds/Animation_Sounds/P06_hammer.mp3", App.soundVolume );
			sndDress = new SoundEx( "sndDress", CONFIG::DATA + "sounds/Animation_Sounds/P08_dress.mp3", App.soundVolume );
			
			
			
			
			orangeSounds = SoundCollection.GetAnimSounds( "P06_ORANGETAP", [1] );
			var orange1Seq:SpriteSheetSequence = new SpriteSheetSequence( "orange1", [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30], false, 60 );
			orange = spriteSheetLoader.Add( "SS_P06_ORANGE", 60, orangeSounds, null, false, false, new <SpriteSheetSequence>[orange1Seq]);
			orange.onComplete = orangeComplete;
			
			hennaSounds = SoundCollection.GetAnimSounds( "P07_cinnapup", [1] );
			spriteSheetLoader.Add( "SS_P06_HENNA", 60, hennaSounds, null, false, false);
			
			lemonSounds = SoundCollection.GetAnimSounds( "P06_LEMONTAP", [1,2] );
			lemon = spriteSheetLoader.Add( "SS_P06_LEMON", 60, lemonSounds, null, false, false);
			lemon.onComplete = lemonComplete;
			
			var shadow1Seq:SpriteSheetSequence = new SpriteSheetSequence( "shadow1", SpriteSheetSequence.createRange( 1, 1, false, 1 ), false, 60 );
			var shadow2Seq:SpriteSheetSequence = new SpriteSheetSequence( "shadow2", SpriteSheetSequence.createRange( 0, 0, false, 0 ), false, 60 );
			flowersTop = spriteSheetLoader.Add( "SS_P06_Flowers_Silhouette", 60, null, null, false, false, new <SpriteSheetSequence>[shadow1Seq,shadow2Seq] );
			wheelBig = spriteSheetLoader.Add( "SS_P06_Wheel_Big_Silhouette", 60, null, null, false, false, new <SpriteSheetSequence>[shadow1Seq,shadow2Seq] );
			wheelSmall = spriteSheetLoader.Add( "SS_P06_Wheel_Small_Silhouette", 60, null, null, false, false, new <SpriteSheetSequence>[shadow1Seq,shadow2Seq] );
			wheelBack = spriteSheetLoader.Add( "SS_P06_Wheel_Back_Silhouette", 60, null, null, false, false, new <SpriteSheetSequence>[shadow1Seq,shadow2Seq] );
			
			
			
			tossableArray = new Array();
			
			itemsArray = new Array;
			itemsArray.push(scene_mc.flowersTossable_mc,
						   scene_mc.bigTossable_mc,
						   scene_mc.smallTossable_mc,
						   scene_mc.backTossable_mc);
			
			itemsXArray = new Array;
			itemsYArray = new Array;
			for(var X:int = 0; X < itemsArray.length; X++)
			{
				itemsXArray.push(itemsArray[X].x);
				itemsYArray.push(itemsArray[X].y);
				Spawn_Tossable(itemsArray[X]);
			}
			initItems();
			
			sparklers.Add(scene_mc.SS_P06_ORANGE_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.SS_P06_LEMON_BTN, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.carriageSparkles_BTN, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.SS_P06_HENNA_BTN, scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.berrykins_BTN, scene_mc.Sparkler5_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic3";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			Particles.Clear_Particles_Que();         //Clear Call.	
			Particles.Clear_All_Particles(this);	 //Clear Call.	
			
			scene_mc.carriageSparkles_BTN.mouseEnabled = false;
			scene_mc.SS_P06_LEMON_BTN.mouseEnabled = true;
			scene_mc.SS_P06_ORANGE_BTN.mouseEnabled = true;
			lemonCounter = 0;
			
			wheelCounter = 0;
			
			berrykinsCounter = 1;
			initItems();
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			Particles.Clear_Particles_Que();        //Clear Call.
			Particles.Clear_All_Particles(this);	//Clear Call.	
			
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			Select_Tossable(mouseX, mouseY);
			
			switch(e.target.name)
			{
				case "berrykins_BTN":
				{
					if(scene_mc.berrykin1.currentFrame == 1 && 
					   scene_mc.berrykin2.currentFrame == 1 && 
					   scene_mc.berrykin3.currentFrame == 1 && berrykinsCounter == 1)
					{
						berrykinsCounter++;
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin1.Start();
						}
						
						scene_mc.berrykin1.play();
					}
					else if(scene_mc.berrykin1.currentFrame == 1 && 
					   scene_mc.berrykin2.currentFrame == 1 && 
					   scene_mc.berrykin3.currentFrame == 1 && berrykinsCounter == 2)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin2.Start();
						}
						berrykinsCounter++;
						
						scene_mc.berrykin2.play();
					}
					else if(scene_mc.berrykin1.currentFrame == 1 && 
					   scene_mc.berrykin2.currentFrame == 1 && 
					   scene_mc.berrykin3.currentFrame == 1 && berrykinsCounter == 3)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykin3.Start();
						}
						berrykinsCounter = 1;
						
						scene_mc.berrykin3.play();
					}
				}
				break;
				
				case "SS_P06_LEMON_BTN":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndlemon.Start();
					}
					
					lemonCounter++;
					scene_mc.SS_P06_LEMON_BTN.mouseEnabled = false;
				}
				break;
				
				case "SS_P06_ORANGE_BTN":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndHammer.Start();
					}
					
					scene_mc.SS_P06_ORANGE_BTN.mouseEnabled = false;
				}
				break;
				
				case "carriageSparkles_BTN":
				{
					if(scene_mc.bigSparkle1.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndDress.Start();
						}
						
						scene_mc.bigSparkle1.play();
					}
				}
				break;
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			UnSelect_Tossable();			
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Particles.SB_Update(this);
				Update_Tossable();
				throwGlitters();
				finishAnim();
			}
			super.handleFrame( e );
		}		
		
		
		private function finishAnim():void
		{
			if(scene_mc.bigSparkle1.currentFrame == 6)
			{
				scene_mc.bigSparkle2.play();
			}
			if(scene_mc.bigSparkle1.currentFrame == 12)
			{
				scene_mc.bigSparkle3.play();
			}
			if(scene_mc.bigSparkle1.currentFrame == 18)
			{
				scene_mc.bigSparkle4.play();
			}
			if(scene_mc.bigSparkle1.currentFrame == 23)
			{
				scene_mc.bigSparkle5.play();
				scene_mc.carriageSparkles_BTN.mouseEnabled = true;
			}
		}
		
		
		private function throwGlitters():void 
		{
			if(lemonCounter > 0)
			{
				lemonCounter++;
			}
			if(lemonCounter == 11)
			{
				Particles.Q_Particles(1, 12, 870, 250, "Fade_Out", "flowerSparkle_mc", 0.03, 240, 25, 5, 90, 1, 1);
				
			}
		}
		
		
		private function initItems():void 
		{
			for(var X:int = 0; X < itemsArray.length; X++)
			{
				itemsArray[X].x = itemsXArray[X];
				itemsArray[X].y = itemsYArray[X];
			}
			
			itemsArray[0].rotation = 0;
			itemsArray[1].rotation = 90;
			itemsArray[2].rotation = 50;
			itemsArray[3].rotation = 75;
			
		}
		
		
		private function Spawn_Tossable(clip:Placeholder):void
		{
			
			var tossable_Props:Array = new Array();
			
			
			var Phase:int = 1;			
			var Force_X:Number = ((Math.random() * 40) - 20);
			var Force_Y:Number = -40.0;
			
			tossable_Props.push(clip);
			tossable_Props.push(Phase);
			tossable_Props.push(Force_X);
			tossable_Props.push(Force_Y);
			tossable_Props.push(0);
			
			tossableArray.push(tossable_Props);
			
		}
		
		private function Select_Tossable(pX:int, pY:int):void
		{
			for(var X:int = 0; X < tossableArray.length; X++)
			{
				if(tossableArray[X][0].hitTestPoint(mouseX, mouseY, true) && tossableArray[X][1] != 99)
				{
					shadowOnOff(X,true);
					tossableArray[X][1] = 2;
					tossableArray[X][4] = 0;
					scene_mc.setChildIndex(tossableArray[X][0], (scene_mc.numChildren - 1));
					return;
				}
			}
		}
		private function UnSelect_Tossable():void
		{
			for(var X:int = 0; X < tossableArray.length; X++)
			{
				switch(tossableArray[X][1])
				{
					case 2:
					{
						shadowOnOff(X,false);
						var Angle:Number = Math.atan2((mouseY - tossableArray[X][0].y), (mouseX - tossableArray[X][0].x));
						var P1:Point = new Point(mouseX, mouseY);
						var P2:Point = new Point(tossableArray[X][0].x, tossableArray[X][0].y);
						var Speed:Number = Point.distance(P1, P2);
						if(Speed > 30){Speed = 30;}
						tossableArray[X][2] = (0 + (Speed * Math.cos(Angle)));
						tossableArray[X][3] = (0 + (Speed * Math.sin(Angle)));
						tossableArray[X][1] = 0;
					}break;
				}
			}
		}
		private function Update_Tossable():void
		{			
		
		
			for(var X:int = 0; X < tossableArray.length; X++){
				
				//scene_mc.setChildIndex(tossableArray[X][0], (scene_mc.numChildren - 1));
				
				switch(tossableArray[X][1]){
					case 0:{					
						
						tossableArray[X][0].x = (tossableArray[X][0].x + tossableArray[X][2]);
						tossableArray[X][0].y = (tossableArray[X][0].y + tossableArray[X][3]);		
						
						tossableArray[X][0].rotation = (tossableArray[X][0].rotation + tossableArray[X][2]);
						
						//Y Force.
						tossableArray[X][3] = Math.floor(tossableArray[X][3] + 3.0);
						
						
						if(tossableArray[X][0].y >= 550){
							tossableArray[X][3] = -Math.floor(tossableArray[X][3] - (tossableArray[X][3] / 2)); 							
							if(tossableArray[X][3] > -4){
								tossableArray[X][3] = 0;
							}
							tossableArray[X][0].y = 550;
							tossableArray[X][4] = (tossableArray[X][4] + 1);
							if(tossableArray[X][4] > 2){tossableArray[X][3] = 0; tossableArray[X][2] = 0;}
							if(tossableArray[X][3] != 0)
							{
								//sndPizza.Start();
								if(X == 0 && this.step == BookPage.STEP_DONE)
								{
									sndFlowers.Start();
								}
								else if(this.step == BookPage.STEP_DONE)
								{
									sndWheel.Start();
								}
							}
						}		
						//X Force.										
						if(tossableArray[X][0].x >= 1024 + clsMain.offsetX){
							tossableArray[X][2] = -Math.floor(tossableArray[X][2] - (tossableArray[X][2] / 3)); 
							tossableArray[X][0].x = 1023 + clsMain.offsetX;
							if(tossableArray[X][2] != 0)
							{
								//sndPizza.Start();
								if(X == 0 && this.step == BookPage.STEP_DONE)
								{
									sndFlowers.Start();
								}
								else if(this.step == BookPage.STEP_DONE)
								{
									sndWheel.Start();
								}
							}
							
						}
						if(tossableArray[X][0].x <= 0 - clsMain.offsetX){
							tossableArray[X][2] = -Math.floor(tossableArray[X][2] - (tossableArray[X][2] / 3)); 
							tossableArray[X][0].x = 1 - clsMain.offsetX;
							if(tossableArray[X][2] != 0)
							{
								//sndPizza.Start();
								if(X == 0 && this.step == BookPage.STEP_DONE)
								{
									sndFlowers.Start();
								}
								else if(this.step == BookPage.STEP_DONE)
								{
									sndWheel.Start();
								}
							}
							
						}		
						if(tossableArray[X][2] < 0){
							tossableArray[X][2] = (tossableArray[X][2] + 0.1);
							if(tossableArray[X][2] >= 0){
								tossableArray[X][2] = 0;
							}
						}
						else if(tossableArray[X][2] > 0){
							tossableArray[X][2] = (tossableArray[X][2] - 0.1);
							if(tossableArray[X][2] <= 0){
								tossableArray[X][2] = 0;
							}
						}
									
					}break;
					case 1:{
						
					}break;		
					case 2:{
						tossableArray[X][0].x = mouseX;
						tossableArray[X][0].y = mouseY;
						
						if(X == 0 && scene_mc.target1.hitTestPoint(mouseX, mouseY, true))
						{
							tossableArray[X][0].x = 576;
							tossableArray[X][0].y = 111;
							tossableArray[X][0].rotation = 0;
							tossableArray[X][1] = 99;
							wheelCounter++;
							if ( this.step == BookPage.STEP_DONE )
							{
								sndFlowers.Start();
							}
							
							
							shadowOnOff(X,false);
						}
						if(X == 1 && scene_mc.target2.hitTestPoint(mouseX, mouseY, true))
						{
							tossableArray[X][0].x = 387;
							tossableArray[X][0].y = 389;
							tossableArray[X][0].rotation = 0;
							tossableArray[X][1] = 99;
							wheelCounter++;
							if ( this.step == BookPage.STEP_DONE )
							{
								sndWheel.Start();
							}
							
							
							shadowOnOff(X,false);
						}
						if(X == 2 && scene_mc.target3.hitTestPoint(mouseX, mouseY, true))
						{
							tossableArray[X][0].x = 640;
							tossableArray[X][0].y = 449;
							tossableArray[X][0].rotation = 0;
							tossableArray[X][1] = 99;
							wheelCounter++;
							if ( this.step == BookPage.STEP_DONE )
							{
								sndWheel.Start();
							}
							
							
							shadowOnOff(X,false);
						}
						if(X == 3 && scene_mc.target4.hitTestPoint(mouseX, mouseY, true))
						{
							tossableArray[X][0].x = 785;
							tossableArray[X][0].y = 423;
							tossableArray[X][0].rotation = 0;
							tossableArray[X][1] = 99;
							wheelCounter++;
							if ( this.step == BookPage.STEP_DONE )
							{
								sndWheel.Start();
							}
							
							
							var index:int = scene_mc.getChildIndex(scene_mc.carriage_mc);
							scene_mc.setChildIndex(tossableArray[X][0], index);
							
							shadowOnOff(X,false);
						}
						if(wheelCounter == 4)
						{
							wheelCounter = 0;
							if ( this.step == BookPage.STEP_DONE )
							{
								sndDone.Start();
							}
							
						}
						
					}break;		
				}
			}
		}
		
		
		private function shadowOnOff(whichItem:int, OnOff:Boolean):void
		{
			if(whichItem == 0)
			{
				if(OnOff)
				{
					flowersTop.playSequence( "shadow1" );
				}
				else
				{
					flowersTop.playSequence( "shadow2" );
				}
			}
			if(whichItem == 1)
			{
				if(OnOff)
				{
					wheelBig.playSequence( "shadow1" );
				}
				else
				{
					wheelBig.playSequence( "shadow2" );
				}
			}
			if(whichItem == 2)
			{
				if(OnOff)
				{
					wheelSmall.playSequence( "shadow1" );
				}
				else
				{
					wheelSmall.playSequence( "shadow2" );
				}
			}
			if(whichItem == 3)
			{
				if(OnOff)
				{
					wheelBack.playSequence( "shadow1" );
				}
				else
				{
					wheelBack.playSequence( "shadow2" );
				}
			}
		}
		
		
		private function lemonComplete(e:TapSpritesheet):void
		{
			scene_mc.SS_P06_LEMON_BTN.mouseEnabled = true;
			lemonCounter = 0;
			if(scene_mc.bigSparkle1.currentFrame == 1)
			{
				if ( this.step == BookPage.STEP_DONE )
				{
					sndDress.Start();
				}
				
				scene_mc.bigSparkle1.play();
			}
		}
		
		
		private function orangeComplete(e:TapSpritesheet):void
		{
			scene_mc.SS_P06_ORANGE_BTN.mouseEnabled = true;
		}
		
		
		
	}
}