﻿package asData  {
	// this file contains all the constants and data settings for the pirate dress up game.
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFormatAlign;
	
	import com.DaveLibrary.clsTextData;
	import asData.clsGlobalConstants;
	
	public class clsManOnTheMoonConstants {

		public var strings_TitleKey:String;
		public var strings_FinaleTextKey:String;
		public var strings_BonusKey:String;
		public var strings_BonusCountSymbol:String;		
		
		public var ProgBar_LocOffset:Point = new Point();
		public var ProgBar_FillColor:Object;

		public function clsManOnTheMoonConstants(gc:clsGlobalConstants) {		
			strings_TitleKey = "ManOnTheMoon_Title";	// the key used in the strings file <Pages> section that will be used for this page.
			strings_FinaleTextKey = "ManOnTheMoon_Finale";	/// the key used int he strings file for the game over finale text;
			strings_BonusKey = "ManOnTheMoon_Bonus";
			strings_BonusCountSymbol = "ManOnTheMoon_BonusCountSymbol";			
			
			ProgBar_LocOffset.x = 0;	// offset the location of the progress bar for this page only by this many pixels. Add to the Global Constants prog bar location values.
			ProgBar_LocOffset.y = 0;
			
			ProgBar_FillColor = null;		// set to null to use the global defined color, otherwise, specify the color here to override for this page.
			
		}		
		public function destroy():void{
			strings_TitleKey = null;
			strings_FinaleTextKey = null;
			strings_BonusKey = null;
			strings_BonusCountSymbol = null;			
			
			ProgBar_LocOffset = null;
			ProgBar_FillColor = null;		// set to null to use the global defined color, otherwise, specify the color here to override for this page.
		}

	}
	
}
