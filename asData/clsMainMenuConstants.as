﻿package asData 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFormatAlign;
	
	import com.DaveLibrary.clsGradient;
	import com.DaveLibrary.clsStandardButtonSettings;
	import com.DaveLibrary.clsTextData;
	
	import asData.clsGlobalConstants;
	
	public class clsMainMenuConstants 
	{		
		public var Help_Loc:Point = new Point();
		public var Help_Button:clsStandardButtonSettings = new clsStandardButtonSettings();
		
		public var Shop_Text:clsTextData = new clsTextData;
		
		public var Credits_Loc:Point = new Point();
		public var Credits_Button:clsStandardButtonSettings = new clsStandardButtonSettings();
		
		public var Language_Buttons:Rectangle = new Rectangle( 1014, 0, 131, 48 );
		
		public var ReadToMe_Loc:Point = new Point();
		public var ReadToMe_Glow:Point = new Point();
		public var ReadToMe_SecondStroke_Color:uint;
		public var ReadToMe_SecondStroke_Size:int;
		public var ReadToMe_Mirrored:Boolean;
		public var ReadToMe_Centered:Boolean;
		public var ReadToMe_Button:clsStandardButtonSettings = new clsStandardButtonSettings();
		public var ReadToMe_OpenOffset:Point = new Point();
		public var JAB_Loc:Point = new Point();
		public var JAB_Glow:Point = new Point();
		public var JAB_SecondStroke_Color:uint;
		public var JAB_SecondStroke_Size:int;
		public var JAB_Mirrored:Boolean;
		public var JAB_Centered:Boolean;
		public var JAB_Button:clsStandardButtonSettings = new clsStandardButtonSettings();
		public var JAB_OpenOffset:Point = new Point();
		public var ReadMyself_Loc:Point = new Point();
		public var ReadMyself_Glow:Point = new Point();
		public var ReadMyself_SecondStroke_Color:uint;
		public var ReadMyself_SecondStroke_Size:int;
		public var ReadMyself_Mirrored:Boolean;
		public var ReadMyself_Centered:Boolean;
		public var ReadMyself_Button:clsStandardButtonSettings = new clsStandardButtonSettings();
		public var ReadMyself_OpenOffset:Point = new Point();
		public var Games_Loc:Point = new Point();
		public var Games_Glow:Point = new Point();
		public var Games_SecondStroke_Color:uint;
		public var Games_SecondStroke_Size:int;
		public var Games_Mirrored:Boolean;
		public var Games_Centered:Boolean;
		public var Games_Button:clsStandardButtonSettings = new clsStandardButtonSettings();
		public var Games_OpenOffset:Point = new Point();
		
		public var GlowFadeSteps:int;
		public var FadeOutTextSpeed:Number;

		public function clsMainMenuConstants(gc:clsGlobalConstants)
		{
			Help_Loc.x = 270;
			Help_Loc.y = 707;
			Help_Button.textInfo.color = gc.gradient_FLAT_WHITE;
			Help_Button.textInfo.font = new noteworthyFont();
			Help_Button.textInfo.fontSize = 26;
			Help_Button.textInfo.alignment = TextFormatAlign.CENTER;
			Help_Button.textInfo.offset.x = -2000;
			Help_Button.textInfo.offset.y = 39;
			Help_Button.textInfo.bounds.width = -200;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
			Help_Button.textInfo.bounds.height = -200;
			Help_Button.textInfo.stroke = 6;
			Help_Button.textInfo.strokeColor = 0xd65697;
			Help_Button.textInfo.strokeAlpha = 1;
			Help_Button.textInfo.leading = 0;
			Help_Button.textInfo.letterSpacing = 5;
			Help_Button.hitAreaRect.x = 0;
			Help_Button.hitAreaRect.y = 0;
			Help_Button.hitAreaRect.width = 0;
			Help_Button.hitAreaRect.height = 0;

			Shop_Text.color = gc.gradient_FLAT_WHITE;
			Shop_Text.font = new noteworthyFont();
			Shop_Text.fontSize = 26;
			Shop_Text.alignment = TextFormatAlign.CENTER;
			Shop_Text.offset.x = 3;
			Shop_Text.offset.y = 39;
			Shop_Text.bounds.width = 200;
			Shop_Text.bounds.height = 50;
			Shop_Text.stroke = 6;
			Shop_Text.strokeColor = 0xd65697;
			Shop_Text.strokeAlpha = 1;
			Shop_Text.leading = 0;
			Shop_Text.letterSpacing = 1;
			
			Credits_Loc.x = 537;
			Credits_Loc.y = 260;
			Credits_Button.textInfo.color = gc.gradient_FLAT_WHITE;
			Credits_Button.textInfo.font = new fontVAGRounded();
			Credits_Button.textInfo.fontSize = 40;
			Credits_Button.textInfo.alignment = TextFormatAlign.CENTER;
			Credits_Button.textInfo.offset.x = 0;
			Credits_Button.textInfo.offset.y = 5;
			Credits_Button.textInfo.bounds.width = -40;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
			Credits_Button.textInfo.bounds.height = 0;
			Credits_Button.textInfo.stroke = 3;
			Credits_Button.textInfo.strokeColor = 0xd65697;
			Credits_Button.textInfo.strokeAlpha = 1;
			Credits_Button.textInfo.leading = 0;
			Credits_Button.textInfo.letterSpacing = 2;
			Credits_Button.hitAreaRect.x = 0;
			Credits_Button.hitAreaRect.y = 0;
			Credits_Button.hitAreaRect.width = 0;
			Credits_Button.hitAreaRect.height = 0;
			
			ReadMyself_Loc.x = 520;
			ReadMyself_Loc.y = 570;
			ReadMyself_Glow.x = 520;
			ReadMyself_Glow.y = 570;
			ReadMyself_SecondStroke_Color = 0xFFFFFF;
			ReadMyself_SecondStroke_Size = 4;
			ReadMyself_Mirrored = false;				// mirror the chest image.
			ReadMyself_Centered = true;
			ReadMyself_Button.textInfo.color = gc.gradient_MainMenu_Purple;
			ReadMyself_Button.textInfo.font = new funtasticFont();
			ReadMyself_Button.textInfo.fontSize = 38;
			ReadMyself_Button.textInfo.alignment = TextFormatAlign.CENTER;
			ReadMyself_Button.textInfo.offset.x = -2000;
			ReadMyself_Button.textInfo.offset.y = 0;
			ReadMyself_Button.textInfo.bounds.width = 0;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
			ReadMyself_Button.textInfo.bounds.height = -20;
			ReadMyself_Button.textInfo.stroke = 0;
			ReadMyself_Button.textInfo.strokeColor = 0xffffff;
			ReadMyself_Button.textInfo.strokeAlpha = 1;
			ReadMyself_Button.textInfo.leading = -5;
			ReadMyself_Button.textInfo.letterSpacing = -3;
			ReadMyself_Button.hitAreaRect.x = 0;
			ReadMyself_Button.hitAreaRect.y = 0;
			ReadMyself_Button.hitAreaRect.width = 0;
			ReadMyself_Button.hitAreaRect.height = 0;			
			ReadMyself_OpenOffset.x = 50;		// offset the center of the "opened" chest by this much so it lines up with the closed chest.
			ReadMyself_OpenOffset.y = -8;
			
			ReadToMe_Loc.x = 520;
			ReadToMe_Loc.y = 200;
			ReadToMe_Glow.x = 520;
			ReadToMe_Glow.y = 200;
			ReadToMe_SecondStroke_Color = 0xFFFFFF;
			ReadToMe_SecondStroke_Size = 4;
			ReadToMe_Mirrored = false;				// mirror the chest image.
			ReadToMe_Centered = true;
			ReadToMe_Button.textInfo.color = gc.gradient_MainMenu_Blue;
			ReadToMe_Button.textInfo.font = new funtasticFont();
			ReadToMe_Button.textInfo.fontSize = 45;
			ReadToMe_Button.textInfo.alignment = TextFormatAlign.CENTER;
			ReadToMe_Button.textInfo.offset.x = -2000;
			ReadToMe_Button.textInfo.offset.y = 7;
			ReadToMe_Button.textInfo.bounds.width = 0;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
			ReadToMe_Button.textInfo.bounds.height = -20;
			ReadToMe_Button.textInfo.stroke = 0;
			ReadToMe_Button.textInfo.strokeColor = 0xffffff;
			ReadToMe_Button.textInfo.strokeAlpha = 1;
			ReadToMe_Button.textInfo.leading = -5;
			ReadToMe_Button.textInfo.letterSpacing = -3;
			ReadToMe_Button.hitAreaRect.x = 0;
			ReadToMe_Button.hitAreaRect.y = 0;
			ReadToMe_Button.hitAreaRect.width = 0;
			ReadToMe_Button.hitAreaRect.height = 0;			
			ReadToMe_OpenOffset.x = -51;		// offset the center of the "opened" chest by this much so it lines up with the closed chest.
			ReadToMe_OpenOffset.y = -8;
			
			JAB_Loc.x = 860;
			JAB_Loc.y = 200;
			JAB_Glow.x = 860;
			JAB_Glow.y = 200;
			JAB_SecondStroke_Color = 0xFFFFFF;
			JAB_SecondStroke_Size = 4;
			JAB_Mirrored = false;				// mirror the chest image.
			JAB_Centered = true;
			JAB_Button.textInfo.color = gc.gradient_MainMenu_Pink;
			JAB_Button.textInfo.font = new funtasticFont();
			JAB_Button.textInfo.fontSize = 38;
			JAB_Button.textInfo.alignment = TextFormatAlign.CENTER;
			JAB_Button.textInfo.offset.x = -2000;
			JAB_Button.textInfo.offset.y = 0;
			JAB_Button.textInfo.bounds.width = 0;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
			JAB_Button.textInfo.bounds.height = -20;
			JAB_Button.textInfo.stroke = 0;
			JAB_Button.textInfo.strokeColor = 0xffffff;
			JAB_Button.textInfo.strokeAlpha = 1;
			JAB_Button.textInfo.leading = -5;
			JAB_Button.textInfo.letterSpacing = -3;
			JAB_Button.hitAreaRect.x = 0;
			JAB_Button.hitAreaRect.y = 0;
			JAB_Button.hitAreaRect.width = 0;
			JAB_Button.hitAreaRect.height = 0;			
			JAB_OpenOffset.x = 50;		// offset the center of the "opened" chest by this much so it lines up with the closed chest.
			JAB_OpenOffset.y = -8;
			
			Games_Loc.x = 860;
			Games_Loc.y = 570;
			Games_Glow.x = 860;
			Games_Glow.y = 570;
			Games_SecondStroke_Color = 0xFFFFFF;
			Games_SecondStroke_Size = 4;
			Games_Mirrored = false;				// mirror the chest image.
			Games_Centered = true;
			Games_Button.textInfo.color = gc.gradient_MainMenu_Blue;
			Games_Button.textInfo.font = new funtasticFont();
			Games_Button.textInfo.fontSize = 45;
			Games_Button.textInfo.alignment = TextFormatAlign.CENTER;
			Games_Button.textInfo.offset.x = -2000;
			Games_Button.textInfo.offset.y = 7;
			Games_Button.textInfo.bounds.width = 0;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
			Games_Button.textInfo.bounds.height = -20;
			Games_Button.textInfo.stroke = 0;
			Games_Button.textInfo.strokeColor = 0xffffff;
			Games_Button.textInfo.strokeAlpha = 1;
			Games_Button.textInfo.leading = -5;
			Games_Button.textInfo.letterSpacing = -3;
			Games_Button.hitAreaRect.x = 0;
			Games_Button.hitAreaRect.y = 0;
			Games_Button.hitAreaRect.width = 0;
			Games_Button.hitAreaRect.height = 0;			
			Games_OpenOffset.x = -51;		// offset the center of the "opened" chest by this much so it lines up with the closed chest.
			Games_OpenOffset.y = -8;
			
			
			GlowFadeSteps = 60;		// when one of the play buttons is pressed, change to the opened image and start to fade in the glow over this many steps.
			FadeOutTextSpeed = 0.05	// as the glow fades in over it's number of steps, fade out the text this much per tick.
		}
		
		public function destroy():void
		{
			Help_Loc = null;
			Help_Button.destroy();
			Help_Button = null;

			Credits_Loc = null;
			Credits_Button.destroy();
			Credits_Button = null;

			ReadToMe_Loc = null;
			ReadToMe_Glow = null;
			ReadToMe_Button.destroy();
			ReadToMe_Button = null;
			
			ReadMyself_Loc = null;
			ReadMyself_Glow = null;
			ReadMyself_Button.destroy();
			ReadMyself_OpenOffset = null;
			
			JAB_Loc = null;
			JAB_Glow = null;
			JAB_Button.destroy();
			JAB_Button = null;
			JAB_OpenOffset = null;
			
			Games_Loc = null;
			Games_Glow = null;
			Games_Button.destroy();
			Games_Button = null;
		}
	}
}