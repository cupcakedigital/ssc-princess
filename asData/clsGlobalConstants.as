﻿package asData  
{
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.Font;
	import flash.text.TextFormatAlign;
	
	import asData.clsMsgboxDefaults;

	import com.DaveLibrary.clsCloneObject;
	import com.DaveLibrary.clsGradient;
	import com.DaveLibrary.clsParticleBurstSettings;
	import com.DaveLibrary.clsSize;
	import com.DaveLibrary.clsStandardButtonSettings;
	import com.DaveLibrary.clsTextData;
	
	import com.cupcake.TextFieldData;

	public class clsGlobalConstants 
	{
		public static var games_ProgressText:TextFieldData		= new TextFieldData( new fontVAGRounded, 0x000000, 36, new Rectangle( 0, 0, 900, 85 ), 0, 0, false, 0, 0, false, 0 );
		
		public var debug_Enabled:Boolean;
		public var debug_showStats:Boolean;
		public var debug_StatsScale:Number;
		public var debug_showCursors:Boolean;
		public var debug_ButtonHitAreaAlpha:Number;
		public var debug_ForceDisabledOnDevice:Boolean;
		
		{
			public var general_DisabledButtonTextAlpha:Number;
			public var general_ButtonHitArea:Rectangle = new Rectangle();
		}
				
		public var button_Highlight:Number;
		
		// predefined gradients used for things like message boxes, buttons, dynamic gradient text, etc...
		{
			public var disabled_Text_StrokeColor:uint;
			public var gradient_Disabled:clsGradient = new clsGradient();
			public var gradient_FLAT_BLACK:clsGradient = new clsGradient();
			public var gradient_FLAT_WHITE:clsGradient = new clsGradient();
			public var gradient_NavbarBlue:clsGradient = new clsGradient();
			
			public var gradient_MainMenu_Blue:clsGradient = new clsGradient();
			public var gradient_MainMenu_Purple:clsGradient = new clsGradient();
			public var gradient_MainMenu_Pink:clsGradient = new clsGradient();
			
			public var gradient_EndOfLevel_Green:clsGradient = new clsGradient();
			public var gradient_EndOfLevel_Yellow:clsGradient = new clsGradient();
			
			public var gradient_MoreApps_Purple:clsGradient = new clsGradient();
		}
			
		// Page turn controls
		{
			public var pageTurn_AutoFlipDelta:int;
			public var pageTurn_MaxPreview:Number;
			public var pageTurn_FlipBack:Number;
			public var pageTurn_FlipSpeed:Number;
			public var pageTurn_SwipeSpeed:Number;
			public var pageTurn_OutsideShadow_OffsetStart:Number;
			public var pageTurn_OutsideShadow_OffsetEnd:Number;
			public var pageTurn_OutsideShadow_LeadIn:Number;
			public var pageTurn_OutsideShadow_LeadOut:Number;
			public var pageTurn_OutsideShadow_Alpha:Number;
			public var pageTurn_InsideShadow_Stationary_Max:Number;
			public var pageTurn_InsideShadow_Flipping_Max:Number;
			public var pageTurn_InsideShadow_Color:uint;
			
			public var pageTurn_DemoTurnAmount:Number = 75;
			public var pageTurn_DemoTurnTime:Number = 1.35;
			
			public var pageTurn_Arrows_Previous:Point = new Point();
			public var pageTurn_Arrows_Next:Point = new Point();
			public var pageTurn_ArrowSettings:clsStandardButtonSettings = new clsStandardButtonSettings();
			
			public var pageTurn_DeadZone:Rectangle = new Rectangle();
			
			public var pageTurn_Border_Alpha:Number;
			public var pageTurn_Border_Size:int;
		}
		
		// Main Menu Constants
		{
			public var mainmenu_ReadToMeText:clsTextData = new clsTextData();
			public var mainmenu_AutoPlayText:clsTextData = new clsTextData();
			public var mainmenu_ReadMyselfText:clsTextData = new clsTextData();
			
			public var mainmenu_TextFadeOutSpeed:Number;			
		}
		
		// Nav Bar constants
		{
			public var navbar_HomeButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			public var navbar_PagesButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			public var navbar_OptionsButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			public var navbar_ArcadeButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			public var navbar_VideosButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			public var navbar_RecordButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			
			public var navbar_Home:Point = new Point();
			public var navbar_Pages:Point = new Point();
			public var navbar_Options:Point = new Point();
			public var navbar_Arcade:Point = new Point();
			public var navbar_Videos:Point = new Point();
			public var navbar_Coloring:Point = new Point();
			
			public var navbar_MenuText:clsTextData = new clsTextData();
			public var navbar_PopupText:clsTextData = new clsTextData();
			public var navbar_VideoPopupText:clsTextData = new clsTextData();		
			
			public var navbar_Home_SecondStroke_Size:int;
			public var navbar_Home_SecondStroke_Color:uint;
			public var navbar_Pages_SecondStroke_Size:int;
			public var navbar_Pages_SecondStroke_Color:uint;
			public var navbar_Options_SecondStroke_Size:int;
			public var navbar_Options_SecondStroke_Color:uint;
			public var navbar_Arcade_SecondStroke_Size:int;
			public var navbar_Arcade_SecondStroke_Color:uint;
			public var navbar_Videos_SecondStroke_Size:int;
			public var navbar_Videos_SecondStroke_Color:uint;
			
			public var navbar_Move_Min:int;
			public var navbar_Move_Max:int;
			public var navbar_Move_Delta:int;
			
			public var navbar_SliderY_Closed:int;
			public var navbar_SliderY_Opened:int;
			public var navbar_SliderSteps:int;
						
			public var navbar_PausedText:clsTextData = new clsTextData();
			public var navbar_PausedBlackAlpha:Number;
			public var navbar_PausedTextAlpha:Number;
			
		}
		
		// Options Dialog
		{
			
			public var optionsDialog_YOffset:int;
			
			public var optionsDialog_Title:clsTextData = new clsTextData();
			public var optionsDialog_MusicText:clsTextData = new clsTextData();
			public var optionsDialog_ShowMoreText:clsTextData = new clsTextData();
			public var optionsDialog_SoundsText:clsTextData = new clsTextData();
			public var optionsDialog_VoiceText:clsTextData = new clsTextData();
			public var optionsDialog_CloseButton:clsStandardButtonSettings = new clsStandardButtonSettings();
			public var optionsDialog_CloseButtonOffset:Point = new Point();
			public var optionsDialog_ShowMoreApps:Point = new Point;
			
			public var optionsDialog_MusicSlider:int;
			public var optionsDialog_SoundsSlider:int;
			public var optionsDialog_VoiceSlider:int;
			public var optionsDialog_Sliders_MinX:int;
			public var optionsDialog_Sliders_MaxX:int;			
		}
		
		// More Apps popup
		{
			public var moreApps_ButtonText:clsTextData = new clsTextData();
			public var moreApps_HeaderBottom:clsTextData = new clsTextData();
			public var moreApps_HeaderTop:clsTextData = new clsTextData();
			public var moreApps_TopApp:clsTextData = new clsTextData();
			public var moreApps_TopTitle:clsTextData = new clsTextData();
		}
				
		// Global Pages variables
		{
			public var pages_progBar_Size:clsSize = new clsSize();
			public var pages_progBar_FillOffset:Point = new Point();
			public var pages_progBar_Loc:Point = new Point();
			public var pages_progBar_FillColor:uint;
			public var pages_progBar_EdgeColor:uint;
			public var pages_progBar_EdgeWidth:int;
			public var pages_TitleBar:Point = new Point();
			public var pages_TitleText:clsTextData = new clsTextData();
			public var pages_TitleIntro_Text:clsTextData = new clsTextData();
			public var pages_TitleIntro_Steps:int;
			public var pages_TitleIntro_FadeOutOnStep:int;
			public var pages_TitleIntro_HoldDelay:int;
			public var pages_TitleIntro_EnableGameOnStep:int;

			public var pages_GameFinale_Text:clsTextData = new clsTextData();
			public var pages_GameFinale_Pause:int;
			public var pages_GameFinale_FadeOutSpeed:Number;	
			
			public var pages_GameFinale_Particles_Delay_Min:Number;
			public var pages_GameFinale_Particles_Delay_Max:Number;
			public var pages_GameFinale_Particles_SpawnLocationOffset:clsSize = new clsSize();
			public var pages_GameFinale_ParticleBurst:clsParticleBurstSettings = new clsParticleBurstSettings();

			public var pages_Games_EndOfLevel_DimmerAlpha:Number;
			public var pages_Games_EndOfLevel_CountSteps:Number;
			public var pages_Games_EndOfLevel_Title:clsTextData = new clsTextData();
			public var pages_Games_EndOfLevel_Line:clsTextData = new clsTextData();
			public var pages_Games_EndOfLevel_Proceed:clsTextData = new clsTextData();
			public var pages_Games_EndOfLevel_Close:clsStandardButtonSettings = new clsStandardButtonSettings();
			
			public var pages_Games_EndOfLevel_Stars_StartScale:Number;
			public var pages_Games_EndOfLevel_Stars_StartAlpha:Number;
			public var pages_Games_EndOfLevel_Stars_ShrinkSteps:int;
			public var pages_Games_EndOfLevel_Stars_BounceFallOff:Number;
			public var pages_Games_EndOfLevel_Stars_Center:Point = new Point();
			
			public var pages_Games_EndOfLevel_Stars_Spacing:int;
			public var pages_Games_EndOfLevel_Stars_MoveSteps:int;
			public var pages_Games_EndOfLevel_Stars_NextStarOnStep:int;
			public var pages_Games_EndOfLevel_Stars_NextStarDelay:int;

			public var pages_Games_EndOfLevel_BoopFrame:int;

			public var pages_HighlightColor:Object;
			public var pages_HighlightFontColor:Object;
			public var pages_HighlightSizeFactor:Number;
			public var pages_HighlightStrokeColor:Object;
			public var pages_HighlightStrokeSize:int;

		}
		
		// message box defaults
		public var msgboxDefaults:clsMsgboxDefaults = new clsMsgboxDefaults();

		// messages - any message box that will appear in the project should have it's options defined here.
		{
			public var messages_captcha:clsMsgboxDefaults;
			
			public var messages_Mainmenu_RateIt:clsMsgboxDefaults;
			public var messages_Mainmenu_Continue:clsMsgboxDefaults;
			public var messages_Mainmenu_Help:clsMsgboxDefaults;
			public var messages_Mainmenu_Interactive:clsMsgboxDefaults;
			public var messages_Mainmenu_Info:clsMsgboxDefaults;
			public var messages_navbar_DeleteRecording:clsMsgboxDefaults;
			public var messages_profileDialog_Remove:clsMsgboxDefaults;
			public var messages_profileDialog_Save:clsMsgboxDefaults;
			
			public var messages_temp_Pages:clsMsgboxDefaults;
			public var messages_temp_Options:clsMsgboxDefaults;
			public var messages_temp_MoreBooks:clsMsgboxDefaults;
			public var messages_temp_Arcade:clsMsgboxDefaults;
			
		}

		public function clsGlobalConstants() 
		{
			// constructor code
						
			debug_Enabled = true;		// enable or disable debug mode.
			debug_showStats = true;		// if debug is enabled, show the stats sprite by default.
			debug_StatsScale = 1.5;		// scale the stats sprite to this size. (on the iphone, 1.0 is too small to read)
			debug_showCursors = true;	// when in debug mode, if true, the green and red cross hairs to show the mouse down/up location will be displayed.
			debug_ForceDisabledOnDevice = true;		// when the ipad/iphone is detected, for debug mode to this value regardless fo the above.
			
			debug_ButtonHitAreaAlpha = 0.0;
						
			button_Highlight = 0.35;		// the highlight alpha for all buttons that use dynamic highlighting
			
			// general settings - needed for support objects, like clsStandardButton
			{
				// this will effect ALL stanard buttons in the game globally.
				general_ButtonHitArea.x = 0;	// offset the button hit area center point by this much
				general_ButtonHitArea.y = 0;
				general_ButtonHitArea.width = 20;	// by default, the hit area width and height are that of the static image. This will increase or decrease it it.
				general_ButtonHitArea.height = 20;

				
				general_DisabledButtonTextAlpha = 0.35;
			}
			
			// Gradients
			{
				
				// This disabled gradient is used for buttons that have text. When the button is disabled, this gradient will be applied. so don't delete it even if you are not using it anywhere else!
				disabled_Text_StrokeColor = 0x000000;
				gradient_Disabled.colors = [0xEEEEEE,0x777777];
				gradient_Disabled.alphas = [1,1];
				gradient_Disabled.ratios = [127,255];
				gradient_Disabled.rotation = 90;
				
				gradient_FLAT_BLACK.colors = [0x000000];
				gradient_FLAT_BLACK.alphas = [1];
				gradient_FLAT_BLACK.ratios = [255];
				gradient_FLAT_BLACK.rotation = 90;
	
				gradient_FLAT_WHITE.colors = [0xFFFFFF];
				gradient_FLAT_WHITE.alphas = [1];
				gradient_FLAT_WHITE.ratios = [255];
				gradient_FLAT_WHITE.rotation = 90;
	
				gradient_NavbarBlue.colors = [0x4b9cd5];
				gradient_NavbarBlue.alphas = [1];
				gradient_NavbarBlue.ratios = [255];
				gradient_NavbarBlue.rotation = 90;

				gradient_MainMenu_Blue.colors = [0x127997];
				gradient_MainMenu_Blue.alphas = [1];
				gradient_MainMenu_Blue.ratios = [255];
				gradient_MainMenu_Blue.rotation = 90;

				gradient_MainMenu_Purple.colors = [0xa224c1];
				gradient_MainMenu_Purple.alphas = [1];
				gradient_MainMenu_Purple.ratios = [255];
				gradient_MainMenu_Purple.rotation = 90;

				gradient_MainMenu_Pink.colors = [0xd65697];
				gradient_MainMenu_Pink.alphas = [1];
				gradient_MainMenu_Pink.ratios = [255];
				gradient_MainMenu_Pink.rotation = 90;

				gradient_EndOfLevel_Green.colors = [0x0C7403];
				gradient_EndOfLevel_Green.alphas = [1];
				gradient_EndOfLevel_Green.ratios = [255];
				gradient_EndOfLevel_Green.rotation = 90;
				
				gradient_EndOfLevel_Yellow.colors = [0xF9FF07];
				gradient_EndOfLevel_Yellow.alphas = [1];
				gradient_EndOfLevel_Yellow.ratios = [255];
				gradient_EndOfLevel_Yellow.rotation = 90;
				
				gradient_MoreApps_Purple.colors = [0x663091];
				gradient_MoreApps_Purple.alphas = [1];
				gradient_MoreApps_Purple.ratios = [255];
				gradient_MoreApps_Purple.rotation = 90;
			}			
			
			// Page turn variables
			{
				// Page turn controls
				pageTurn_AutoFlipDelta = 200;	// number of pixels the mouse needs to move left or right in order for the page flip to "complete" automatically.
				pageTurn_MaxPreview = 0.30;		// if the mouse is dragged less than auto flip delta, it will do the page flip to a maximum of this value.
				pageTurn_FlipBack = 0.04;		// this is the precentage per tick the page will return to resting position if they release the mouse before a full flip is complete.
				pageTurn_FlipSpeed = 0.04;		// if the exceed the flip delta, continue the full flip at this speed (percentage per tick)
				pageTurn_SwipeSpeed = 40;		// if the dx value of the mouse moving is more than this number of pixels within one frame, assume it's a quick swpie and advance the page.
				
				
				// the outside shadow is a copy of the flipping page, but the rotation angle is offset using a bezier formula.
				pageTurn_OutsideShadow_OffsetStart = -0.25;		// the inital offset, set to negative number to delay it appearing.
				pageTurn_OutsideShadow_OffsetEnd = -0.15;		// the end offset, set to negative number to make it dissappear before the page is completely turned.
				pageTurn_OutsideShadow_LeadIn = 1;		// control point 1 in degrees
				pageTurn_OutsideShadow_LeadOut = 5;		// control point 2 in degrees
				pageTurn_OutsideShadow_Alpha = 0.25;
				
				
				pageTurn_InsideShadow_Flipping_Max = 0.0;		// the max alpha of the GRADIENT for the shadow on the inside FLIPPING page.
				pageTurn_InsideShadow_Stationary_Max = 0.4;		// the max alpha of the GRADIENT for the shadow on the inside stationary page.
				pageTurn_InsideShadow_Color = 0x000000;			// the color of the shadow - this should always be black, but just in case, it's here.
				
				pageTurn_Arrows_Previous.x = 45;				// the x,y location of the center of the arrow
				pageTurn_Arrows_Previous.y = 55;
				
				pageTurn_Arrows_Next.x = 979;
				pageTurn_Arrows_Next.y = 55;
				
				pageTurn_ArrowSettings.hitAreaRect.x = 0;		// size and offset for both arrow hit areas				
				pageTurn_ArrowSettings.hitAreaRect.y = 0;
				pageTurn_ArrowSettings.hitAreaRect.width = 0;
				pageTurn_ArrowSettings.hitAreaRect.height = 0;
				
				pageTurn_DeadZone.x = 256;
				pageTurn_DeadZone.y = 0;
				pageTurn_DeadZone.width = 512;
				pageTurn_DeadZone.height = 768;
				
				pageTurn_Border_Alpha = 0.4;
				pageTurn_Border_Size = 2;
				
			}
			
			// Main Menu Constants
			{
				mainmenu_ReadToMeText.alignment = TextFormatAlign.CENTER;
				mainmenu_ReadToMeText.bounds.width = 160;
				mainmenu_ReadToMeText.bounds.height = 160;
				mainmenu_ReadToMeText.color = gradient_FLAT_WHITE;
				mainmenu_ReadToMeText.font = new funtasticFont();
				mainmenu_ReadToMeText.fontSize = 48;
				mainmenu_ReadToMeText.offset.x = 0;
				mainmenu_ReadToMeText.offset.y = -10;
				mainmenu_ReadToMeText.stroke = 3;
				mainmenu_ReadToMeText.strokeColor = 0xb43c79;
				mainmenu_ReadToMeText.leading = -5;
				mainmenu_ReadToMeText.letterSpacing = 3;

				mainmenu_AutoPlayText.alignment = TextFormatAlign.CENTER;
				mainmenu_AutoPlayText.bounds.width = 160;
				mainmenu_AutoPlayText.bounds.height = 160;
				mainmenu_AutoPlayText.color = gradient_FLAT_WHITE;
				mainmenu_AutoPlayText.font = new funtasticFont();
				mainmenu_AutoPlayText.fontSize = 48;
				mainmenu_AutoPlayText.offset.x = 0;
				mainmenu_AutoPlayText.offset.y = -10;
				mainmenu_AutoPlayText.stroke = 3;
				mainmenu_AutoPlayText.strokeColor = 0x883f86;
				mainmenu_AutoPlayText.leading = -5;
				mainmenu_AutoPlayText.letterSpacing = 3;

				mainmenu_ReadMyselfText.alignment = TextFormatAlign.CENTER;
				mainmenu_ReadMyselfText.bounds.width = 160;
				mainmenu_ReadMyselfText.bounds.height = 160;
				mainmenu_ReadMyselfText.color = gradient_FLAT_WHITE;
				mainmenu_ReadMyselfText.font = new funtasticFont();
				mainmenu_ReadMyselfText.fontSize = 48;
				mainmenu_ReadMyselfText.offset.x = 0;
				mainmenu_ReadMyselfText.offset.y = -10;
				mainmenu_ReadMyselfText.stroke = 3;
				mainmenu_ReadMyselfText.strokeColor = 0x126ca4;
				mainmenu_ReadMyselfText.leading = -5;
				mainmenu_ReadMyselfText.letterSpacing = 3;

				mainmenu_TextFadeOutSpeed = 0.1;		// fade out the balloons & text by reducing the alpha this much per tick
			
			}
			
			// Nav Bar constants
			{
				navbar_Home.x = 170;
				navbar_Home.y = 60;
				navbar_HomeButton.textInfo.alignment = TextFormatAlign.CENTER;
				navbar_HomeButton.textInfo.bounds.width = 0;
				navbar_HomeButton.textInfo.bounds.height = 0;
				navbar_HomeButton.textInfo.color = gradient_FLAT_WHITE;
				navbar_HomeButton.textInfo.font = new fontVAGRounded();
				navbar_HomeButton.textInfo.fontSize = 42;
				navbar_HomeButton.textInfo.offset.x = 0;
				navbar_HomeButton.textInfo.offset.y = 50;
				navbar_HomeButton.textInfo.stroke = 6;
				navbar_HomeButton.textInfo.strokeColor = 0xF25588;
				navbar_HomeButton.hitAreaRect.x = 0;
				navbar_HomeButton.hitAreaRect.y = 0;
				navbar_HomeButton.hitAreaRect.width = -general_ButtonHitArea.width;
				navbar_HomeButton.hitAreaRect.height = -general_ButtonHitArea.height;
				
				navbar_Pages.x = 363;
				navbar_Pages.y = 55;
				navbar_PagesButton.textInfo.alignment = TextFormatAlign.CENTER;
				navbar_PagesButton.textInfo.bounds.width = 0;
				navbar_PagesButton.textInfo.bounds.height = 0;
				navbar_PagesButton.textInfo.color = gradient_FLAT_WHITE;
				navbar_PagesButton.textInfo.font = new fontVAGRounded();
				navbar_PagesButton.textInfo.fontSize = 42;
				navbar_PagesButton.textInfo.offset.x = 0;
				navbar_PagesButton.textInfo.offset.y = 50;
				navbar_PagesButton.textInfo.stroke = 6;
				navbar_PagesButton.textInfo.strokeColor = 0xF25588;
				navbar_PagesButton.hitAreaRect.x = 0;
				navbar_PagesButton.hitAreaRect.y = 0;
				navbar_PagesButton.hitAreaRect.width = -general_ButtonHitArea.width;
				navbar_PagesButton.hitAreaRect.height = -general_ButtonHitArea.height;
				
				navbar_Options.x = 772;
				navbar_Options.y = 60;
				navbar_OptionsButton.textInfo.alignment = TextFormatAlign.CENTER;
				navbar_OptionsButton.textInfo.bounds.width = 0;
				navbar_OptionsButton.textInfo.bounds.height = 0;
				navbar_OptionsButton.textInfo.color = gradient_FLAT_WHITE;
				navbar_OptionsButton.textInfo.font = new fontVAGRounded();
				navbar_OptionsButton.textInfo.fontSize = 42;
				navbar_OptionsButton.textInfo.offset.x = 0;
				navbar_OptionsButton.textInfo.offset.y = 50;
				navbar_OptionsButton.textInfo.stroke = 6;
				navbar_OptionsButton.textInfo.strokeColor = 0xF25588;
				navbar_OptionsButton.hitAreaRect.x = 0;
				navbar_OptionsButton.hitAreaRect.y = 0;
				navbar_OptionsButton.hitAreaRect.width = -general_ButtonHitArea.width;
				navbar_OptionsButton.hitAreaRect.height = -general_ButtonHitArea.height;

				navbar_Arcade.x = 645;
				navbar_Arcade.y = 60;
				navbar_ArcadeButton.textInfo.alignment = TextFormatAlign.CENTER;
				navbar_ArcadeButton.textInfo.bounds.width = 0;
				navbar_ArcadeButton.textInfo.bounds.height = 0;
				navbar_ArcadeButton.textInfo.color = gradient_FLAT_WHITE;
				navbar_ArcadeButton.textInfo.font = new fontVAGRounded();
				navbar_ArcadeButton.textInfo.fontSize = 42;
				navbar_ArcadeButton.textInfo.offset.x = 0;
				navbar_ArcadeButton.textInfo.offset.y = 50;
				navbar_ArcadeButton.textInfo.stroke = 6;
				navbar_ArcadeButton.textInfo.strokeColor = 0xF25588;
				navbar_ArcadeButton.hitAreaRect.x = 0;
				navbar_ArcadeButton.hitAreaRect.y = 0;
				navbar_ArcadeButton.hitAreaRect.width = -general_ButtonHitArea.width;
				navbar_ArcadeButton.hitAreaRect.height = -general_ButtonHitArea.height;
				
				navbar_Videos.x = 505;
				navbar_Videos.y = 60;
				navbar_VideosButton.textInfo.alignment = TextFormatAlign.CENTER;
				navbar_VideosButton.textInfo.bounds.width = 0;
				navbar_VideosButton.textInfo.bounds.height = 0;
				navbar_VideosButton.textInfo.color = gradient_FLAT_WHITE;
				navbar_VideosButton.textInfo.font = new fontVAGRounded();
				navbar_VideosButton.textInfo.fontSize = 42;
				navbar_VideosButton.textInfo.offset.x = 5;
				navbar_VideosButton.textInfo.offset.y = 50;
				navbar_VideosButton.textInfo.stroke = 6;
				navbar_VideosButton.textInfo.strokeColor = 0xF25588;
				navbar_VideosButton.hitAreaRect.x = 0;
				navbar_VideosButton.hitAreaRect.y = 0;
				navbar_VideosButton.hitAreaRect.width = -general_ButtonHitArea.width;
				navbar_VideosButton.hitAreaRect.height = -general_ButtonHitArea.height;
				
				navbar_Coloring.x = 849;
				navbar_Coloring.y = 60;
				
				navbar_MenuText.alignment = TextFormatAlign.LEFT;
				navbar_MenuText.bounds.width = 100;
				navbar_MenuText.bounds.height = 100;
				navbar_MenuText.color = gradient_FLAT_WHITE;
				navbar_MenuText.font = new fontVAGRounded();
				navbar_MenuText.fontSize = 60;
				navbar_MenuText.offset.x = -10;
				navbar_MenuText.offset.y = 0;
				navbar_MenuText.stroke = 3;
				navbar_MenuText.strokeColor = 0xF25588;
				
				navbar_PopupText.alignment = TextFormatAlign.CENTER;
				navbar_PopupText.bounds.width = 150;
				navbar_PopupText.bounds.height = 25;
				navbar_PopupText.color = gradient_FLAT_WHITE;
				navbar_PopupText.font = new noteworthyFont();
				navbar_PopupText.fontSize = 14;
				navbar_PopupText.offset.x = 0;
				navbar_PopupText.offset.y = -3;
				navbar_PopupText.stroke = 0;
				navbar_PopupText.strokeColor = 0x000000;
				
				navbar_VideoPopupText.alignment = TextFormatAlign.CENTER;
				navbar_VideoPopupText.bounds.width = 150;
				navbar_VideoPopupText.bounds.height = 25;
				navbar_VideoPopupText.color = gradient_FLAT_WHITE;
				navbar_VideoPopupText.font = new fontVAGRounded();
				navbar_VideoPopupText.fontSize = 14;
				navbar_VideoPopupText.offset.x = 0;
				navbar_VideoPopupText.offset.y = -3;
				navbar_VideoPopupText.stroke = 0;
				navbar_VideoPopupText.strokeColor = 0x000000;
				
				navbar_Home_SecondStroke_Size = 3;
				navbar_Home_SecondStroke_Color = 0xFFFFFF;
				navbar_Pages_SecondStroke_Size = 3;
				navbar_Pages_SecondStroke_Color = 0xFFFFFF;
				navbar_Options_SecondStroke_Size = 3;
				navbar_Options_SecondStroke_Color = 0xFFFFFF;
				navbar_Arcade_SecondStroke_Size = 3;
				navbar_Arcade_SecondStroke_Color = 0xFFFFFF;
				navbar_Videos_SecondStroke_Size = 3;
				navbar_Videos_SecondStroke_Color = 0xFFFFFF;
								
				navbar_Move_Min = 1;	// minimum movement speed (initial) for the open and close of the nav bar
				navbar_Move_Max = 10;	// maximum movement speed for the open and close of the nav bar
				navbar_Move_Delta = 1;	// accelerate and decelerate speed for the nav bar
				
				navbar_SliderY_Closed = 91;			// Y position of the slider tabs relative to the navbar background
				navbar_SliderY_Opened = -70;			// Y position of the slider tabs relative to the navbar background
				navbar_SliderSteps = 24;				// number of steps it will take for all the sub tabs on the nav bar to open/close;
				
				navbar_PausedText
				
				navbar_PausedText.alignment = TextFormatAlign.CENTER;
				navbar_PausedText.bounds.width = 1024;
				navbar_PausedText.bounds.height = 768;
				navbar_PausedText.color = gradient_FLAT_WHITE;
				navbar_PausedText.font = new noteworthyFont();
				navbar_PausedText.fontSize = 60;
				navbar_PausedText.offset.x = 0;		// centers on the stage.
				navbar_PausedText.offset.y = 0;
				navbar_PausedText.stroke = 3;
				navbar_PausedText.strokeColor = 0x000000;
				
				navbar_PausedTextAlpha = 0.0;
				navbar_PausedBlackAlpha = 0.25;			// the bitmap that is generated and displayed when the nav bar opens will have a black rectnagle placed over it with an alpha of this much.
			}

			// Options Dialog
			{
				optionsDialog_YOffset = -70;		// moves the entire dialog up or down.
				
				optionsDialog_Title.color = gradient_FLAT_WHITE;// the color gradient to use for all dialog titles
				optionsDialog_Title.font = new fontVAGRounded();// the font to use (defined in the font swc) to use for the title.
				optionsDialog_Title.fontSize = 40;// the font size to use
				optionsDialog_Title.stroke = 3;// the glow filter (aka: stroke) to apply to the text as defined above.
				optionsDialog_Title.strokeColor = 0x0f6da3;
				optionsDialog_Title.strokeAlpha = 1;
				optionsDialog_Title.offset.x = 0;// the x and y offset to apply to the title text.
				optionsDialog_Title.offset.y = 43;
				optionsDialog_Title.bounds.width = 860;
				optionsDialog_Title.bounds.height = 60;
				optionsDialog_Title.leading = 0;
				optionsDialog_Title.letterSpacing = 2;

				optionsDialog_MusicText.color = gradient_FLAT_WHITE;
				optionsDialog_MusicText.font = new fontVAGRounded();
				optionsDialog_MusicText.fontSize = 32;
				optionsDialog_MusicText.stroke = 3;
				optionsDialog_MusicText.strokeColor = 0x0f6da3;
				optionsDialog_MusicText.strokeAlpha = 1;
				optionsDialog_MusicText.offset.x = 97;	
				optionsDialog_MusicText.offset.y = 143;
				optionsDialog_MusicText.bounds.width = 170;
				optionsDialog_MusicText.bounds.height = 104;
				optionsDialog_MusicText.leading = 0;
				optionsDialog_MusicText.letterSpacing = 2;
				
				optionsDialog_ShowMoreText.color = gradient_FLAT_WHITE;
				optionsDialog_ShowMoreText.font = new fontVAGRounded();
				optionsDialog_ShowMoreText.fontSize = 32;
				optionsDialog_ShowMoreText.stroke = 3;
				optionsDialog_ShowMoreText.strokeColor = 0x0f6da3;
				optionsDialog_ShowMoreText.strokeAlpha = 1;
				optionsDialog_ShowMoreText.offset.x = 426;	
				optionsDialog_ShowMoreText.offset.y = 471;
				optionsDialog_ShowMoreText.bounds.width = 508;
				optionsDialog_ShowMoreText.bounds.height = 104;
				optionsDialog_ShowMoreText.leading = 0;
				optionsDialog_ShowMoreText.letterSpacing = 2;

				optionsDialog_SoundsText.color = gradient_FLAT_WHITE;
				optionsDialog_SoundsText.font = new fontVAGRounded();
				optionsDialog_SoundsText.fontSize = 32;
				optionsDialog_SoundsText.stroke = 3;
				optionsDialog_SoundsText.strokeColor = 0x0f6da3;
				optionsDialog_SoundsText.strokeAlpha = 1;
				optionsDialog_SoundsText.offset.x = 97;	
				optionsDialog_SoundsText.offset.y = 254;
				optionsDialog_SoundsText.bounds.width = 170;
				optionsDialog_SoundsText.bounds.height = 104;
				optionsDialog_SoundsText.leading = 0;
				optionsDialog_SoundsText.letterSpacing = 2;

				optionsDialog_VoiceText.color = gradient_FLAT_WHITE;
				optionsDialog_VoiceText.font = new fontVAGRounded();
				optionsDialog_VoiceText.fontSize = 32;
				optionsDialog_VoiceText.stroke = 3;
				optionsDialog_VoiceText.strokeColor = 0x0f6da3;
				optionsDialog_VoiceText.strokeAlpha = 1;
				optionsDialog_VoiceText.offset.x = 97;	
				optionsDialog_VoiceText.offset.y = 366;
				optionsDialog_VoiceText.bounds.width = 170;
				optionsDialog_VoiceText.bounds.height = 104;
				optionsDialog_VoiceText.leading = 0;
				optionsDialog_VoiceText.letterSpacing = 2;
				
				optionsDialog_CloseButton.textInfo.color = gradient_FLAT_WHITE;
				optionsDialog_CloseButton.textInfo.font = new fontVAGRounded();
				optionsDialog_CloseButton.textInfo.fontSize = 36;
				optionsDialog_CloseButton.textInfo.alignment = TextFormatAlign.CENTER;
				optionsDialog_CloseButton.textInfo.offset.x = 0;
				optionsDialog_CloseButton.textInfo.offset.y = 4;
				optionsDialog_CloseButton.textInfo.bounds.width = 45;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
				optionsDialog_CloseButton.textInfo.bounds.height = 10;
				optionsDialog_CloseButton.textInfo.stroke = 3;
				optionsDialog_CloseButton.textInfo.strokeColor = 0x0f6da3;
				optionsDialog_CloseButton.textInfo.strokeAlpha = 1;
				optionsDialog_CloseButton.textInfo.leading = 0;
				optionsDialog_CloseButton.textInfo.letterSpacing = 2;
				optionsDialog_CloseButton.hitAreaRect.x = 0;
				optionsDialog_CloseButton.hitAreaRect.y = 0;
				optionsDialog_CloseButton.hitAreaRect.width = 0;
				optionsDialog_CloseButton.hitAreaRect.height = 0;
				
				optionsDialog_CloseButtonOffset.x = 0;		// offset the center of the close button from the bottom center of the background.
				optionsDialog_CloseButtonOffset.y = -10;
				
				optionsDialog_ShowMoreApps.x = 97;
				optionsDialog_ShowMoreApps.y = 470;
				
				optionsDialog_MusicSlider = 143;		// Y location of the sliders
				optionsDialog_SoundsSlider = 255;
				optionsDialog_VoiceSlider = 366;
			
				optionsDialog_Sliders_MinX = 203;		// min and max y values of the sliders.
				optionsDialog_Sliders_MaxX = 671;			
			}
			
			// More Apps popup
			{
				moreApps_HeaderBottom.color = gradient_MoreApps_Purple;
				moreApps_HeaderBottom.font = new fontVAGRounded();
				moreApps_HeaderBottom.fontSize = 48;
				moreApps_HeaderBottom.offset.x = 0;
				moreApps_HeaderBottom.offset.y = 0;
				moreApps_HeaderBottom.bounds.width = 744;
				moreApps_HeaderBottom.bounds.height = 48;
				moreApps_HeaderBottom.leading = 0;
				moreApps_HeaderBottom.letterSpacing = 4;
				
				moreApps_HeaderTop.color = gradient_FLAT_BLACK;
				moreApps_HeaderTop.font = new fontVAGRounded();
				moreApps_HeaderTop.fontSize = 48;
				moreApps_HeaderTop.offset.x = 0;
				moreApps_HeaderTop.offset.y = 0;
				moreApps_HeaderTop.bounds.width = 744;
				moreApps_HeaderTop.bounds.height = 48;
				moreApps_HeaderTop.leading = 0;
				moreApps_HeaderTop.letterSpacing = 4;
				
				moreApps_ButtonText.alignment = TextFormatAlign.LEFT;
				moreApps_ButtonText.color = gradient_FLAT_WHITE;
				moreApps_ButtonText.font = new fontVAGRounded();
				moreApps_ButtonText.fontSize = 45;
				moreApps_ButtonText.stroke = 5;
				moreApps_ButtonText.strokeColor = 0x32acdd;
				moreApps_ButtonText.strokeAlpha = 1;
				moreApps_ButtonText.offset.x = 0;
				moreApps_ButtonText.offset.y = 0;
				moreApps_ButtonText.bounds.width = 300;
				moreApps_ButtonText.bounds.height = 88;
				moreApps_ButtonText.leading = 0;
				moreApps_ButtonText.letterSpacing = 2;
				
				moreApps_TopApp.alignment = TextFormatAlign.LEFT;
				moreApps_TopApp.color = gradient_FLAT_WHITE;
				moreApps_TopApp.font = new fontVAGRounded();
				moreApps_TopApp.fontSize = 65;
				moreApps_TopApp.stroke = 5;
				moreApps_TopApp.strokeColor = 0x64bc34;
				moreApps_TopApp.strokeAlpha = 1;
				moreApps_TopApp.offset.x = 0;
				moreApps_TopApp.offset.y = 0;
				moreApps_TopApp.bounds.width = 685;
				moreApps_TopApp.bounds.height = 65;
				moreApps_TopApp.leading = 0;
				moreApps_TopApp.letterSpacing = 2;
				
				moreApps_TopTitle.alignment = TextFormatAlign.LEFT;
				moreApps_TopTitle.color = gradient_FLAT_WHITE;
				moreApps_TopTitle.font = new fontVAGRounded();
				moreApps_TopTitle.fontSize = 139;
				moreApps_TopTitle.stroke = 5;
				moreApps_TopTitle.strokeColor = 0x64bc34;
				moreApps_TopTitle.strokeAlpha = 1;
				moreApps_TopTitle.offset.x = 0;
				moreApps_TopTitle.offset.y = 0;
				moreApps_TopTitle.bounds.width = 685;
				moreApps_TopTitle.bounds.height = 139;
				moreApps_TopTitle.leading = 0;
				moreApps_TopTitle.letterSpacing = 2;
			}
			
			
			// Global Pages variables
			{
				pages_progBar_Size.width = 800;			// the width and height of the progress bar that appears on pages like games, etc.
				pages_progBar_Size.height = 75;
				pages_progBar_FillOffset.x = 13;			// moves the "fill bar" in from the top left corner by this much.
				pages_progBar_FillOffset.y = 15;
				pages_progBar_Loc.x = 0;					// the progress bar, by default, is centered on the screen. This will offset from that location
				pages_progBar_Loc.y = 23;					// number of pixels from the top of the screen to the top of the progress bar.
				pages_progBar_FillColor = 0x6DE02B;		// the color of the fill bar for all pages. Each Pages will have this value also, and if the specific pages has a value of -1, then this color will be used.
				pages_progBar_EdgeColor = 0x5e9543;		// the color of the edge that preceeds the bar
				pages_progBar_EdgeWidth = 2;			// the width of the edge that preceeds the bar in pixels
				
				// some pages, like games, have a title text. 
				pages_TitleBar.x = 0;		// horizontally centered.
				pages_TitleBar.y = -1;		// top of bar from top of screen.
				
				pages_TitleText.alignment = TextFormatAlign.CENTER;
				pages_TitleText.bounds.width = 440;
				pages_TitleText.bounds.height = 50;
				pages_TitleText.color = gradient_FLAT_WHITE;
				pages_TitleText.font = new noteworthyFont();
				pages_TitleText.fontSize = 40;
				pages_TitleText.offset.x = 0;		// center horizontally
				pages_TitleText.offset.y = -1;		// center of text from the center of the bar
				pages_TitleText.stroke = 4;
				pages_TitleText.strokeColor = 0x00578c; //0x0f6da3;

				// all the arcade game pages will have their title text appear really big and then scale down to the their title bar.
				// this text will control that intial text for both the intro title.
				pages_TitleIntro_Text.alignment = TextFormatAlign.CENTER;
				pages_TitleIntro_Text.bounds.width = 1024;
				pages_TitleIntro_Text.bounds.height = 768;
				pages_TitleIntro_Text.color = gradient_FLAT_WHITE;
				pages_TitleIntro_Text.font = new noteworthyFont();
				pages_TitleIntro_Text.fontSize = 90;
				pages_TitleIntro_Text.offset.x = 0;		// center horizontally
				pages_TitleIntro_Text.offset.y = -50;		// center vertically
				pages_TitleIntro_Text.stroke = 10; //6;
				pages_TitleIntro_Text.strokeColor = 0x00578c; //0x0f6da3;
								
				pages_TitleIntro_HoldDelay = 2000;		// show the text for this many milliseconds and then animate it out.
				pages_TitleIntro_Steps = 30;			// number of steps it takes to go from full size to the size of the title bar
				pages_TitleIntro_FadeOutOnStep = 29;	// start fading out on this step number
				pages_TitleIntro_EnableGameOnStep = 20;	// Enable and start the game when the intro text reaches this step.

				pages_GameFinale_Text.alignment = TextFormatAlign.CENTER;
				pages_GameFinale_Text.bounds.width = 1024;
				pages_GameFinale_Text.bounds.height = 768;
				pages_GameFinale_Text.color = gradient_FLAT_WHITE;
				pages_GameFinale_Text.font = new noteworthyFont();
				pages_GameFinale_Text.fontSize = 90;
				pages_GameFinale_Text.offset.x = 0;		// center horizontally
				pages_GameFinale_Text.offset.y = -50;		// center vertically
				pages_GameFinale_Text.stroke = 6;
				pages_GameFinale_Text.strokeColor = 0x0f6da3;
				
				pages_GameFinale_Pause = 4000;			// milliseconds to pause before fading out. Particles will fade out with the text
				pages_GameFinale_FadeOutSpeed = 0.1;	// alpha fade out per tick.
				
				pages_GameFinale_Particles_Delay_Min = 500;		// spawn a new particle burst every x ms
				pages_GameFinale_Particles_Delay_Max = 900;
				
				pages_GameFinale_Particles_SpawnLocationOffset.width = 0;		// by default, the start bursts can happen anywhere inside the finale text bounds. You can add/remove to the bounds
				pages_GameFinale_Particles_SpawnLocationOffset.height = 0;		// width and height by adding or subtracting these values.

				// the settings for each particle burst that will occur
				pages_GameFinale_ParticleBurst.controlPoint1Offset.x = 0;				// if using a bezier move mode, this will offset the control points from the start and end points.
				pages_GameFinale_ParticleBurst.controlPoint1Offset.y = 0;
				pages_GameFinale_ParticleBurst.controlPoint2Offset.x = 0;
				pages_GameFinale_ParticleBurst.controlPoint2Offset.y = 0;
				pages_GameFinale_ParticleBurst.fadeInSpeed = 0.1;							// fade in speed between 0 and 1;
				pages_GameFinale_ParticleBurst.fadeOutAt = 0.75;						// percentage to start fading out
				pages_GameFinale_ParticleBurst.fadeOutSpeed = 0.1;						// fade out per tick
				pages_GameFinale_ParticleBurst.individualParticleSettings = false;		// if true each particle will use it's own rotation and radius, otherwise, all the particles will use the same rotation and radius
				pages_GameFinale_ParticleBurst.maxAlpha = 1;							// max alpha of the particle.
				pages_GameFinale_ParticleBurst.numberOfParticles = 10;					// total number of particles in the burst.
				pages_GameFinale_ParticleBurst.particleDelay = 0;						// each particle will delay this many ticks before moving. if zero, all particles will move each tick.
				pages_GameFinale_ParticleBurst.particleImageScale_Start = 0.0;				// scale the particle image
				pages_GameFinale_ParticleBurst.particleImageScale_End = 0.5;				// scale the particle image
				pages_GameFinale_ParticleBurst.radius_Max = 120;						// maximum distance the particle will move
				pages_GameFinale_ParticleBurst.radius_Min = 90;						// min travel distance.
				pages_GameFinale_ParticleBurst.randomAngles = false;					// if true, eahc particle will move on a random angle, otherwise, the angle of each particle will be evenly put in a circle.
				pages_GameFinale_ParticleBurst.rotation_Max = 5;						// maximum angle to rotate per tick in degrees
				pages_GameFinale_ParticleBurst.rotation_Min = -5;						// minimum angle to rotate per tick in degrees
				pages_GameFinale_ParticleBurst.spawnRect.x = 0;							// for the game finale, the bursts can set their start points to anywhere within the game finale text box bounds.
				pages_GameFinale_ParticleBurst.spawnRect.y = 0;							// these values will offset thoughs bounds to allow the burst to spawn outside the text, or be constrained to a smaller area
				pages_GameFinale_ParticleBurst.spawnRect.width = 0;
				pages_GameFinale_ParticleBurst.spawnRect.height = 0;
				pages_GameFinale_ParticleBurst.startAlpha = 0;							// the starting alpha of each particle. it will not fade in until it starts to move.
				pages_GameFinale_ParticleBurst.steps = 30;								// number of steps to move the radius.
				pages_GameFinale_ParticleBurst.UseBezier = true;						// use bezier movement or straight line code


				pages_Games_EndOfLevel_DimmerAlpha = 0.20;				// the alpha value of the mouse intercept sprite that is behind the end of level summary dialog.
				pages_Games_EndOfLevel_CountSteps = 30;					// the number of steps the scores and times will count up.

				pages_Games_EndOfLevel_Title.color = gradient_FLAT_WHITE;// the color gradient to use for all dialog titles
				pages_Games_EndOfLevel_Title.font = new noteworthyFont();// the font to use (defined in the font swc) to use for the title.
				pages_Games_EndOfLevel_Title.fontSize = 40;// the font size to use
				pages_Games_EndOfLevel_Title.stroke = 0;// the glow filter (aka: stroke) to apply to the text as defined above.
				pages_Games_EndOfLevel_Title.strokeColor = 0x0f6da3;
				pages_Games_EndOfLevel_Title.strokeAlpha = 1;
				pages_Games_EndOfLevel_Title.alignment = TextFormatAlign.CENTER;
				pages_Games_EndOfLevel_Title.offset.x = 0;// the x and y offset to apply to the title text.
				pages_Games_EndOfLevel_Title.offset.y = 222;
				pages_Games_EndOfLevel_Title.bounds.width = 820;
				pages_Games_EndOfLevel_Title.bounds.height = 200;
				pages_Games_EndOfLevel_Title.leading = 0;
				pages_Games_EndOfLevel_Title.letterSpacing = 0;

				pages_Games_EndOfLevel_Line.color = gradient_FLAT_WHITE;
				pages_Games_EndOfLevel_Line.font = new noteworthyFont();
				pages_Games_EndOfLevel_Line.fontSize = 300;
				pages_Games_EndOfLevel_Line.stroke = 5;
				pages_Games_EndOfLevel_Line.strokeColor = 0x0f6da3;
				pages_Games_EndOfLevel_Line.strokeAlpha = 1;
				pages_Games_EndOfLevel_Line.alignment = TextFormatAlign.CENTER;
				pages_Games_EndOfLevel_Line.offset.x = 0;
				pages_Games_EndOfLevel_Line.offset.y = 110;
				pages_Games_EndOfLevel_Line.bounds.width = 820;
				pages_Games_EndOfLevel_Line.bounds.height = 200;
				pages_Games_EndOfLevel_Line.leading = 0;
				pages_Games_EndOfLevel_Line.letterSpacing = 0;
				
				pages_Games_EndOfLevel_Proceed.color = gradient_EndOfLevel_Yellow;
				pages_Games_EndOfLevel_Proceed.font = new noteworthyFont();
				pages_Games_EndOfLevel_Proceed.fontSize = 50;
				pages_Games_EndOfLevel_Proceed.stroke = 0;
				pages_Games_EndOfLevel_Proceed.strokeColor = 0x0C7405;
				pages_Games_EndOfLevel_Proceed.strokeAlpha = 1;
				pages_Games_EndOfLevel_Line.alignment = TextFormatAlign.CENTER;
				pages_Games_EndOfLevel_Proceed.offset.x = 421;
				pages_Games_EndOfLevel_Proceed.offset.y = 305;
				pages_Games_EndOfLevel_Proceed.bounds.width = 562;
				pages_Games_EndOfLevel_Proceed.bounds.height = 60;
				pages_Games_EndOfLevel_Proceed.leading = 0;
				pages_Games_EndOfLevel_Proceed.letterSpacing = 0;

				pages_Games_EndOfLevel_Close.textInfo.color = gradient_FLAT_WHITE;
				pages_Games_EndOfLevel_Close.textInfo.font = new noteworthyFont();
				pages_Games_EndOfLevel_Close.textInfo.fontSize = 36;
				pages_Games_EndOfLevel_Close.textInfo.alignment = TextFormatAlign.CENTER;
				pages_Games_EndOfLevel_Close.textInfo.offset.x = 728;
				pages_Games_EndOfLevel_Close.textInfo.offset.y = 298;
				pages_Games_EndOfLevel_Close.textInfo.bounds.width = 45;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
				pages_Games_EndOfLevel_Close.textInfo.bounds.height = 10;
				pages_Games_EndOfLevel_Close.textInfo.stroke = 3;
				pages_Games_EndOfLevel_Close.textInfo.strokeColor = 0x0f6da3;
				pages_Games_EndOfLevel_Close.textInfo.strokeAlpha = 1;
				pages_Games_EndOfLevel_Close.textInfo.leading = 0;
				pages_Games_EndOfLevel_Close.textInfo.letterSpacing = 2;
				pages_Games_EndOfLevel_Close.hitAreaRect.x = 0;
				pages_Games_EndOfLevel_Close.hitAreaRect.y = 0;
				pages_Games_EndOfLevel_Close.hitAreaRect.width = 0;
				pages_Games_EndOfLevel_Close.hitAreaRect.height = 0;

				pages_Games_EndOfLevel_Stars_StartScale = 1.8;			// the star will appear at this scale size
				pages_Games_EndOfLevel_Stars_StartAlpha = 0.2;			// the star will appear with this alpha
				pages_Games_EndOfLevel_Stars_ShrinkSteps = 5;			// the star will go from it's start scale to 1.0 over this many steps. The "speed" at which is shrinks will be caluclated to dot he bounce.
				pages_Games_EndOfLevel_Stars_BounceFallOff = 0.8;		// Multiple the scale per step value by this much each time it changes direction
				
				pages_Games_EndOfLevel_Stars_Center.x = 0;				// distance from the CENTER of the background image to the center of the middle star.
				pages_Games_EndOfLevel_Stars_Center.y = 370;			// distance from the top of the background image to the center of the stars
				
				pages_Games_EndOfLevel_Stars_Spacing = 119;				// distance between centers of each star.
				pages_Games_EndOfLevel_Stars_MoveSteps = 8;			// number of steps to move the star over to the left or right as the next star springs in
				pages_Games_EndOfLevel_Stars_NextStarOnStep = 5;		// as each star "moves" over, once that move step has reached this value, start the next star animating
				pages_Games_EndOfLevel_Stars_NextStarDelay = 380;		// when a star stars animating, wait this many milliseconds before starting moving for the next star.
			
				pages_Games_EndOfLevel_BoopFrame = 3;		// as the number count up, boop every 5 frames/changes.
				
				pages_HighlightColor = 0xffff00;
				pages_HighlightFontColor = 0x2188cb;
				pages_HighlightSizeFactor = 1;
				pages_HighlightStrokeColor = 0x2e4695;
				pages_HighlightStrokeSize = 5;				
				
			}
			
			// Message Box Defaults
			{
				msgboxDefaults.location.x = 0;// msgbox's by default will be centered on the screen. This will allow you to move the entire message box.
				msgboxDefaults.location.y = 0;
	
				msgboxDefaults.ScaleOnAppear_Steps = 6;		// the number of steps to "pulse" in.
				msgboxDefaults.ScaleOnAppear_Max = 1.05;			// the pulses uses a bezier fomula... this the is the scale size control points.
	
				msgboxDefaults.minimumSize_1Button.width = 250;
				msgboxDefaults.minimumSize_1Button.height = 250;
				msgboxDefaults.minimumSize_2Buttons.width = 350;
				msgboxDefaults.minimumSize_2Buttons.height = 100;
	
				msgboxDefaults.margins.width = 25;// the margins control how much space is added around the edges of the content on each size;
				msgboxDefaults.margins.height = 110;
	
				msgboxDefaults.contentY = 10;// offset the content by this many pixels. 
				msgboxDefaults.bounds.width = 800;// this is the auto sizing bounds for the message box, meaning it will never get bigger thant his width and height.
				msgboxDefaults.bounds.height = 600;
	
				msgboxDefaults.title.color = gradient_FLAT_WHITE;// the color gradient to use for all dialog titles
				msgboxDefaults.title.font = new noteworthyFont();// the font to use (defined in the font swc) to use for the title.
				msgboxDefaults.title.fontSize = 40;// the font size to use
				msgboxDefaults.title.stroke = 3;// the glow filter (aka: stroke) to apply to the text as defined above.
				msgboxDefaults.title.strokeColor = 0x0f6da3;
				msgboxDefaults.title.strokeAlpha = 1;
				msgboxDefaults.title.offset.x = 0;// the x and y offset to apply to the title text.
				msgboxDefaults.title.offset.y = 43;
				msgboxDefaults.title.bounds.width = 860;
				msgboxDefaults.title.bounds.height = 60;
				msgboxDefaults.title.leading = 0;
				msgboxDefaults.title.letterSpacing = 2;
	
				msgboxDefaults.prompt.color = gradient_FLAT_WHITE;
				msgboxDefaults.prompt.font = new noteworthyFont();
				msgboxDefaults.prompt.fontSize = 40;
				msgboxDefaults.prompt.stroke = 3;
				msgboxDefaults.prompt.strokeColor = 0x0f6da3;
				msgboxDefaults.prompt.strokeAlpha = 1;
				msgboxDefaults.prompt.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.prompt.offset.x = 0;
				msgboxDefaults.prompt.offset.y = 0;
				msgboxDefaults.prompt.bounds.width = 800;
				msgboxDefaults.prompt.bounds.height = 600;
				msgboxDefaults.prompt.leading = 0;
				msgboxDefaults.prompt.letterSpacing = 2;
				msgboxDefaults.prompt_padding.width = 2;// number of extra pixels to space around the prompt when additional content is above or below it.
				msgboxDefaults.prompt_padding.height = 2;
	
				msgboxDefaults.caption1.color = gradient_FLAT_WHITE;
				msgboxDefaults.caption1.font = new noteworthyFont();
				msgboxDefaults.caption1.fontSize = 46;
				msgboxDefaults.caption1.stroke = 3;
				msgboxDefaults.caption1.strokeColor = 0x0f6da3;
				msgboxDefaults.caption1.strokeAlpha = 1;
				msgboxDefaults.caption1.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.caption1.offset.x = 0;
				msgboxDefaults.caption1.offset.y = 0;
				msgboxDefaults.caption1.bounds.width = 800;
				msgboxDefaults.caption1.bounds.height = 600;
				msgboxDefaults.caption1_padding.width = 2;
				msgboxDefaults.caption1_padding.height = 2;
				msgboxDefaults.caption1.leading = 0;
				msgboxDefaults.caption1.letterSpacing = 2;
	
				msgboxDefaults.caption2.color = gradient_FLAT_WHITE;
				msgboxDefaults.caption2.font = new noteworthyFont();
				msgboxDefaults.caption2.fontSize = 46;
				msgboxDefaults.caption2.stroke = 3;
				msgboxDefaults.caption2.strokeColor = 0x0f6da3;
				msgboxDefaults.caption2.strokeAlpha = 1;
				msgboxDefaults.caption2.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.caption2.offset.x = 0;
				msgboxDefaults.caption2.offset.y = 0;
				msgboxDefaults.caption2.bounds.width = 800;
				msgboxDefaults.caption2.bounds.height = 600;
				msgboxDefaults.caption2.leading = 0;
				msgboxDefaults.caption2.letterSpacing = 2;
				msgboxDefaults.caption2_padding.width = 2;
				msgboxDefaults.caption2_padding.height = 2;
	
				msgboxDefaults.image_data = null;// by default, there is no image, so set to null
				msgboxDefaults.image_offset.x = 0;
				msgboxDefaults.image_offset.y = 0;
				msgboxDefaults.image_padding.width = 2;
				msgboxDefaults.image_padding.height = 2;
	
				// 1 = image, 2 = caption1, 3 = caption2, 4 = prompt.
				msgboxDefaults.displayOrder = [1,2,3,4];// set the display order for the content.
	
				msgboxDefaults.backgroundAlpha = 0.25;// the back cover that goes behind the message box, default it's alpha to this.
	
				msgboxDefaults.ok_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" );
				msgboxDefaults.ok_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" );
				msgboxDefaults.ok_location.x = 0;
				msgboxDefaults.ok_location.y = -20;
				msgboxDefaults.ok_Settings.textInfo.color = gradient_FLAT_WHITE;
				msgboxDefaults.ok_Settings.textInfo.font = new noteworthyFont();
				msgboxDefaults.ok_Settings.textInfo.fontSize = 36;
				msgboxDefaults.ok_Settings.textInfo.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.ok_Settings.textInfo.offset.x = 0;
				msgboxDefaults.ok_Settings.textInfo.offset.y = 4;
				msgboxDefaults.ok_Settings.textInfo.bounds.width = 45;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
				msgboxDefaults.ok_Settings.textInfo.bounds.height = 10;
				msgboxDefaults.ok_Settings.textInfo.stroke = 3;
				msgboxDefaults.ok_Settings.textInfo.strokeColor = 0x0f6da3;
				msgboxDefaults.ok_Settings.textInfo.strokeAlpha = 1;
				msgboxDefaults.ok_Settings.textInfo.leading = 0;
				msgboxDefaults.ok_Settings.textInfo.letterSpacing = 2;
				msgboxDefaults.ok_Settings.hitAreaRect.x = 0;
				msgboxDefaults.ok_Settings.hitAreaRect.y = 0;
				msgboxDefaults.ok_Settings.hitAreaRect.width = 0;
				msgboxDefaults.ok_Settings.hitAreaRect.height = 0;
	
				msgboxDefaults.continue_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" );
				msgboxDefaults.continue_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" );
				msgboxDefaults.continue_location.x = 0;
				msgboxDefaults.continue_location.y = -20;
				msgboxDefaults.continue_Settings.textInfo.color = gradient_FLAT_WHITE;
				msgboxDefaults.continue_Settings.textInfo.font = new noteworthyFont();
				msgboxDefaults.continue_Settings.textInfo.fontSize = 36;
				msgboxDefaults.continue_Settings.textInfo.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.continue_Settings.textInfo.offset.x = 0;
				msgboxDefaults.continue_Settings.textInfo.offset.y = 0;
				msgboxDefaults.continue_Settings.textInfo.bounds.width = 45;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
				msgboxDefaults.continue_Settings.textInfo.bounds.height = 10;
				msgboxDefaults.continue_Settings.textInfo.stroke = 3;
				msgboxDefaults.continue_Settings.textInfo.strokeColor = 0x0f6da3;
				msgboxDefaults.continue_Settings.textInfo.strokeAlpha = 1;
				msgboxDefaults.continue_Settings.textInfo.leading = 0;
				msgboxDefaults.continue_Settings.textInfo.letterSpacing = 2;
				msgboxDefaults.continue_Settings.hitAreaRect.x = 0;
				msgboxDefaults.continue_Settings.hitAreaRect.y = 4;
				msgboxDefaults.continue_Settings.hitAreaRect.width = 0;
				msgboxDefaults.continue_Settings.hitAreaRect.height = 0;
	
				msgboxDefaults.yes_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Narrow_s" );
				msgboxDefaults.yes_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Narrow_h" );
				msgboxDefaults.yes_location.x = -110;
				msgboxDefaults.yes_location.y = -20;
				msgboxDefaults.yes_Settings.textInfo.color = gradient_FLAT_WHITE;
				msgboxDefaults.yes_Settings.textInfo.font = new noteworthyFont();
				msgboxDefaults.yes_Settings.textInfo.fontSize = 36;
				msgboxDefaults.yes_Settings.textInfo.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.yes_Settings.textInfo.offset.x = 0;
				msgboxDefaults.yes_Settings.textInfo.offset.y = 3;
				msgboxDefaults.yes_Settings.textInfo.bounds.width = 45;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
				msgboxDefaults.yes_Settings.textInfo.bounds.height = 10;
				msgboxDefaults.yes_Settings.textInfo.stroke = 3;
				msgboxDefaults.yes_Settings.textInfo.strokeColor = 0x0f6da3;
				msgboxDefaults.yes_Settings.textInfo.strokeAlpha = 1;
				msgboxDefaults.yes_Settings.textInfo.leading = 0;
				msgboxDefaults.yes_Settings.textInfo.letterSpacing = 2;
				msgboxDefaults.yes_Settings.hitAreaRect.x = 0;
				msgboxDefaults.yes_Settings.hitAreaRect.y = 0;
				msgboxDefaults.yes_Settings.hitAreaRect.width = 0;
				msgboxDefaults.yes_Settings.hitAreaRect.height = 0;
	
				msgboxDefaults.no_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Narrow_s" );
				msgboxDefaults.no_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Narrow_s" );
				msgboxDefaults.no_location.x = 110;
				msgboxDefaults.no_location.y = -20;
				msgboxDefaults.no_Settings.textInfo.color = gradient_FLAT_WHITE;
				msgboxDefaults.no_Settings.textInfo.font = new noteworthyFont();
				msgboxDefaults.no_Settings.textInfo.fontSize = 36;
				msgboxDefaults.no_Settings.textInfo.alignment = TextFormatAlign.CENTER;
				msgboxDefaults.no_Settings.textInfo.offset.x = 0;
				msgboxDefaults.no_Settings.textInfo.offset.y = 3;
				msgboxDefaults.no_Settings.textInfo.bounds.width = 45;// as for all buttons, the auto size bounds are the size of the button graphic MINUS the specified bounds.
				msgboxDefaults.no_Settings.textInfo.bounds.height = 10;
				msgboxDefaults.no_Settings.textInfo.stroke = 3;
				msgboxDefaults.no_Settings.textInfo.strokeColor = 0x0f6da3;
				msgboxDefaults.no_Settings.textInfo.strokeAlpha = 1;
				msgboxDefaults.no_Settings.textInfo.leading = 0;
				msgboxDefaults.no_Settings.textInfo.letterSpacing = 2;
				msgboxDefaults.no_Settings.hitAreaRect.x = 0;
				msgboxDefaults.no_Settings.hitAreaRect.y = 0;
				msgboxDefaults.no_Settings.hitAreaRect.width = 0;
				msgboxDefaults.no_Settings.hitAreaRect.height = 0;
				
				msgboxDefaults.cancel_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Close_s" );
				msgboxDefaults.cancel_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Close_h" );
				msgboxDefaults.cancel_location.x = 0;
				msgboxDefaults.cancel_location.y = 42;
				msgboxDefaults.cancel_Settings.hitAreaRect.x = 0;
				msgboxDefaults.cancel_Settings.hitAreaRect.y = 0;
				msgboxDefaults.cancel_Settings.hitAreaRect.width = 0;
				msgboxDefaults.cancel_Settings.hitAreaRect.height = 0;
				
				msgboxDefaults.extraBottomPadding = 0;		// Some message boxes put the button locates in the content area. This variable will add spacing to the bottom of the content area without effecting the content positions to account for this.
			}			
			
			// messages in the game - base them all on a clone of hte message box defaults, then afterwards you can alter the message settings for that box.
			{
				messages_captcha = msgboxDefaults.CloneMessageBoxDefaults();
				messages_captcha.image_data = new BitmapData(1, 40, true, 0);
				messages_captcha.displayOrder = [4,1];
				
				messages_Mainmenu_Help = msgboxDefaults.CloneMessageBoxDefaults();
				messages_Mainmenu_RateIt = msgboxDefaults.CloneMessageBoxDefaults();
				messages_Mainmenu_Continue = msgboxDefaults.CloneMessageBoxDefaults();
				messages_Mainmenu_Interactive = msgboxDefaults.CloneMessageBoxDefaults();
				messages_Mainmenu_Info = msgboxDefaults.CloneMessageBoxDefaults();
				messages_navbar_DeleteRecording = msgboxDefaults.CloneMessageBoxDefaults();
				messages_profileDialog_Remove = msgboxDefaults.CloneMessageBoxDefaults();
				messages_profileDialog_Save = msgboxDefaults.CloneMessageBoxDefaults();

				// these are just temp for now
				{
					messages_temp_Pages = msgboxDefaults.CloneMessageBoxDefaults();
					messages_temp_Options = msgboxDefaults.CloneMessageBoxDefaults();
					messages_temp_MoreBooks = msgboxDefaults.CloneMessageBoxDefaults();
					messages_temp_Arcade = msgboxDefaults.CloneMessageBoxDefaults();
				}
				
				messages_Mainmenu_Continue.yes_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" );
				messages_Mainmenu_Continue.yes_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" );
				messages_Mainmenu_Continue.no_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" );
				messages_Mainmenu_Continue.no_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" );
				messages_Mainmenu_Continue.yes_location.x = -140;
				messages_Mainmenu_Continue.no_location.x = 140;
				
				messages_Mainmenu_Interactive.yes_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" );
				messages_Mainmenu_Interactive.yes_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" );
				messages_Mainmenu_Interactive.no_bitmapData_s = GlobalData.getSheetBitmap( "Dialog_Button_Normal_s" );
				messages_Mainmenu_Interactive.no_bitmapData_h = GlobalData.getSheetBitmap( "Dialog_Button_Normal_h" );
				messages_Mainmenu_Interactive.yes_location.x = -140;
				messages_Mainmenu_Interactive.no_location.x = 140;
				messages_Mainmenu_Interactive.no_Settings.textInfo.fontSize = 30;
				messages_Mainmenu_Interactive.yes_Settings.textInfo.fontSize = 30;
				
				messages_Mainmenu_Info.prompt.fontSize = 36;
			}
		}
	}
}