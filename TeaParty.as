﻿package 
{
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.SoundEx;
	import com.cupcake.SoundCollection;
	import com.cupcake.SpriteSheetSequence;
	import com.cupcake.Utils;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.MP3Loader;
	import com.greensock.TweenMax;
	import com.SBLib.*;
	import fl.motion.Color;	
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	public final class TeaParty extends WBZ_BookPage
	{
		//INSTRUCTIONS
		private var StartInstructionVO:SoundEx;
		private var TreatInstructionVO:SoundEx;
		private var BlendInstructionVO:SoundEx;
		private var GreatVO:SoundEx;
		private var SipSound:SoundEx;
		
		//SOUND EFFECTS
		private var RandomSoundArray:Array = new Array();
		private var sndConfetti:SoundEx;
		
		private var BlenderSound:SoundCollection;
		private var Plate1Sound:SoundCollection;
		private var Plate2Sound:SoundCollection;
		private var Plate3Sound:SoundCollection;
		private var BlendPourSound:SoundCollection;
		private var JuicePourSound:SoundCollection;
		//private var SipSound:SoundCollection;
		private var FruitSound:SoundCollection;
		private var ClothSound:SoundCollection;
		private var CupSound:SoundCollection;
		private var PlacePlateSound:SoundCollection;
		
		//PARTICLES
		private var Particles1:SB_Particles			= null;
		private var Particles2:SB_Particles			= null;
		private var Particles3:SB_Particles			= null;
		private var Particles4:SB_Particles			= null;
		private var Particles5:SB_Particles			= null;
		private var Particles6:SB_Particles			= null;
		private var confettiCounter:int = 2000;
		
		//HEXADECIMAL COLOUR						    CC0C7F  EE2B74  0076C0  EE2965  FFE5A6  FFBB77  F6862E  FFEE55  DB829A
		private var DefaultReds:Array = new Array	(	204,	238,	0,		238,	255,	255,	246,	255,	219		);
		private var DefaultGreens:Array = new Array	(	12,		43,		118,	41,		229,	187,	134,	238,	130		);
		private var DefaultBlues:Array = new Array	(	127,	116,	192,	101,	166,	119,	46,		85,		192		); //154
		private var placedChunks:Array = new Array();
		private var MixRed:int = 0;
		private var MixGreen:int = 0;
		private var MixBlue:int = 0;
		
		//STARTING POSITIONS
		private var PlateX:Array = new Array();
		private var PlateY:Array = new Array();
		private var CupX:Array = new Array();
		private var CupY:Array = new Array();
		private var FillX:Array = new Array();
		private var FillY:Array = new Array();
		private var NapkinX:Array = new Array();
		private var NapkinY:Array = new Array();
		private var TreatX:Array = new Array();
		private var TreatY:Array = new Array();
		private var BlenderX:int;
		private var BlenderY:int;
		private var GlassX:int;
		private var GlassY:int;
		private var JuiceX:int;
		private var JuiceY:int;
		private var ReplaceX:int;
		private var ReplaceY:int;
		private var slidePxAmount:int = 600;
		
		//FRUIT VARIABLES
		private var GuideFruitArray:Array;
		private var TrayFruitArray:Array;
		private var FruitSheetArray:Array = new Array();
		private var TrayFruitSheetArray:Array = new Array();
		
		//TREAT VARIABLES
		private var GuideTreatArray:Array;
		private var TrayTreatArray:Array;
		private var TreatSheetArray:Array = new Array();
		private var TrayTreatSheetArray:Array = new Array();
		
		//TEACUP VARIABLES
		private var GuideCupArray:Array;
		private var TrayCupArray:Array;
		private var CupSheetArray:Array = new Array();
		private var TrayCupSheetArray:Array = new Array();
		
		private var CupFillArray:Array;
		private var CupFillSheets:Array = new Array();
		private var JugSheet:TapSpritesheet;
		private var JugSpoutSheet:TapSpritesheet;
		
		//PLATE VARIABLES
		private var GuidePlateArray:Array;
		private var TrayPlateArray:Array;
		private var PlateSheetArray:Array = new Array();
		private var TrayPlateSheetArray:Array = new Array();
		
		//NAPKIN VARIABLES
		private var NapkinArray:Array;
		
		//TABLE CLOTHE VARIABLES
		private var iTotalSwatches:int = 3;
		private var SwatchOptionsUsedArray = new Array();
		private var SwatchPngArray:Array = new Array("SS_TEAPARTY_CLOTHSWATCHES_0001.png","SS_TEAPARTY_CLOTHSWATCHES_0002.png","SS_TEAPARTY_CLOTHSWATCHES_0003.png",
											         "SS_TEAPARTY_CLOTHSWATCHES_0004.png","SS_TEAPARTY_CLOTHSWATCHES_0005.png","SS_TEAPARTY_CLOTHSWATCHES_0006.png");
										 
		//BLENDER VARIABLES
		private var _blue:TapSpritesheet;
		private var _green:TapSpritesheet;
		private var _yellow:TapSpritesheet;
		private var _spin:TapSpritesheet;
		
		//TIMING VARIABLES
		private var nTrayTweenTime:Number = 1;
		private var nObjectFadeTime:Number = 0.5;
		private var nMiscDelayTime:Number = 0.25;
		
		//STATE RELATED VARIABLES
		private var bFirstPass:Boolean = true;
		private var bDragDelay:Boolean = false;
		private var bPauseInteractions:Boolean = true;
		private var bDragJuice:Boolean = false;
		private var bDragBlender:Boolean = false;
		private var bBlenderReady:Boolean = false;
		private var bBlenderAnim:Boolean = false;
		private var iCurrentState:int = 0;
		private var lastFill:Array = new Array(false, false, false);
		private var bMouseDown:Boolean = false;
		
		//MISC VARIABLES
		private var SequenceVector:Vector.<SpriteSheetSequence> = new Vector.<SpriteSheetSequence>;
		private var nGuideOpacity:Number = 0.2;
		private var treatRemoved:Placeholder;
		private var _draggable:Placeholder;
		private var _currentGuide;
		
		private var Finale:clsGameFinale;
		private var EndOfLevel:clsGames_EndOfLevelSummary;
		private var sndEnd:SoundEx;
		private var Crowd:CheeringCrowd;
		private var winSound:SoundEx = new SoundEx( "win", CONFIG::DATA + "sounds/games/GameCompleted.mp3", App.soundVolume);
		
		public var Background:Rectangle;
		
		
		public function TeaParty()
		{
			super();
			
			Background = new Rectangle( 0, 0, 1024, 768 );
			
			Particles1 = new SB_Particles(false);
			Particles2 = new SB_Particles(false);
			Particles3 = new SB_Particles(false);
			Particles4 = new SB_Particles(false);
			Particles5 = new SB_Particles(false);
			Particles6 = new SB_Particles(false);
			
			BlenderSound = SoundCollection.GetAnimSounds("blender", [1, 2, 3]);
			Plate1Sound = SoundCollection.GetAnimSounds("plate1", [1, 1, 1, 2]);
			Plate2Sound = SoundCollection.GetAnimSounds("plate1", [1, 1, 1, 3]);
			Plate3Sound = SoundCollection.GetAnimSounds("plate1", [1, 1, 1, 2]);
			BlendPourSound = SoundCollection.GetAnimSounds("pour", [1]);
			JuicePourSound = SoundCollection.GetAnimSounds("pour", [2]);
			//SipSound = SoundCollection.GetAnimSounds("sip", [1]);
			FruitSound = SoundCollection.GetAnimSounds("fruit", [1]);
			ClothSound = SoundCollection.GetAnimSounds("tablecloth", [1]);
			CupSound = SoundCollection.GetAnimSounds("cup", [1]);
			PlacePlateSound = SoundCollection.GetAnimSounds("plate", [1]);
			
			imageFolder = CONFIG::DATA + "images/pages/TeaParty/";
			assets.push( new ExternalImage( "Background/SS_pinkdots_BG.png", scene_mc.start_BG ) );
			assets.push( new ExternalImage( "SS_TEAPARTY_TRAY.png", scene_mc.tray_mc.tray ) );
			assets.push( new ExternalImage( "SS_TEAPARTY_BLENDER.png", scene_mc.blender_mc.blender_base ) );
			assets.push( new ExternalImage( "SS_TEAPARTY_BLENDER_JUG.png", scene_mc.blender_mc.blender_glass_mc.glass ) );
			assets.push( new ExternalImage( "SS_TEAPARTY_BLENDER_SMOOTHIE.png", scene_mc.blender_mc.blender_glass_mc.blender_juice.actor ) );
			assets.push( new ExternalImage( "SS_TEAPARTY_JUICE_POUR_0003.png", scene_mc.blender_mc.blender_glass_mc.pour ) );
			assets.push( new ExternalImage( "SS_TEAPARTY_PRINCESS.png", scene_mc.princess.actor ) );
			
			//assets.push( new ExternalImage( "EndScreen/EndOfLevel_Background.png", scene_mc.EndImage.actor ) );
			//assets.push( new ExternalImage( "EndScreen/home_teaparty.png", scene_mc.EndImage.home ) );
			//assets.push( new ExternalImage( "EndScreen/replay_teaparty.png", scene_mc.EndImage.replay ) );
			scene_mc.EndImage.visible = false;
			
			scene_mc.blender_mc.blender_glass_mc.blender_juice.alpha = 0;
			scene_mc.blender_mc.blender_glass_mc.blender_juice.scaleX = 0;
			scene_mc.blender_mc.blender_glass_mc.blender_juice.scaleY = 0;
			
			JugSheet = spriteSheetLoader.Add("jug", 30, null, null, false, false, SequenceVector, null, scene_mc.juice_jug_mc.jug, null);
			JugSpoutSheet = spriteSheetLoader.Add("pour", 30, null, null, false, false, SequenceVector, null, scene_mc.juice_jug_mc.pour, null);
			
			_spin = spriteSheetLoader.Add("blender", 30, null, null, false, false, null, null, scene_mc.blender_mc.blender_spin, null);
			_spin.onComplete = BlenderAnimDone;
			_blue = spriteSheetLoader.Add("blue", 30, BlenderSound, null, false, false, null, null, scene_mc.blender_mc.blue_button, null);
			_green = spriteSheetLoader.Add("green", 30, BlenderSound, null, false, false, null, null, scene_mc.blender_mc.green_button, null);
			_yellow = spriteSheetLoader.Add("yellow", 30, BlenderSound, null, false, false, null, null, scene_mc.blender_mc.yellow_button, null);
			
			BlenderX = scene_mc.blender_mc.x;
			BlenderY = scene_mc.blender_mc.y;
			GlassX = scene_mc.blender_mc.blender_glass_mc.x;
			GlassY = scene_mc.blender_mc.blender_glass_mc.y;
			JuiceX = scene_mc.juice_jug_mc.x;
			JuiceY = scene_mc.juice_jug_mc.y;
			
			scene_mc.blender_mc.y += slidePxAmount;
			scene_mc.blender_mc.blender_glass_mc.pour.alpha = 0;
			scene_mc.juice_jug_mc.y += slidePxAmount;
			scene_mc.juice_jug_mc.pour.alpha = 0;
			
			scene_mc.tray_mc.y = 768;
			
			CreateSpriteSheetSequences(3);
			CupFillArray = new Array(scene_mc.cupFill1, scene_mc.cupFill2, scene_mc.cupFill3);
			for(var i:int = 0; i < CupFillArray.length; i++)
			{
				CupFillSheets.push(spriteSheetLoader.Add("juice_fill", 30, null, null, false, false, SequenceVector, null, CupFillArray[i].actor, null));
				CupFillArray[i].alpha = 0;
				CupFillArray[i].scaleX = 0;
				CupFillArray[i].scaleY = 0;
			}
			
			CreateSpriteSheetSequences(45);				//ADD ADDITIONAL FRAMES TO THE VECTOR; "TREATS" HAS MANY MORE FRAMES
			GuideTreatArray = new Array(scene_mc.placedTreat1, scene_mc.placedTreat2, scene_mc.placedTreat3, scene_mc.dragTreat);
			TrayTreatArray = new Array(scene_mc.tray_mc.treats_mc.treat1A, scene_mc.tray_mc.treats_mc.treat1B, scene_mc.tray_mc.treats_mc.treat1C,
									   scene_mc.tray_mc.treats_mc.treat2A, scene_mc.tray_mc.treats_mc.treat2B, scene_mc.tray_mc.treats_mc.treat2C,
									   scene_mc.tray_mc.treats_mc.treat3A, scene_mc.tray_mc.treats_mc.treat3B, scene_mc.tray_mc.treats_mc.treat3C);
			AssignGuideSpritesheets(4, TreatSheetArray, "treats", GuideTreatArray);
			AssignTraySpritesheets(TrayTreatSheetArray, "treats", TrayTreatArray);
			
			NapkinArray = new Array(scene_mc.napkin1, scene_mc.napkin2, scene_mc.napkin3);
			for(var i:int = 0; i < NapkinArray.length; i++)
			{
				assets.push( new ExternalImage( "SS_TEAPARTY_NAPKIN.png", NapkinArray[i] ) );
				NapkinX.push(NapkinArray[i].x);
				NapkinY.push(NapkinArray[i].y);
				TreatX.push(GuideTreatArray[i].x);
				TreatY.push(GuideTreatArray[i].y);
			}
			
			for(var i:int = 0; i < NapkinArray.length; i++)
			{
				assets.push( new ExternalImage( "SS_TEAPARTY_NAPKIN.png", NapkinArray[i] ) );
				NapkinX.push(NapkinArray[i].x);
				NapkinY.push(NapkinArray[i].y);
			}
			
			NapkinArray[0].x -= slidePxAmount;
			NapkinArray[1].y -= slidePxAmount;
			NapkinArray[2].x += slidePxAmount;
			
			AssignRandomSwatches();
			CreateSpriteSheetSequences(6);
			
			GuidePlateArray = new Array(scene_mc.placedPlate1, scene_mc.placedPlate2, scene_mc.placedPlate3, scene_mc.dragPlate);
			TrayPlateArray = new Array(scene_mc.tray_mc.plates_mc.plate1, scene_mc.tray_mc.plates_mc.plate2, scene_mc.tray_mc.plates_mc.plate3);
			AssignGuideSpritesheets(4, PlateSheetArray, "plates", GuidePlateArray);
			AssignTraySpritesheets(TrayPlateSheetArray, "plates", TrayPlateArray);
			
			GuideCupArray = new Array(scene_mc.placedCup1, scene_mc.placedCup2, scene_mc.placedCup3, scene_mc.dragCup);
			TrayCupArray = new Array(scene_mc.tray_mc.cups_mc.cup1, scene_mc.tray_mc.cups_mc.cup2, scene_mc.tray_mc.cups_mc.cup3);
			AssignGuideSpritesheets(4, CupSheetArray, "cups", GuideCupArray);
			AssignTraySpritesheets(TrayCupSheetArray, "cups", TrayCupArray);
			
			CreateSpriteSheetSequences(9);
			GuideFruitArray = new Array(scene_mc.blender_chunk1, scene_mc.blender_chunk2, scene_mc.blender_chunk3, scene_mc.dragFruit);
			TrayFruitArray = new Array(scene_mc.tray_mc.berry_bowl_mc.bowl1, scene_mc.tray_mc.berry_bowl_mc.bowl2, scene_mc.tray_mc.berry_bowl_mc.bowl3);
			AssignGuideSpritesheets(4, FruitSheetArray, "chunks", GuideFruitArray); 
			AssignTraySpritesheets(TrayFruitSheetArray, "fruit", TrayFruitArray);
			
			StartInstructionVO = new SoundEx( "intro", CONFIG::DATA + "languages/en/sounds/games/TeaParty/GAME_PRINCESSPARTY_02.mp3", App.voiceVolume );
			TreatInstructionVO = new SoundEx( "intro", CONFIG::DATA + "languages/en/sounds/games/TeaParty/GAME_PRINCESSPARTY_03.mp3", App.voiceVolume );
			BlendInstructionVO = new SoundEx( "intro", CONFIG::DATA + "languages/en/sounds/games/TeaParty/GAME_PRINCESSPARTY_04.mp3", App.voiceVolume );
			GreatVO = new SoundEx( "intro", CONFIG::DATA + "languages/en/sounds/games/GreatJob.mp3", App.voiceVolume );
			
			RandomSoundArray.push(new SoundEx( "0", CONFIG::DATA + "languages/en/sounds/games/Berry_Exciting.mp3", App.voiceVolume ) );
			RandomSoundArray.push(new SoundEx( "1", CONFIG::DATA + "languages/en/sounds/games/Delicious.mp3", App.voiceVolume ) );
			RandomSoundArray.push(new SoundEx( "2", CONFIG::DATA + "languages/en/sounds/games/Yum.mp3", App.voiceVolume ) );
			
			sndConfetti = new SoundEx( "sndConfetti", CONFIG::DATA + "sounds/Animation_Sounds/P14_confetti.mp3", App.soundVolume );
			SipSound = new SoundEx( "sip", CONFIG::DATA + "sounds/Animation_Sounds/sip/sip_1.mp3", App.soundVolume );
			
			SaveGuidesPlacement();
			
			
			
			Finale = new clsGameFinale(cMain, "", gc.pages_GameFinale_Text, Finale_Complete);
			Finale.x = Background.x + (Background.width/2) + gc.pages_GameFinale_Text.offset.x;
			Finale.y = Background.y + (Background.height/2) + gc.pages_GameFinale_Text.offset.y;
			Finale.Reset();
			Finale.mouseEnabled = false;
			Finale.mouseChildren = false;
			this.addChild(Finale);
			
			EndOfLevel = null;
			
			var endldr:MP3Loader = new MP3Loader( CONFIG::DATA + "languages/en/sounds/games/GreatJob.mp3", { autoPlay: false, onComplete: endLoaded } );
			endldr.load();
			
			Crowd = new CheeringCrowd( this );
		}
		
		public final override function Init():void
		{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic2";
			super.Init();
		}		
		
		public final override function Reset():void
		{			
			super.Reset();
			cMain.removeStageMouseDown( OnDown );
			cMain.removeStageMouseUp( OnUp );
			
			Particles1.Clear_Particles_Que();         //Clear Call.	
			Particles1.Clear_All_Particles(this);	 //Clear Call.	
			Particles2.Clear_Particles_Que();         //Clear Call.	
			Particles2.Clear_All_Particles(this);	 //Clear Call.	
			Particles3.Clear_Particles_Que();         //Clear Call.	
			Particles3.Clear_All_Particles(this);	 //Clear Call.
			Particles4.Clear_Particles_Que();         //Clear Call.	
			Particles4.Clear_All_Particles(this);	 //Clear Call.	
			Particles5.Clear_Particles_Que();         //Clear Call.	
			Particles5.Clear_All_Particles(this);	 //Clear Call.	
			Particles6.Clear_Particles_Que();         //Clear Call.	
			Particles6.Clear_All_Particles(this);	 //Clear Call.	
			
			bFirstPass = true;
			VariableDefect();
			
			Finale.Reset();
			if(EndOfLevel != null){EndOfLevel.destroy();}
			if(Crowd != null){Crowd.Reset();}
		}
		
		public final override function Start():void
		{			
			super.Start();	
			cMain.addStageMouseDown( OnDown );
			cMain.addStageMouseUp( OnUp );
			TweenMax.delayedCall(3, TrayUp);
			
			if(bFirstPass){FirstInstruction();}
			
			Finale.Reset();
		}	
		
		public override function Stop():void
		{
			super.Stop();
			StartInstructionVO.Stop();
			TreatInstructionVO.Stop();
			BlendInstructionVO.Stop();
			GreatVO.Stop();
			sndConfetti.Stop();
			SipSound.Stop();
			
			if(sndEnd != null){sndEnd.Stop();}
			if(Finale != null){Finale.Stop();Finale.Reset();}
		}
		
		public override function Destroy():void
		{
			super.Destroy();
			cMain.removeStageMouseDown( OnDown );
			cMain.removeStageMouseUp( OnUp );
			
			TweenMax.killAll(true, true, true);
			StartInstructionVO.Destroy();
			TreatInstructionVO.Destroy();
			BlendInstructionVO.Destroy();
			GreatVO.Destroy();
			sndConfetti.Destroy();
			SipSound.Destroy();
			
			Particles1.Clear_Particles_Que();        //Clear Call.
			Particles1.Clear_All_Particles(this);	//Clear Call.	
			Particles2.Clear_Particles_Que();        //Clear Call.
			Particles2.Clear_All_Particles(this);	//Clear Call.	
			Particles3.Clear_Particles_Que();        //Clear Call.
			Particles3.Clear_All_Particles(this);	//Clear Call.
			Particles4.Clear_Particles_Que();        //Clear Call.
			Particles4.Clear_All_Particles(this);	//Clear Call.	
			Particles5.Clear_Particles_Que();        //Clear Call.
			Particles5.Clear_All_Particles(this);	//Clear Call.	
			Particles6.Clear_Particles_Que();        //Clear Call.
			Particles6.Clear_All_Particles(this);	//Clear Call.
			
			if(winSound != null){winSound.Destroy();}
			if(sndEnd != null){sndEnd.Destroy();}
			if(Finale != null){Finale.destroy();Finale = null;}
			if(EndOfLevel != null){EndOfLevel.destroy();EndOfLevel = null;}
			if(Crowd != null){Crowd.Destroy();}
			Background = null;
		}
		
		private function OnDown( e:MouseEvent ):void
		{
			switch(e.target.name)
			{
				case "home_BTN":
				{
					cMain.PrevPage(-1);
					break;
				}
				case "replay_BTN":
				{
					trace("RESTART NOW");
				scene_mc.EndImage.visible = false;
				//TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime, ReplaceGuides);
				//Reset();
				ResetCloth();
				ReplaceGuides();
					break;
				}
			}
			
			if(!bPauseInteractions && !bDragDelay)
			{
				if(iCurrentState == 5 && _draggable == null)
				{
					for(var i:int = 0; i < GuideTreatArray.length; i++)
					{
						if(GuideTreatArray[i].hitTestPoint(mouseX, mouseY, true))
						{
							if(TreatSheetArray[i].currentFrame % 5 < 4 && GuideTreatArray[i].alpha == 1)
							{
								//PlayRandom();
								TreatSheetArray[i].playSequence((TreatSheetArray[i].currentFrame + 1).toString());
								
								
								switch(i)
								{
									case 0:Plate1Sound.Play(TreatSheetArray[i].currentFrame % 5 - 1);break;
									case 1:Plate2Sound.Play(TreatSheetArray[i].currentFrame % 5 - 1);break;
									case 2:Plate3Sound.Play(TreatSheetArray[i].currentFrame % 5 - 1);break;
								}
							}
							
							else
							{TweenMax.to(GuideTreatArray[i], nObjectFadeTime, {alpha:0});}
							
							CompletionCheck();
						}
					}
				}
				TrayCheck();
			}
			
			bMouseDown = true;
			
			if(scene_mc.juice_jug_mc.hitTestPoint(mouseX, mouseY, true)){bDragJuice = true;}
			
		//BLENDER COLLISIONS
			if(!bBlenderAnim && !bDragBlender && placedChunks.length > 0)
			{
				//BLENDER RELATED
				if(scene_mc.blender_mc.blue_button.hitTestPoint(mouseX, mouseY, true))
				{
					_blue.doTap();
					Blend();
					bBlenderAnim = true;
				}
				
				else if(scene_mc.blender_mc.green_button.hitTestPoint(mouseX, mouseY, true))
				{
					_green.doTap();
					Blend();
					bBlenderAnim = true;
				}
				
				else if(scene_mc.blender_mc.yellow_button.hitTestPoint(mouseX, mouseY, true))
				{
					_yellow.doTap();
					Blend();
					bBlenderAnim = true;
				}
				
				else if(scene_mc.blender_mc.hitTestPoint(mouseX, mouseY, true) && iCurrentState >= 3 && bBlenderReady)
				{bDragBlender = true;}
			}
		}
		
		private function OnUp( e:MouseEvent ):void
		{
			bMouseDown = false;
			
			if(!bPauseInteractions && _draggable != null)
			{DidItCollide();}
			
			if(!bDragDelay){ReturnDraggable();}
			
			if(bDragJuice && !bPauseInteractions)
			{JugReturn();}
			
			else if(bDragBlender && !bPauseInteractions)
			{BlenderReturn();}
		}
		
		private function TrayCheck():void
		{
			switch(iCurrentState)
			{
				case 0:AssignBackground();break;
				case 1:ShowGuides(WhichPlateWasTouched(TrayCupArray), CupSheetArray);break;
				case 2:ShowGuides(WhichPlateWasTouched(TrayFruitArray), FruitSheetArray);break;
				case 3:break;
				case 4:ShowGuides(WhichPlateWasTouched(TrayPlateArray), PlateSheetArray);break;
				case 5:ShowGuides(Math.floor(WhichPlateWasTouched(TrayTreatArray)/3), TreatSheetArray, 5);break;
				default:trace("!!! - INVALID CHECK STATE - !!!");break;
			}
		}
		
		private function NextRound():void
		{
			switch(iCurrentState)
			{
				case 1:
				{
					TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime + nMiscDelayTime, AssignRandomCups);
					scene_mc.tray_mc.plates_mc.alpha = 0;
				}break;
				
				case 2:
				{
					_currentGuide = GuideFruitArray;
					TweenMax.delayedCall(nObjectFadeTime, TrayDown);
					RemoveTreatGuides();
					scene_mc.tray_mc.cups_mc.alpha = 0;
					TweenMax.delayedCall(nObjectFadeTime, SetUpForSmoothies);
					TweenMax.delayedCall(nTrayTweenTime + (nObjectFadeTime*2) + nMiscDelayTime + 1, AssignRandomFruit);
					if(bFirstPass){BlendInstructionVO.Start();}
				}break;
				
				case 3:
				{}break;
				
				case 4:
				{
					//RemoveBlender();
					//RemoveCups();
					TweenMax.to(scene_mc.tray_mc.berry_bowl_mc, nObjectFadeTime, {alpha:0, onComplete:TrayDown});
					TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime, AssignRandomPlates);
				}break;
				
				case 5:
				{
					//ReplaceCups();
					//for(var i:int = 0; i < CupFillArray.length; i++)
					//{CupFillArray[i].transform.colorTransform = new ColorTransform();}
					//TweenMax.to(scene_mc.tray_mc.plates_mc, nObjectFadeTime, {alpha:0, onComplete:TrayDown});
					//TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime, AssignRandomCups);
					//TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime, TrayUp);
					_currentGuide = GuideTreatArray;
					TweenMax.to(scene_mc.tray_mc.cups_mc, nObjectFadeTime, {alpha:0, onComplete:TrayDown});
					//TweenMax.delayedCall(nTrayTweenTime, PlaceJuice);
					TweenMax.delayedCall(nTrayTweenTime, PlaceNapkin);
					TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime + nMiscDelayTime, AssignRandomTreats);
					if(bFirstPass){TreatInstructionVO.Start();}
					EnableInteraction();
				}break;
				
				case 6:
				{
					ClearTheTable();
				}break;
				
				
				default:trace("!!! - INVALID NEXTROUND STATE - !!!");break;
			}
		}
		
		protected final override function handleFrame( e:Event ):void
		{
			Particles1.SB_Update(this);
			Particles2.SB_Update(this);
			Particles3.SB_Update(this);
			Particles4.SB_Update(this);
			Particles5.SB_Update(this);
			Particles6.SB_Update(this);
				
			if(confettiCounter < 2000)
			{
				confettiCounter+= 20;
			}
				
		   if(isRunning && !isPaused && !bPauseInteractions)
		   {
			   if(bDragJuice)
			   {
				   DragItem(scene_mc.juice_jug_mc);
				   PourCheck();
			   }
			   
			   else if(bDragBlender)
			   {
				   DragItem(scene_mc.blender_mc.blender_glass_mc);
				   PourCheck();
			   }
			   
			   else if(_draggable != null && !bDragDelay)
			   {DragItem(_draggable);}
			   
			   else if(bMouseDown){HitJuice();}
		   }
		   super.handleFrame( e );
		}
		
		private function SetUpForSmoothies():void
		{
			TweenMax.to(scene_mc.juice_jug_mc, nTrayTweenTime, {y:scene_mc.juice_jug_mc.y + slidePxAmount});
			TweenMax.to(scene_mc.blender_mc, nTrayTweenTime, {y:BlenderY});
		}
		
		private function GetCupsForSmoothies():void
		{
			for(var i:int = 0; i < 3; i++)
			{
				CupFillArray[i].alpha = 0;
				CupFillArray[i].scaleX = 0;
				CupFillArray[i].scaleY = 0;
				CupSheetArray[i].playSequence((2).toString());
			}
			TweenMax.to(scene_mc.blender_mc, nTrayTweenTime, {y:BlenderY});
			
			TweenMax.to(CupFillArray[0], nTrayTweenTime, {x:CupFillArray[0].x + slidePxAmount});
			TweenMax.to(CupFillArray[1], nTrayTweenTime, {y:CupFillArray[1].y + slidePxAmount});
			TweenMax.to(CupFillArray[2], nTrayTweenTime, {x:CupFillArray[2].x - slidePxAmount});
			
			TweenMax.to(GuideCupArray[0], nTrayTweenTime, {x:GuideCupArray[0].x + slidePxAmount});
			TweenMax.to(GuideCupArray[1], nTrayTweenTime, {y:GuideCupArray[1].y + slidePxAmount});
			TweenMax.to(GuideCupArray[2], nTrayTweenTime, {x:GuideCupArray[2].x - slidePxAmount});
		}
		
		private function ClearTheTable():void
		{
			RemoveCups();
			RemoveNapkins();
			RemoveBlender();			
			//ResetCloth();
			RemovePlates();
			
			TrayDown();
			
			TweenMax.to(scene_mc.juice_jug_mc, nTrayTweenTime, {y:scene_mc.juice_jug_mc.y + slidePxAmount});
			
			TweenMax.delayedCall(nTrayTweenTime, Great);
			
			//UNCOMMENT TO LOOP CONTINUOUSLY
			//TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime, ReplaceGuides);
			
			//UNCOMMENT TO END AFTER TREATS
			//gameOver();
		}
		
		private function RemoveBlender():void
		{
			TweenMax.killTweensOf(scene_mc.blender_mc);
			TweenMax.to(scene_mc.blender_mc, nTrayTweenTime, {y:scene_mc.blender_mc.y + slidePxAmount});
		}
		
		private function RemoveTreatGuides():void
		{
			for(var i:int = 0; i < 3; i++)
			{TweenMax.to(GuideTreatArray[0], nObjectFadeTime, {alpha:0});}
		}
		
		private function RemoveCups():void
		{
			TweenMax.to(CupFillArray[0], nTrayTweenTime, {x:CupFillArray[0].x - slidePxAmount});
			TweenMax.to(CupFillArray[1], nTrayTweenTime, {y:CupFillArray[1].y - slidePxAmount});
			TweenMax.to(CupFillArray[2], nTrayTweenTime, {x:CupFillArray[2].x + slidePxAmount});
			
			TweenMax.to(GuideCupArray[0], nTrayTweenTime, {x:GuideCupArray[0].x - slidePxAmount});
			TweenMax.to(GuideCupArray[1], nTrayTweenTime, {y:GuideCupArray[1].y - slidePxAmount});
			TweenMax.to(GuideCupArray[2], nTrayTweenTime, {x:GuideCupArray[2].x + slidePxAmount});
		}
		
		private function RemoveNapkins():void
		{
			TweenMax.to(NapkinArray[0], nTrayTweenTime, {x:NapkinArray[0].x - slidePxAmount});
			TweenMax.to(NapkinArray[1], nTrayTweenTime, {y:NapkinArray[1].y - slidePxAmount});
			TweenMax.to(NapkinArray[2], nTrayTweenTime, {x:NapkinArray[2].x + slidePxAmount});
		}
		
		private function RemovePlates():void
		{
			TweenMax.to(GuidePlateArray[0], nTrayTweenTime, {x:GuidePlateArray[0].x - slidePxAmount});
			TweenMax.to(GuidePlateArray[1], nTrayTweenTime, {y:GuidePlateArray[1].y - slidePxAmount});
			TweenMax.to(GuidePlateArray[2], nTrayTweenTime, {x:GuidePlateArray[2].x + slidePxAmount});
			
			TweenMax.to(GuideTreatArray[0], nTrayTweenTime, {x:GuideTreatArray[0].x - slidePxAmount});
			TweenMax.to(GuideTreatArray[1], nTrayTweenTime, {y:GuideTreatArray[1].y - slidePxAmount});
			TweenMax.to(GuideTreatArray[2], nTrayTweenTime, {x:GuideTreatArray[2].x + slidePxAmount});
		}
		
		private function ResetCloth():void
		{
			RemoveGraphic(scene_mc.BG);
			TweenMax.to(scene_mc.BG, nObjectFadeTime, {alpha:0});
			RemoveGraphic(scene_mc.tray_mc.swatches_mc.swatch1);
			RemoveGraphic(scene_mc.tray_mc.swatches_mc.swatch2);
			RemoveGraphic(scene_mc.tray_mc.swatches_mc.swatch3);
			AssignRandomSwatches();
		}
		
		private function BeginAgain():void
		{
			scene_mc.blender_mc.blender_glass_mc.blender_juice.alpha = 0;
			scene_mc.blender_mc.blender_glass_mc.blender_juice.scaleX = 0;
			scene_mc.blender_mc.blender_glass_mc.blender_juice.scaleY = 0;
			scene_mc.blender_mc.blender_glass_mc.pour.alpha = 0;
			scene_mc.juice_jug_mc.pour.alpha = 0;
			
			for(var i:int = 0; i < CupFillArray.length; i++)
			{CupFillArray[i].transform.colorTransform = new ColorTransform();}
			
			placedChunks.splice(0);
			
			for(var i:int = 0; i < TrayTreatArray.length; i++)
			{TrayTreatArray[i].visible = true;}
			
			VariableDefect();
			TweenMax.delayedCall(6, TrayUp);
		}
		
		private function ReplaceCups():void
		{
			for(var i:int = 0; i < 3; i++)
			{
				CupFillArray[i].x = FillX[i];
				CupFillArray[i].y = FillY[i];
				CupFillArray[i].scaleX = 0;
				CupFillArray[i].scaleY = 0;
				CupFillArray[i].alpha = 0;
				
				GuideCupArray[i].x = CupX[i];
				GuideCupArray[i].y = CupY[i];
				GuideCupArray[i].alpha = 0;
			}
		}
		
		private function ReplaceGuides():void
		{
			for(var i:int = 0; i < 3; i++)
			{
				GuidePlateArray[i].x = PlateX[i];
				GuidePlateArray[i].y = PlateY[i];
				GuidePlateArray[i].alpha = 0;
			}
			ReplaceCups();
			BeginAgain();
		}
		
		private function SaveGuidesPlacement():void
		{
			for(var i:int = 0; i < 3; i++)
			{
				PlateX.push(GuidePlateArray[i].x);
				PlateY.push(GuidePlateArray[i].y);
				
				CupX.push(GuideCupArray[i].x);
				CupY.push(GuideCupArray[i].y);
				
				FillX.push(CupFillArray[i].x);
				FillY.push(CupFillArray[i].y);
			}
		}
		
		private function VariableDefect():void
		{
			confettiCounter = 2000;
			
			bMouseDown = false;
			bPauseInteractions = true;
			bDragDelay = false;
			bDragJuice = false;
			bDragBlender = false;
			bBlenderReady = false;
			bBlenderAnim = false;
			scene_mc.tray_mc.y = 768;
			scene_mc.tray_mc.swatches_mc.alpha = 1;
			scene_mc.tray_mc.plates_mc.alpha = 0;
			scene_mc.tray_mc.cups_mc.alpha = 0;
			scene_mc.tray_mc.treats_mc.alpha = 0;
			scene_mc.tray_mc.berry_bowl_mc.alpha = 0;
			scene_mc.juice_jug_mc.pour.alpha = 0;
			iCurrentState = 0;
			
			for(var i:int = 0; i < 3; i++)
			{
				GuideTreatArray[i].alpha = 0;
				GuideTreatArray[i].x = TreatX[i];
				GuideTreatArray[i].y = TreatY[i];
			}
				
			for(var i:int = 0; i < lastFill.length; i++)
			{lastFill[i] = false;}
			
			MixRed = 0;
			MixGreen = 0;
			MixBlue = 0;
		}
		
		private function AssignGuideSpritesheets(num:int, SheetArray, anim:String, GuideArray ):void
		{
			for(var i:int = 0; i < num; i++)
			{
				SheetArray.push(spriteSheetLoader.Add(anim, 30, null, null, false, false, SequenceVector, null, GuideArray[i], null));
				if(i < num - 1){GuideArray[i].alpha = 0;}
			}
		}
		
		private function AssignTraySpritesheets(TrayArray, anim:String, placeholders):void
		{
			for(var i:int = 0; i < placeholders.length; i++)
			{TrayArray.push(spriteSheetLoader.Add(anim, 30, null, null, false, false, SequenceVector, null, placeholders[i], null));}
		}
		
		private function AssignRandomTreats():void
		{
			FillSwatchOptionsUsedArray(9);
			for(var i:int = 0; i < 3; i++)
			{
				for(var j:int = 0; j < 3; j++)
				{TrayTreatSheetArray[(i*3) + j].playSequence((SwatchOptionsUsedArray[i] * 5).toString());}
			}
			
			scene_mc.tray_mc.treats_mc.alpha = 1;
			scene_mc.tray_mc.plates_mc.alpha = 1;
			TweenMax.delayedCall(nMiscDelayTime, TrayUp);
		}
		
		private function AssignRandomFruit():void
		{
			FillSwatchOptionsUsedArray(9);
			
			for(var i:int = 0; i < TrayFruitSheetArray.length; i++)
			{TrayFruitSheetArray[i].playSequence(SwatchOptionsUsedArray[i].toString());}
			
			/*TrayFruitSheetArray[0].playSequence("0");
			TrayFruitSheetArray[1].playSequence("1");
			TrayFruitSheetArray[2].playSequence("2");*/
			
			scene_mc.tray_mc.berry_bowl_mc.alpha = 1;
			scene_mc.tray_mc.plates_mc.alpha = 0;
			TrayUp();
		}
		
		private function AssignRandomCups():void
		{
			_currentGuide = GuideCupArray;
			FillSwatchOptionsUsedArray(6);
			for(var i:int = 0; i < TrayCupSheetArray.length; i++)
			{TrayCupSheetArray[i].playSequence(SwatchOptionsUsedArray[i].toString());}
			scene_mc.tray_mc.cups_mc.alpha = 1;
			TweenMax.delayedCall(nMiscDelayTime, TrayUp);
		}
		
		private function AssignRandomPlates():void
		{
			_currentGuide = GuidePlateArray;
			FillSwatchOptionsUsedArray(6);
			for(var i:int = 0; i < TrayPlateSheetArray.length; i++)
			{TrayPlateSheetArray[i].playSequence(SwatchOptionsUsedArray[i].toString());}
			scene_mc.tray_mc.plates_mc.alpha = 1;
			TweenMax.delayedCall(nMiscDelayTime, TrayUp);
		}
		
		private function AssignImage(_image:String, _place):void
		{
			var imageLoader:ImageLoader = new ImageLoader(_image, { autoDispose: true, container: _place });
			imageLoader.load();
			TweenMax.to(_place, nObjectFadeTime, {alpha:1});
		}
		
		private function AssignBackground():void
		{			
			var tempElement:int;
			if(scene_mc.tray_mc.swatches_mc.swatch1.hitTestPoint(mouseX, mouseY, true)){ClothSound.Play(0);tempElement = 0;}
			else if(scene_mc.tray_mc.swatches_mc.swatch2.hitTestPoint(mouseX, mouseY, true)){ClothSound.Play(0);tempElement = 1;}
			else if(scene_mc.tray_mc.swatches_mc.swatch3.hitTestPoint(mouseX, mouseY, true)){ClothSound.Play(0);tempElement = 2;}
			else{tempElement = -1;}
			
			if(tempElement > -1)
			{
				bPauseInteractions = true;
				AssignImage(imageFolder + "Background/SS_TEAPARTY_BG0" + (SwatchOptionsUsedArray[tempElement]+1) + ".png", scene_mc.BG);
				TweenMax.to(scene_mc.tray_mc.swatches_mc, nObjectFadeTime, {alpha:0, onComplete:TrayDown});
				iCurrentState ++;
				NextRound();
			}
		}
		
		private function AssignRandomSwatches():void
		{
			FillSwatchOptionsUsedArray(6);
			AssignImage(imageFolder + "Swatches/" + SwatchPngArray[SwatchOptionsUsedArray[0]], scene_mc.tray_mc.swatches_mc.swatch1);
			AssignImage(imageFolder + "Swatches/" + SwatchPngArray[SwatchOptionsUsedArray[1]], scene_mc.tray_mc.swatches_mc.swatch2);
			AssignImage(imageFolder + "Swatches/" + SwatchPngArray[SwatchOptionsUsedArray[2]], scene_mc.tray_mc.swatches_mc.swatch3);
		}
		
		private function FillSwatchOptionsUsedArray(iMaxOptions:int):void
		{
			//CREATE TEMPORARY ARRAY OF INTEGER TO REPRESENT THE POSSIBLE SWATCHES
			var tempArray:Array = new Array();
			var tempInt:int;
			
			for(var i:int = 0; i < iMaxOptions; i++)
			{tempArray.push(i);}
			
			//RANDOMLY SELECT 3 INTEGERS FROM THE TEMPORARY ARRAY, AND SAVE THEM TO THE CLASS LEVEL ARRAY
			for(var j:int = 0; j < iTotalSwatches; j++)
			{
				tempInt = Math.floor(Math.random() * tempArray.length);
				SwatchOptionsUsedArray[j] = tempArray[tempInt];
				tempArray.splice(tempInt, 1)
			}
		}
		
		private function TrayUp():void
		{TweenMax.to(scene_mc.tray_mc, nTrayTweenTime, {y:768 - scene_mc.tray_mc.tray.height, onComplete:EnableInteraction});}
		
		private function TrayDown():void
		{TweenMax.to(scene_mc.tray_mc, nTrayTweenTime, {y:768});}
		
		private function CreateSpriteSheetSequences(totalSequences:int):void
		{
			for(var i:int = SequenceVector.length; i < totalSequences; i++)
			{SequenceVector.push(new SpriteSheetSequence(i.toString(), SpriteSheetSequence.createRange( i, i, false)));}
		}
		
		private function ShowGuides(_grabbed, _sheetArray, framesPerAnim:int = 1):void
		{
			if(_grabbed >= 0 && _currentGuide != null)
			{
				AssignDraggable(_currentGuide[3]);
				
				//NOTE: THE 4TH GUIDE PLATE IS THE ONE THAT YOU DRAG; ITS OPACITY SHOULD ALWAYS BE 1;
				for(var i:int = 0; i < _sheetArray.length -1; i++)
				{
					if(_currentGuide[i].alpha != 1)
					{
						_sheetArray[i].playSequence((SwatchOptionsUsedArray[_grabbed] * framesPerAnim).toString());
						if(iCurrentState != 2){TweenMax.to(_currentGuide[i], nObjectFadeTime, {alpha:nGuideOpacity});}
					}
				}
				_sheetArray[_sheetArray.length-1].playSequence((SwatchOptionsUsedArray[_grabbed] * framesPerAnim).toString())
			}
		}
		
		private function HideGuides():void
		{
			if(_currentGuide != null)
			{
				for(var j:int = 0; j < _currentGuide.length - 1; j++)
				{
					if(_currentGuide[j].alpha != 1)
					{TweenMax.to(_currentGuide[j], nObjectFadeTime, {alpha:0});}
				}
			}
		}
		
		private function AssignPlateSpritesheets(numberOfPlates:int):void
		{
			for(var i:int = 0; i < numberOfPlates; i++)
			{
				PlateSheetArray.push(spriteSheetLoader.Add("plates", 30, null, null, false, false, SequenceVector, null, GuidePlateArray[i], null));
				if(i < numberOfPlates - 1){GuidePlateArray[i].alpha = 0;}
			}
		}
		
		private function WhichPlateWasTouched(TrayArray):int
		{
			for(var i:int = 0; i < TrayArray.length; i++)
			{
				if(TrayArray[i].hitTestPoint(mouseX, mouseY, true) && TrayArray[i].visible)
				{
					if(iCurrentState == 5){RemoveTreat(i);}
					ReplaceX = scene_mc.tray_mc.x + TrayArray[i].parent.x + TrayArray[i].x + (TrayArray[i].width / 2);
					ReplaceY = scene_mc.tray_mc.y + TrayArray[i].parent.y + TrayArray[i].y + (TrayArray[i].height / 2);
					return i;
				}
			}
			return -1;
		}
		
		private function EnableInteraction():void
		{bPauseInteractions = false;}
		
		private function AssignDraggable(dragObject:Placeholder):void
		{_draggable = dragObject;}
		
		private function DragItem(_dragItem):void
		{
			if(bDragBlender)
			{
				var speedX:int = (mouseX - (_dragItem.x + _dragItem.parent.x + _dragItem.width / 2)) / 5;
				var speedY:int = (mouseY - (_dragItem.y + _dragItem.parent.y + _dragItem.height / 2)) / 5;
				_dragItem.x += speedX;
				_dragItem.y += speedY;
			}
			
			else if(bDragJuice)
			{
				var speedX:int = (mouseX - (_dragItem.x + _dragItem.width / 2)) / 5;
				var speedY:int = (mouseY - (_dragItem.y + _dragItem.height / 2)) / 5;
				_dragItem.x += speedX;
				_dragItem.y += speedY;
			}
			
			else
			{
				_dragItem.x = mouseX - (_dragItem.width / 2);
				_dragItem.y = mouseY - (_dragItem.height / 2);
			}
		}
		
		private function ReturnDraggable():void
		{
			if(_draggable != null && !bPauseInteractions)
			{
				HideGuides();
				bDragDelay = true;
				
				if(_draggable.hitTestObject(scene_mc.tray_mc)){var RetTime:Number = 0.2;}
				else{var RetTime:Number = 0.4;}
				
				TweenMax.to(_draggable, RetTime, {x:ReplaceX - (_draggable.width / 2), y:ReplaceY - (_draggable.height / 2), onComplete:HideDraggable});
				
				if(iCurrentState==5){TweenMax.delayedCall(RetTime, ReturnTreat);}
			}
		}
		
		private function HideDraggable():void
		{
			_draggable.x = 1300;
			_draggable = null;
			bDragDelay = false;
			bPauseInteractions = false;
		}
		
		private function DidItCollide():void
		{
			for(var i:int = 0; i < 3; i++)
			{
				if(_draggable.hitTestObject(_currentGuide[i]) && (iCurrentState == 5 && (TreatSheetArray[i].currentFrame % 5 == 4)))
				{
					TreatSheetArray[i].playSequence((TreatSheetArray[3].currentFrame).toString());
					_currentGuide[i].alpha = 0;
					PlaceDraggable(i);return;
				}
				
				else if(_draggable.hitTestObject(_currentGuide[i]) && _currentGuide[i].alpha != 1)
				{
					switch(iCurrentState)
					{
						case 4:PlacePlateSound.Play(0);break;
						case 1:CupSound.Play(0);break;
						case 2:AddColourValues();break;
					}
					PlaceDraggable(i);return;
				}
			}
		}
		
		private function CompletionCheck():void
		{
			switch(iCurrentState)
			{
				case 2:
				{if(placedChunks.length < 3){return;}}break;
				
				case 3:
				{
					for(var k:int = 0; k < lastFill.length; k++)
					{
						if(!lastFill[k])
						{return;}
					}
				}break;
				
				case 5:
				{
					for(var j:int = 0; j < TrayTreatArray.length; j++)
					{if(TrayTreatArray[j].visible){return;}}
					
					for(var i:int = 0; i < 3; i++)
					{if(_currentGuide[i].alpha == 1 && TreatSheetArray[i].currentFrame % 5 < 4){return;}}
				}break;
				
				default:
				{
					for(var i:int = 0; i < 3; i++)
					{if(_currentGuide[i].alpha != 1){return;}}
				}
			}
			 
			bPauseInteractions = true;
			iCurrentState++;
			NextRound();
		}
		
		private function PlaceJuice():void
		{TweenMax.to(scene_mc.juice_jug_mc, nTrayTweenTime, {y:JuiceY});}
		
		private function PlaceNapkin():void
		{
			TweenMax.to(NapkinArray[0], nTrayTweenTime, {x:NapkinX[0]});
			TweenMax.to(NapkinArray[1], nTrayTweenTime, {y:NapkinY[1]});
			TweenMax.to(NapkinArray[2], nTrayTweenTime, {x:NapkinX[2]});
		}
		
		private function PourCheck():void
		{
			if(bDragJuice)
			{
				var tempDrag = scene_mc.juice_jug_mc;
				
				for(var i:int = 0; i < CupFillArray.length; i++)
				{
					if(tempDrag.pour.hitTestObject(GuideCupArray[i]) && CupFillArray[i].scaleY < 0.9)
					{
						if(bDragBlender)
						{lastFill[i] = true;}
						
						bPauseInteractions = true;
						var tempX:int = GuideCupArray[i].x + 40;
						var tempY:int = GuideCupArray[i].y + 50;
						TweenMax.to(tempDrag, nObjectFadeTime, {x:tempDrag.parent.x + tempX, y:tempDrag.parent.y + tempY, onComplete:PourOn, onCompleteParams:[i, tempDrag]});
					}
				}
			}
			
			if (bDragBlender)
			{
				var tempDrag = scene_mc.blender_mc.blender_glass_mc;
			
				for(var i:int = 0; i < CupFillArray.length; i++)
				{
					if(scene_mc.blender_mc.blender_glass_mc.hitTestObject(GuideCupArray[i]) && CupFillArray[i].scaleY < 0.9)
					{
						if(bDragBlender)
						{lastFill[i] = true;}
						
						bPauseInteractions = true;
						var tempX:int = GuideCupArray[i].x + 65 - tempDrag.parent.x;
						var tempY:int = GuideCupArray[i].y + 60 - tempDrag.parent.y;
						TweenMax.to(tempDrag, nObjectFadeTime, {x:tempX, y:tempY, onComplete:PourOn, onCompleteParams:[i, tempDrag]});
					}
				}
			}
		}
		
		private function PourOn(_element:int, tempDrag):void
		{
			if(bDragBlender){BlendPourSound.Play(0);}
			else{JuicePourSound.Play(0);}
			
			TweenMax.to(tempDrag.pour, (nObjectFadeTime / 2) + 0.1, {alpha:1, onComplete:PourOff, onCompleteParams:[tempDrag]});			
			TweenMax.to(CupFillArray[_element], nObjectFadeTime, {alpha:1, scaleX:0.95, scaleY:0.9});
		}
		
		private function PourOff(tempDrag):void
		{TweenMax.to(tempDrag.pour, nObjectFadeTime / 2 + 0.1, {alpha:0, onComplete:PourOffComplete, onCompleteParams:[tempDrag]});}
		
		private function PourOffComplete(tempDrag):void
		{
			if(bMouseDown)
			{
				DragItem(tempDrag);
				EnableInteraction();
			}
			
			else if(bDragJuice){TweenMax.delayedCall(nObjectFadeTime, JugReturn);}
			
			else if(bDragBlender){TweenMax.delayedCall(nObjectFadeTime, BlenderReturn);}
		}
		
		private function BlenderReturn():void
		{
			bDragBlender = false;
			TweenMax.to(scene_mc.blender_mc.blender_glass_mc, 0.5, {x:GlassX, y:GlassY, onComplete:EnableInteraction});
			if(iCurrentState == 3){CompletionCheck();}
		}
		
		private function JugReturn():void
		{
			bDragJuice = false;
			TweenMax.to(scene_mc.juice_jug_mc, 0.5, {x:JuiceX, y:JuiceY, onComplete:EnableInteraction});
		}
		
		private function ChangeJuiceColour():void
		{
			var tempInt:int = Math.floor(Math.random() * 3);
			for(var i :int = 0; i < 3; i++)
			{CupFillSheets[i].playSequence(tempInt.toString());}
			JugSheet.playSequence(tempInt.toString());
			JugSpoutSheet.playSequence(tempInt.toString());
		}
		
		private function ReturnTreat():void
		{treatRemoved.visible = true;}
		
		private function RemoveTreat(i:int):void
		{
			treatRemoved = TrayTreatArray[i];
			treatRemoved.visible = false;
		}
		
		private function HitJuice():void
		{
			if(!bDragJuice && !bDragBlender && iCurrentState > 3)
			{
				for(var i:int = 0; i < CupFillArray.length; i++)
				{
					if(CupFillArray[i].hitTestPoint(mouseX, mouseY, true))
					{
						if(!SipSound.isRunning && CupFillArray[i].alpha > 0)
						{SipSound.Start();}
				
						CupFillArray[i].alpha -= (0.25 / 2.5);
						CupFillArray[i].scaleX -= (0.04 / 3);
						CupFillArray[i].scaleY -= (0.09 / 3);
					}
				}
				
				if(iCurrentState == 3){CompletionCheck();}
			}
		}
		
		private function PlaceDraggable(plateNum:int):void
		{
			TweenMax.killTweensOf(_currentGuide[plateNum]);
			bPauseInteractions = true;
			var tempX:int = _currentGuide[plateNum].x;
			var tempY:int = _currentGuide[plateNum].y;
			TweenMax.to(_draggable, 0.2, {x:tempX, y:tempY, onComplete:FinishPlaceDraggable, onCompleteParams:[_currentGuide[plateNum]]});
		}
		
		private function FinishPlaceDraggable(placed_mc):void
		{
			placed_mc.alpha = 1;
			CompletionCheck();
			HideDraggable();
			HideGuides();
		}
		
		private function AddColourValues():void
		{
			FruitSound.Play(0);
			if(placedChunks.length < 3)
			{
				placedChunks.push(FruitSheetArray[3].currentFrame)
				MixRed += DefaultReds[FruitSheetArray[3].currentFrame];
				MixGreen += DefaultGreens[FruitSheetArray[3].currentFrame];
				MixBlue += DefaultBlues[FruitSheetArray[3].currentFrame];
			}
		}
		
		private function Blend():void
		{
			_spin.doTap();
			
			for(var i:int = 0; i < placedChunks.length; i++)
			{TweenMax.to(GuideFruitArray[i], nObjectFadeTime, {alpha:0});}
			
			switch(placedChunks.length)
			{
				case 1:var tempVal = .75;break;
				case 2:var tempVal = .85;break;
				case 3:var tempVal = .95;break;
			}
			
			TweenMax.to(scene_mc.blender_mc.blender_glass_mc.blender_juice, nObjectFadeTime, {alpha:tempVal, scaleX:tempVal, scaleY:tempVal});			
			
			var _Red:int = MixRed / placedChunks.length;
			var _Green:int = MixGreen / placedChunks.length;
			var _Blue:int = MixBlue / placedChunks.length;
			var intVal:int = _Red << 16 | _Green << 8 | _Blue;
			var hexVal:String = "0x" + intVal.toString(16);
			var finalVal:uint = uint(hexVal);
			var cTint:Color = new Color();
			cTint.setTint(finalVal, 1);
			scene_mc.blender_mc.blender_glass_mc.blender_juice.actor.transform.colorTransform = cTint;
			
			if(placedChunks.length > 2 && !bBlenderReady)
			{
				for(var i:int = 0; i < CupFillArray.length; i++)
				{CupFillArray[i].transform.colorTransform = cTint;}
				scene_mc.blender_mc.blender_glass_mc.pour.transform.colorTransform = cTint;
				scene_mc.blender_mc.blender_glass_mc.pour.alpha = 0;
				bBlenderReady = true;
			}
		}
		
		private function RemoveGraphic(_graphic):void
		{_graphic.removeChildAt(0);}
		
		private function BlenderAnimDone(e:TapSpritesheet):void
		{bBlenderAnim = false;}
		
		private function FirstInstruction():void
		{
			bFirstPass = false;
			StartInstructionVO.Start();
			scene_mc.princess.play();
		}
		
		private function Great():void
		{
			GreatVO.Start();
			DropConfetti();
			scene_mc.EndImage.visible = true;
		}
		
		private function DropConfetti():void
		{
			if(confettiCounter >= 240)
			{
				sndConfetti.Start();
				confettiCounter = 0;
			}
			
			Particles1.Q_Particles(1, 12, 229, -50, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
			Particles2.Q_Particles(1, 12, 579, -50, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
			Particles3.Q_Particles(1, 12, 829, -50, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
			Particles4.Q_Particles(1, 12, 229, -250, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
			Particles5.Q_Particles(1, 12, 579, -250, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
			Particles6.Q_Particles(1, 12, 829, -250, "gems_sparkles", "gems_mc", 0, 90, 5, 60, 90, 0, 0);
		}
		
		private function PlayRandom():void
		{
			if(Math.floor(Math.random() * 15) == 9)
			{RandomSoundArray[Math.floor(Math.random() * RandomSoundArray.length)].Start();}
		}
		
		private function Finale_Complete():void
		{
			if ( isRunning )
			{
				var returnToPrevPage:Boolean = this.loadedFrom == 2;
		
				EndOfLevel = new clsGames_EndOfLevelSummary(cMain, "BejeweledGame", returnToPrevPage, sndEnd);
				EndOfLevel.x = Background.x;
				EndOfLevel.y = Background.y;
				this.addChild(EndOfLevel);
			}
		}
		
		private function endLoaded( e:LoaderEvent ):void
		{
			sndEnd = new SoundEx( "sndEnd", GlobalData.langFolder + "sounds/games/GreatJob.mp3", App.voiceVolume );
			( e.target as MP3Loader ).dispose( true );
		}
		
		private function gameOver():void
		{
			winSound.Start();
			TweenMax.delayedCall(2, Finale_Complete);
			this.setChildIndex(Crowd, this.numChildren-1);
			Crowd.Start();
		}
	}
}