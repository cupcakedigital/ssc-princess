﻿package  
{
	import flash.xml.*;
	import com.cupcake.App;
	import com.cupcake.BookPage;
	import com.cupcake.Utils;
	
	public class clsStaticMenu extends WBZ_ScrollingMenu 
	{
		public function clsStaticMenu( clickCallbackFunction:Function, Main:clsMain ) 
			{ super(1, clickCallbackFunction); cMain = Main; }
			
		public override function InitializeButtons( ):void
		{
			var xml:XML = BookPage.xmlData;
			var PageOrder:Vector.<String> = cMain.cPages.StaticPageOrder;
			var len:int = int( PageOrder.length );
			var dex:int = 0;
			var buttonText:String;
			
			addButton( "Menu", xml.Page.(attribute("pageName")=="Menu").ButtonText.toString(), 0, BUTTON_X );
			
			for ( var t:int = 0 ; t < len ; t++ )
			{
				dex = t + 1;
				buttonText = PageOrder[ t ];
				addButton( "Page" + Utils.zeroPad( t + 1, 2 ), buttonText, dex, BUTTON_X + ( CELL_WIDTH * dex ) );
			}
			
			super.InitializeButtons();
		}
		
		public final function updateButtonText():void
		{
			var PageOrder:Vector.<String> = cMain.cPages.StaticPageOrder;
			var len:int = int( PageOrder.length );
			var dex:int = 0;
			var xml:XML = BookPage.xmlData;
			var buttonText:String;
			
			updateText( 0, xml.Page.(attribute("pageName")=="Menu").ButtonText.toString() );

			for ( var t:int = 0 ; t < len; t++ )
			{
				dex = t + 1;
				//buttonText = PageOrder[ t ];
				buttonText = xml.StaticPageOrder.Page[ t ].toString();
				updateText( dex, buttonText );				
			}
		}

	}
}
