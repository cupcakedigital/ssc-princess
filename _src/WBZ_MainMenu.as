﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.*;
	import flash.filesystem.File;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.text.Font;
	import flash.utils.Dictionary;

	import asData.clsMainMenuConstants;
	
	import com.DaveLibrary.clsGradientDynamicText;
	import com.DaveLibrary.clsMsgBox;
	import com.DaveLibrary.clsMusic;
	import com.DaveLibrary.clsStandardButton;
	import com.DaveLibrary.clsSupport;

	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.cupcake.MemoryTracker;
	import com.cupcake.Utils;	
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	import com.greensock.easing.Linear;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.MP3Loader;
	
	public class WBZ_MainMenu extends WBZ_BookPage
	{
		public static var firstRun:Boolean					= true;
		
		public var cMusic:clsMusic;
		public var MusicFile:String;
		public var Data:clsMainMenuConstants;
		public var RestartMusic:Boolean						= true;		
		
		protected var AnimBitmap:Bitmap;
		protected var AnimBitmap_ReadToMe:Bitmap;
		protected var AnimBitmap_Games:Bitmap;
		protected var AnimBitmap_ReadMyself:Bitmap;
		protected var AnimBitmap_JustABook:Bitmap;
		protected var ButtonImage_ReadToMe:BitmapData;
		protected var AltImage_ReadToMe:BitmapData;
		protected var ButtonImage_Games:BitmapData;
		protected var AltImage_Games:BitmapData;
		protected var ButtonImage_ReadMyself:BitmapData;
		protected var AltImage_ReadMyself:BitmapData;
		protected var ButtonImage_JustABook:BitmapData;
		protected var AltImage_JustABook:BitmapData;
		
		protected var LanguageButtons:Vector.<Sprite>		= new Vector.<Sprite>;
		protected var LanguageImages:Vector.<Bitmap>		= new Vector.<Bitmap>;
		protected var TitleButtons:Vector.<SimpleButton>	= new Vector.<SimpleButton>;
		protected var TitleImages:Vector.<Sprite>			= new Vector.<Sprite>;
		protected var TitleWobblers:Vector.<Wobbler>		= new Vector.<Wobbler>;
		
		private var Mode_Selected:Boolean					= false;
		
		private static var fps:ForParentsScreen;
		
		protected var btnHelp:clsStandardButton;
		protected var btnJAB:clsStandardButton;
		protected var btnReadMyself:clsStandardButton;
		protected var btnReadToMe:clsStandardButton;
		protected var btnGames:clsStandardButton;
		protected var btnNew:MovieClip;
		//protected var txtShop:clsGradientDynamicText;
		private var FadeOutText:clsStandardButton;
		private var gStrings:Dictionary;
		private var iconLoader:ImageLoader;
		
		protected var wowSoundIndex:int						= 3;

		public function WBZ_MainMenu()
		{
			super();
			
			//try
			{
				MusicFile = MusicFile == null ? CONFIG::DATA + "sounds/music/sscmusic1.mp3" : MusicFile;
				scene_mc.Background.addChild( new Bitmap( GlobalData.getBitmapData( "MainMenu_Background" ) ) );
				
				//var logoBmp:Bitmap = new Bitmap( GlobalData.getBitmapData( "MainMenu_LogoBar" ) );
				//scene_mc.Logo_Bar.addChild( logoBmp );
				
				gStrings = cMain.gStrings;			
				Data = new clsMainMenuConstants(gc);
				
				this.name = "Main Menu";
				
				var ldr:MP3Loader = new MP3Loader( MusicFile, { autoPlay: false, onComplete: soundLoaded } );
				ldr.load();			
				
				// add the buttons
				var btnIndex:int = (scene_mc.numChildren - 1);
				
				btnReadMyself = new clsStandardButton(cMain, gStrings["MainMenu"]["ReadMyself"], Data.ReadMyself_Button, ButtonImage_ReadMyself, clsSupport.CreateHighlightedImage( ButtonImage_ReadMyself, 0xFFFFFF, gc.button_Highlight),  btnReadMyself_MouseUp, null, null, null, true, false, Data.ReadMyself_Mirrored);
				btnReadMyself.x = Data.ReadMyself_Loc.x;
				btnReadMyself.y = Data.ReadMyself_Loc.y;
				btnReadMyself.visible = false;
				scene_mc.addChildAt( btnReadMyself, btnIndex );
				
				btnReadToMe = new clsStandardButton(cMain, gStrings["MainMenu"]["ReadToMe"], Data.ReadToMe_Button, ButtonImage_ReadToMe, clsSupport.CreateHighlightedImage( ButtonImage_ReadToMe, 0xFFFFFF, gc.button_Highlight),  btnReadToMe_MouseUp, null, null, null, true, false, Data.ReadToMe_Mirrored);
				btnReadToMe.x = Data.ReadToMe_Loc.x;
				btnReadToMe.y = Data.ReadToMe_Loc.y;
				btnReadToMe.visible = false;
				scene_mc.addChildAt( btnReadToMe, btnIndex );
	
				btnJAB = new clsStandardButton(cMain, gStrings["MainMenu"]["JAB"], Data.JAB_Button, ButtonImage_JustABook, clsSupport.CreateHighlightedImage( ButtonImage_JustABook, 0xFFFFFF, gc.button_Highlight),  btnJAB_MouseUp, null, null, null, true, false, Data.JAB_Mirrored);
				btnJAB.x = Data.JAB_Loc.x;
				btnJAB.y = Data.JAB_Loc.y;
				btnJAB.visible = false;
				scene_mc.addChildAt( btnJAB, btnIndex );
				
				btnGames = new clsStandardButton(cMain, gStrings["MainMenu"]["ReadMyself"], Data.Games_Button, ButtonImage_Games, clsSupport.CreateHighlightedImage( ButtonImage_Games, 0xFFFFFF, gc.button_Highlight),  btnGames_MouseUp, null, null, null, true, false, Data.Games_Mirrored);
				btnGames.x = Data.Games_Loc.x;
				btnGames.y = Data.Games_Loc.y;
				btnGames.visible = false;
				scene_mc.addChildAt( btnGames, btnIndex );
				
				if ( scene_mc.read_to_me_btn != null ) 
				{ 
					scene_mc.setChildIndex( scene_mc.read_to_me_btn, scene_mc.numChildren - 1 ); 
					Utils.addHandler( scene_mc.read_to_me_btn, MouseEvent.CLICK, btnReadToMe_MouseUp ); 
					scene_mc.read_to_me_btn.mouseEnabled = false;
				}
			
				if ( scene_mc.read_myself_btn != null ) 
				{
					scene_mc.setChildIndex( scene_mc.read_myself_btn, scene_mc.numChildren - 1 ); 
					Utils.addHandler( scene_mc.read_myself_btn, MouseEvent.CLICK, btnReadMyself_MouseUp ); 
					scene_mc.read_myself_btn.mouseEnabled = false;
				}
				
				if ( scene_mc.jab_btn != null ) 
				{
					scene_mc.setChildIndex( scene_mc.jab_btn, scene_mc.numChildren - 1 ); 
					Utils.addHandler( scene_mc.jab_btn, MouseEvent.CLICK, btnJAB_MouseUp ); 
					scene_mc.jab_btn.mouseEnabled = false;
				}
				
				if ( scene_mc.games_btn != null ) 
				{
					scene_mc.setChildIndex( scene_mc.games_btn, scene_mc.numChildren - 1 ); 
					Utils.addHandler( scene_mc.games_btn, MouseEvent.CLICK, btnGames_MouseUp ); 
					scene_mc.games_btn.mouseEnabled = false;
				}
				
				btnHelp = new clsStandardButton(cMain, gStrings["MainMenu"]["Help"], Data.Help_Button, GlobalData.getBitmapData( "For_Parents" ), clsSupport.CreateHighlightedImage( GlobalData.getBitmapData( "For_Parents" ), 0xFFFFFF, gc.button_Highlight), btnHelp_MouseUp, null, null, null, true, false);
				btnHelp.x = Data.Help_Loc.x;
				btnHelp.y = Data.Help_Loc.y;
				btnHelp.visible = false;
				this.addChild(btnHelp);
				/**/
				btnNew = new MoreAppsAnim;
				btnNew.x = Data.Help_Loc.x - btnNew.width + 160;
				btnNew.y = Data.Help_Loc.y - 120;
				btnNew.visible = false;
				Utils.addHandler( btnNew, MouseEvent.CLICK, btnNew_MouseUp );
				this.addChild( btnNew );
				/*
				txtShop = new clsGradientDynamicText( cMain, gStrings["MainMenu"]["Shop"], Data.Shop_Text );
				txtShop.x += Data.Shop_Text.offset.x;
				txtShop.y += Data.Shop_Text.offset.y;
				txtShop.visible = false;
				btnNew.addChild( txtShop );
				*/
				//showLanguageButtons();

				//updateText();
			} 
			//catch ( e:Error ) { App.Log( "Error in WBZ_MainMenu(): " + e.message ); }			
		}
		
		public override function Destroy():void
		{
			super.Destroy();
			
			cMain.removeStageMouseDown(Stage_MouseDown);
			cMain.removeStageMouseUp(Stage_MouseUp);				
			
			if(cMusic != null) cMusic.Destroy();			
			cMusic = null;
			
			clsSupport.DisposeOfSprite(this);			
			
			if(btnReadToMe != null) btnReadToMe.destroy();
			if(btnJAB != null) btnJAB.destroy();
			if(btnReadMyself != null) btnReadMyself.destroy();
			if(btnGames != null) btnGames.destroy();
			if(btnHelp != null) btnHelp.destroy();		
			
			if(FadeOutText != null) { FadeOutText.destroy(); FadeOutText = null; }		
						
			gStrings = null;
			Data = null;
			
			if ( iconLoader != null ) { iconLoader.dispose(true); iconLoader = null; }
		}
		
		public override function Init():void
		{  
			if ( isInitialized ) return;
			
			if ( AnimBitmap_ReadMyself != null )
			{
				AnimBitmap_ReadMyself.alpha = 0;
				AnimBitmap_ReadMyself.visible = false;
				scene_mc.addChildAt( AnimBitmap_ReadMyself, scene_mc.getChildIndex( btnReadMyself ) + 1 );
			}
			
			if ( AnimBitmap_ReadToMe != null )
			{
				AnimBitmap_ReadToMe.alpha = 0;
				AnimBitmap_ReadToMe.visible = false;
				scene_mc.addChildAt( AnimBitmap_ReadToMe, scene_mc.getChildIndex( btnReadToMe ) + 1 );
			}
			
			if ( AnimBitmap_JustABook != null )
			{
				AnimBitmap_JustABook.alpha = 0;
				AnimBitmap_JustABook.visible = false;
				scene_mc.addChildAt( AnimBitmap_JustABook, scene_mc.getChildIndex( btnJAB ) + 1 );
			}
			
			if ( AnimBitmap_Games != null )
			{
				AnimBitmap_Games.alpha = 0;
				AnimBitmap_Games.visible = false;
				scene_mc.addChildAt( AnimBitmap_Games, scene_mc.getChildIndex( btnGames ) + 1 );
			}
			
			super.Init();
			Reset();
		}		
				
		public override function Reset():void
		{
			Mode_Selected = false;	
			
			//scene_mc.fader.alpha = 0;
			
			btnReadToMe.UpdateImages( ButtonImage_ReadToMe, ButtonImage_ReadToMe, false, Data.ReadToMe_Mirrored, new Point(0,0));
			btnReadMyself.UpdateImages( ButtonImage_ReadMyself, ButtonImage_ReadMyself, false, Data.ReadMyself_Mirrored, new Point(0,0));
			btnJAB.UpdateImages( ButtonImage_JustABook, ButtonImage_JustABook, false, Data.JAB_Mirrored, new Point(0,0));
			btnGames.UpdateImages( ButtonImage_Games, ButtonImage_Games, false, Data.Games_Mirrored, new Point(0,0));
			
			btnReadMyself.rotation = 0;
			btnReadToMe.rotation = 0;
			btnJAB.rotation = 0;
			btnGames.rotation = 0;
			
			//scene_mc.fader.alpha = 0;
			
			if ( AnimBitmap_ReadMyself != null )
			{
				AnimBitmap_ReadMyself.alpha = 0;
				AnimBitmap_ReadMyself.visible = false;
			}
			
			if ( AnimBitmap_ReadToMe != null )
			{
				AnimBitmap_ReadToMe.alpha = 0;
				AnimBitmap_ReadToMe.visible = false;
			}
			
			if ( AnimBitmap_JustABook != null )
			{
				AnimBitmap_JustABook.alpha = 0;
				AnimBitmap_JustABook.visible = false;
			}
			
			if ( AnimBitmap_Games != null )
			{
				AnimBitmap_Games.alpha = 0;
				AnimBitmap_Games.visible = false;
			}
			
			btnReadToMe.Textbox.alpha = 1;
			btnReadMyself.Textbox.alpha = 1;
			btnJAB.Textbox.alpha = 1;
			btnGames.Textbox.alpha = 1;
		}
		
		protected final function SetTitle():void
		{
			// This space reserved for when I remember why we're doing this... something related to L10N maybe?
			// Bring title images to top
			for ( var t:uint = 0 ; t < TitleImages.length ; t++ )
			{
				TitleWobblers.push( new Wobbler( TitleButtons[ t ], TitleImages[ t ] ) );
				controls.push( TitleWobblers[ t ] );
				scene_mc.setChildIndex( TitleImages[ t ], scene_mc.numChildren - 1 );
			}
			
			for ( var u:uint = 0 ; u < TitleImages.length ; u++ )
				{ scene_mc.setChildIndex( TitleButtons[ u ], scene_mc.numChildren - 1 ); }
			
			updateText();
		}
		
		public override function Start():void
		{
			super.Start();
			if(RestartMusic && cMusic != null) 
			{
				cMusic.Stop();
				cMusic.Start();
			}
			RestartMusic = true;
			
			TweenLite.delayedCall( 1, MoreApps.checkMoreApps );
			
			cMain.addStageMouseUp(Stage_MouseDown);
			cMain.addStageMouseUp(Stage_MouseUp);			
		}
		
		public override function Stop():void
		{
			if ( isRunning )
			{
				super.Stop();
				if ( cMusic != null ) cMusic.Stop();
				cMain.removeStageMouseUp(Stage_MouseDown);
				cMain.removeStageMouseUp(Stage_MouseUp);				
			}
		}	
		
		public function StartMusic():void { if ( cMusic != null ) cMusic.Start(); }		
		
		protected function StartModeAnim():void
		{		
			Mode_Selected = true;
			
			if ( AnimBitmap != null )
			{
				AnimBitmap.alpha = 0;
				AnimBitmap.visible = true;
				TweenLite.to( AnimBitmap, 2, { alpha: 1, onComplete: AnimComplete, ease: Linear.easeNone } );
			}
			
			GlobalData.playSound( "MainMenu_Mode_Select" );
			TweenLite.to( FadeOutText.Textbox, .66, { alpha: 0, ease: Linear.easeNone } );
		}		
		
		protected final function AnimComplete():void
		{
			if ( AnimBitmap != null ) { AnimBitmap.alpha = 1; AnimBitmap = null; }
			cMain.NextPage(0);
		}
		
		protected function Stage_MouseDown(e:MouseEvent):void { /* stub for override */ }
		protected function Stage_MouseUp(e:MouseEvent):void { /* stub for override */ }		
		
		private final function btnNew_MouseUp(e:MouseEvent):void
		{
			if ( !isRunning || isPaused || Mode_Selected ) return;
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_IOS )
				{ Captcha.Show( MoreApps_Captcha_Return ); }
			else { MoreApps_Captcha_Return( true, false ); }
		}
		
		public function MoreApps_Captcha_Return( success:Boolean, cancel:Boolean ):void
		{
			GlobalData.playSound( "Button_Generic" );
			if ( success ) { clsMain.showMoreApps(); }
		}
		
		private final function btnHelp_MouseUp(e:MouseEvent):void
		{
			if ( !isRunning || isPaused || Mode_Selected ) return;
			if ( MoreApps.apps.length > 0 && CONFIG::MARKET != "none" ) { Captcha.Show(
				ForParents_Captcha_Return ); }
			else { Captcha.Show( Help_Captcha_Return ); }
		}
		
		public function Help_Captcha_Return( success:Boolean, cancel:Boolean ):void
		{
			GlobalData.playSound( "Button_Generic" );
			if ( success ) { cMain.ShowHelp(); }
		}
			
		
		private final function getPageName():String
		{
			return clsOptions.interactive ? GlobalData.pagesXML.Page.(attribute("pageName")==cMain.cPages.PageOrder[clsOptions.lastPage]).ButtonText.toString() :
											GlobalData.pagesXML.PageName + ( clsOptions.lastPage + 1 ).toString();
		}
		
		protected final function btnReadMyself_MouseUp(e:MouseEvent):void
		{
			if ( Mode_Selected || !isRunning ) { return; }
			/*
			if ( !clsOptions.interactive )
			{
				clsOptions.lastPage = 0;
				clsOptions.interactive = true;
				cMain.cPages.InitializeArrays();
				cMain.cNavBar.SetPages();
				clsOptions.Save();
			}
		
			if(clsOptions.lastPage > 0) 
			{
				var d:Dictionary = Dictionary(clsSupport.copyObject(cMain.gStrings["Messages"]["Msgbox_Mainmenu_Continue"]));
				var pageName:String = getPageName();
				d["Prompt"] = clsSupport.replace(d["Prompt"], "{PAGE}", pageName);
				var Params:Dictionary = new Dictionary();
				Params["PlayMode"] = MODE_READ_MYSELF;
				GlobalData.playSound( "Dialog_Open" );
				var msgbox:clsMsgBox = new clsMsgBox(cMain, d, gc.messages_Mainmenu_Continue, "YesNo", "btnReadMyself_MouseUp", ContinueMsgbox_Callback, Params, null, cMain.gStrings["MainMenu"]["StartOver"], cMain.gStrings["MainMenu"]["Continue"],false,true,true);
			} 
			else 
			{
				if ( AnimBitmap_ReadMyself != null )
				{
					AnimBitmap = AnimBitmap_ReadMyself;
					var m:Matrix = AnimBitmap.transform.matrix;
					if(Data.ReadMyself_Mirrored) {
						m.a = -1;
						m.b = 0;
						m.c = 0;
						m.d = 1;
						m.tx = Data.ReadMyself_Glow.x + AnimBitmap.bitmapData.width;
						m.ty = Data.ReadMyself_Glow.y;
					} else {
						m.a = 1;
						m.b = 0;
						m.c = 0;
						m.d = 1;
						m.tx = Data.ReadMyself_Glow.x;
						m.ty = Data.ReadMyself_Glow.y;
					}
					AnimBitmap.transform.matrix = m;
					if ( Data.ReadMyself_Centered )
					{
						AnimBitmap.x = (AnimBitmap.x - (AnimBitmap.width / 2));
						AnimBitmap.y = (AnimBitmap.y - (AnimBitmap.height / 2));
					}
					
				}
				
				FadeOutText = btnReadMyself;
				if ( AltImage_ReadMyself != null )
				{
					btnReadMyself.UpdateImages( AltImage_ReadMyself, AltImage_ReadMyself,false, Data.ReadMyself_Mirrored, Data.ReadMyself_OpenOffset);
				}
				
				
				clsOptions.readToMe = false;
				clsOptions.Save();
				cMain.playMode = MODE_READ_MYSELF;
				
				StartModeAnim();
			}
			*/
			//CROQUET
			
			var targetPage:Class = cMain.cNavBar.ArcadePages[1];
			//TweenLite.to( scene_mc.game2_Glow, .5, {alpha:1, onComplete:cMain.FadeToPage, onCompleteParams:[targetPage, 2], ease: Linear.easeNone } );
			cMain.FadeToPage( targetPage, 2 );
			GlobalData.playSound( "MainMenu_Mode_Select" );
			cMain.cNavBar.CloseNav();
			
			//scene_mc.setChildIndex(scene_mc.fader, scene_mc.numChildren-1);
			//scene_mc.fader.x = Data.ReadMyself_Loc.x;
			//scene_mc.fader.y = Data.ReadMyself_Loc.y;
			////scene_mc.fader.alpha = 0;
			//TweenLite.to(scene_mc.fader, 2, {alpha:0.9});
		}
		
		protected function btnReadToMe_MouseUp(e:MouseEvent):void
		{
			if ( Mode_Selected || !isRunning ){ return; }
			if ( !clsOptions.interactive )
			{
				/*scene_mc.setChildIndex(scene_mc.fader, scene_mc.numChildren-1);
				scene_mc.fader.x = Data.ReadToMe_Loc.x;
				scene_mc.fader.y = Data.ReadToMe_Loc.y;
				scene_mc.fader.alpha = 0;
				TweenLite.to(scene_mc.fader, 2, {alpha:0.9});*/
			
				clsOptions.lastPage = 0;
				clsOptions.interactive = true;
				cMain.cPages.InitializeArrays();
				cMain.cNavBar.SetPages();
				clsOptions.Save();
			}
			
			if(clsOptions.lastPage > 0) 
			{
				var d:Dictionary = Dictionary(clsSupport.copyObject(cMain.gStrings["Messages"]["Msgbox_Mainmenu_Continue"]));
				var pageName:String = getPageName();
				d["Prompt"] = clsSupport.replace(d["Prompt"], "{PAGE}", pageName);
				var Params:Dictionary = new Dictionary();
				Params["PlayMode"] = MODE_READ_TO_ME;
				GlobalData.playSound( "Dialog_Open" );
				var msgbox:clsMsgBox = new clsMsgBox(cMain, d, gc.messages_Mainmenu_Continue, "YesNo", "btnReadToMe_MouseUp", ContinueMsgbox_Callback, Params, null, cMain.gStrings["MainMenu"]["StartOver"], cMain.gStrings["MainMenu"]["Continue"],false,true,true);
			} 
			else 
			{
				/*scene_mc.setChildIndex(scene_mc.fader, scene_mc.numChildren-1);
				scene_mc.fader.x = Data.ReadToMe_Loc.x;
				scene_mc.fader.y = Data.ReadToMe_Loc.y;
				scene_mc.fader.alpha = 0;
				TweenLite.to(scene_mc.fader, 2, {alpha:0.9});*/
			
				if ( AnimBitmap_ReadToMe != null )
				{
					AnimBitmap = AnimBitmap_ReadToMe;
					var m:Matrix = AnimBitmap.transform.matrix;
					if(Data.ReadToMe_Mirrored) {
						m.a = -1;
						m.b = 0;
						m.c = 0;
						m.d = 1;
						m.tx = Data.ReadToMe_Glow.x + AnimBitmap.bitmapData.width;
						m.ty = Data.ReadToMe_Glow.y;
					} else {
						m.a = 1;
						m.b = 0;
						m.c = 0;
						m.d = 1;
						m.tx = Data.ReadToMe_Glow.x;
						m.ty = Data.ReadToMe_Glow.y;
					}
					AnimBitmap.transform.matrix = m;
					if ( Data.ReadToMe_Centered )
					{
						AnimBitmap.x = (AnimBitmap.x - (AnimBitmap.width / 2));
						AnimBitmap.y = (AnimBitmap.y - (AnimBitmap.height / 2));
					}
				}
				
				FadeOutText = btnReadToMe;
				if ( AltImage_ReadToMe != null )
				{
					btnReadToMe.UpdateImages(AltImage_ReadToMe,AltImage_ReadToMe,false, Data.ReadToMe_Mirrored, Data.ReadToMe_OpenOffset);
				}
				clsOptions.readToMe = true;
				clsOptions.Save();
				cMain.playMode = MODE_READ_TO_ME;
				
				StartModeAnim();
			}			
		}
		
		protected function btnJAB_MouseUp(e:MouseEvent):void
		{			
			if ( Mode_Selected || !isRunning ){ return; }
		/*
			if ( clsOptions.interactive )
			{
				clsOptions.lastPage = 0;
				clsOptions.interactive = false;
				cMain.cPages.InitializeArrays(false);
				cMain.cNavBar.SetPages();
				clsOptions.Save();
			}
			
			if(clsOptions.lastPage > 0) 
			{
				var d:Dictionary = Dictionary(clsSupport.copyObject(cMain.gStrings["Messages"]["Msgbox_Mainmenu_Continue"]));
				var pageName:String = getPageName();
				d["Prompt"] = clsSupport.replace(d["Prompt"], "{PAGE}", pageName);
				var Params:Dictionary = new Dictionary();
				Params["PlayMode"] = MODE_JUST_A_BOOK;
				GlobalData.playSound( "Dialog_Open" );
				var msgbox:clsMsgBox = new clsMsgBox(cMain, d, gc.messages_Mainmenu_Continue, "YesNo", "btnJAB_MouseUp", ContinueMsgbox_Callback, Params, null, cMain.gStrings["MainMenu"]["StartOver"], cMain.gStrings["MainMenu"]["Continue"],false,true,true);
			} 
			else 
			{
				if ( AnimBitmap_JustABook != null )
				{
					AnimBitmap = AnimBitmap_JustABook;
					var m:Matrix = AnimBitmap.transform.matrix;
					if(Data.JAB_Mirrored) {
						m.a = -1;
						m.b = 0;
						m.c = 0;
						m.d = 1;
						m.tx = Data.JAB_Glow.x + AnimBitmap.bitmapData.width;
						m.ty = Data.JAB_Glow.y;
					} else {
						m.a = 1;
						m.b = 0;
						m.c = 0;
						m.d = 1;
						m.tx = Data.JAB_Glow.x;
						m.ty = Data.JAB_Glow.y;
					}
					AnimBitmap.transform.matrix = m;
					if ( Data.JAB_Centered )
					{
						AnimBitmap.x = (AnimBitmap.x - (AnimBitmap.width / 2));
						AnimBitmap.y = (AnimBitmap.y - (AnimBitmap.height / 2));
					}
				}
				
				FadeOutText = btnJAB;
				if ( AltImage_JustABook != null )
				{
					btnJAB.UpdateImages(AltImage_JustABook,AltImage_JustABook,false, Data.JAB_Mirrored, Data.JAB_OpenOffset);
				}
				
				clsOptions.readToMe = true;
				clsOptions.Save();
				cMain.playMode = MODE_JUST_A_BOOK;			
				
				StartModeAnim();
			}			
			*/
			
			
			var targetPage:Class = cMain.cNavBar.ArcadePages[0];
			//TweenLite.to( scene_mc.game2_Glow, .5, {alpha:1, onComplete:cMain.FadeToPage, onCompleteParams:[targetPage, 2], ease: Linear.easeNone } );
			cMain.FadeToPage( targetPage, 2 );
			GlobalData.playSound( "MainMenu_Mode_Select" );
			cMain.cNavBar.CloseNav();
			
			/*scene_mc.setChildIndex(scene_mc.fader, scene_mc.numChildren-1);
			scene_mc.fader.x = Data.JAB_Loc.x;
			scene_mc.fader.y = Data.JAB_Loc.y;
			scene_mc.fader.alpha = 0;
			TweenLite.to(scene_mc.fader, 2, {alpha:0.9});*/
		}
		
		protected final function btnGames_MouseUp(e:MouseEvent):void
		{
			if ( !isRunning || isPaused || Mode_Selected ) return;
			
			
			
			//if(Buttons_No_Longer_Active){return;}
			//Buttons_No_Longer_Active = true;
			var targetPage:Class = cMain.cNavBar.The_Color_Page[0];
			//TweenLite.to( scene_mc.create2_Glow, .5, {alpha:1, onComplete:cMain.FadeToPage, onCompleteParams:[targetPage, 2], ease: Linear.easeNone } );
			cMain.FadeToPage( targetPage, 2 );
			GlobalData.playSound( "MainMenu_Mode_Select" );
			cMain.cNavBar.CloseNavFULL();
			/*
			if ( AnimBitmap_Games != null )
			{
				AnimBitmap = AnimBitmap_Games;
				var m:Matrix = AnimBitmap.transform.matrix;
				if(Data.Games_Mirrored) 
				{
					m.a = -1;
					m.b = 0;
					m.c = 0;
					m.d = 1;
					m.tx = Data.Games_Glow.x + AnimBitmap.bitmapData.width;
					m.ty = Data.Games_Glow.y;
				} else {
					m.a = 1;
					m.b = 0;
					m.c = 0;
					m.d = 1;
					m.tx = Data.Games_Glow.x;
					m.ty = Data.Games_Glow.y;
				}
				AnimBitmap.transform.matrix = m;
				if ( Data.Games_Centered )
				{
					AnimBitmap.x = (AnimBitmap.x - (AnimBitmap.width / 2));
					AnimBitmap.y = (AnimBitmap.y - (AnimBitmap.height / 2));
				}
				
			}
			
			FadeOutText = btnGames;
			if ( AltImage_Games != null )
			{
				btnGames.UpdateImages( AltImage_Games, AltImage_Games,false, Data.Games_Mirrored, Data.Games_OpenOffset);
			}
			
			GlobalData.playSound( "Button_Generic" );
			cMain.ShowGames();
			*/
			/*
			clsOptions.readToMe = false;
			clsOptions.Save();
			cMain.playMode = MODE_READ_MYSELF;
			
			StartModeAnim();
			*/
			/*scene_mc.setChildIndex(scene_mc.fader, scene_mc.numChildren-1);
			scene_mc.fader.x = Data.Games_Loc.x;
			scene_mc.fader.y = Data.Games_Loc.y;
			scene_mc.fader.alpha = 0;
			TweenLite.to(scene_mc.fader, 2, {alpha:0.9});*/
		}
		
		private final function ContinueMsgbox_Callback(src:clsMsgBox, Button:String, Params:Dictionary):void
		{
			if(Button == "Yes") 
			{
				clsOptions.lastPage = 0;
				clsOptions.Save();
				
				switch(src.Msgbox_Id) 
				{
					case "btnJAB_MouseUp": btnJAB_MouseUp(null); break;
					case "btnReadToMe_MouseUp": btnReadToMe_MouseUp(null); break;
					case "btnReadMyself_MouseUp": btnReadMyself_MouseUp(null); break;
				}			
			} else if(Button == "No") 
			{
				switch(src.Msgbox_Id) 
				{
					case "btnJAB_MouseUp": 
						clsOptions.readToMe = true;
						clsOptions.Save();
						cMain.playMode = MODE_JUST_A_BOOK;			
						break;
						
					case "btnReadToMe_MouseUp": 
						clsOptions.readToMe = true;
						clsOptions.Save();
						cMain.playMode = MODE_READ_TO_ME;				
						break;
						
					case "btnReadMyself_MouseUp": 
						clsOptions.readToMe = false;
						clsOptions.Save();
						cMain.playMode = MODE_READ_MYSELF;		
						break;
				}
				cMain.NextPage(clsOptions.lastPage);
			}
		}
		
		private final function soundLoaded( e:LoaderEvent ):void
		{
			cMusic = new clsMusic( e.target.content as Sound, App.musicVolume, ( isRunning && RestartMusic ) );
			( e.target as MP3Loader ).dispose( true );
		}
		
		public function updateText():void
		{
			btnReadMyself.text = cMain.gStrings["MainMenu"]["ReadMyself"];
			btnReadToMe.text = cMain.gStrings["MainMenu"]["ReadToMe"];
			btnJAB.text = cMain.gStrings["MainMenu"]["JAB"];
			btnGames.text = cMain.gStrings["MainMenu"]["ReadMyself"];
			
			Data.Help_Button.textInfo.letterSpacing = GlobalData.localXML.MainMenu.HelpSpacing;
			btnHelp.text = cMain.gStrings["MainMenu"]["Help"];
			
			//txtShop.text = cMain.gStrings["MainMenu"]["Shop"];
			
			var dropShadow:DropShadowFilter = new DropShadowFilter(6,45,0,.8,6,6,1,3);
			var stroke:GlowFilter = new GlowFilter( 0xffffff, 1, 2, 2, 20, 3 );
			
			btnReadMyself.Textbox.Image = Utils.replaceDisplayObjectWithBitmap( btnReadMyself.Textbox.Image, 1, false, [stroke,dropShadow] );
			btnReadMyself.RepositionText(false);
			btnReadToMe.Textbox.Image = Utils.replaceDisplayObjectWithBitmap( btnReadToMe.Textbox.Image, 1, false, [stroke,dropShadow] );
			btnReadToMe.RepositionText(false);
			btnJAB.Textbox.Image = Utils.replaceDisplayObjectWithBitmap( btnJAB.Textbox.Image, 1, false, [stroke,dropShadow] );
			btnJAB.RepositionText(false);
			btnGames.Textbox.Image = Utils.replaceDisplayObjectWithBitmap( btnGames.Textbox.Image, 1, false, [stroke,dropShadow] );
			btnGames.RepositionText(false);
			
			for ( var t:int = 0 ; t < TitleImages.length ; t++ )
			{
				if ( TitleImages[ t ].numChildren > 0 ) { TitleImages[ t ].removeChildAt( 0 ); }
				TitleImages[ t ].mouseEnabled = true;
				TitleImages[ t ].mouseChildren = true;
				TitleImages[ t ].addChild( new Bitmap( GlobalData.getBitmapData( "MainMenu_Title" + ( t + 1 ) ) ) );
				TitleImages[ t ].x = GlobalData.localXML.MainMenu.Title.(@index==t).x;
				TitleImages[ t ].y = GlobalData.localXML.MainMenu.Title.(@index==t).y;
				TitleWobblers[ t ].setStart();
				TitleWobblers[ t ].Start();
			}
			
			if ( LanguageImages.length > 0 )
			{
				var langs:Vector.<String> = GlobalData.languages;
				var sprite:Sprite;
				var bitmap:Bitmap;
				var glow:GlowFilter = new GlowFilter( 0xffff00 );
				
				for ( var u:int = 0 ; u < langs.length ; u++ )
				{
					sprite = LanguageButtons[ u ];
					Utils.ClearContainer( sprite );
					LanguageImages[u].filters = ( clsOptions.language == langs[ u ] ) ? [glow] : null;
					bitmap = new Bitmap( new BitmapData( LanguageImages[u].width, LanguageImages[u].height, true, 0 ) );
					bitmap.bitmapData.draw( LanguageImages[ u ] );
					sprite.addChild( bitmap );
				}
			}
		}
		
		private function showLanguageButtons():void
		{
			var button:SimpleButton		= null;
			var flag:Bitmap				= null;
			var langs:Vector.<String> 	= GlobalData.languages;
			var sprite:Sprite			= null;
			
			if ( langs.length < 2 ) { return; }
			
			for ( var t:int = 0 ; t < langs.length ; t++ )
			{
				flag = new Bitmap( GlobalData.getSheetBitmap( "flag_" + langs[t] ) );
				LanguageImages.push( flag );
				sprite = new Sprite;
				//sprite.addChild( flag );
				sprite.name = langs[t];
				sprite.x = Data.Language_Buttons.x - ( Data.Language_Buttons.width * ( t + 1 ) ) - ( 10 * t );
				sprite.y = Data.Language_Buttons.y;
				addChild( sprite );
				Utils.addHandler( sprite, MouseEvent.CLICK, LanguageClicked );
				LanguageButtons.push( sprite );
			}
		}
		
		private function LanguageClicked( e:MouseEvent ):void
		{
			var lang:String = e.target.name;
			App.Log( "Clicked language '" + lang + "'" );
			if ( lang != clsOptions.language ) { clsMain.setLanguage( lang ); }
		}
		
		public final function showMoreAppsButton():void
		{
			if ( MoreApps.apps.length > 0 && MoreApps.moreAppsLoaded )
			{
				if ( clsOptions.showMoreApps )
				{
					TweenLite.to( btnNew, 1, { alpha:1 } );
					iconLoader = new ImageLoader( "app-storage:/" + MoreApps.apps[ 0 ].altFile, { onComplete: iconLoaded } );
					iconLoader.load();				
				}
			}
			else
			{
				if ( MoreApps.apps.length == 0 ) { App.Log( "No more apps to show" ); }
				if ( !MoreApps.moreAppsLoaded ) { App.Log( "More apps list not loaded" ); }
			}
			
		}
		
		private final function iconLoaded( e:LoaderEvent ):void
		{
			var iconSprite:Sprite = new Sprite;
			var iconImg:Bitmap = new Bitmap( iconLoader.rawContent.bitmapData );
			var dropShadow:DropShadowFilter = new DropShadowFilter(6,45,0,.8,6,6,1,3);
			iconImg = Utils.replaceDisplayObjectWithBitmap( iconImg, 1, false, [dropShadow] );
			
			iconSprite.addChild( iconImg );
			
			clsOptions.moreAppsShown++;
			clsOptions.Save();

			btnNew.icon_mc.addChild( iconSprite );
			btnNew.icon_mc.alpha = 0;
			TweenLite.to( btnNew.icon_mc, 1, { alpha: 1 } );
			
			var sprkl:Sparkler = new Sparkler( iconSprite );
			controls.push( sprkl );
			sprkl.Start();
			App.Log( "Showing More Apps icon" );
			
		}
		
		public final function hideMoreAppsButton():void
		{
			btnNew.visible = false;
		}
		
		public final function showAllButton():void
		{
			btnReadMyself.visible = true;
			btnReadToMe.visible = true;
			btnJAB.visible = true;
			btnGames.visible = true;
			if(CONFIG::MARKET != "none")
			{
				btnHelp.visible = true;
				btnNew.visible = true;
			}
			btnNew.alpha = 1;
			//txtShop.visible = true;
			/*
			scene_mc.read_to_me_btn.mouseEnabled = true;
			scene_mc.read_myself_btn.mouseEnabled = true;
			scene_mc.jab_btn.mouseEnabled = true;
			scene_mc.games_btn.mouseEnabled = true;
			*/
		}
		
		public final function showAllButtonStart():void
		{
			btnReadMyself.visible = true;
			btnReadToMe.visible = true;
			btnJAB.visible = true;
			btnGames.visible = true;
			btnHelp.visible = true;
			btnNew.visible = true;
			//txtShop.visible = true;
		}
		
		public final function hideAllButton():void
		{
			btnReadMyself.visible = false;
			btnReadToMe.visible = false;
			btnJAB.visible = false;
			btnGames.visible = false;
			btnHelp.visible = false;
			btnNew.visible = false;
			//txtShop.visible = false;
		}
		
		public function ForParents_Captcha_Return( success:Boolean, cancel:Boolean ):void
		{
			GlobalData.playSound( "Button_Generic" );
			if ( success )
			{
				fps = new ForParentsScreen();
				fps.x = -171;
				cMain.addChild( fps );
			}
		}
		
		public static function CloseForParents():void
		{
			if ( fps != null )
			{
				if ( cMain.contains( fps ) ) { cMain.removeChild( fps ); }
				fps.Hide();
				fps = null;
			}
		}
		
		public static function OpenHelp():void
		{
			CloseForParents();
			cMain.ShowHelp();
		}
	}
}