﻿package  
{	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	
	import com.DaveLibrary.clsSupport;
	import com.cupcake.App;
	import com.cupcake.Utils;	
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.greensock.events.LoaderEvent;
	import com.greensock.events.TweenEvent;
	import com.greensock.loading.ImageLoader;
	
	public class clsSplashScreens extends Sprite 
	{
		public static var Delay:Number 				= 3;		// the number of seconds to pause before fading to the next
		public static var FadeTime:Number			= .5;		// number of seconds to fade
		public static var AllowTapAfter:Number		= .6;		// allow click/tap ahead of the screen after this many seconds

		private var cMain:clsMain;
		
		private var Splashs:Vector.<String>;
		private var loader:ImageLoader				= null;
		private var Splash:Sprite					= null;

		public function clsSplashScreens(Main:clsMain) 
		{
			cMain = Main;
			
			// if we are in debug mode, disable the hold limits.
			if(cMain.gc.debug_Enabled) AllowTapAfter = 0;

			Splashs = new Vector.<String>;
			//Splashs.push(CONFIG::DATA + "images/bolder_splash.png");
			//Splashs.push(CONFIG::DATA + "images/cupcakedigital_splash.png");
			
			alpha = 0;
			x = clsMain.APP_X;
			
			
			
			//App.Log( "Constructor in clsSplashScreens" );
		}
		
		public function destroy():void
		{
			if(cMain != null) { cMain.removeStageMouseUp(MOUSE_UP); }
			TweenLite.killDelayedCallsTo( MainTimer_Tick );
			TweenLite.killDelayedCallsTo( TapAhead_Tick );
			clsSupport.DisposeOfSprite(this);
			cMain = null;			
		}
		
		public function Start():void 
		{
			//LoadNext();
			cMain.PreMainMenuInit();
			cMain.cMainMenu.StartMusic();
			cMain.showMainMenu(false);
		}
				
		public function EndFade():void
		{
			if ( this.contains( Splash ) ) { this.removeChild( Splash ); Splash = null; }
			if ( loader != null ) { loader.dispose(true); loader = null; }
			
			if( Splashs.length > 0 ) { LoadNext(); }
			else 
			{
				//App.Log( "EndFade() in clsSplashScreens" );
				
				cMain.PreMainMenuInit();
				cMain.cMainMenu.StartMusic();
				cMain.showMainMenu(false);
			}
		}
		
		public function MOUSE_UP(e:Event):void { MainTimer_Tick(); }
		
		private function LoadNext():void
		{
			Splash = new Sprite;
			this.addChild(Splash);
			var ldr:ImageLoader = new ImageLoader( Splashs[ 0 ], { container: Splash, onComplete: ImageLoaded } );
			ldr.load();
			Splashs.shift();
		}
		
		private function ImageLoaded( e:LoaderEvent ):void
		{
			TweenLite.delayedCall( Delay, MainTimer_Tick );
			TweenLite.delayedCall( AllowTapAfter, TapAhead_Tick );
			TweenLite.to( this, FadeTime, { alpha: 1, ease: Linear.easeNone } );
		}
		
		private function MainTimer_Tick():void
		{
			TweenLite.killDelayedCallsTo( TapAhead_Tick );
			cMain.removeStageMouseUp(MOUSE_UP);
			TweenLite.to( this, FadeTime, { alpha: 0, onComplete: EndFade, ease: Linear.easeNone } );
		}
		
		private function TapAhead_Tick():void
			{ cMain.addStageMouseUp(MOUSE_UP); }
	}
}