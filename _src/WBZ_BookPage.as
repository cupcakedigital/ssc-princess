﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.text.Font;
	import flash.utils.getDefinitionByName;
	
	import asData.clsGlobalConstants;
	
	import com.cupcake.App;
	import com.cupcake.BookPage;
	import com.cupcake.ExternalImage;
	import com.cupcake.IControllable;
	import com.cupcake.Images;
	import com.cupcake.MemoryTracker;
	import com.cupcake.ReaderBox;
	import com.cupcake.SoundEx;
	import com.cupcake.SpriteSheet;
	import com.cupcake.SpriteSheetAnimation;
	import com.cupcake.SpriteSheetLoader;
	import com.cupcake.Utils;
	
	import com.DaveLibrary.clsMusic;
	
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.MP3Loader;
	import com.greensock.TweenLite;
	
	public class WBZ_BookPage extends BookPage
	{
		public static const FROM_OTHER_PAGE:uint	= 0;
		public static const FROM_PAGES_MENU:uint	= 1;
		public static const FROM_ARCADE_MENU:uint	= 2;
		public static const FROM_VIDEO_MENU:uint	= 3;
		
		public static const NEXT_PAGE:uint			= 0;
		public static const PREV_PAGE:uint			= 1;
		public static const FADE_PAGE:uint			= 2;
		public static const STAND_ALONE:uint		= 3;
		
		public static var cMain:clsMain				= null;
		public static var defaultHighlightFont:Font	= new fontVAGRoundedBlack;
		public static var defaultRegFont:Font		= new fontVAGRounded;
		public static var defaultVoiceover:Sound	= null;
		public static var gc:clsGlobalConstants		= null;
		
		public var readerToggle:ReaderToggle;
		public var scene_mc:MovieClip;
		public var textbox_mc:MovieClip;
		
		public var imagesLoader:SpriteSheetImageLoader;
		public var imageLoaders:Vector.<SpriteSheetImageLoader>;

		public var tapAnimLoader:TapAnimationLoader;
		public var sparklers:SparklersController;
		public var spriteSheetLoader:SpriteSheetLoader;
		public var W_Loader:WobblerLoader;

		public var _LoadedFrom:int					= FROM_OTHER_PAGE;
		public var bPausedSetByMain:Boolean 		= false;
		public var FlipAfterLoad:Boolean			= true;
		public var FlipToPage:int					= -2;
		public var imageFolder:String				= "";
		public var reusableFolder:String			= CONFIG::DATA + "images/pages/Reusable/";
		public var musicFile:String					= "";
		public var loadedCallback:Function			= null;
		public var PageLoaded:Boolean				= true;
		public var pageTarget:uint					= NEXT_PAGE;
		
		public var animAssets:Vector.<SpriteSheet>	= null;
		public var assets:Vector.<ExternalImage>	= null;
		public var controls:Vector.<IControllable> 	= null;	
		public var AssetLoader:LoaderMax			= null;
		public var music:clsMusic					= null;
		public var singleSounds:Vector.<SoundEx>	= null;
		public var word:MP3Loader					= null;
		
		public var anims:Vector.<IControllable>		= null;

		// Static functions
		public static function loadXML( ):void
		{
			addEnterFrame = cMain.addEnterFrame;
			removeEnterFrame = cMain.removeEnterFrame;
		}
		
		// Standard functions
		public function WBZ_BookPage() 
		{ 
			MemoryTracker.track( this, "Page object" );
			
			var fontClass:Class = getDefinitionByName( GlobalData.pagesXML.FontClass.toString() ) as Class;
			voiceover = defaultVoiceover;
			fontSize = GlobalData.pagesXML.FontSize;
			if ( GlobalData.pagesXML.FontLeading > -1 ) { leading = GlobalData.pagesXML.FontLeading; }
			regFont = new fontClass;
			regColor = 0x000000;
			strokeColor = 0x2e4695;
			strokeSize = 0;
			
			highlightColor = gc.pages_HighlightColor;
			highlightFont = defaultHighlightFont;
			highlightFontColor = uint( gc.pages_HighlightFontColor );
			highlightFontSizeFactor = gc.pages_HighlightSizeFactor;
			highlightStrokeSize = gc.pages_HighlightStrokeSize;
			highlightStrokeColor = gc.pages_HighlightStrokeColor;
			
			//if ( reader_mc != null ) reader_mc.app = cMain;
			playMode = cMain.playMode;
			
			super();
			controls = new Vector.<IControllable>; 
			assets = new Vector.<ExternalImage>;
			animAssets = new Vector.<SpriteSheet>;
			singleSounds = new Vector.<SoundEx>;
			
			imageLoaders = new Vector.<SpriteSheetImageLoader>;
			imagesLoader = new SpriteSheetImageLoader( this );
			imageLoaders.push( imagesLoader );
			Images.page = this;

			tapAnimLoader = new TapAnimationLoader( this );
			sparklers = new SparklersController;
			spriteSheetLoader = new SpriteSheetLoader( this );
			W_Loader = new WobblerLoader( this );
			
			Images.page = this;
			TapMultiLite.page = this;
			TapSpritesheet.page = this;
			Wobbler.page = this;
			
			if ( textbox_mc != null ) 
			{ 
				textbox_mc.addChild( new Bitmap( GlobalData.getBitmapData( "Textbox" ) ) ); 
				readerToggle = new ReaderToggle( this );
			}
		}
		
		public function CheckSparkler( Container:String, Trigger:SimpleButton ):void
		{
			if ( Trigger != null )
			{
				var sprite:Sprite = scene_mc.getChildByName( Container + "_sprkl" ) as Sprite;
				if ( sprite != null ) { sparklers.Add( Trigger, sprite ); }
			}
		}
		
		public override function Destroy():void
		{
			super.Destroy();
			//App.Log( "Disposing of assets..." );
			if ( AssetLoader != null ) { AssetLoader.dispose( true ); AssetLoader = null; }
			if ( music != null ) { music.Stop(); music.Destroy(); music = null; }
			if ( W_Loader != null ) { W_Loader.Destroy( ); W_Loader = null; }
			if ( imagesLoader != null ) { imagesLoader = null; }
			if ( tapAnimLoader != null ) { tapAnimLoader.Destroy(); tapAnimLoader = null; }
			if ( spriteSheetLoader != null ) { spriteSheetLoader.Destroy(); spriteSheetLoader = null; }
			if ( word != null ) { word.dispose(true); word = null; }
			for each ( var snd:SoundEx in singleSounds ) { if ( snd != null ) { snd.Destroy(); } }
			
			cMain.removeOptionsChanged( optionsChanged );
			
			Utils.destroyControls( controls );
			
			if ( readerToggle != null ) { readerToggle.Destroy(); }
			
			controls.length = 0;
			assets.length = 0;
			animAssets.length = 0;
		}
		
		public override function Init():void
		{			
			highlightFades = true;
			doMusicLoad();
			tapAnimLoader.Init();
			spriteSheetLoader.Init();
			W_Loader.Init();
			controls.push( sparklers );
			
			super.Init();
		}
		
		public override function Pause():void
		{
			super.Pause();
			if ( music != null ) music.Pause();
			Utils.pauseControls( controls );
		}
		
		public override function Reset():void
		{
			super.Reset();
			Utils.resetControls( controls );
		}
		
		public override function Resume():void
		{
			if (cMain.Paused) return;
			super.Resume();
			if ( music != null ) music.Resume();
			Utils.resumeControls( controls );
		}
		
		public override function Start():void
		{
			if ( !isRunning ) { optionsChanged(); }
			
			if ( music != null && step == STEP_WAIT_START ) { music.Start(); }
			
			super.Start();
			
			cMain.addOptionsChanged( optionsChanged );
			Utils.startControls( controls );
			//if ( music != null && step == STEP_WAIT_START ) { music.Start(); }
		}
		
		public override function Stop():void
		{
			if ( isRunning )
			{
				if ( music != null ) music.Stop();
				cMain.removeOptionsChanged( optionsChanged );
				Utils.stopControls( controls );
				super.Stop();
			}
		}
		
		// Public functions
		public function get loadedFrom():int { return _LoadedFrom; }
		public function set loadedFrom( LoadedFromSource:int ):void 
			{  _LoadedFrom = LoadedFromSource; }
		
		public function PageShown(LoadedFromSource:int):void
			{ loadedFrom = LoadedFromSource; }
		
		public function SetScreen( flipToPage:int, targetPage:uint, flipAfterLoad:Boolean ):Boolean
		{
			FlipToPage = flipToPage;
			pageTarget = targetPage;
			FlipAfterLoad = flipAfterLoad;
			
			return LoadAssets();
		}
		
		public function LoadStandalone( callback:Function ):void
		{
			loadedCallback = callback;
			pageTarget = STAND_ALONE;
			if ( LoadAssets() ) { callback(); }
		}
		
		public function LoadAssets():Boolean
		{
			for each ( var ldr:SpriteSheetImageLoader in imageLoaders )
			{ ldr.Init(); }
			
			
			if ( assets.length > 0 )
			{				
				
				AssetLoader = new LoaderMax( { onComplete: AssetsLoaded } );
				for ( var t:int = 0 ; t < assets.length ; t++ )
				{
					
					
					if ( Check_If_Reusable( assets[t].fileName ) )
					{
						assets[t].fileName = assets[t].fileName.substr(9);
						assets[t].addToQueue( AssetLoader, reusableFolder );
					} else { assets[t].addToQueue( AssetLoader, imageFolder ); }
				} 
			}
			
			if ( animAssets.length > 0 )
			{
				if ( AssetLoader == null ) AssetLoader = new LoaderMax( { onComplete: AssetsLoaded } );
				for ( t = 0 ; t < animAssets.length ; t++ )
				{
					if ( Check_If_Reusable( animAssets[t].imageFile ) )
					{
						if ( animAssets[t].jsonFile.indexOf( "Reusable/" ) == 0 )
						{ animAssets[t].jsonFile = animAssets[t].jsonFile.substr(9); }
						animAssets[t].imageFile = animAssets[t].imageFile.substr(9);
						animAssets[t].addToQueue( AssetLoader, reusableFolder );
					} else { animAssets[t].addToQueue( AssetLoader, imageFolder ); }
				}
			}
			
			if ( AssetLoader != null )
			{
				AssetLoader.load(true);
				return false;
			} else return true;
		}		
		private function AssetsLoaded( e:LoaderEvent ):void{
			PageLoaded = true;
			switch ( pageTarget )
			{
				case NEXT_PAGE: 
					cMain.cPages.NextPageLoaded( FlipToPage, FlipAfterLoad );
					break;
				
				case PREV_PAGE:
					cMain.cPages.PrevPageLoaded( FlipToPage, FlipAfterLoad );
					break;
				
				case FADE_PAGE:
					cMain.cPages.PageLoaded();
					break;
			}
		}
		
		// Private and protected functions
		protected function doMusicLoad():void
		{
			if ( musicFile != "" ) 
			{
				var ldr:MP3Loader = new MP3Loader( CONFIG::DATA + "sounds/music/" + musicFile + ".mp3", { autoPlay: false, onComplete: loadMusic } );
				ldr.load();
			}
		}
		
		private function doPause( e:MouseEvent ):void { Pause(); }
		private function doResume( e:MouseEvent ):void { Resume(); }
		
		protected override function handleFrame( e:Event ):void
		{
			if( isRunning ) 
			{
				if(cMain.Paused && isPaused == false) 
				{
					bPausedSetByMain = true;
					Pause();
				}
				else if(!cMain.Paused && isPaused && bPausedSetByMain) { Resume(); }
				
				if ( !isPaused && !cMain.Paused ) super.handleFrame(e);
			}
		}
		
		protected function loadMusic( e:LoaderEvent ):void
		{ 
			music = new clsMusic( e.target.content as Sound, App.musicVolume, isRunning ); 
			( e.target as MP3Loader ).dispose( true );
		}
				
		private function optionsChanged( ):void
		{ 
			for each ( var singleReader:ReaderBox in readers )
				{ singleReader.readingVolume = clsOptions.voiceVolume; } 
		}
					
		public override function tapWord( rdr:ReaderBox, e:int ):void
		{ 
			if ( !isPaused && isRunning && step == STEP_DONE )
			{
				var className:String = Utils.filterString( rdr.getWord( e ) ).toUpperCase();
				if ( GlobalData.substitutions[className] != null ) { className = GlobalData.substitutions[className]; }
				
				className = GlobalData.langFolder + "sounds/words/" + className + ".mp3";
				word = new MP3Loader( className, { autoPlay: true, volume: App.voiceVolume.volume } );
				word.load();
				
				rdr.highlightWord( e );
				TweenLite.killDelayedCallsTo( rdr.hideHighlight );
				TweenLite.delayedCall( 1, rdr.hideHighlight );
			}
		}
		
		private function Check_If_Reusable(Name:String):Boolean
		{			
			if ( Name.indexOf( "Reusable/" ) == 0 ) return true;
			return false;
		}
	}
}
