﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;
	
	import com.DaveLibrary.clsGradientDynamicText;
	import com.cupcake.App;
	import com.cupcake.ScrollingMenu;
	import com.greensock.TweenLite;
	import com.greensock.easing.Sine;
	
	public class WBZ_ScrollingMenu extends ScrollingMenu
	{
		public static var BUTTON_X:Number			= 75;
		public static var BUTTON_Y:Number			= 68;
		public static var CELL_WIDTH:Number			= 150;
		public static var TEXT_X:Number				= 0;
		public static var TEXT_Y:Number				= 55;
		public static var THUMB_X:Number			= -60;
		public static var THUMB_Y:Number			= -54;
		
		public var cMain:clsMain;
		
		public var Closed:Boolean					= true;
		public var Closing:Boolean					= false;
		public var Opened:Boolean					= false;
		public var OpenY:Number						= 0;
		public var CloseY:Number					= 0;
		public var OpenTime:Number					= .75;

		public function WBZ_ScrollingMenu( menuType:uint, callbackFunc:Function ) 
		{ 
			addChildAt( new Bitmap( GlobalData.getBitmapData( "Navbar_SliderBackground" ) ), 0 );
			super( menuType, callbackFunc ); 
		}
		
		public function addButton( iconClass:String, buttonText:String, index:int, buttonX:Number, fadeIcon:Boolean = false ):void
		{
			var btn:MovieClip	= new MovieClip;
			var bmp:Bitmap		= new Bitmap( GlobalData.getBitmapData( iconClass ) );
			
			bmp.x = THUMB_X;
			bmp.y = THUMB_Y;
			btn.addChild( bmp );
			
			var txt:clsGradientDynamicText = new clsGradientDynamicText( cMain, buttonText, cMain.gc.navbar_PopupText );
			txt.x = TEXT_X;
			txt.y = TEXT_Y;
			txt.name = "text";
			btn.addChild( txt );
			btn.textClip = txt;
			
			btn.x = buttonX;
			btn.y = BUTTON_Y;
			btn.name = "button_" + index;
			menu_mc.addChild( btn );
			
			var fade:Sprite = new IconGray;
			fade.name = "fade";
			fade.x = THUMB_X;
			fade.y = THUMB_Y;
			fade.width = bmp.width + 1;
			fade.visible = fadeIcon;
			btn.addChild( fade );
		}
		
		public function updateText( index:int, buttonText:String ):Boolean
		{
			var result:Boolean = false;
			var button:MovieClip = menu_mc.getChildByName( "button_" + index ) as MovieClip;
			if ( button != null )
			{
				var txt:clsGradientDynamicText = button.textClip;
				if ( txt != null ) { txt.text = buttonText; result = true; }
			}
			return result;
		}
		
		public function Close():void
		{
			if ( Closed ) return;
			Opened = false;
			Closing = true;
			var time:Number = ( ( CloseY - this.y ) / ( CloseY - OpenY - 30 ) ) * OpenTime;
			TweenLite.killTweensOf( this );
			TweenLite.to( this, time, { y: CloseY, ease: Sine.easeInOut, onComplete: CloseComplete } );
		}
		
		public function Open():void
		{
			if ( Opened ) return;
			Closed = false;
			var time:Number = ( ( this.y - OpenY - 30 ) / ( CloseY - OpenY - 30 ) ) * OpenTime;
			TweenLite.killTweensOf( this );
			TweenLite.to( this, time, { y: OpenY - 30, ease: Sine.easeInOut, onComplete: OpenComplete } );
			this.visible = true;
		}
		
		protected function CloseComplete():void { Closed = true; Closing = false; this.visible = false; this.Stop(); }
		protected function OpenComplete():void { Opened = true; this.Start(); }
	}
}
