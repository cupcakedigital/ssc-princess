﻿package  
{
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Scene;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.IControllable;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;	
	
	public dynamic class TapMultiLite implements IControllable
	{
		public static var page:WBZ_BookPage			= null;
		public static var addEnterFrame:Function	= null;
		public static var addMouseDown:Function		= null;
		public static var removeEnterFrame:Function	= null;
		public static var removeMouseDown:Function	= null;
		
		public var invisibleWhileInactive:Boolean;
		public var onComplete:Function				= null;
		public var onTap:Function					= null;
		public var vars:Object						= null;
		
		public var actor:MovieClip;
		public var hit:SimpleButton;
		public var tapSounds:SoundCollection		= null;
		
		private var frameSection:int				= 0;
		private var section:int						= 0;
		
		private var _endFrames:Vector.<int>			= null;
		private var _swapFrames:Vector.<int>		= null;
		private var _frameImages:Vector.<Sprite>	= null;
		
		private var _initialized:Boolean			= false;
		private var _paused:Boolean					= false;
		private var _playing:Boolean				= false;
		private var _running:Boolean				= false;
		
		// Optional vars:
		// align (int): how to align the images (default: Align.UPPER_LEFT)
		// actorClip (MovieClip): animation container
		// button (SimpleButton): button that triggers the control
		// endFrames (Vector.<int>): the vector of frames the animation will stop on
		// flipFrames (Vector.<int>): the vector of frames the image will flip to the next image
		// fromFiles (Boolean): if true, will pull the images from individual files rather than the Page sprite sheet
		// imageName (String): the name of the sprite sheet images or files (minus the .png extension) to use
		// invisible (Boolean): if true, the animation will be invisible while not being played
		// onTap (Function): callback function when the control is tapped
		// onComplete (Function): callback function when an animation section has completed
		// reusable (Boolean): if true, draw images from the reusable folder
		// sounds (SoundCollection): the sounds to play when the animation is tapped
		public static function Add( asset:String, vars:Object ):TapMultiLite
		{
			var control:TapMultiLite	= null;
			var actorClip:MovieClip		= ( vars.actorClip ) ? ( ( vars.actorClip is MovieClip ) ? vars.actorClip : page.scene_mc.getChildByName( vars.actorClip ) as MovieClip )
														 	 : ( page.scene_mc.getChildByName( asset ) as MovieClip );
			var button:SimpleButton 	= ( vars.button ) ? ( ( vars.button is SimpleButton ) ? vars.button : page.scene_mc.getChildByName( vars.button ) as SimpleButton )
													  	  : ( page.scene_mc.getChildByName( asset + "_btn" ) as SimpleButton );
			
			if ( actorClip == null ) { App.Log( "TapMultiLite.Add(): actorClip '" + asset + "' is null." ); return null; }
			if ( actorClip.actor == null ) { App.Log( "TapMultiLite.Add(): actorClip does not contain an actor" ); return null; }
			
			// Search actorClip and vars for endFrames and stopFrames
			var imageName:String 		= ( vars.imageName ) ? vars.imageName : asset;
			var images:Vector.<String>	= new Vector.<String>;
			var scene:Scene				= actorClip.currentScene;
			var fromFiles:Boolean		= ( vars.fromFiles ) ? vars.fromFiles : false;
			var reusable:Boolean		= ( vars.reusable ) ? vars.reusable : false;
			var prefix:String			= ( reusable ) ? "Reusable/" : "";
			var suffix:String			= fromFiles ? ".png" : "";
			var frameLabel:String		= "";
			
			images.push( imageName + "01" + suffix );
			
			var endFrames:Vector.<int>	= ( vars.endFrames ) ? vars.endFrames : null;
			if ( endFrames == null )
			{
				endFrames = new Vector.<int>;
				for ( var t:int = 0 ; t < scene.labels.length ; t++ )
				{
					frameLabel = scene.labels[t].name;
					if ( frameLabel.indexOf( "stop" ) > -1 || frameLabel.indexOf( "end" ) > -1 )
						{ endFrames.push( scene.labels[t].frame ); }
				}
			}
			
			var flipFrames:Vector.<int> = ( vars.flipFrames ) ? vars.flipFrames : null;
			if ( flipFrames == null )
			{
				var frameNum:String;
				flipFrames = new Vector.<int>;
				for ( var u:int = 0 ; u < scene.labels.length ; u++ )
				{
					frameLabel = scene.labels[u].name;
					if ( frameLabel.indexOf( "flip_" ) == 0 )
					{
						frameNum = frameLabel.substr( 5 );
						images.push( prefix + imageName + frameNum + suffix );
						flipFrames.push( scene.labels[u].frame );
					}
				}
			}
			
			if ( endFrames.length > 0 && flipFrames.length > 0 )
			{
				var align:int			= ( vars.align ) ? vars.align : Align.UPPER_LEFT;
				var invisible:Boolean 	= ( vars.invisible ) ? vars.invisible : false;
				var onTap:Function 		= ( vars.onTap ) ? vars.onTap : null;
				var onComplete:Function = ( vars.onComplete ) ? vars.onComplete : null;
				var externalSounds:Boolean = vars.hasOwnProperty( "sounds" );
				var sounds:SoundCollection = ( vars.sounds ) ? vars.sounds : SoundCollection.GetSounds( asset, vars );
				var frameImages:Vector.<Sprite> = fromFiles ? ExternalImage.getBatch( images, page.assets, actorClip.actor, align, false, true )
															: page.imagesLoader.GetBatch( images, actorClip.actor, align, true );
				control = new TapMultiLite( button, actorClip, endFrames, flipFrames, frameImages, invisible, sounds, onTap, onComplete );
				control.vars = vars;
				page.controls.push( control );
				if ( sounds != null && !externalSounds ) { page.controls.push( sounds ); }
				page.CheckSparkler( actorClip.name, button );
			}
			
			return control;
		}

		public function TapMultiLite( hitButton:SimpleButton, actorClip:MovieClip, endFrames:Vector.<int>, swapFrames:Vector.<int>, 
									  frameImages:Vector.<Sprite>, invisible:Boolean = true, sounds:SoundCollection = null, doOnTap:Function = null, doOnComplete:Function = null ) 
		{
			hit = hitButton;
			actor = actorClip;
			invisibleWhileInactive = invisible;
			tapSounds = sounds;
			onTap = doOnTap;
			onComplete = doOnComplete;
			_endFrames = endFrames;
			_swapFrames = swapFrames;
			_frameImages = frameImages;
		}
		
		public function doTap( overridePlaying:Boolean = false ):Boolean
		{
			var res:Boolean = false;
			var cancel:Boolean = false;
			if ( isRunning && !isPaused && ( !_playing || overridePlaying ) )
			{
				if ( onTap != null ) cancel = onTap( this );
				
				if ( !cancel )
				{
					res = true;
					if ( tapSounds != null && page.step == BookPage.STEP_DONE ) { tapSounds.PlayRandom(); }
					actor.visible = true;
					_playing = true;
					startEnterFrame();
				}
			}
			return res;
		}
		
		public final function get isInitialized():Boolean { return _initialized; }
		public final function get isPaused():Boolean { return _paused; }
		public final function get isRunning():Boolean { return _running; }
		
		public final function Destroy():void { if ( isRunning ) Stop(); }
		
		public final function Init():void
		{
			if ( isInitialized ) return;
			
			_initialized = true;
		}
		
		public final function Pause():void
		{ 
			if ( isRunning && !isPaused )
			{
				if ( _playing ) stopEnterFrame();
				_paused = true;
			} 
		}
		
		public final function Reset():void
		{
			if ( !isInitialized ) Init(); 
			if ( isRunning ) Stop(); 
			
			if ( invisibleWhileInactive ) actor.visible = false;
			actor.gotoAndStop(1);
			frameSection = 0;
			section = 0;
			actor.actor.addChild( _frameImages[frameSection] );
			_playing = false;
		}
		
		public final function Resume():void
		{
			if ( isRunning && isPaused )
			{
				if ( _playing ) startEnterFrame();
				_paused = false;
			}
		}
		
		public final function Start():void
		{
			if ( !isInitialized ) Init();
			if ( isRunning ) Reset();
		
			startMouseDown();
		
			_running = true;
			_paused = false;
		}
		
		public final function Stop():void
		{
			if ( isRunning )
			{
				//if ( tapSound != null ) tapSound.Stop();
				stopMouseDown();
				stopEnterFrame();
				_running = false;
			}
		}
		
		// Private and protected functions
		private final function handleFrame( e:Event ):void
		{
			if ( isRunning && !isPaused && _playing )
			{				
				if ( actor.currentFrame == _endFrames[section] )
				{
					if ( invisibleWhileInactive ) actor.visible = false;
					_playing = false;
					section++;
					
					if ( section >= _endFrames.length ) 
					{
						actor.gotoAndStop(1);
						section = 0;
						frameSection = 0;
						actor.actor.addChild( _frameImages[frameSection] );
					}

					stopEnterFrame();
					if ( onComplete != null ) onComplete( this );
				} else { actor.gotoAndStop( actor.currentFrame + 1 ); }
				
				if ( actor.currentFrame == _swapFrames[frameSection] ) 
				{
					frameSection++;
					Utils.ClearContainer( actor.actor );
					actor.actor.addChild( _frameImages[frameSection] );
					if ( frameSection >= _swapFrames.length ) { frameSection = 0; }
				}
			}
		}
		
		private final function hitTapped( e:MouseEvent ):void
			{ if ( isRunning && !isPaused && e.target == hit ) { doTap(); } }
		
		private final function startEnterFrame():void
		{
			if ( addEnterFrame != null ) addEnterFrame( handleFrame );
				else Utils.addHandler( actor, Event.ENTER_FRAME, handleFrame );
		}
		
		private final function startMouseDown():void
		{
			if ( addMouseDown != null ) addMouseDown( hitTapped );
				else Utils.addHandler( hit, MouseEvent.MOUSE_DOWN, hitTapped );
		}
		
		private final function stopEnterFrame():void
		{
			if ( removeEnterFrame != null ) removeEnterFrame( handleFrame );
				else Utils.removeHandler( actor, Event.ENTER_FRAME, handleFrame );
		}
		
		private final function stopMouseDown():void
		{
			if ( removeMouseDown != null ) removeMouseDown( hitTapped );
				else Utils.removeHandler( hit, MouseEvent.MOUSE_DOWN, hitTapped );
		}
	}
}
