﻿package  
{
	CONFIG::NOOK { import bn.nook.api.nativeExtensions.NOOKAPI.NOOKAPI; }
	
	// Imports
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.PNGEncoderOptions;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getTimer;
	import flash.xml.*;
	
	import asData.clsGlobalConstants;
		
	import com.DaveLibrary.clsMsgBox;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.DaveLibrary.clsStandardButton;
	import com.DaveLibrary.clsSupport;
	import com.DaveLibrary.clsXMLtoDict;
	import com.DaveLibrary.Stats;
	
	import com.cupcake.App;
	import com.cupcake.BookPage;
	import com.cupcake.Console;
	import com.cupcake.DeviceInfo;
	import com.cupcake.MemoryTracker;
	import com.cupcake.OrientationHandler;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.greensock.events.TweenEvent;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.ImageLoader;
	import com.greensock.events.LoaderEvent;
	import flash.filesystem.File;
	
	
	public class clsMain extends MovieClip 
	{
		// To hide menu and arrows for screen shots
		private var PrintScreenMode:Boolean 		= false;
		private var PreloadNext:Boolean				= false;
		private var PreloadPrev:Boolean				= false;
		// ****************************************************************************************
		
		public static var APP_WIDTH:int				= 1366;
		public static var APP_HEIGHT:int			= 768;
		public static var APP_X:int					= -171;
		public static var APP_Y:int					= 0;
		
		public static var MOVIE_WIDTH:int			= 1024;
		public static var MOVIE_HEIGHT:int			= 768;
		
		public static var Main:clsMain;
		public static var isSlow:Boolean			= true;
		public var loader_mc:MovieClip;
		
		public var backPressed:Boolean				= false;
		public var playMode:int						= BookPage.MODE_READ_TO_ME;
		public var appQuality:String				= StageQuality.LOW;
		public var appRect:Rectangle;				
				
		public var _Paused:Boolean					= false;
		
		
		public static var firstTurn:Boolean			= true;

		/*public var soundTrans:clsSoundTransformEX;
		public var musicTrans:clsSoundTransformEX;
		public var voiceTrans:clsSoundTransformEX;*/
		
		private var EnterFrameCallbacks:Vector.<Function>		= new Vector.<Function>;
		private var StageMouseDownCallbacks:Vector.<Function>	= new Vector.<Function>;
		private var StageMouseUpCallbacks:Vector.<Function>		= new Vector.<Function>;
		private var OptionsChangedCallbacks:Vector.<Function>	= new Vector.<Function>;
		
		public var IsMouseDown:Boolean;
		private var MouseDownPoint:Point		= new Point();
		private var LastMouseX:int				= 0;
		
		public var Modal:Array					= new Array();
		
		public var CurrentPage:int				= -1;
		public var ReturnPage:int				= -1;
		
		public var AutoPageTurn_InProgress:clsPageTurn	= null;
		public var AutoPageTurn_ToPage:int				= 0;
		public var AutoPageTurnPlaySound:Boolean		= false;
		public var LoadingNext:Boolean					= false;
		public var LoadingPrev:Boolean					= false;
		public var PageTurnNext:clsPageTurn;
		public var PageTurnPrev:clsPageTurn;
		public var PageTurnPrevLoaded:Boolean 			= false;
		public var PageTurnNextLoaded:Boolean 			= false;
		
		private var CrossFade_LoadedFrom:int;

		private var bmBlack:Sprite;
		private var bmTrans:Bitmap;
		public var moreApps:MoreApps					= null;
		private var transRect:Rectangle;
		public var cPages:clsPages;
		private var CrossFadeTween:TweenLite			= null;
		private var CrossFade_FadeToImage:Bitmap		= null;
		private var cSplashScreens:clsSplashScreens;
		private var msgbox:clsMsgBox;
		public var PageTurnArrowPrev:clsStandardButton;
		public var PageTurnArrowNext:clsStandardButton;
		
		public var cMainMenu:clsMainMenu;
		public var cNavBar:mcNavBar;
		public var cOptionsDialog:clsOptionsDialog;
		public var cVideoBox:WBZ_VideoBox; 
		public var gc:clsGlobalConstants				= null;
		public var gStrings:Dictionary;
		public var MainMenuBitmap:Bitmap;
		public var MainMenuSprite:Sprite;
		public var PageObjects:Sprite;
		public var profileImage:BitmapData				= null;
		public var stats:Sprite;
		
		private var credits:Credits						= null;
		private var creditsLoader:ImageLoader			= null;
		private var help:MovieClip						= null;
		private var helpLoader:LoaderMax				= null;
		private var games:MovieClip						= null;
		private var gamesLoader:LoaderMax				= null;
		private static var MouseIntercept:Sprite;
		
		// Properties
		public function get Paused():Boolean { return _Paused; }
		public function set Paused(IsPaused:Boolean):void { _Paused = IsPaused; }
		
		// Screen sizing
		public static var screenWidth:int;
		public static var screenHeight:int;
		
		public static var screenRatio:Number;
		public static var movieRatio:Number;
		
		public static var offsetX:Number = 0;
		public static var offsetY:Number = 0;
		public static var diffRatio:Number;
		
		public static var localWidth:int;
		public static var localHeight:int;
		
		private var preTime:int;
		private var postTime:int;
				
		// Constructor and other Initialization functions.
		public function clsMain() 
		{
			Main = this;
			
			// Measure screen size and offsets for placing outside of stage area
			screenWidth = stage.fullScreenWidth;
			screenHeight = stage.fullScreenHeight;
			
			screenRatio = screenHeight / screenWidth;
			movieRatio = MOVIE_HEIGHT / MOVIE_WIDTH;
			
			// calculate offsets
			if ( screenRatio > movieRatio )
			{
				diffRatio = MOVIE_WIDTH / screenWidth;
				localHeight = screenHeight * diffRatio;
				localWidth = MOVIE_WIDTH;
			}
			else
			{
				diffRatio = MOVIE_HEIGHT / screenHeight;
				localHeight = MOVIE_HEIGHT;
				localWidth = screenWidth * diffRatio;
			}
			
			if ( localWidth > APP_WIDTH ) { localWidth = APP_WIDTH; }
			if ( localHeight > APP_HEIGHT ) { localHeight = APP_HEIGHT; }
			offsetX = ( localWidth - MOVIE_WIDTH ) / 2;
			offsetY = ( localHeight - MOVIE_HEIGHT ) / 2;
			
			Console.Setup( this );
			if ( !CONFIG::DEBUG ) { Console.toggleButton();	}
			
			DeviceInfo.Init();
			isSlow = DeviceInfo.isSlow;
			if ( DeviceInfo.platform != DeviceInfo.PLATFORM_IOS ) { refreshTouch(); }
			setKeepAwake();
			
			// Get current orientation (which must be landscape) and set possible orientations
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_IOS || CONFIG::MARKET == "amazon" ) 
				{ OrientationHandler.Start( stage ); }
			
			// load the options from file.
			clsOptions.Load();
			
			// Set up our App vars
			App.Setup( this, { soundVolume: clsOptions.soundVolume, musicVolume: clsOptions.musicVolume, 
					  		   voiceVolume: clsOptions.voiceVolume, logFunc: MessageLog } );
			
			AppCode.preLoad();
			appRect = new Rectangle( APP_X, APP_Y, APP_WIDTH, APP_HEIGHT );
			
			GlobalData.Init( clsOptions.language, fadeLoader );
		}
		
		public function fadeLoader():void
			{ TweenLite.to( loader_mc, .5, { alpha: 0, onComplete:Init } ); }
		
		public function Init():void
		{
			removeChild( loader_mc );
			loader_mc = null;
						
			gc = new clsGlobalConstants; 
			
			AppCode.postLoad();
			
			// get information about the device this app is running on	
			if ( DeviceInfo.platform != DeviceInfo.PLATFORM_DESKTOP && 
				 gc.debug_ForceDisabledOnDevice )
				gc.debug_Enabled = false;
			
			if( !gc.debug_Enabled )
			{
				gc.debug_showCursors = false;
				gc.debug_ButtonHitAreaAlpha = 0;
				gc.debug_showStats = false;
			}
			
			PageObjects = new Sprite();
			PageObjects.name = "PageObjects";
			this.addChildAt(PageObjects,0);

			// create our state object.
			stats = new Stats();
			stats.name = "Stats";
			stats.scaleX = gc.debug_StatsScale;
			stats.scaleY = gc.debug_StatsScale;
			
			// Load the strings file.
			LoadStrings();
			
			cVideoBox = new WBZ_VideoBox(this); 

			// add our stage handlers.
			changeQuality( appQuality );
			Utils.addHandler( stage, Event.DEACTIVATE, windowNotActive );
			Utils.addHandler( stage, Event.ACTIVATE, windowActive );
			Utils.addHandler( stage, Event.ENTER_FRAME, ENTER_FRAME );
			Utils.addHandler( stage, MouseEvent.MOUSE_DOWN, MOUSE_DOWN );
			Utils.addHandler( stage, MouseEvent.MOUSE_UP, MOUSE_UP );

			// Last thing we need to do is initialize our page bitmaps.
			cPages = new clsPages(this);
			
			MainMenuSprite = new Sprite;
			
			PageTurnArrowPrev = new clsStandardButton(this, "", gc.pageTurn_ArrowSettings, GlobalData.getSheetBitmap( "Arrow_Left_s" ), GlobalData.getSheetBitmap( "Arrow_Left_h" ), PageTurnArrowPrev_MouseUp, null, null, null, true, false);
			PageTurnArrowPrev.name = "PageTurnArrowPrev";
			PageTurnArrowPrev.x = -offsetX + PageTurnArrowPrev.width / 2;
			PageTurnArrowPrev.y = gc.pageTurn_Arrows_Previous.y;
			stats.y = PageTurnArrowPrev.y + PageTurnArrowPrev.height/2;
			PageTurnArrowPrev.visible = false;
			this.addChild(PageTurnArrowPrev);
			
			PageTurnArrowNext = new clsStandardButton(this, "", gc.pageTurn_ArrowSettings, GlobalData.getSheetBitmap( "Arrow_Right_s" ), GlobalData.getSheetBitmap( "Arrow_Right_h" ), PageTurnArrowNext_MouseUp, null, null, null, true, false);
			PageTurnArrowNext.name = "PageTurnArrowNext";
			PageTurnArrowNext.x = MOVIE_WIDTH + offsetX - PageTurnArrowNext.width / 2;
			PageTurnArrowNext.y = gc.pageTurn_Arrows_Next.y;
			PageTurnArrowNext.visible = false;
			this.addChild(PageTurnArrowNext);
						
			cSplashScreens = new clsSplashScreens(this);
			cSplashScreens.name = "cSplashScreens";
			this.addChild(cSplashScreens);
			
			cNavBar = new mcNavBar();
			cNavBar.name = "cNavBar";
			if ( PrintScreenMode ) { cNavBar.visible = false; }
			cNavBar.Initialize(this);				
			cNavBar.alpha = 0;
			this.addChild(cNavBar);
			
			bmBlack = new BlackFill;
			bmBlack.name = "bmBlack";
			bmBlack.x = APP_X;
			bmBlack.width = APP_WIDTH;
			bmBlack.height = APP_HEIGHT;
			bmBlack.mouseEnabled = false;
			bmBlack.mouseChildren = false;			
			bmBlack.alpha = 0;
			this.addChild(bmBlack);	
									
			cSplashScreens.Start();
			
			// Bring console back up to top
			Console.bringToTop();
		}
		
		public function PreMainMenuInit():void
		{
			cMainMenu = new clsMainMenu();
			cMainMenu.RestartMusic = false;
			cMainMenu.Reset();
			PageObjects.addChild(cMainMenu);
			bmBlack.alpha = 1;
			TweenLite.to( bmBlack, 1, { alpha: 0, onComplete: fadeComplete, ease: Linear.easeNone } );
		}
		
		private final function fadeComplete():void { removeChild( bmBlack ); bmBlack = null; }
									
		private function LoadStrings():void
		{
			var x2d:clsXMLtoDict = new clsXMLtoDict();
			gStrings = x2d.Convert(GlobalData.stringsXML);
		}
		
		// Enter Frame and Mouse Handlers
		private final function ENTER_FRAME(e:Event)
		{			
			// call all the enter frame functions in our callback list.
			for ( var i:int = 0; i < EnterFrameCallbacks.length; i++) 
			{
				if (gc.debug_Enabled) { EnterFrameCallbacks[i](e); }
				else 
				{
					try { EnterFrameCallbacks[i](e); } 
						catch(err:Error) { App.Log( "EnterFrameCallbacks error:" + err.message); }
				}
			}
			
			if ( AutoPageTurn_InProgress != null ) { ProcessPageTurn(); }
			
			if ( stats != null && this.contains( stats ) ) this.setChildIndex(stats, this.numChildren-1);			
		}

		private function MOUSE_DOWN(e:MouseEvent):void
		{
			IsMouseDown = true;
			MouseDownPoint.x = stage.mouseX;
			MouseDownPoint.y = stage.mouseY;
			LastMouseX = stage.mouseX;
						
			for ( var i:int = 0; i < StageMouseDownCallbacks.length; i++) 
			{
				//try 
				{ StageMouseDownCallbacks[i](e); }
					//catch(err:Error) { App.Log( "Error in stage mouse down callback: " + err.message ); }
			}
		}

		private function MOUSE_UP(e:MouseEvent):void
		{
			IsMouseDown = false;
			
			for ( var i:int = 0; i < StageMouseUpCallbacks.length; i++) {
				//try
				{
					StageMouseUpCallbacks[i](e);
				//} catch(e:Error) {
				}
			}		
		}			
		
		private function PageTurnArrowPrev_MouseUp(e:MouseEvent):void
			{ PrevPage( CurrentPage -1 ); }
		
		private function PageTurnArrowNext_MouseUp(e:MouseEvent):void
			{ NextPage( CurrentPage + 1 ); }
		
		// Page intialization and navigation
		public function InitializePages(page:int, LoadedFrom:int, AutoStart:Boolean = true):void
		{
			if ( cMainMenu != null && page >= 0 ) 
			{ 
				PageObjects.removeChild( cMainMenu );
				cMainMenu.Destroy(); 
				cMainMenu = null; 
			}
						
			// Remove whichever page turn was used
			if ( PageTurnNext != null ) { PageTurnNext.destroy(); PageTurnNext = null; }
			if ( PageTurnPrev != null ) { PageTurnPrev.destroy(); PageTurnPrev = null; }
			
			if ( LoadedFrom == WBZ_BookPage.FROM_ARCADE_MENU )
			{
				if ( DeviceInfo.platform != DeviceInfo.PLATFORM_IOS ) { refreshTouch(); }
				PageObjects.addChildAt( cPages.CurrentPage, 0 );
				cPages.CurrentPage.PageShown(LoadedFrom);
				cPages.CurrentPage.Start();
				this.hideArrows();
				if ( CurrentPage > -2 ) ReturnPage = CurrentPage;
				CurrentPage = -2;
				return;
			}
			
			// No matter what else is happening, we need to remove
			// the current page from the stage
			if ( cPages.CurrentPage != null && PageObjects.contains( cPages.CurrentPage ) )
			{
				//App.Log( "Removing " + cPages.CurrentPage.pageName + " from stage." );
				cPages.CurrentPage.Reset();
				PageObjects.removeChild( cPages.CurrentPage );
			}
			
			// Decide what to do with currently loaded page
			if ( page > CurrentPage )
			{
				if ( page > CurrentPage + 1 && page > 0 ) 
				{
					// We won't be using current page for anything, so destroy it
					// but only if we're in interactive mode
					if ( clsOptions.interactive ) { cPages.UnloadCurrentPage(); }
					cPages.CurrentPage = null;
					
					// Initialize the previous page since we have no
					// active object to work with
					if ( PreloadPrev ) { InitPrev( page - 1 ); }
				}
				else if ( page > 0 )
				{
					// In this case, we already have an active object
					// to initialize our page with
					if ( clsOptions.interactive ) 
					{
						if ( cPages.PrevPage != null ) { cPages.UnloadCurrentPage( cPages.PrevPage ); cPages.PrevPage = null; }
						
						// Reset the current page and make previous page if it exists
						if ( cPages.CurrentPage != null )
						{
							cPages.CurrentPage.Reset();
							cPages.PrevPage = cPages.CurrentPage;
							PageTurnPrevLoaded = true;
						}
					}
					else 
					{ 
						cPages.PrevStaticBitmap.bitmapData.drawWithQuality( cPages.StaticBitmap, null, null, null, null, false, StageQuality.BEST );
						PageTurnPrevLoaded = true;
					}
				}
				else if ( page == 0 ) { PageTurnPrevLoaded = true; }
				
				if ( clsOptions.interactive ) 
				{
					cPages.CurrentPage = cPages.NextPage;
					cPages.NextPage = null;
				}
				else cPages.StaticBitmap.bitmapData.drawWithQuality( cPages.NextStaticBitmap, null, null, null, null, false, StageQuality.BEST );
				
				if ( page < int( cPages.PageOrder.length ) - 1 ) { if ( PreloadNext ) { InitNext( page + 1 ); } }
					else { PageTurnNextLoaded = true; }
			}
			else if ( page < CurrentPage )
			{
				// Same process as above, but with Next and Prev pages switched
				if ( page < CurrentPage - 1 )
				{
					if ( clsOptions.interactive ) cPages.UnloadCurrentPage();
					cPages.CurrentPage = null;
					if ( PreloadNext && page > -1 ) { InitNext( page + 1 ); }
				}
				else
				{
					if ( clsOptions.interactive )
					{
						if ( cPages.NextPage != null ) { cPages.UnloadCurrentPage( cPages.NextPage ); cPages.NextPage = null; }
						
						// Reset the current page and make next page if it exists
						if ( cPages.CurrentPage != null )
						{
							cPages.CurrentPage.Reset();
							cPages.NextPage = cPages.CurrentPage;
							PageTurnNextLoaded = true;
						}
					}
					else 
					{
						cPages.NextStaticBitmap.bitmapData.drawWithQuality( cPages.StaticBitmap, null, null, null, null, false, StageQuality.BEST );
						PageTurnNextLoaded = true;
					}
				}
				
				if ( clsOptions.interactive )
				{
					if ( page > -1 ) { cPages.CurrentPage = cPages.PrevPage; }
						else { cPages.UnloadCurrentPage( cPages.PrevPage ); }
					cPages.PrevPage = null;
				}
				else cPages.StaticBitmap.bitmapData.drawWithQuality( cPages.PrevStaticBitmap, null, null, null, null, false, StageQuality.BEST );
				
				if ( page > 0 ) { if ( PreloadPrev ) { InitPrev( page - 1 ); } }
					else { PageTurnPrevLoaded = true; }
			}
			
			CurrentPage = page;
			
			if(page >= 0) 
			{
				if ( !clsOptions.interactive ) 
				{ 
					cPages.CurrentPage = cPages.StaticPage; 
					cPages.StaticPage.visible = true; 
				}
				else if ( DeviceInfo.platform != DeviceInfo.PLATFORM_IOS ) { /*refreshTouch();*/ }
				
				PageObjects.addChildAt(cPages.CurrentPage, 0);
				cPages.CurrentPage.PageShown(LoadedFrom);
				
				if ( AutoStart ) 
				{
					CheckPageArrows();
					StartCurrentPage();
				}		
			} 
			else 
			{
				if ( cMainMenu == null ) { cMainMenu = new clsMainMenu(); }
				cMainMenu.Reset();
				PageObjects.addChild(cMainMenu);
				cMainMenu.Start();
				
				if ( cPages.PrevPage != null ) { cPages.UnloadCurrentPage( cPages.PrevPage ); cPages.PrevPage = null; }
				if ( cPages.CurrentPage != null ) { cPages.UnloadCurrentPage( cPages.CurrentPage ); cPages.CurrentPage = null; }
				if ( cPages.NextPage != null ) { cPages.UnloadCurrentPage( cPages.NextPage ); cPages.NextPage = null; }
				
				this.hideArrows();
				
				LoadingNext = false;
				LoadingPrev = false;
			}		
										
			if(CurrentPage >= 0)
			{
				if ( CurrentPage < int( cPages.PageOrder.length ) - 1 )
					{ clsOptions.lastPage = CurrentPage; }
				else 
				{
					clsOptions.bookCompleted = true;
					clsOptions.lastPage = 0;
				}
					
				clsOptions.Save();
			}
		}
		
		public function CheckPageArrows():void
		{
			if ( 
				 ( cPages.CurrentPage != null  
				   && ( PageTurnNextLoaded || !PreloadNext )
				   && ( PageTurnPrevLoaded || !PreloadPrev ) )
				 || playMode == BookPage.MODE_JUST_A_BOOK 
			   )
			{ showArrows(); }
		}
		
		private function StartCurrentPage():void
		{
			if ( !cPages.CurrentPage.isRunning ) { cPages.CurrentPage.Start(); }
			if ( !PreloadNext ) { showArrows(); }
		}
		
		private function InitPrev( pageNum:int = -1, turnWhenLoaded:Boolean = false ):void
		{
			if ( LoadingPrev ) { return; }
			if ( turnWhenLoaded ) { hideArrows( true ); }
			var pg:int = ( pageNum > -1 ) ? pageNum : CurrentPage - 1;
			LoadingPrev = true;
			if ( turnWhenLoaded && !PreloadNext ) { TakeCurrentSnapshot( true ); }
			cPages.LoadPrevPage( pageNum, turnWhenLoaded );
		}
		
		private function InitNext( pageNum:int = -1, turnWhenLoaded:Boolean = false ):void
		{
			if ( LoadingNext ) { return; }
			if ( turnWhenLoaded ) { hideArrows( true ); }
			var pg:int = ( pageNum > -1 ) ? pageNum : CurrentPage + 1;
			LoadingNext = true;
			if ( turnWhenLoaded && !PreloadPrev ) { TakeCurrentSnapshot( true ); }
			cPages.LoadNextPage( pageNum, turnWhenLoaded );
		}
			
		public function showMainMenu(DoCrossFade:Boolean = true):void
		{
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID || DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) 
				{ Utils.addHandler( NativeApplication.nativeApplication, KeyboardEvent.KEY_DOWN, handleKeys ); }
			
			// remove the splash screen object if it's still loaded.
			if(cSplashScreens != null) 
			{
				removeChild( cSplashScreens );
				cSplashScreens.destroy();
				cSplashScreens = null;
				cNavBar.Activate();
				cNavBar.alpha = 1;
			}			
			
			if ( cMainMenu == null )
			{
				cMainMenu = new clsMainMenu();
				PageObjects.addChild(cMainMenu);
			}
			cMainMenu.Start();

			checkMainMenuBitmap();
		}
		
		public function checkMainMenuBitmap():void
		{
			// Make main menu bitmap if it doesn't exist
			if ( MainMenuBitmap == null )
			{
				//trace("cMainMenu.scene_mc.BlackScreen: " + cMainMenu.scene_mc.BlackScreen);
				cMainMenu.scene_mc.BlackScreen.visible = false;
				cMainMenu.showAllButtonStart();
				MainMenuBitmap = Utils.createCroppedBitmap(cMainMenu, new Rectangle( APP_X, APP_Y, APP_WIDTH, APP_HEIGHT ) );
				MainMenuBitmap.x = APP_X;
				MainMenuSprite.addChild(MainMenuBitmap);
				cMainMenu.scene_mc.BlackScreen.visible = true;
				cMainMenu.hideAllButton();
			}
		}

		private function ProcessPageTurn():void
		{
			if(this.Paused) return;
			
			if(AutoPageTurn_InProgress != null) 
			{					
				if(AutoPageTurnPlaySound) 
				{
					AutoPageTurnPlaySound = false;
					if(AutoPageTurn_InProgress == PageTurnPrev) 
						GlobalData.playSound( "PageFlip_Back" );
					else 
						GlobalData.playSound( "PageFlip_Next" );
				}				
				
				if(AutoPageTurn_InProgress.percentage < 1) {
					if(AutoPageTurn_InProgress.percentage + gc.pageTurn_FlipSpeed < 1) {
						AutoPageTurn_InProgress.redraw(AutoPageTurn_InProgress.percentage + gc.pageTurn_FlipSpeed);
					} else {
						AutoPageTurn_InProgress.redraw(1);
					}
				} else {
					MouseDownPoint.x = stage.mouseX;
					MouseDownPoint.y = stage.mouseY;
					LastMouseX = stage.mouseX;
					AutoPageTurn_InProgress = null;
					if ( bmTrans != null ) { bmTrans.bitmapData.dispose(); bmTrans = null; }
					InitializePages(AutoPageTurn_ToPage, 0, true);
				}					
			}
		}
		
		public function StopCurrentPage():void
		{
			PageTurnPrevLoaded = false;
			PageTurnNextLoaded = false;
			if ( cMainMenu != null ) cMainMenu.Stop();
			if ( cPages.CurrentPage != null ) cPages.CurrentPage.Stop();
		}
		
		public function AddPageTurnNext():void
		{
			PageTurnNext = new clsPageTurn(this,1, APP_WIDTH, APP_HEIGHT, APP_X);
			PageTurnNext.name = "PageTurnNext";
			PageTurnNext.visible = false;
			if ( bmTrans != null )
				{ this.addChildAt(PageTurnNext, this.getChildIndex( bmTrans ) + 1); }
			else { this.addChildAt(PageTurnNext, this.getChildIndex( PageObjects ) + 1); }
			PageTurnNext.InitializeHelperObjects();
		}
		
		public function AddPageTurnPrev():void
		{
			PageTurnPrev = new clsPageTurn(this,-1, APP_WIDTH, APP_HEIGHT, APP_X);
			PageTurnPrev.name = "PageTurnPrev";
			PageTurnPrev.visible = false;
			if ( bmTrans != null )
				{ this.addChildAt(PageTurnPrev, this.getChildIndex( bmTrans ) + 1); }
			else { this.addChildAt(PageTurnPrev, this.getChildIndex( PageObjects ) + 1); }
			PageTurnPrev.InitializeHelperObjects();
		}
		
		public function AddTransitionBitmap():void
		{
			bmTrans = new Bitmap( new BitmapData( APP_WIDTH, APP_HEIGHT ) );
			bmTrans.x = APP_X;
		}
		
		public function AddFadeToBitmap():void
		{
			CrossFade_FadeToImage = new Bitmap( new BitmapData( APP_WIDTH, APP_HEIGHT ) );
			CrossFade_FadeToImage.x = APP_X;
		}
		
		private function TakeCurrentSnapshot( UnloadPage:Boolean ):void
		{
			// draw the current page into the transition bitmap
			// and then remove the current page
			if ( cPages.CurrentPage != null && PageObjects.contains( cPages.CurrentPage ) )
			{
				AddTransitionBitmap();
				transRect = cPages.CurrentPage.scrollRect;
				cPages.CurrentPage.scrollRect = appRect;
				bmTrans.bitmapData.drawWithQuality( cPages.CurrentPage, null, null, null, null, false, StageQuality.BEST );
				cPages.CurrentPage.scrollRect = transRect;
				
				PageObjects.removeChild( cPages.CurrentPage );
				
				if ( UnloadPage )
				{
					cPages.UnloadCurrentPage( cPages.CurrentPage );
					cPages.CurrentPage = null;
				}
				else { cPages.CurrentPage.Reset(); }
				
				addChildAt( bmTrans, this.getChildIndex( PageObjects ) + 1 );
			}
		}
		
		public function NextPage(GoToPage:int = -1):void
		{
			preTime = getTimer();
			if( GoToPage < 0 ) { GoToPage = CurrentPage + 1; }
			if( GoToPage < cPages.PageOrder.length && GoToPage >= 0 ) 
			{				
				if ( GoToPage < CurrentPage ) { PrevPage( GoToPage ); }
				else
				{
					if ( cPages.PrevPage != null ) 
					{ 
						cPages.UnloadCurrentPage( cPages.PrevPage ); 
						cPages.PrevPage = null; 
					}
					
					if ( GoToPage == CurrentPage + 1 && CurrentPage != -1 && PageTurnNextLoaded ) 
						{ StartNextPageTurn( GoToPage ); }
					else { InitNext( GoToPage, true ); }
				}
			} else PrevPage(-1);
		}
		
		// Main Menu is -1. 
		public function PrevPage(GoToPage:int = -2):void
		{
			preTime = getTimer();
			
			if(GoToPage < -1) GoToPage = CurrentPage - 1;
			if(GoToPage == -1) 
			{
				if ( cPages.PrevPage != null ) { cPages.UnloadCurrentPage( cPages.PrevPage ); cPages.PrevPage = null; }
				if ( cPages.NextPage != null ) { cPages.UnloadCurrentPage( cPages.NextPage ); cPages.NextPage = null; }
				StartPrevPageTurn( GoToPage );
			}  
			else 
			{
				if ( cPages.NextPage != null ) { cPages.UnloadCurrentPage( cPages.NextPage ); cPages.NextPage = null; }
				if ( GoToPage == CurrentPage - 1 && PageTurnPrevLoaded ) { StartPrevPageTurn( GoToPage ); }
				else { InitPrev( GoToPage, true ); }
			}
		}
		
		public function StartNextPageTurn( GoToPage ):void
		{
			postTime = getTimer();
			//App.Log( "Time to initiate page turn on page " + CurrentPage + ": " + ( postTime - preTime ) );
			
			StopCurrentPage();
			hideArrows(true);
			AddPageTurnNext();

			if ( clsOptions.interactive )
			{
				TakeCurrentSnapshot( !PreloadPrev );
				
				if ( cPages.NextPage == null ) { App.Log( "No next page to draw into page turn in clsMain.StartNextPageTurn()" ); }
				PageTurnNext.InitializeDO( cPages.NextPage );
				
			} else { PageTurnNext.Initialize( cPages.NextStaticBitmap.bitmapData ); }
			
			PageTurnNext.visible = true;
			AutoPageTurn_ToPage = GoToPage;
			AutoPageTurn_InProgress = PageTurnNext;
			AutoPageTurnPlaySound = true;
		}
		
		public function StartPrevPageTurn( GoToPage:int ):void
		{
			postTime = getTimer();
			//App.Log( "Time to initiate page turn: " + ( postTime - preTime ) );
			
			StopCurrentPage();
			hideArrows(true);
			AddPageTurnPrev();
			
			if ( clsOptions.interactive )
			{
				TakeCurrentSnapshot( !PreloadNext );
				
				if ( GoToPage == -1 ) { PageTurnPrev.InitializeDO( MainMenuSprite ); }
				 else { PageTurnPrev.InitializeDO( cPages.PrevPage ); }
			} 
			else 
			{ 
				if ( GoToPage == -1 ) { PageTurnPrev.InitializeDO( MainMenuSprite ); }
					else { PageTurnPrev.Initialize( cPages.PrevStaticBitmap.bitmapData ); }
			}
			
			PageTurnPrev.visible = true;
			AutoPageTurn_ToPage = GoToPage;
			AutoPageTurn_InProgress = PageTurnPrev;
			AutoPageTurnPlaySound = true;
		}
		
		public function FadeToPage(PageToFadeTo:Class, LoadedFrom:int):void
		{
			var bmData:BitmapData;
			CrossFade_LoadedFrom = LoadedFrom;
			
			StopCurrentPage();
			AddTransitionBitmap();
			AddFadeToBitmap();
			
			if(this.CurrentPage == -1)
			{
				cMainMenu.scrollRect = appRect;
				bmTrans.bitmapData.drawWithQuality( cMainMenu, null, null, null, null, false, StageQuality.BEST );
				cMainMenu.scrollRect = null;
			}
			else
			{
				cPages.CurrentPage.scrollRect = appRect;
				bmTrans.bitmapData.drawWithQuality( cPages.CurrentPage, null, null, null, null, false, StageQuality.BEST );
				cPages.CurrentPage.scrollRect = null;
			}
			
			if(games == null)
			{
				this.PageObjects.addChild( bmTrans );
			}
			else
			{
				this.PageObjects.addChild( games );
			}
			
			
			
			if(cMainMenu != null) { cMainMenu.Destroy(); cMainMenu = null; }
			
			cPages.LoadPage( PageToFadeTo );
		}
		
		public function StartFade():void
		{
			cPages.CurrentPage.scrollRect = appRect;
			CrossFade_FadeToImage.bitmapData.drawWithQuality( cPages.CurrentPage, null, null, null, null, false, StageQuality.BEST );
			cPages.CurrentPage.scrollRect = null;			
			
			CrossFade_FadeToImage.alpha = 0;
			this.PageObjects.addChild(CrossFade_FadeToImage);
			
			TweenLite.to( CrossFade_FadeToImage, 1, { alpha: 1, onComplete: CrossFade_Complete, ease: Linear.easeNone } );
		}
		
		private function CrossFade_Complete():void
		{
			InitializePages(-2, CrossFade_LoadedFrom, true);
			
			if(games == null)
			{
				this.PageObjects.removeChild( bmTrans );
			}
			else
			{
				this.PageObjects.removeChild( games );
			}
			
			this.PageObjects.removeChild( CrossFade_FadeToImage );
			
			bmTrans.bitmapData.dispose();
			bmTrans = null;
			CrossFade_FadeToImage.bitmapData.dispose();
			CrossFade_FadeToImage = null;
			
			if(games != null)
			{
				gamesLoader.dispose( true );
				gamesLoader = null;
				games = null;
			}
			
		}
		
		// System Monitors
		private final function windowNotActive(e:Event):void
		{
			Paused = true;
			if ( cPages.CurrentPage != null && cPages.CurrentPage is WBZ_BookPage )
				( cPages.CurrentPage as WBZ_BookPage ).Pause();
				
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID )
				{ cVideoBox.pauseVideo(); }
				
			App.musicVolume.volume = 0;
			App.soundVolume.volume = 0;
			App.voiceVolume.volume = 0;
		}
		
		private final function setKeepAwake():void
		{
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && CONFIG::MARKET != "nook" ) 
			{ 
				NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE; 
				//App.Log( "Forcing keep awake system idle mode" );
			}
		}
		
		private final function windowActive(e:Event):void
		{
			setKeepAwake();
				
			Paused = (Modal.length > 0);
			if ( !Paused && cPages.CurrentPage != null && cPages.CurrentPage is WBZ_BookPage )
					( cPages.CurrentPage as WBZ_BookPage ).Resume();
		
			App.musicVolume.volume = clsOptions.musicVolume;
			App.soundVolume.volume = clsOptions.soundVolume;
			App.voiceVolume.volume = clsOptions.voiceVolume;

			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && CONFIG::MARKET != "nook" ) 
			{ 
				// let's try switching the stage quality to boot it back into action maybe?
				stage.quality = StageQuality.HIGH_8X8_LINEAR;
				stage.quality = appQuality;
			}
		}		
		
		private final function handleKeys(event:KeyboardEvent):void
		{
			if ( event.keyCode == Keyboard.BACK )
			{
				event.stopImmediatePropagation();
				event.preventDefault();

				if ( CONFIG::MARKET == "nook" )
				{
					if ( Modal.length > 0 )
					{
						for ( var t:int = Modal.length - 1 ; t >= 0 ; t-- )
						{
							if ( Modal[ t ] is clsMsgBox ) 
								{ ( Modal[ t ] as clsMsgBox ).Hide(); return; }
							
							if ( Modal[ t ] is clsOptionsDialog )
								{ cOptionsDialog.Hide(); return; }
							
							if ( Modal[ t ] is MoreApps )
								{ hideMoreApps(); return; }
						}
					}
					
					if ( credits != null ) { Credits_Close( null ); }
					else if ( help != null ) { menuClick( null ); }
					//else if ( this.contains( cVideoBox ) ) { cVideoBox.closeClick( null ); }
					else if ( cMainMenu != null ) { Exit_App(); }
					else if ( cPages.CurrentPage != null ) { PrevPage( -1 ); }
					else { App.Log( "clsMain.handleKeys failed" ); }
				}
				else
				{
					if ( !backPressed )
					{
						backPressed = true;
						event.preventDefault();
						GlobalData.playSound( "Dialog_Open" );
						var msg:clsMsgBox = new clsMsgBox(this, gStrings["Messages"]["Msgbox_App_Close"], gc.messages_Mainmenu_Info, "YesNo", "Close_App", Close_Callback, null, null, gStrings["MainMenu"]["Yes"], gStrings["MainMenu"]["No"],false,true,true);
					} 
					else { Exit_App(); }
				}
				
			}
			
			if ( event.keyCode == Keyboard.SPACE && DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP )
			{
				if ( cPages.CurrentPage != null )
				{
					Utils.TakeScreenShot( cPages.CurrentPage, appRect );
				}
			}
			
			if ( event.keyCode == Keyboard.TAB && DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP )
			{
				// check for individual words in page text
				App.Log( "Checking for unused individual word sound files..." );
				
				var dir:File = File.applicationDirectory;
				dir = dir.resolvePath( GlobalData.langFolder + "sounds/words/" );
				var dirFiles:Array = dir.getDirectoryListing();
				var file:File;
				var word:String;
				var book:String = Utils.filterString( GlobalData.pagesXML.toString().toLowerCase() );
				
				for ( var u:int = 0 ; u < dirFiles.length ; u++ )
				{
					file = dirFiles[ u ] as File;
					word = file.name.substr( 0, file.name.indexOf( "." ) ).toLowerCase();
					if ( book.indexOf( word ) == -1 ) { App.Log( "Could not find word '" + word + "'" ); }
				}
				
				App.Log( "Done checking individual word files." );
			}
		}
		
		private final function Close_Callback(src:clsMsgBox, Button:String, Params:Dictionary):void
		{ 
			if ( Button == "Yes" ) { Exit_App(); } 
				else backPressed = false; 
		}
		
		private final function Exit_App():void
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			NativeApplication.nativeApplication.exit(); 
		}
					
		// Dialogs Control
		public final function showOptions():void
		{
			if(cOptionsDialog == null) { cOptionsDialog = new clsOptionsDialog(this); }
			cOptionsDialog.Show();
		}
		
		public function destroyOptions():void
		{
			if(cOptionsDialog != null) { cOptionsDialog.destroy(); }
			if ( this.contains( cOptionsDialog ) ) { removeChild( cOptionsDialog ); }
			cOptionsDialog = null;
		}

		public function Options_Changed():void
		{
			for( var i:int = 0; i < OptionsChangedCallbacks.length; i++) 
			{
				try { OptionsChangedCallbacks[i](); } 
					catch (e:Error) { App.Log( "OptionChangedCallbacks error: " + e.message ); }
			}
		}
		
		// Add and Remove Method Array Callbacks.
 		public function addEnterFrame(Callback:Function):void
		{
			if(EnterFrameCallbacks.indexOf(Callback) < 0)
				EnterFrameCallbacks.push(Callback);
		}
		
		public function removeEnterFrame(Callback:Function):void
		{
			for( var i:int = 0; i < EnterFrameCallbacks.length; i++) 
			{
				if(EnterFrameCallbacks[i] == Callback) 
				{
					EnterFrameCallbacks.splice(i,1);
					i--;
				}
			}
		}
			
		public function addStageMouseDown(Callback:Function):void
		{
			if(StageMouseDownCallbacks.indexOf(Callback) < 0)
				StageMouseDownCallbacks.push(Callback);
		}
		
		public function removeStageMouseDown(Callback:Function):void
		{
			for( var i:int = 0; i < StageMouseDownCallbacks.length; i++) 
			{
				if(StageMouseDownCallbacks[i] == Callback) 
				{
					StageMouseDownCallbacks.splice(i,1);
					i--;
				}
			}
		}
			
		public function addStageMouseUp(Callback:Function):void
		{
			if(StageMouseUpCallbacks.indexOf(Callback) < 0)
				StageMouseUpCallbacks.push(Callback);
		}
			
		public function removeStageMouseUp(Callback:Function):void
		{
			for( var i:int = 0; i < StageMouseUpCallbacks.length; i++) 
			{
				if(StageMouseUpCallbacks[i] == Callback) 
				{
					StageMouseUpCallbacks.splice(i,1);
					i--;
				}
			}
		}
			
		public function addOptionsChanged(Callback:Function):void
		{
			if(OptionsChangedCallbacks.indexOf(Callback) < 0)
				OptionsChangedCallbacks.push(Callback);
		}
		
		public function removeOptionsChanged(Callback:Function):void
		{
			for( var i:int = 0; i < OptionsChangedCallbacks.length; i++) 
			{
				if(OptionsChangedCallbacks[i] == Callback) 
				{
					OptionsChangedCallbacks.splice(i,1);
					i--;
				}
			}
		}
		
		// Helpers
		public final function hideArrows( instant:Boolean = false ):void
		{
			if ( instant )
			{
				PageTurnArrowNext.visible = false;
				PageTurnArrowPrev.visible = false;
			}
			else
			{
				TweenLite.to( PageTurnArrowNext, .75, { alpha: 0, ease: Linear.easeNone } );
				TweenLite.to( PageTurnArrowPrev, .75, { alpha: 0, onComplete: arrowsHidden, ease: Linear.easeNone } );
			}
		}
		
		private final function arrowsHidden():void 
			{ PageTurnArrowNext.visible = false; PageTurnArrowPrev.visible = false; }
		
		public final function showArrows():void
		{
			var ArrowAlpha:Number = PageTurnArrowPrev.alpha;
			if(PageTurnArrowPrev.visible == false && PageTurnArrowNext.visible == false)
				ArrowAlpha = 0;
				
			if(!PrintScreenMode){
				PageTurnArrowPrev.alpha = ArrowAlpha;
				PageTurnArrowNext.alpha = ArrowAlpha;
			}
			else{
				PageTurnArrowPrev.alpha = 0.0;
				PageTurnArrowNext.alpha = 0.0;
			}				
			
			PageTurnArrowPrev.visible = CurrentPage > 0;
			PageTurnArrowNext.visible = CurrentPage < cPages.PageOrder.length-1;
			if(!PrintScreenMode){
				TweenLite.to( PageTurnArrowNext, .75 * ( 1 - ArrowAlpha ), { alpha: 1, ease: Linear.easeNone } );
				TweenLite.to( PageTurnArrowPrev, .75 * ( 1 - ArrowAlpha ), { alpha: 1, ease: Linear.easeNone } );
			}
		}
		
		public static function playVideo( videoIndex:int ):void 
		{
			Main.PageObjects.visible = false;
			Main.cNavBar.visible = false;
			Main.cVideoBox.playWWRVideo( videoIndex ); 
		}	
		
				
		public static function openMarket( id:String = "" ):void
		{
			id = id == "" ? GlobalData.marketID : id;
			
			CONFIG::NOOK
			{
				var nook:NOOKAPI = new NOOKAPI;
				nook.fire( id );
			}
			
			if ( CONFIG::MARKET != "nook" )
			{
				var url:String;			
				url = ( CONFIG::MARKET == "blackberry" ) ? "http://appworld.blackberry.com/webstore/content/" + id :
					  ( CONFIG::MARKET == "amazon" ) ? "http://www.amazon.com/gp/mas/dl/android?p=" + id :
					  ( CONFIG::MARKET == "google" ) ? "market://details?id=" + id :
					  "itms-apps://itunes.com/apps/" + id;
				App.Log( "Navigating to: '" + url + "'" );
				navigateToURL( new URLRequest( url ) );
			}
		}
		
		public static function showMoreApps():void
		{
			Main.moreApps = new MoreApps();
			Main.moreApps.Show();
		}
		
		public static function hideMoreApps():void
		{
			if ( Main.moreApps != null )
			{
				Main.moreApps.Hide();
				Main.moreApps = null;
			}
		}
		
		public static function changeQuality( newQuality:String ):void { if ( Main.stage.quality != newQuality ) { Main.stage.quality = newQuality; } }
		public static function MessageLog( msg:String, debug:Boolean = false ):void 
			{ if ( !debug || CONFIG::DEBUG ) { Console.write( msg ); } }
		
		public final function ShowHelp():void
		{
			help = new Help;
			
			var page_1:Sprite = new Sprite;
			var page_2:Sprite = new Sprite;
			
			help.page_1 = page_1;
			help.page_1.x = clsMain.APP_X;
			help.page_2 = page_2;
			help.page_2.x = clsMain.APP_X;
									 
			help.addChildAt( help.page_1, 0 );
			help.addChildAt( help.page_2, 0 );
			
			Utils.addHandler( help.credits_btn, MouseEvent.CLICK, creditsClick );
			Utils.addHandler( help.rate_btn, MouseEvent.CLICK, rateClick );
			Utils.addHandler( help.menu_btn, MouseEvent.CLICK, menuClick );
			Utils.addHandler( help.feedback_btn, MouseEvent.CLICK, feedbackClick );
			Utils.addHandler( help.facebook_btn, MouseEvent.CLICK, facebookClick );
			Utils.addHandler( help.twitter_btn, MouseEvent.CLICK, twitterClick );
			Utils.addHandler( help.toggle_btn, MouseEvent.CLICK, toggleClick );
			
			helpLoader = new LoaderMax( { onComplete: helpLoaded } );
			helpLoader.append( new ImageLoader( GlobalData.langFolder + GlobalData.localXML.Help.Page1, { container: page_1 } ) );
			helpLoader.append( new ImageLoader( GlobalData.langFolder + GlobalData.localXML.Help.Page2, { container: page_2 } ) );
			helpLoader.load();
		}
		
		private function helpLoaded( e:LoaderEvent ):void { addChild( help ); }
		
		
		public final function ShowGames():void
		{
			games = new Games;
			
			var page_1:Sprite = new Sprite;
			
			games.page_1 = page_1;
			games.page_1.x = clsMain.APP_X;
									 
			games.addChildAt( games.page_1, 0 );
			
			Utils.addHandler( games.nails_btn, MouseEvent.CLICK, nailsClick );
			Utils.addHandler( games.hair_btn, MouseEvent.CLICK, hairClick );
			Utils.addHandler( games.beejeweled_btn, MouseEvent.CLICK, beejeweledClick );
			Utils.addHandler( games.home_btn, MouseEvent.CLICK, menuGamesClick );
			
			gamesLoader = new LoaderMax( { onComplete: gamesLoaded } );
			gamesLoader.append( new ImageLoader( GlobalData.langFolder + GlobalData.localXML.Games.Page1, { container: page_1 } ) );
			gamesLoader.load();
		}
		
		private function gamesLoaded( e:LoaderEvent ):void { addChild( games ); }
		
		
		private final function rateClick( e:MouseEvent ):void { showRate( "Star" ); }
		
		private final function showRate( src:String ):void
		{
			GlobalData.playSound( "Dialog_Open" );
			
			var m:clsMsgBox;
			if ( CONFIG::MARKET != "none" )
			{
				m = new clsMsgBox(this, gStrings["Messages"]["Msgbox_Mainmenu_Rate" + src], gc.messages_Mainmenu_Help, "OkOnly", "Main Menu Help", RateIt_Closed, null, null, gStrings["MainMenu"]["RateItNow"], "",false,true,true);
			}
			else
			{
				m = new clsMsgBox(this, gStrings["Messages"]["Msgbox_Mainmenu_RatePopup"], gc.messages_Mainmenu_Help, "OkOnly", "Main Menu Help", null, null, null, gStrings["General"]["Ok"], "",false,true,false);
			}
			
		}
		
		private final function RateIt_Closed(src:clsMsgBox, Button:String, Params:Dictionary):void
		{ 
			if ( Button == "Ok" ) 
			{ 
				clsOptions.reviewHit = true;
				clsOptions.Save();
				clsMain.openMarket(); 
			}
			else if ( clsOptions.reviewPrompts < 3 && !clsOptions.reviewHit )
			{
				clsOptions.bookCompleted = false;
				clsOptions.Save();
			}
		}	
		
		private final function menuClick( e:MouseEvent ):void
		{
			GlobalData.playSound( "Button_Generic" );
			Utils.removeHandler( help.credits_btn, MouseEvent.CLICK, creditsClick );
			Utils.removeHandler( help.rate_btn, MouseEvent.CLICK, rateClick );
			Utils.removeHandler( help.menu_btn, MouseEvent.CLICK, menuClick );
			Utils.removeHandler( help.feedback_btn, MouseEvent.CLICK, feedbackClick );
			Utils.removeHandler( help.facebook_btn, MouseEvent.CLICK, facebookClick );
			Utils.removeHandler( help.twitter_btn, MouseEvent.CLICK, twitterClick );
			Utils.removeHandler( help.toggle_btn, MouseEvent.CLICK, toggleClick );
			removeChild( help );
			helpLoader.dispose( true );
			helpLoader = null;
			help = null;
		}
		
		private final function menuGamesClick( e:MouseEvent ):void
		{
			GlobalData.playSound( "Button_Generic" );
			Utils.removeHandler( games.nails_btn, MouseEvent.CLICK, nailsClick );
			Utils.removeHandler( games.hair_btn, MouseEvent.CLICK, hairClick );
			Utils.removeHandler( games.beejeweled_btn, MouseEvent.CLICK, beejeweledClick );
			Utils.removeHandler( games.home_btn, MouseEvent.CLICK, menuGamesClick );
			removeChild( games );
			gamesLoader.dispose( true );
			gamesLoader = null;
			games = null;
		}
		
		
		private final function hairClick( e:MouseEvent ):void
		{
			var targetPage:Class = cNavBar.ArcadePages[2];

			Utils.removeHandler( games.nails_btn, MouseEvent.CLICK, nailsClick );
			Utils.removeHandler( games.hair_btn, MouseEvent.CLICK, hairClick );
			Utils.removeHandler( games.beejeweled_btn, MouseEvent.CLICK, beejeweledClick );
			Utils.removeHandler( games.home_btn, MouseEvent.CLICK, menuGamesClick );
			
			removeChild( games );
			
			FadeToPage( targetPage, 2 );			
		}
		
		private final function beejeweledClick( e:MouseEvent ):void
		{
			var targetPage:Class = cNavBar.ArcadePages[1];
			
			
			Utils.removeHandler( games.nails_btn, MouseEvent.CLICK, nailsClick );
			Utils.removeHandler( games.hair_btn, MouseEvent.CLICK, hairClick );
			Utils.removeHandler( games.beejeweled_btn, MouseEvent.CLICK, beejeweledClick );
			Utils.removeHandler( games.home_btn, MouseEvent.CLICK, menuGamesClick );
			
			removeChild( games );
			
			FadeToPage( targetPage, 2 );
			/*
			
			gamesLoader.dispose( true );
			gamesLoader = null;
			games = null;
			*/
			GlobalData.playSound( "Button_Generic" );
		}
		
		private final function nailsClick( e:MouseEvent ):void
		{
			var targetPage:Class = cNavBar.ArcadePages[0];
			
			
			Utils.removeHandler( games.nails_btn, MouseEvent.CLICK, nailsClick );
			Utils.removeHandler( games.hair_btn, MouseEvent.CLICK, hairClick );
			Utils.removeHandler( games.beejeweled_btn, MouseEvent.CLICK, beejeweledClick );
			Utils.removeHandler( games.home_btn, MouseEvent.CLICK, menuGamesClick );
			
			removeChild( games );
			
			FadeToPage( targetPage, 2 );
			/*
			
			gamesLoader.dispose( true );
			gamesLoader = null;
			games = null;
			*/
			GlobalData.playSound( "Button_Generic" );
		}
		
		
		private final function toggleClick( e:MouseEvent ):void
		{ 
			if ( help.page_1 == null ) return;
			if ( help.page_1.visible ) { help.page_1.visible = false; }
			else { help.page_1.visible = true; }
			
			GlobalData.playSound( "Button_Generic" );
		}
		
		private final function feedbackClick( e:MouseEvent ):void
		{ 
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID ) navigateToURL( new URLRequest( "http://cupcakedigital.com/contact/" ) );
			else navigateToURL( new URLRequest( GlobalData.localXML.Help.Feedback.toString().replace( "{APP_VERSION}", clsSupport.getAppVersion() ).split( "\\n" ).join( "\n" ) ) ); 
		}
		
		private final function facebookClick( e:MouseEvent ):void { navigateToURL( new URLRequest( "https://www.facebook.com/cupcakedigital?fref=ts" ) ); }		
		private final function twitterClick( e:MouseEvent ):void { navigateToURL( new URLRequest( "https://twitter.com/cupcake_digital" ) ); }		
		
		
		private final function creditsClick(e:MouseEvent):void
		{
			GlobalData.playSound( "Button_Generic" );
			ShowCredits();
		}
		
		public final function ShowCredits():void
		{
			credits = new Credits;
			credits.x = this.x;
			credits.y = this.y;
			var creditsImage:Sprite = new Sprite;
			creditsImage.x = APP_X;
			credits.addChildAt( creditsImage, 0 );
			
			creditsLoader = new ImageLoader( GlobalData.langFolder + GlobalData.localXML.Help.Credits, { container: creditsImage, onComplete: Credits_Loaded } );
			creditsLoader.load();
		}
		
		private final function Credits_Loaded( e:LoaderEvent ):void
		{ 
			if ( credits != null ) 
			{
				Utils.addHandler( credits.close_btn, MouseEvent.CLICK, Credits_Close );
				var txt:TextField = new TextField;
				var fmt:TextFormat = new TextFormat;
				txt.text = "Version " + clsSupport.getAppVersion();
				txt.width = 200;
				fmt.font = ( new fontTWCenMT ).fontName;
				fmt.size = 13;
				fmt.letterSpacing = 1;
				txt.setTextFormat( fmt );
				txt.x = GlobalData.localXML.Credits.Version.x.toString();
				txt.y = GlobalData.localXML.Credits.Version.y.toString();
				txt.selectable = false;
				credits.addChild( txt );
				addChild( credits );
			} 
		}
		
		private final function Credits_Close( e:MouseEvent ):void
		{
			GlobalData.playSound( "Button_Generic" );
			Utils.removeHandler( credits.close_btn, MouseEvent.CLICK, Credits_Close );
			removeChild( credits );
			creditsLoader.dispose( true );
			creditsLoader = null;
			credits = null;
		}
			
		public static function setLanguage( lang:String ):void
		{
			if ( GlobalData.CheckLanguage( lang ) )
			{
				var bmData:BitmapData;
				var img:Bitmap;
				
				// create the mouse intercept;
				MouseIntercept = new Sprite;			
				bmData = new BitmapData(1,1,false,0);
				img = new Bitmap(bmData);
				MouseIntercept.addChild(img);
				MouseIntercept.x = clsMain.APP_X;
				MouseIntercept.y = clsMain.APP_Y;
				MouseIntercept.width = clsMain.APP_WIDTH;
				MouseIntercept.height = clsMain.APP_HEIGHT;
				MouseIntercept.alpha = Main.gc.msgboxDefaults.backgroundAlpha;
				Main.addChild(MouseIntercept);
				
				hideMoreAppsButton();
				
				clsOptions.language = lang;
				clsOptions.Save();
				GlobalData.Init( lang, languageLoaded );
			}
			/*else
			{
				var ld:LanguageDownloader = new LanguageDownloader;
				ld.PromptDownload( lang );
			}*/
		}
		
		public static function languageLoaded():void
		{
			Main.LoadStrings();
			Main.cNavBar.updateButtonText();
			
			AppCode.updateLanguage();
			
			Main.MainMenuSprite.removeChild( Main.MainMenuBitmap );
			Main.MainMenuBitmap.bitmapData.dispose();
			Main.MainMenuBitmap = null;
			
			if ( Main.cMainMenu != null ) 
			{
				Main.cMainMenu.updateText();
				Main.checkMainMenuBitmap();
			}
			else
			{
				Main.cMainMenu = new clsMainMenu();
				Main.checkMainMenuBitmap();
				Main.cMainMenu = null;
				Main.PrevPage( -1 );
			}
			
			if ( Main.contains( MouseIntercept ) ) { Main.removeChild( MouseIntercept ); MouseIntercept = null; }
		}
		
		public static function showMoreAppsButton():void
			{ if ( Main.cMainMenu != null ) { Main.cMainMenu.showMoreAppsButton(); } }
		
		public static function hideMoreAppsButton():void
			{ if ( Main.cMainMenu != null ) { Main.cMainMenu.hideMoreAppsButton(); } }
		
		public function refreshTouch():void
		{
			//App.Log( "Called refreshTouch on page " + this.CurrentPage );
			stage.frameRate = 4;
			TweenLite.delayedCall( 2, updateFrameRate );
		}
		
		private function updateFrameRate():void { stage.frameRate = 30; }
		
		public function toggleStats():void
		{
			if ( this.contains( stats ) ) { removeChild( stats ); }
				else { addChild( stats ); }
		}
		
		public function get_localWidth():int{return localWidth;}
		public function get_localHeight():int{return localHeight;}
		
	}
}