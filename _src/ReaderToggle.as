﻿package  
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import com.cupcake.App;
	import com.cupcake.BookPage;
	import com.cupcake.Utils;
	
	public class ReaderToggle extends Sprite
	{
		public var isPressed:Boolean	= false;
		
		public var page:WBZ_BookPage;
		public var offState:Sprite;
		public var onState:Sprite;
		
		public function ReaderToggle( sourcePage:WBZ_BookPage ) 
		{
			page = sourcePage;
			
			//page.addChildAt( this, sourcePage.getChildIndex( sourcePage.textbox_mc ) - 1 );
			page.textbox_mc.addChild( this );
			this.name = "Mute Toggle";
			
			offState = new Sprite;
			offState.name = "Mute Toggle Off";
			offState.visible = BookPage.mute;
			offState.addChild( new Bitmap( GlobalData.getSheetBitmap( "Mute_Off" ) ) );
			this.addChild( offState );
			
			onState = new Sprite;
			onState.name = "Mute Toggle On";
			onState.visible = !BookPage.mute;
			onState.addChild( new Bitmap( GlobalData.getSheetBitmap( "Mute_On" ) ) );
			this.addChild( onState );
			
			//this.x = int( GlobalData.localXML.ReaderToggle.xCoord.toString() );
			//this.y = int( GlobalData.localXML.ReaderToggle.yCoord.toString() );
			
			this.x = page.textbox_mc.width - this.width;
			this.y = page.textbox_mc.height - this.height;
			
			clsMain.Main.addStageMouseDown( TogglePressed );
			clsMain.Main.addStageMouseUp( ToggleReleased );
		}
		
		public function Destroy():void
		{
			clsMain.Main.removeStageMouseDown( TogglePressed );
			clsMain.Main.removeStageMouseUp( ToggleReleased );
		}

		private function TogglePressed( e:MouseEvent ):void
		{ 
			if ( e.target == this || this.contains( e.target as DisplayObject ) ) 
				{ isPressed = true; /*App.Log( "Mute Toggle Pressed" );*/ } 
		}
		
		private function ToggleReleased( e:MouseEvent ):void
		{
			var tgt:DisplayObject = e.target as DisplayObject;
			
			if ( ( tgt != null && ( tgt == this || this.contains( tgt ) ) )
				 && page.isRunning && !page.isPaused && isPressed )
			{
				// Decide what we're doing based on page step
				if ( page.step == BookPage.STEP_READING )
				{
					onState.visible = false;
					offState.visible = true;
					BookPage.mute = true;
					page.reader.stopReading();
					page.step = BookPage.STEP_DONE;
				}
				else if ( page.step == BookPage.STEP_DONE )
				{
					onState.visible = true;
					offState.visible = false;
					BookPage.mute = false;
					page.reader.startReading();
					page.step = BookPage.STEP_READING;
				}
			}
			
			isPressed = false;
		}
	}
	
}
