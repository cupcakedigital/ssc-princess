﻿package com.cupcake
{
	import flash.events.Event;
	
	public class ReaderBoxEvent extends Event
	{
		public static const DONE_READING	= "ReaderBox.DoneReadingEvent";
		public static const REPLAY			= "ReaderBox.ReplayEvent";
		public static const TAP				= "ReaderBox.TapEvent";
		public static const TAP_WORD		= "ReaderBox.TapWordEvent";
		
		public var word:int		= -1;

		public function ReaderBoxEvent( eventType:String, tapWord:int = -1, 
									    bubbles:Boolean = false, cancelable:Boolean = false ) 
		{
			super( eventType, bubbles, cancelable );
			this.word = tapWord;
		}
	}
}
