﻿package com.cupcake
{
	public class SpriteSheetSequence {
		public var name:String;
		public var frames:Array;
		public var loop:Boolean;
		public var rate:int;
		
		public static function createRange( startFrame:int, endFrame:int, bookend:Boolean = true, bookendFrame:int = 0 ):Array
		{
			var arOut:Array = [];
			if ( bookend ) arOut.push( bookendFrame );
			for ( var t:int = startFrame ; t <= endFrame ; t++ )
			{
				arOut.push(t);
			}
			if ( bookend ) arOut.push( bookendFrame );
			return arOut;
		}

		public function SpriteSheetSequence( sequenceName:String, frameset:Array, looping:Boolean = false, frameRate:int = 60 ) 
		{
			name = sequenceName;
			frames = frameset;
			loop = looping;
			rate = frameRate;
		}
	}
}
