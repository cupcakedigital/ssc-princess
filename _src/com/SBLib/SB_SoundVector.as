﻿package com.SBLib {
	
	import flash.events.*;
	import flash.utils.*;
	import com.cupcake.Utils;	
	
	import com.DaveLibrary.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.MP3Loader;
	import com.DaveLibrary.clsSoundTransformEX;
	
	public class SB_SoundVector {	
	
		//Debug.
		private var Show_Messages:Boolean;
		
		//Sound.
		private var TheSounds:clsSound;
		private var ThePath:String;
		
		//Constructor.
		//--------------------------------------------------------
		// -> View Debug or not.		
		//--------------------------------------------------------
		public function SB_SoundVector(Path:String, SoundString:String){							
			ThePath = Path;
			Create_Sounds(SoundString);			
		}	
		public function SB_Update(){		
			
		}		
		public function SB_Destroy(){		
			Destroy_Sounds();
		}
		public function SB_Play(){		
			TheSounds.Start(clsPages.App.soundVolume.volume);
		}
		public function SB_Stop(){		
			Stop_Playing_Sounds();
		}
		//--------------------------------------------------------				
		
		//--------------------------------------------------------		
		//Sounds.
		//--------------------------------------------------------		
		private function Create_Sounds(tapSounds:String):void{				
			var TheFileName:String = (ThePath + tapSounds);				
			Load_Sound(TheFileName, clsPages.App.soundVolume.volume);			
		}
		private function Destroy_Sounds():void{	
			if(TheSounds == null){return;}
			TheSounds = null;
		}
		private function Sound_Playing():Boolean{			
			if(TheSounds.running){return true;}else{return false;}			
		}
		private function Stop_Playing_Sounds():void{
			if(TheSounds == null){return;}
			TheSounds.Stop(false);
		}
		private function Load_Sound(file:String, vol:Number):void{
			var ldr:MP3Loader = new MP3Loader( file, { volume: 0, onComplete:Loaded_Sounds_Complete } );
			ldr.load();
		}		
		private function Loaded_Sounds_Complete( e:LoaderEvent):void{		
			var ldr:MP3Loader = e.target as MP3Loader;
			var snd:Sound = ldr.content as Sound; 
			TheSounds = new clsSound(snd, clsPages.App.soundVolume, false, null, false);		
			ldr.dispose( true );
		}
		//--------------------------------------------------------		
		
		
		//--------------------------------------------------------	
		//Debug.
		//--------------------------------------------------------
		private function Print_Message(Message:String):void{
			if(!Show_Messages){return;}
			App.Log(Message);
		}
		//--------------------------------------------------------		
	
	}
	
}