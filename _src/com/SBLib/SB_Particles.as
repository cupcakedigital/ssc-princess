﻿package com.SBLib {
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.SimpleButton;
	import flash.events.*;
	import flash.utils.*;
	import flash.geom.Point;
	import com.cupcake.ExternalImage;
	import com.cupcake.IControllable;	
	import com.cupcake.Utils;	
	
	public class SB_Particles {	
	
		//Debug.
		private var Show_Messages:Boolean;
		
		//Paticles.
		private var All_Particles:Array = null;
		private var Particle_Que:Array  = null;		
		
		//Constructor.
		//--------------------------------------------------------
		// -> View Debug or not.		
		//--------------------------------------------------------
		public function SB_Particles(Testing:Boolean = false){		
			
			//Debug.
			Show_Messages = Testing;				
			Print_Message("------");
			Print_Message("Creating Particle Class Thanks you for using :D !");
			Print_Message("------");
			
			//The Que.
			Init_Particles_Que();			
			Init_All_Particles();
			
		}	
		public function SB_Update(Page_Master:Object){		
			Update_Q(Page_Master);
			Update_Particles(Page_Master);
		}
		//--------------------------------------------------------		
		
		
		
		
		
		
		
		//Here we Que the particles to be spawned.
		//--------------------------------------------------------
		public function Init_Particles_Que():void{			
			Print_Message("Init Particles Que");			
			Print_Message("...");
			if(Particle_Que == null){
				Particle_Que = new Array();
				Print_Message("Was null Created.");
			}				
			Print_Message("Particles Que is Initialized!");
		}
		public function Clear_Particles_Que():void{	
			if(Particle_Que == null){return;}
			Particle_Que.length = 0;
		}
		// -> Position we Start.
		// -> Spawn rate per update.
		// -> The Type of update we want it to do.
		// ---> Fade_Out.
		// ---> Fade_In.
		// ---> Speratic_Skechy.
		// ---> Loose_Loops.
		// ---> water
		// ---> 
		// ---> 
		// ---> 
		// ---> 
		// ---> 
		// -> The MovieClip we are duping.
		// -> The Amount we want to create.
		// -> The angle direction we will be traveling in Degrees.
		// -> The speed we travle at.
		// -> If we want an aditional force here we add the angle in Degrees.
		// -> ..... the angle in Degrees.
		// -> ..... The buildup rate of the speed of the extra force.
		// -> Scater based on 0 - 100% of curent angle and speed to add difference will be random from source - to +.
		//--------------------------------------------------------
		public function Q_Particles( pSpawn_Rate:int,
									 pCount:int,
									 pX:int, pY:int,
									 pType:String,
									 pClip:String,	
									 pARate:Number,
									 pDirection_Deg:int,
									 pSpeed:Number,
									 pScaterFactor:int   = 0,
									 xDirection_Deg:int  = 0,
									 xSpeed:Number       = 0.0,
									 xBuildup:Number     = 0.0 ){
			//-------------------------------------------------
			if(Particle_Que.length >= 1){return;}
			var This_Particle_Load:Array = new Array();			
			
			This_Particle_Load.push(pSpawn_Rate);
			This_Particle_Load.push(pCount);		
			
			var This_Particle:Array = new Array();		
			
			This_Particle.push(pClip);
			
			This_Particle.push(pType);
			This_Particle.push(new Point(pX, pY));		
			This_Particle.push(pDirection_Deg);
			This_Particle.push(pSpeed);
			This_Particle.push(xDirection_Deg);
			This_Particle.push(xSpeed);
			This_Particle.push(xBuildup);
			This_Particle.push(pScaterFactor);
			This_Particle.push(pARate);	
			This_Particle.push(0);//Pahse if needed.
			
			This_Particle_Load.push(This_Particle);
			
			Particle_Que.push(This_Particle_Load);
			//-------------------------------------------------									 
		}
		//--------------------------------------------------------
		public function Update_Q(Page_P:Object):void{			
			// Element 0 of the array is the count we load per update.
			// So we start at 1.
			for(var Q:int = 0; Q < Particle_Que.length; Q++){				
				Print_Message("------------------------------------");
				Print_Message("Array Currently has " + Particle_Que[Q][1] + " Particles to Spawn");
				Print_Message("and the Rate is " + Particle_Que[Q][0] + " Per update.");
				for(var P:int = 0; P < Particle_Que[Q][0]; P++){								
					if(All_Particles.length < 50){
						Print_Message("Spawn a mofo in this bitch.");
						Spawn_Particle(Page_P, Particle_Que[Q][2]);
						Particle_Que[Q][1] = (Particle_Que[Q][1] - 1);
					}
				}				
				if(Particle_Que[Q][1] <= 0){
					Print_Message("Clearing QType Array.");
					Particle_Que.splice(Q, 1);
					Q = (Q - 1);
					continue;		
				}
				Print_Message("------------------------------------");
			}			
		}
		//--------------------------------------------------------		
		
		
		//--------------------------------------------------------
		public function Init_All_Particles():void{			
			Print_Message("Init Particles Holder");
			Print_Message("...");
			if(All_Particles == null){
				All_Particles = new Array();
				Print_Message("Was null Created.");
			}				
			Print_Message("Particles Holder is Initialized!");
		}
		public function Clear_All_Particles(Page_C:Object):void{	
			if(All_Particles == null){return;}
			for(var X:int = 0; X < All_Particles.length; X++){
				MovieClip(Page_C.scene_mc.removeChild(All_Particles[X][0]));	
			}
			All_Particles.length = 0;
		}
		//--------------------------------------------------------
		public function Spawn_Particle(Page_C:Object, pParticle:Array = null):void{
			if(pParticle == null){return;}		
			Print_Message(pParticle[0]);			
			var Lib_Objec:Class = getDefinitionByName(pParticle[0]) as Class;		
			if(Lib_Objec == null){return;}
			
			var The_Particle:Array = new Array();			
			
			var Particle_MC:MovieClip = new Lib_Objec();
			Particle_MC.mouseEnabled = false;
			Particle_MC.gotoAndStop(1);
			Particle_MC.rotation = (Math.random() * 360);
			Particle_MC.x = pParticle[2].x;
			Particle_MC.y = pParticle[2].y;	
			
			The_Particle.push(Particle_MC);
			var PartProps:Array = new Array();
			for(var Pp:int = 1; Pp < pParticle.length; Pp++){
				PartProps.push(pParticle[Pp]);
			}
			The_Particle.push(PartProps);			
			The_Particle.push(true);
			
			Prep_Particle_Type(Page_C, The_Particle);
			
			All_Particles.push(The_Particle);
		}
		public function Prep_Particle_Type(Page_Cc:Object, The_Particle:Array):void{			
		
			if(The_Particle[1][7] != 0){				
				The_Particle[1][2] = (The_Particle[1][2] + ((Math.random() * (The_Particle[1][7] * 2)) - The_Particle[1][7]));
				//The_Particle[1][3] = (The_Particle[1][3] + ((Math.random() * (The_Particle[1][7] * 2)) - The_Particle[1][7]));
				The_Particle[1][3] = ((Math.random() * The_Particle[1][3]) + 2);
				//The_Particle[1][4] = (The_Particle[1][4] + ((Math.random() * (The_Particle[1][7] * 2)) - The_Particle[1][7]));				
				//The_Particle[1][5] = (The_Particle[1][5] + ((Math.random() * (The_Particle[1][7] * 2)) - The_Particle[1][7]));
			}
		
			The_Particle[1][2] = (The_Particle[1][2] * (Math.PI / 180));
			The_Particle[1][4] = (The_Particle[1][4] * (Math.PI / 180));
			switch(The_Particle[1][0]){					
				case "Fade_Out":{						
					The_Particle[0].alpha = 1.0;
				}break;	
				case "Fade_In":{						
					The_Particle[0].alpha = 0.0;
				}break;	
				case "Bats":{						
					The_Particle[0].rotation = 0.0;
					The_Particle[0].alpha = 1.0;
				}break;
				case "Coal":{	
					The_Particle[0].alpha = 1.0;
				}break;
				case "Sound":{	
					The_Particle[0].alpha = 1.0;
					The_Particle[0].rotation = 0.0;
				}break;
				case "water":{	
					The_Particle[0].alpha = 1.0;
					The_Particle[0].rotation = 0.0;
				}break;
				case "waterRight":{	
					The_Particle[0].alpha = 1.0;
					The_Particle[0].rotation = 0.0;
				}break;
				case "ZoomBy":{	
					The_Particle[0].alpha = 1.0;
					The_Particle[0].rotation = 0.0;
				}break;
				case "rainbow_sparkles":{						
					The_Particle[0].alpha = 1.0;
					
					var chooseColor:int;
					
					chooseColor = Math.ceil(Math.random() * 21) + 2;
					The_Particle[0].gotoAndStop(chooseColor);
					
				}break;	
				case "hearts":{						
					The_Particle[0].alpha = 1.0;
					
					var chooseColor:int;
					
					chooseColor = Math.ceil(Math.random() * 6) + 1;
					The_Particle[0].gotoAndStop(chooseColor);
					
				}break;	
				case "gems_sparkles":{						
					The_Particle[0].alpha = 1.0;
					
					var chooseColor:int;
					
					chooseColor = Math.ceil(Math.random() * 11) + 1;
					The_Particle[0].gotoAndStop(chooseColor);
					
				}break;	
			}				
			MovieClip(Page_Cc.scene_mc.addChild(The_Particle[0]));					
		}
		//--------------------------------------------------------
		public function Update_Particles(Page_U:Object):void{
			
			for(var X:int = 0; X < All_Particles.length; X++){			
				
				switch(All_Particles[X][1][0]){					
					case "Fade_Out":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;		
					case "Fade_In":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha + All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha >= 1.0){
							All_Particles[X][0].alpha = 1.0;
							All_Particles[X][2] = false;
						}						
					}break;		
					case "Bats":{	
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][0].currentFrame == All_Particles[X][0].totalFrames){
							All_Particles[X][0].gotoAndStop(1);
						}
						else{
							All_Particles[X][0].gotoAndStop(All_Particles[X][0].currentFrame + 1);
						}
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}										
					}break;
					case "Coal":{				
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){							
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						switch(All_Particles[X][1][9]){
							case 0:{
								if(All_Particles[X][0].y >= 500.0){										
									All_Particles[X][1][5] = 1;
									All_Particles[X][1][9] = 1;
								}									
							}break;
							case 1:{
								if(All_Particles[X][0].y >= 800.0){		
									All_Particles[X][2] = false;
								}
							}break;
						}
					}break;
					case "Sound":{
						if(All_Particles[X][0].currentFrame == All_Particles[X][0].totalFrames){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}
						else{
							All_Particles[X][0].gotoAndStop(All_Particles[X][0].currentFrame + 1);
						}
					}break;
					case "water":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						
						
						All_Particles[X][0].rotation -= 10;
						
						
						if(All_Particles[X][0].rotation < -100)
						{
							All_Particles[X][0].rotation = -100;
						}
						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;		
					case "waterRight":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						
						
						All_Particles[X][0].rotation += 10;
						
						
						if(All_Particles[X][0].rotation > 100)
						{
							All_Particles[X][0].rotation = 100;
						}
						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;		
					case "ZoomBy":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;		
					case "rainbow_sparkles":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;				
					case "hearts":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].alpha <= 0.0){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;		
					case "gems_sparkles":{						
						All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][3] * Math.cos(All_Particles[X][1][2])));
						All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][3] * Math.sin(All_Particles[X][1][2])));						
						if(All_Particles[X][1][5] != 0.0){
							All_Particles[X][0].x = (All_Particles[X][0].x + (All_Particles[X][1][5] * Math.cos(All_Particles[X][1][4])));
							All_Particles[X][0].y = (All_Particles[X][0].y + (All_Particles[X][1][5] * Math.sin(All_Particles[X][1][4])));						
							All_Particles[X][1][5] = (All_Particles[X][1][5] + All_Particles[X][1][6]);
						}						
						All_Particles[X][0].alpha = (All_Particles[X][0].alpha - All_Particles[X][1][8]);
						if(All_Particles[X][0].y >= 780){
							All_Particles[X][0].alpha = 0.0;
							All_Particles[X][2] = false;
						}						
					}break;		
				}				
				
				if(!All_Particles[X][2]){					
					MovieClip(Page_U.scene_mc.removeChild(All_Particles[X][0]));		
					All_Particles.splice(X, 1);
					X = (X - 1);
					continue;
				}				
				
			}
			
		}
		//--------------------------------------------------------		
		
		
		
		//Debug.
		//--------------------------------------------------------
		private function Print_Message(Message:String):void{
			//if(!Show_Messages){return;}
			//App.Log(Message);
		}
		//--------------------------------------------------------		
	
	}
	
}