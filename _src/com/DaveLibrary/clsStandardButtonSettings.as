﻿package com.DaveLibrary {
	
	import com.DaveLibrary.clsTextData
	import flash.geom.Rectangle;
	
	public class clsStandardButtonSettings {

		public var textInfo:clsTextData = new clsTextData();
		public var hitAreaRect:Rectangle = new Rectangle();

		public function clsStandardButtonSettings():void
		{
			
		}

		public function destroy():void
		{
			textInfo.destroy();
			textInfo = null;
			hitAreaRect = null;
		}
	}
	
	
}
