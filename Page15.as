﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	public final class Page15 extends WBZ_BookPage
	{
		public var sndLetsTalkTitle:SoundEx		= null;
		public var sndLetsTalk1:SoundEx			= null;
		public var sndLetsTalk2:SoundEx			= null;
		public var sndLetsTalk3:SoundEx			= null;
		
		public var sndLetsThinkTitle:SoundEx	= null;
		public var sndLetsThink1:SoundEx		= null;
		public var sndLetsThink2:SoundEx		= null;
		public var sndLetsThink3:SoundEx		= null;
		
		public var sndGrownUp:SoundEx			= null;
		
		//Core.
		public function Page15(){
			super();
			
			imageFolder = GlobalData.pagesFolder;
			Images.Add( "p15", { container: "BG", fromFile: true, extension: ".jpg" } );
			
			sndLetsTalkTitle = new SoundEx( "sndLetsTalkTitle", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_TALK_TITLE_A.mp3", new clsSoundTransformEX() );
			sndLetsTalk1 = new SoundEx( "sndLetsTalk1", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_TALK_1.mp3", new clsSoundTransformEX() );
			sndLetsTalk2 = new SoundEx( "sndLetsTalk2", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_TALK_2.mp3", new clsSoundTransformEX() );
			sndLetsTalk3 = new SoundEx( "sndLetsTalk3", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_TALK_3.mp3", new clsSoundTransformEX() );
			
			sndLetsThinkTitle = new SoundEx( "sndLetsThinkTitle", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_THINK_TITLE_A.mp3", new clsSoundTransformEX() );
			sndLetsThink1 = new SoundEx( "sndLetsThink1", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_THINK_1_A.mp3", new clsSoundTransformEX() );
			sndLetsThink2 = new SoundEx( "sndLetsThink2", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_THINK_2_A.mp3", new clsSoundTransformEX() );
			sndLetsThink3 = new SoundEx( "sndLetsThink3", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/LETS_THINK_3_A.mp3", new clsSoundTransformEX() );
			
			sndGrownUp = new SoundEx( "sndGrownUp", CONFIG::DATA + "sounds/Animation_Sounds/grownUpCorner/GROWN_UP.mp3", new clsSoundTransformEX() );
		}			
		
		public final override function Init():void
		{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic6";
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			cMain.removeStageMouseDown( StageTap );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseDown( StageTap );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			stopSounds();
			cMain.removeStageMouseDown( StageTap );
		}
		
		//Updates.
		private function StageTap( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "letsThinkTitle_BTN":
					stopSounds();
					sndLetsThinkTitle.Start();
					break;
				
				case "letsThink1_BTN":
					stopSounds();
					sndLetsThink1.Start();
					break;
				
				case "letsThink2_BTN":
					stopSounds();
					sndLetsThink2.Start();
					break;
				
				case "letsThink3_BTN":
					stopSounds();
					sndLetsThink3.Start();
					break;
				
				case "letsTalkTitle_BTN":
					stopSounds();
					sndLetsTalkTitle.Start();
					break;
				
				case "letsTalk1_BTN":
					stopSounds();
					sndLetsTalk1.Start();
					break;
				
				case "letsTalk2_BTN":
					stopSounds();
					sndLetsTalk2.Start();
					break;
				
				case "letsTalk3_BTN":
					stopSounds();
					sndLetsTalk3.Start();
					break;
				
				case "grownUp_BTN":
					stopSounds();
					sndGrownUp.Start();
					break;
			}
		}				

		
		private function stopSounds():void
		{
			sndLetsTalkTitle.Stop();
			sndLetsTalk1.Stop();
			sndLetsTalk2.Stop();
			sndLetsTalk3.Stop();
			
			sndLetsThinkTitle.Stop();
			sndLetsThink1.Stop();
			sndLetsThink2.Stop();
			sndLetsThink3.Stop();
			
			sndGrownUp.Stop();
		}
	}
}