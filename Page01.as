﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page01 extends WBZ_BookPage
	{
		private var strawberry:TapSpritesheet;
		private var roses:TapSpritesheet;
		
		
		public var blueberrySounds:SoundCollection;
		public var strawberrySounds:SoundCollection;
		public var pupcakeSounds:SoundCollection;
		
		public var sndballoons:SoundEx			= null;
		
		
		private var strawberryCounter:int = 0;
		private var rosesCounter:int = 1;
		
		//Core.
		public function Page01(){
			super();
			
			imageFolder = CONFIG::DATA + "images/pages/page01/";
			Images.Add( "SS_P01_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_P01_VASE_OL", { container: "vase_mc", fromFile: true } );
			Images.Add( "SS_P01_BALLOON_02", { container: scene_mc.balloon1_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P01_BALLOON_01", { container: scene_mc.balloon2_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P01_BALLOON_03", { container: scene_mc.balloon3_mc.actor.balloon_mc, fromFile: true } );
			Images.Add( "SS_P01_BALLOON_04", { container: scene_mc.balloon4_mc.actor.balloon_mc, fromFile: true } );

			sndballoons = new SoundEx( "sndballoons", CONFIG::DATA + "sounds/Animation_Sounds/P01_balloon.mp3", App.soundVolume );

			blueberrySounds = SoundCollection.GetAnimSounds( "P01_BLUEBERRYtap", [1] );
			TapMultiLite.Add( "blueberry", { imageName: "SS_P01_BLUEBERRY_00", sounds: blueberrySounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "berrykin1", { imageName: "SS_P01_PURPLE_BERRYKIN_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "berrykin2", { imageName: "SS_P01_ORANGE_BERRYKIN_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			TapMultiLite.Add( "berrykin3", { imageName: "SS_P01_PINK_BERRYKIN_00", align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			
			pupcakeSounds = SoundCollection.GetAnimSounds( "P01_pupcake", [1] );
			spriteSheetLoader.Add( "SS_P01_PUPCAKE_ANIM", 60, pupcakeSounds, null, false, false);
			
			strawberrySounds = SoundCollection.GetAnimSounds( "P01_STRAWBERRYtap", [1] );
			strawberry = spriteSheetLoader.Add( "SS_P01_STRAWBERRY_ANIM", 60, strawberrySounds, null, false, false);
			strawberry.onComplete = strawberryComplete;
			
			var roses1Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses1", SpriteSheetSequence.createRange( 1, 1, false, 1 ), false, 60 );
			var roses2Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses2", SpriteSheetSequence.createRange( 2, 2, false, 2 ), false, 60 );
			var roses3Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses3", SpriteSheetSequence.createRange( 3, 3, false, 3 ), false, 60 );
			var roses4Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses4", SpriteSheetSequence.createRange( 4, 4, false, 4 ), false, 60 );
			var roses5Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses5", SpriteSheetSequence.createRange( 5, 5, false, 5 ), false, 60 );
			var roses6Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses6", SpriteSheetSequence.createRange( 6, 6, false, 6 ), false, 60 );
			var roses7Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses7", SpriteSheetSequence.createRange( 7, 7, false, 7 ), false, 60 );
			var roses8Seq:SpriteSheetSequence = new SpriteSheetSequence( "roses8", SpriteSheetSequence.createRange( 0, 0, false, 0 ), false, 60 );
			roses = spriteSheetLoader.Add( "SS_P01_ROSES", 60, null, null, false, false, new <SpriteSheetSequence>[roses1Seq,roses2Seq,roses3Seq,roses4Seq,roses5Seq,roses6Seq,roses7Seq,roses8Seq]);
			
			sparklers.Add(scene_mc.SS_P01_STRAWBERRY_ANIM_BTN,scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.blueberry_btn, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.SS_P01_PUPCAKE_ANIM_BTN, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.balloons_BTN, scene_mc.Sparkler4_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic2";

			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			scene_mc.SS_P01_STRAWBERRY_ANIM_BTN.mouseEnabled = true;
			strawberryCounter = 0;
			
			rosesCounter = 1;
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();

			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "SS_P01_STRAWBERRY_ANIM_BTN":
				{
					strawberryCounter++;
					scene_mc.SS_P01_STRAWBERRY_ANIM_BTN.mouseEnabled = false;
				}
				break;
				
				case "balloons_BTN":
				{
					if(scene_mc.balloon1_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndballoons.Start();
						}
						
						scene_mc.balloon1_mc.play();
						scene_mc.balloon2_mc.play();
						scene_mc.balloon3_mc.play();
						scene_mc.balloon4_mc.play();
						
						(controls[1] as TapMultiLite).doTap();
						(controls[2] as TapMultiLite).doTap();
						(controls[3] as TapMultiLite).doTap();
					}
				}
				break;	
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
	
		}
		
		
		protected final override function handleFrame( e:Event ):void{
		   if(isRunning && !isPaused)
		   {
			   
			   if(strawberryCounter > 0)
			   {
				   strawberryCounter++;
			   }
			   if(strawberryCounter == 14)
			   {
				   if(rosesCounter < 8)
				   {
					    roses.doTap();
				   		strawberryCounter = 0;
				   }
				  rosesCounter++;
			   }
			   
		   }
		   super.handleFrame( e );
		}		
		
		private function strawberryComplete(e:TapSpritesheet):void
		{
			scene_mc.SS_P01_STRAWBERRY_ANIM_BTN.mouseEnabled = true;
		}
	}
}