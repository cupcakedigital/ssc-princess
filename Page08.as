﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page08 extends WBZ_BookPage
	{
		private var strawberry:TapSpritesheet;
		private var chiffon:TapSpritesheet;
		private var hats:TapSpritesheet;
		
		
		public var raspberrySounds:SoundCollection;
		public var strawberrySounds:SoundCollection;
		public var dogSounds:SoundCollection;
		public var lightsSounds:SoundCollection;
		
		public var sndDress:SoundEx			= null;
		public var sndThrowBow:SoundEx			= null;
		public var sndStrawberryClap:SoundEx			= null;
		
		
		
		private var basketCounter:int = 0;
		
		
		private var Cupcakes:Array;
		
		
		//Core.
		public function Page08(){
			super();
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page08/";
			Images.Add( "SS_P08_BACKGROUND", { container: "BG", fromFile: true } );
			Images.Add( "SS_P08_DRESS", { container: "dress_mc", fromFile: true } );
			Images.Add( "SS_P08_SPARKLE_SMALLER", { container: scene_mc.littleSparkle_mc.littleSparkle_mc.littleSparkle_mc, fromFile: true } );
			
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle1.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle2.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle3.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle4.bigSparkle.bigSparkle, fromFile: true } );
			Images.Add( "SS_P08_SPARKLE", { container: scene_mc.bigSparkle5.bigSparkle.bigSparkle, fromFile: true } );
			
			
			/*
			P08_strawberry_clap
			
			sndStrawberryClap
			
			
			*/
			
			sndDress = new SoundEx( "sndDress", CONFIG::DATA + "sounds/Animation_Sounds/P08_dress.mp3", App.soundVolume );
			sndThrowBow = new SoundEx( "sndThrowBow", CONFIG::DATA + "sounds/Animation_Sounds/P08_throw_bow.mp3", App.soundVolume );
			sndStrawberryClap = new SoundEx( "sndStrawberryClap", CONFIG::DATA + "sounds/Animation_Sounds/P08_strawberry_clap.mp3", App.soundVolume );
			
			W_Loader.Add( "SS_P08_BASKET", null, null, "basket");
			
			strawberrySounds = SoundCollection.GetAnimSounds( "P08_STRAWBERRYTAP", [1] );
			strawberry = spriteSheetLoader.Add( "SS_P08_STRAWBERRY", 60, strawberrySounds, null, false, false);
			strawberry.onComplete = strawberryComplete;

			raspberrySounds = SoundCollection.GetAnimSounds( "P08_RASPBERRYTAP", [1,2,3] );
			spriteSheetLoader.Add( "SS_P08_RASPBERRY", 60, raspberrySounds, null, false, false);

			dogSounds = SoundCollection.GetAnimSounds( "P08_dog", [1] );
			chiffon = spriteSheetLoader.Add( "SS_P08_CHIFFON", 60, dogSounds, null, false, false);
			chiffon.onComplete = chiffonComplete;

			lightsSounds = SoundCollection.GetAnimSounds( "P08_lights", [1,2] );
			var lights1Seq:SpriteSheetSequence = new SpriteSheetSequence( "lights1", SpriteSheetSequence.createRange( 1, 1, false, 1 ), false, 60 );
			var lights2Seq:SpriteSheetSequence = new SpriteSheetSequence( "lights2", SpriteSheetSequence.createRange( 0, 0, false, 0 ), false, 60 );
			spriteSheetLoader.Add( "SS_P08_LIGHT", 60, lightsSounds, null, false, false, new <SpriteSheetSequence>[lights1Seq,lights2Seq], null, scene_mc.light1, scene_mc.light_BTN );
			spriteSheetLoader.Add( "SS_P08_LIGHT", 60, null, null, false, false, new <SpriteSheetSequence>[lights1Seq,lights2Seq], null, scene_mc.light2, scene_mc.light_BTN );
			spriteSheetLoader.Add( "SS_P08_LIGHT", 60, null, null, false, false, new <SpriteSheetSequence>[lights1Seq,lights2Seq], null, scene_mc.light3, scene_mc.light_BTN );

			var hats1Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats1", SpriteSheetSequence.createRange( 0, 0, false, 0 ), false, 60 );
			var hats2Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats2", SpriteSheetSequence.createRange( 1, 1, false, 1 ), false, 60 );
			var hats3Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats3", SpriteSheetSequence.createRange( 2, 2, false, 2 ), false, 60 );
			var hats4Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats4", SpriteSheetSequence.createRange( 3, 3, false, 3 ), false, 60 );
			var hats5Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats5", SpriteSheetSequence.createRange( 4, 4, false, 4 ), false, 60 );
			var hats6Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats6", SpriteSheetSequence.createRange( 5, 5, false, 5 ), false, 60 );
			var hats7Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats7", SpriteSheetSequence.createRange( 6, 6, false, 6 ), false, 60 );
			var hats8Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats8", SpriteSheetSequence.createRange( 7, 7, false, 7 ), false, 60 );
			var hats9Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats9", SpriteSheetSequence.createRange( 8, 8, false, 8 ), false, 60 );
			var hats10Seq:SpriteSheetSequence = new SpriteSheetSequence( "hats10", SpriteSheetSequence.createRange( 9, 9, false, 9 ), false, 60 );
			hats = spriteSheetLoader.Add( "SS_P08_BANDS_N_BOWS", 60, null, null, false, false, new <SpriteSheetSequence>[hats1Seq,hats2Seq,hats3Seq,hats4Seq,hats5Seq,hats6Seq,hats7Seq,hats8Seq,hats9Seq,hats10Seq]);

			chooseHat(0);

			sparklers.Add(scene_mc.SS_P08_RASPBERRY_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.SS_P08_STRAWBERRY_BTN, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.basket_BTN, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.SS_P08_CHIFFON_BTN, scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.light_BTN, scene_mc.Sparkler5_sprkl);
			sparklers.Add(scene_mc.dressSparkles_BTN, scene_mc.Sparkler6_sprkl);
		}
		
		public final override function Init():void
		{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic5";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			scene_mc.SS_P08_CHIFFON_BTN.mouseEnabled = true;
			scene_mc.basket_BTN.mouseEnabled = true;
			scene_mc.SS_P08_STRAWBERRY_BTN.mouseEnabled = true;
			
			Init_Cupcake();
			chooseHat(0);
			
			basketCounter = 0;
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			Destroy_Cupcake();
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			Select_Cupcake(mouseX, mouseY);
			
			switch(e.target.name)
			{
				case "SS_P08_CHIFFON_BTN":
				{
					scene_mc.SS_P08_CHIFFON_BTN.mouseEnabled = false;
				}
				break;
				
				case "dressSparkles_BTN":
				{
					if(scene_mc.bigSparkle1.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndDress.Start();
						}
						
						scene_mc.bigSparkle1.play();
					}
				}
				break;
				
				case "basket_BTN":
				{
					basketCounter = 1;
					scene_mc.basket_BTN.mouseEnabled = false;
					Spawn_Tart(775, 260);
				}
				break;
				
				case "SS_P08_STRAWBERRY_BTN":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndStrawberryClap.Start();
					}
					
					scene_mc.SS_P08_STRAWBERRY_BTN.mouseEnabled = false;
				}
				break;
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			Unselect_Cupcake();			
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Update_Cupcake();
				
				finishAnim();
				
			}
			super.handleFrame( e );
		}		
		
		
		private function finishAnim():void
		{
			if(scene_mc.littleSparkle_mc.currentFrame == scene_mc.littleSparkle_mc.totalFrames)
			{
				scene_mc.SS_P08_CHIFFON_BTN.mouseEnabled = true;
			}
			
			
			if(scene_mc.bigSparkle1.currentFrame == 6)
			{
				scene_mc.bigSparkle2.play();
			}
			if(scene_mc.bigSparkle1.currentFrame == 12)
			{
				scene_mc.bigSparkle3.play();
			}
			if(scene_mc.bigSparkle1.currentFrame == 18)
			{
				scene_mc.bigSparkle4.play();
			}
			if(scene_mc.bigSparkle1.currentFrame == 23)
			{
				scene_mc.bigSparkle5.play();
			}
			
			
			if(basketCounter > 0)
			{
				basketCounter++;
			}
			if(basketCounter >= 20)
			{
				scene_mc.basket_BTN.mouseEnabled = true;
				basketCounter = 0;
			}
			
		}
		
		
		private function chiffonComplete(e:TapSpritesheet):void
		{
			scene_mc.littleSparkle_mc.play();
		}
		
		
		private function strawberryComplete(e:TapSpritesheet):void
		{
			scene_mc.SS_P08_STRAWBERRY_BTN.mouseEnabled = true;
		}
		
		
		private function chooseHat(targetHat:int):void
		{
			if(targetHat == 0)
			{
				scene_mc.SS_P08_BANDS_N_BOWS.visible = false;
			}
			else
			{
				scene_mc.SS_P08_BANDS_N_BOWS.visible = true;
			}
			
			if(targetHat == 1)
			{
				hats.playSequence( "hats1" );
			}
			if(targetHat == 2)
			{
				hats.playSequence( "hats2" );
			}
			if(targetHat == 3)
			{
				hats.playSequence( "hats3" );
			}
			if(targetHat == 4)
			{
				hats.playSequence( "hats4" );
			}
			if(targetHat == 5)
			{
				hats.playSequence( "hats5" );
			}
			if(targetHat == 6)
			{
				hats.playSequence( "hats6" );
			}
			if(targetHat == 7)
			{
				hats.playSequence( "hats7" );
			}
			if(targetHat == 8)
			{
				hats.playSequence( "hats8" );
			}
			if(targetHat == 9)
			{
				hats.playSequence( "hats9" );
			}
			if(targetHat == 10)
			{
				hats.playSequence( "hats10" );
			}
		}
		
		
		private function Init_Cupcake():void
		{
			if(Cupcakes == null)
			{
				Cupcakes = new Array();
			}
			else
			{
				Destroy_Cupcake();
			}
		}
		
		
		private function Spawn_Tart(pX:int, pY:int):void
		{
			
			var Cupcake_Props:Array = new Array();
			var NewCupcake:tossableHats_mc = new tossableHats_mc();
			var whichHat:int = 0;
			whichHat = Math.floor((Math.random() * 10) + 1);
			NewCupcake.gotoAndStop(whichHat);
			NewCupcake.x = pX;
			NewCupcake.y = pY;
			scene_mc.addChildAt(NewCupcake, (scene_mc.numChildren - 1));
			var Phase:int = 0;			
			var Force_X:Number = ((Math.random() * 40) - 20);
			var Force_Y:Number = -40.0;
			
			Cupcake_Props.push(NewCupcake);
			Cupcake_Props.push(Phase);
			Cupcake_Props.push(Force_X);
			Cupcake_Props.push(Force_Y);
			Cupcake_Props.push(0);
			
			Cupcakes.push(Cupcake_Props);
			
		}
		private function Select_Cupcake(pX:int, pY:int):void
		{
			for(var X:int = 0; X < Cupcakes.length; X++){
				if(Cupcakes[X][0].hitTestPoint(mouseX, mouseY, true)){
					Cupcakes[X][1] = 2;
					Cupcakes[X][4] = 0;
					return;
				}
			}
		}
		private function Unselect_Cupcake():void{
			for(var X:int = 0; X < Cupcakes.length; X++){
				switch(Cupcakes[X][1]){
					case 2:{
						var Angle:Number = Math.atan2((mouseY - Cupcakes[X][0].y), (mouseX - Cupcakes[X][0].x));
						var P1:Point = new Point(mouseX, mouseY);
						var P2:Point = new Point(Cupcakes[X][0].x, Cupcakes[X][0].y);
						var Speed:Number = Point.distance(P1, P2);
						if(Speed > 30){Speed = 30;}
						Cupcakes[X][2] = (0 + (Speed * Math.cos(Angle)));
						Cupcakes[X][3] = (0 + (Speed * Math.sin(Angle)));
						Cupcakes[X][1] = 0;
					}break;
				}
			}
		}
		private function Update_Cupcake():void{			
			if(Cupcakes.length > 6){
				scene_mc.removeChild(Cupcakes[X][0]);
				Cupcakes.splice(0, 1);				
			}			
			for(var X:int = 0; X < Cupcakes.length; X++)
			{
				
				if(scene_mc.SS_P08_BANDS_N_BOWS.hitTestObject(Cupcakes[X][0]) && Cupcakes[X][1] == 2)
				{
					//sndHatHead.Start();
					if(scene_mc.SS_P08_STRAWBERRY_BTN.mouseEnabled == true)
					{
						strawberry.doTap();
						if ( this.step == BookPage.STEP_DONE )
						{
							sndStrawberryClap.Start();
						}
						
						scene_mc.SS_P08_STRAWBERRY_BTN.mouseEnabled = false;
					}
					
					Cupcakes[X][0].x = 1400;
					Cupcakes[X][1] = 1;
					chooseHat(Cupcakes[X][0].currentFrame);
				}
				
				
				
				
				scene_mc.setChildIndex(Cupcakes[X][0], (scene_mc.numChildren - 1));
				
				switch(Cupcakes[X][1]){
					case 0:{					
						
						Cupcakes[X][0].x = (Cupcakes[X][0].x + Cupcakes[X][2]);
						Cupcakes[X][0].y = (Cupcakes[X][0].y + Cupcakes[X][3]);		
						
						Cupcakes[X][0].rotation = (Cupcakes[X][0].rotation + Cupcakes[X][2]);
						
						//Y Force.
						Cupcakes[X][3] = Math.floor(Cupcakes[X][3] + 3.0);
						
						
						if(Cupcakes[X][0].y >= 550){
							Cupcakes[X][3] = -Math.floor(Cupcakes[X][3] - (Cupcakes[X][3] / 2)); 							
							if(Cupcakes[X][3] > -4){
								Cupcakes[X][3] = 0;
							}
							Cupcakes[X][0].y = 550;
							Cupcakes[X][4] = (Cupcakes[X][4] + 1);
							if(Cupcakes[X][4] > 2){Cupcakes[X][3] = 0; Cupcakes[X][2] = 0;}
							if(Cupcakes[X][3] != 0 && this.step == BookPage.STEP_DONE)
							{
								sndThrowBow.Start();
							}
						}		
						//X Force.										
						if(Cupcakes[X][0].x >= 1024 + clsMain.offsetX){
							Cupcakes[X][2] = -Math.floor(Cupcakes[X][2] - (Cupcakes[X][2] / 3)); 
							Cupcakes[X][0].x = 1023 + clsMain.offsetX;
							if(Cupcakes[X][2] != 0 && this.step == BookPage.STEP_DONE)
							{
								sndThrowBow.Start();
							}
							
						}
						if(Cupcakes[X][0].x <= 0 - clsMain.offsetX){
							Cupcakes[X][2] = -Math.floor(Cupcakes[X][2] - (Cupcakes[X][2] / 3)); 
							Cupcakes[X][0].x = 1 - clsMain.offsetX;
							if(Cupcakes[X][2] != 0 && this.step == BookPage.STEP_DONE)
							{
								sndThrowBow.Start();
							}
							
						}		
						if(Cupcakes[X][2] < 0){
							Cupcakes[X][2] = (Cupcakes[X][2] + 0.1);
							if(Cupcakes[X][2] >= 0){
								Cupcakes[X][2] = 0;
							}
						}
						else if(Cupcakes[X][2] > 0){
							Cupcakes[X][2] = (Cupcakes[X][2] - 0.1);
							if(Cupcakes[X][2] <= 0){
								Cupcakes[X][2] = 0;
							}
						}
									
					}break;
					case 1:{
						
					}break;		
					case 2:{
						Cupcakes[X][0].x = mouseX;
						Cupcakes[X][0].y = mouseY;
					}break;		
				}
			}
		}
		private function Destroy_Cupcake():void{
			if(Cupcakes == null){return;}
			for(var X:int = 0; X < Cupcakes.length; X++){
				scene_mc.removeChild(Cupcakes[X][0]);
			}
			Cupcakes.length = 0;
		}
		
		
		
		
		
	}
}