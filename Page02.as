﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page02 extends WBZ_BookPage
	{
		
		private var blueberry:TapSpritesheet;
		private var strawberry:TapSpritesheet;
		private var strawberrySmall:TapSpritesheet;
		
		
		public var blueberrySounds:SoundCollection;
		public var strawberrySounds:SoundCollection;
		
		public var sndThoughtBlueOn:SoundEx			= null;
		public var sndThoughtBlueOff:SoundEx			= null;
		public var sndThoughtStrawOn:SoundEx			= null;
		public var sndThoughtStrawOff:SoundEx			= null;
		
		
		
		private var blueberryBubbleX:int = 0;
		private var blueberryBubbleY:int = 0;
		private var blueberryBubbleSmallX:int = 300;
		private var blueberryBubbleSmallY:int = 300;
		private var blueberryBubbleSpeedX:int = 0;
		private var blueberryBubbleSpeedY:int = 0;
		private var blueberryCounter:int = 0;
		
		private var strawberryBubbleX:int = 0;
		private var strawberryBubbleY:int = 0;
		private var strawberryBubbleSmallX:int = 730;
		private var strawberryBubbleSmallY:int = 300;
		private var strawberryBubbleSpeedX:int = 0;
		private var strawberryBubbleSpeedY:int = 0;
		private var strawberryCounter:int = 0;
		
		//Core.
		public function Page02(){
			super();
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page02/";
			Images.Add( "SS_P02_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_P02_STRAWBERRY_BUBBLE", { container: scene_mc.strawberryThought_mc.strawberryThought_mc, fromFile: true } );
			Images.Add( "SS_P02_BLUEBERRY_BUBBLE", { container: scene_mc.blueberryThought_mc.blueberryThought_mc, fromFile: true } );
			Images.Add( "SS_P02_SCROLL", { container: scene_mc.blueberryThought_mc.scroll_mc, fromFile: true } );
			
			
			/*
		public var sndThoughtBlueOn:SoundEx			= null;
		public var sndThoughtBlueOff:SoundEx			= null;
		public var sndThoughtStrawOn:SoundEx			= null;
		public var sndThoughtStrawOff:SoundEx			= null;
			
			
			
			*/
			
			sndThoughtBlueOn = new SoundEx( "sndThoughtBlueOn", CONFIG::DATA + "sounds/Animation_Sounds/P2_thought_blue_on.mp3", App.soundVolume );
			sndThoughtBlueOff = new SoundEx( "sndThoughtBlueOff", CONFIG::DATA + "sounds/Animation_Sounds/P2_thought_blue_off.mp3", App.soundVolume );
			sndThoughtStrawOn = new SoundEx( "sndThoughtStrawOn", CONFIG::DATA + "sounds/Animation_Sounds/P2_thought_straw_on 2.mp3", App.soundVolume );
			sndThoughtStrawOff = new SoundEx( "sndThoughtStrawOff", CONFIG::DATA + "sounds/Animation_Sounds/P2_thought_straw_off 2.mp3", App.soundVolume );
			
			
			
			blueberrySounds = SoundCollection.GetAnimSounds( "P02_BLUEBERRYtap", [1,2,3,4] );
			var blueberry1Seq:SpriteSheetSequence = new SpriteSheetSequence( "blueberry1", SpriteSheetSequence.createRange( 0, 25, false, 25 ), false, 60 );
			var blueberry2Seq:SpriteSheetSequence = new SpriteSheetSequence( "blueberry2", SpriteSheetSequence.createRange( 43, 55, false, 0 ), false, 60 );
			blueberry = spriteSheetLoader.Add( "SS_P02_BLUEBERRY", 60, blueberrySounds, null, false, false, new <SpriteSheetSequence>[blueberry1Seq,blueberry2Seq] );
			blueberry.onComplete = blueberryComplete;
			
			strawberrySounds = SoundCollection.GetAnimSounds( "P02_STRAWBERRYtap", [1,2,3] );
			var strawberry1Seq:SpriteSheetSequence = new SpriteSheetSequence( "strawberry1", SpriteSheetSequence.createRange( 0, 25, false, 25 ), false, 60 );
			var strawberry2Seq:SpriteSheetSequence = new SpriteSheetSequence( "strawberry2", SpriteSheetSequence.createRange( 43, 56, false, 0 ), false, 60 );
			strawberry = spriteSheetLoader.Add( "SS_P02_STRAWBERRY", 60, strawberrySounds, null, false, false, new <SpriteSheetSequence>[strawberry1Seq,strawberry2Seq] );
			strawberry.onComplete = strawberryComplete;
			
			strawberrySmall = spriteSheetLoader.Add( "SS_P02_MINI_STRAWBERRY_ANIM", 60, null, null, false, false, null, null, scene_mc.blueberryThought_mc.SS_P02_MINI_STRAWBERRY_ANIM);
			
			
			initBubblesXY();
			initBubbles();
			
			sparklers.Add(scene_mc.SS_P02_STRAWBERRY_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.SS_P02_BLUEBERRY_BTN, scene_mc.Sparkler2_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic3";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			scene_mc.SS_P02_BLUEBERRY_BTN.mouseEnabled = true;
			blueberryCounter = 0;
			
			scene_mc.SS_P02_STRAWBERRY_BTN.mouseEnabled = true;
			strawberryCounter = 0;
			
			initBubbles();
			stopSounds();
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "SS_P02_BLUEBERRY_BTN":
				{
					stopSounds();
					if(blueberryCounter == 0 && this.step == BookPage.STEP_DONE)
					{
						sndThoughtBlueOn.Start();
					}
					else if(this.step == BookPage.STEP_DONE)
					{
						sndThoughtBlueOff.Start();
					}
					blueberryCounter++;
					scene_mc.SS_P02_BLUEBERRY_BTN.mouseEnabled = false;
				}
				break;
				
				case "SS_P02_STRAWBERRY_BTN":
				{
					stopSounds();
					if(strawberryCounter == 0 && this.step == BookPage.STEP_DONE)
					{
						sndThoughtStrawOn.Start();
					}
					else if(this.step == BookPage.STEP_DONE)
					{
						sndThoughtStrawOff.Start();
					}
					strawberryCounter++;
					scene_mc.SS_P02_STRAWBERRY_BTN.mouseEnabled = false;
				}
				break;
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void{
		   if(isRunning && !isPaused)
		   {
			   moveBubbles();
			   
			   
		   }
		   super.handleFrame( e );
		}		
		
		
		private function moveBubbles():void
		{
			if(blueberryCounter > 0 && blueberryCounter < 900 && scene_mc.blueberryThought_mc.alpha < 1)
			{
				scene_mc.blueberryThought_mc.scaleX += 0.1;
				scene_mc.blueberryThought_mc.scaleY += 0.1;
				scene_mc.blueberryThought_mc.alpha += 0.1;
				scene_mc.blueberryThought_mc.x += blueberryBubbleSpeedX;
				scene_mc.blueberryThought_mc.y += blueberryBubbleSpeedY;
				
				if(scene_mc.blueberryThought_mc.alpha >= 1)
				{
					blueberryCounter = 900;
					scene_mc.blueberryThought_mc.scaleX = 1;
					scene_mc.blueberryThought_mc.scaleY = 1;
					scene_mc.blueberryThought_mc.alpha = 1;
					scene_mc.blueberryThought_mc.x = blueberryBubbleX;
					scene_mc.blueberryThought_mc.y = blueberryBubbleY;
				}
			}
			if(scene_mc.blueberryThought_mc.alpha == 1)
			{
				strawberrySmall.doTap();
			}
			if(blueberryCounter > 900 && scene_mc.blueberryThought_mc.alpha > 0)
			{
				scene_mc.blueberryThought_mc.scaleX -= 0.1;
				scene_mc.blueberryThought_mc.scaleY -= 0.1;
				scene_mc.blueberryThought_mc.alpha -= 0.1;
				scene_mc.blueberryThought_mc.x -= blueberryBubbleSpeedX;
				scene_mc.blueberryThought_mc.y -= blueberryBubbleSpeedY;
				
				if(scene_mc.blueberryThought_mc.alpha <= 0)
				{
					blueberryCounter = 0;
					scene_mc.blueberryThought_mc.scaleX = 0;
					scene_mc.blueberryThought_mc.scaleY = 0;
					scene_mc.blueberryThought_mc.alpha = 0;
					scene_mc.blueberryThought_mc.x = blueberryBubbleSmallX;
					scene_mc.blueberryThought_mc.y = blueberryBubbleSmallY;
				}
			}
			
			
			
			
			if(strawberryCounter > 0 && strawberryCounter < 900 && scene_mc.strawberryThought_mc.alpha < 1)
			{
				scene_mc.strawberryThought_mc.scaleX += 0.1;
				scene_mc.strawberryThought_mc.scaleY += 0.1;
				scene_mc.strawberryThought_mc.alpha += 0.1;
				scene_mc.strawberryThought_mc.x += strawberryBubbleSpeedX;
				scene_mc.strawberryThought_mc.y += strawberryBubbleSpeedY;
				
				if(scene_mc.strawberryThought_mc.alpha >= 1)
				{
					strawberryCounter = 900;
					scene_mc.strawberryThought_mc.scaleX = 1;
					scene_mc.strawberryThought_mc.scaleY = 1;
					scene_mc.strawberryThought_mc.alpha = 1;
					scene_mc.strawberryThought_mc.x = strawberryBubbleX;
					scene_mc.strawberryThought_mc.y = strawberryBubbleY;
				}
			}
			if(strawberryCounter > 900 && scene_mc.strawberryThought_mc.alpha > 0)
			{
				scene_mc.strawberryThought_mc.scaleX -= 0.1;
				scene_mc.strawberryThought_mc.scaleY -= 0.1;
				scene_mc.strawberryThought_mc.alpha -= 0.1;
				scene_mc.strawberryThought_mc.x -= strawberryBubbleSpeedX;
				scene_mc.strawberryThought_mc.y -= strawberryBubbleSpeedY;
				
				if(scene_mc.strawberryThought_mc.alpha <= 0)
				{
					strawberryCounter = 0;
					scene_mc.strawberryThought_mc.scaleX = 0;
					scene_mc.strawberryThought_mc.scaleY = 0;
					scene_mc.strawberryThought_mc.alpha = 0;
					scene_mc.strawberryThought_mc.x = strawberryBubbleSmallX;
					scene_mc.strawberryThought_mc.y = strawberryBubbleSmallY;
					
				}
			}
			
			
			
			if(blueberryCounter > 0 && blueberryCounter != 900)
			{
			   blueberryCounter++;
			}
			
			if(strawberryCounter > 0 && strawberryCounter != 900)
			{
			   strawberryCounter++;
			}
		}
		
		
		private function initBubblesXY():void
		{
			blueberryBubbleX = scene_mc.blueberryThought_mc.x;
			blueberryBubbleY = scene_mc.blueberryThought_mc.y;
			strawberryBubbleX = scene_mc.strawberryThought_mc.x;
			strawberryBubbleY = scene_mc.strawberryThought_mc.y;
		}
		
		
		private function initBubbles():void
		{
			scene_mc.blueberryThought_mc.scaleX = 0;
			scene_mc.blueberryThought_mc.scaleY = 0;
			scene_mc.blueberryThought_mc.alpha = 0;
			scene_mc.blueberryThought_mc.x = blueberryBubbleSmallX;
			scene_mc.blueberryThought_mc.y = blueberryBubbleSmallY;
			blueberryBubbleSpeedX = (blueberryBubbleX - scene_mc.blueberryThought_mc.x) / 10;
			blueberryBubbleSpeedY = (blueberryBubbleY - scene_mc.blueberryThought_mc.y) / 10;
			
			
			scene_mc.strawberryThought_mc.scaleX = 0;
			scene_mc.strawberryThought_mc.scaleY = 0;
			scene_mc.strawberryThought_mc.alpha = 0;
			scene_mc.strawberryThought_mc.x = strawberryBubbleSmallX;
			scene_mc.strawberryThought_mc.y = strawberryBubbleSmallY;
			strawberryBubbleSpeedX = (strawberryBubbleX - scene_mc.strawberryThought_mc.x) / 10;
			strawberryBubbleSpeedY = (strawberryBubbleY - scene_mc.strawberryThought_mc.y) / 10;
		}
		
		
		private function blueberryComplete(e:TapSpritesheet):void
		{
			scene_mc.SS_P02_BLUEBERRY_BTN.mouseEnabled = true;
		}
		
		
		private function strawberryComplete(e:TapSpritesheet):void
		{
			scene_mc.SS_P02_STRAWBERRY_BTN.mouseEnabled = true;
		}
		
		
		private function stopSounds():void
		{
			sndThoughtBlueOn.Stop();
			sndThoughtBlueOff.Stop();
			sndThoughtStrawOn.Stop();
			sndThoughtStrawOff.Stop();
		}
		
		
		
		
		
	}
}