﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page07 extends WBZ_BookPage
	{
		private var baton:TapSpritesheet;
		
		
		public var cherrySounds:SoundCollection;
		public var plumSounds:SoundCollection;
		public var cinnapupSounds:SoundCollection;
		public var pitterpatchSounds:SoundCollection;
		
		public var sndBoombox1:SoundEx			= null;
		public var sndBoombox2:SoundEx			= null;
		public var sndBoombox3:SoundEx			= null;
		public var sndBush1:SoundEx			= null;
		public var sndBush2:SoundEx			= null;
		public var sndFlower1:SoundEx			= null;
		public var sndFlower2:SoundEx			= null;
		
		
		private var Particles:SB_Particles			= null;
		
		
		private var notesToPlay:Boolean = false;
		private var noteSpeed:int = 5;
		
		private var boomboxCounter:int = 0;
		
		
		
		//Core.
		public function Page07(){
			super();
			
			Particles = new SB_Particles(false);
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page07/";
			Images.Add( "SS_P07_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_P07_GRASS_OL", { container: "bushOL_mc", fromFile: true } );
			
			Images.Add( "SS_P07_BIZOOM_BOX", { container: scene_mc.boombox_mc.boombox_mc.boombox_mc, fromFile: true } );
			
			Images.Add( "SS_P07_STRAWBERRIES_02", { container: scene_mc.bushRight_mc.bushRight_mc.bushRight_mc, fromFile: true } );
			Images.Add( "SS_P07_DAFFODILS", { container: scene_mc.flowerRight_mc.flowerRight_mc.flowerRight_mc, fromFile: true } );
			Images.Add( "SS_P07_TULIPS", { container: scene_mc.flowerTop_mc.flowerTop_mc.flowerTop_mc, fromFile: true } );
			Images.Add( "SS_P07_DAISY", { container: scene_mc.flowerMid_mc.flowerMid_mc.flowerMid_mc, fromFile: true } );
			Images.Add( "SS_P07_STRAWBERRIES_01", { container: scene_mc.bushLeft_mc.bushLeft_mc.bushLeft_mc, fromFile: true } );
			Images.Add( "SS_P07_SMALL_FLOWERS", { container: scene_mc.littleFlowersRight_mc.littleFlowersRight_mc.littleFlowersRight_mc, fromFile: true } );
			Images.Add( "SS_P07_NOTE_01", { container: scene_mc.note1_mc.note_mc.note_mc, fromFile: true } );
			Images.Add( "SS_P07_NOTE_03", { container: scene_mc.note2_mc.note_mc.note_mc, fromFile: true } );
			Images.Add( "SS_P07_NOTE_04", { container: scene_mc.note3_mc.note_mc.note_mc, fromFile: true } );
			Images.Add( "SS_P07_NOTE_05", { container: scene_mc.note4_mc.note_mc.note_mc, fromFile: true } );
			
			
			/*
			P07_cinnapup
			P07_pitterpatch
			
		public var cinnapupSounds:SoundCollection;
		public var pitterpatchSounds:SoundCollection;
			
			*/
			
			sndBoombox1 = new SoundEx( "sndBoombox1", CONFIG::DATA + "sounds/Animation_Sounds/P07_boombox_1.mp3", App.soundVolume );
			sndBoombox2 = new SoundEx( "sndBoombox2", CONFIG::DATA + "sounds/Animation_Sounds/P07_boombox_2.mp3", App.soundVolume );
			sndBoombox3 = new SoundEx( "sndBoombox3", CONFIG::DATA + "sounds/Animation_Sounds/P07_boombox_3.mp3", App.soundVolume );
			sndBush1 = new SoundEx( "sndBush1", CONFIG::DATA + "sounds/Animation_Sounds/P07_bush_1.mp3", App.soundVolume );
			sndBush2 = new SoundEx( "sndBush2", CONFIG::DATA + "sounds/Animation_Sounds/P07_bush_2.mp3", App.soundVolume );
			sndFlower1 = new SoundEx( "sndFlower1", CONFIG::DATA + "sounds/Animation_Sounds/P07_flower_1.mp3", App.soundVolume );
			sndFlower2 = new SoundEx( "sndFlower2", CONFIG::DATA + "sounds/Animation_Sounds/P07_flower_2.mp3", App.soundVolume );
			
			
			
			pitterpatchSounds = SoundCollection.GetAnimSounds( "P07_pitterpatch", [1] );
			TapMultiLite.Add( "pitterpatch", { imageName: "SS_P07_PITTERPATCH_00", sounds: pitterpatchSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );

			cherrySounds = SoundCollection.GetAnimSounds( "P07_CHERRYTAP", [1] );
			spriteSheetLoader.Add( "SS_P07_CHERRY_SING", 60, cherrySounds, null, false, false);
			
			cinnapupSounds = SoundCollection.GetAnimSounds( "P07_cinnapup", [1] );
			spriteSheetLoader.Add( "SS_P07_CINNAPUP", 60, cinnapupSounds, null, false, false);
			
			plumSounds = SoundCollection.GetAnimSounds( "P07_PLUMTAP", [1,2] );
			spriteSheetLoader.Add( "SS_P07_PLUM_TOSS", 60, plumSounds, null, false, false);
			
			baton = spriteSheetLoader.Add( "SS_P07_BATON_SPIN", 60, null, null, false, false, null, null, scene_mc.baton_mc.baton_mc.baton_mc );
			
			sparklers.Add(scene_mc.SS_P07_CHERRY_SING_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.SS_P07_PLUM_TOSS_BTN, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.flowerTop_BTN, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.pitterpatch_btn, scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.littleFlowersRight_BTN, scene_mc.Sparkler5_sprkl);
			sparklers.Add(scene_mc.SS_P07_CINNAPUP_BTN, scene_mc.Sparkler6_sprkl);
			sparklers.Add(scene_mc.bushRight_BTN, scene_mc.Sparkler7_sprkl);
			sparklers.Add(scene_mc.boombox_BTN, scene_mc.Sparkler8_sprkl);
			sparklers.Add(scene_mc.flowerMid_BTN, scene_mc.Sparkler9_sprkl);
			sparklers.Add(scene_mc.flowerRight_BTN, scene_mc.Sparkler10_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic4";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			Particles.Clear_Particles_Que();         //Clear Call.	
			Particles.Clear_All_Particles(this);	 //Clear Call.	
			
			scene_mc.note1_mc.gotoAndStop(1);
			scene_mc.note2_mc.gotoAndStop(1);
			scene_mc.note3_mc.gotoAndStop(1);
			scene_mc.note4_mc.gotoAndStop(1);
			
			scene_mc.note1_mc.x = 1500;
			scene_mc.note2_mc.x = 1500;
			scene_mc.note3_mc.x = 1500;
			scene_mc.note4_mc.x = 1500;
			
			notesToPlay = false;
			
			boomboxCounter = 0;
			
			scene_mc.SS_P07_PLUM_TOSS_BTN.mouseEnabled = true;
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			Particles.Clear_Particles_Que();        //Clear Call.
			Particles.Clear_All_Particles(this);	//Clear Call.	
			
			
			stopBoombox();
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "SS_P07_PLUM_TOSS_BTN":
				{
					scene_mc.baton_mc.play();
					scene_mc.SS_P07_PLUM_TOSS_BTN.mouseEnabled = false;
				}
				break;
				
				case "boombox_BTN":
				{
					if(scene_mc.boombox_mc.currentFrame == 1)
					{
						if(scene_mc.note1_mc.x > 1400 &&
						   scene_mc.note2_mc.x > 1400 &&
						   scene_mc.note3_mc.x > 1400 &&
						   scene_mc.note4_mc.x > 1400)
						{
							notesToPlay = true;
						}
						
						stopBoombox();
						boomboxCounter++;
						
						if(boomboxCounter == 1 && this.step == BookPage.STEP_DONE)
						{
							sndBoombox1.Start();
						}
						if(boomboxCounter == 2 && this.step == BookPage.STEP_DONE)
						{
							sndBoombox2.Start();
						}
						if(boomboxCounter >= 3 && this.step == BookPage.STEP_DONE)
						{
							sndBoombox3.Start();
							boomboxCounter = 0;
						}
						
						
						scene_mc.boombox_mc.play();
					}
				}
				break;
				
				case "bushRight_BTN":
				{
					if(scene_mc.bushRight_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBush1.Start();
						}
						
						Particles.Q_Particles(1, 12, 1050, 350, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 20, 20, 90, 1, 1);
						
						scene_mc.bushRight_mc.play();
					}
				}
				break;
				
				case "flowerRight_BTN":
				{
					if(scene_mc.flowerRight_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower1.Start();
						}
						
						Particles.Q_Particles(1, 12, 850, 100, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 20, 20, 90, 1, 1);
						
						scene_mc.flowerRight_mc.play();
					}
				}
				break;
				
				case "flowerTop_BTN":
				{
					if(scene_mc.flowerTop_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower2.Start();
						}
						
						Particles.Q_Particles(1, 12, 450, 100, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 270, 20, 20, 90, 1, 1);
						
						scene_mc.flowerTop_mc.play();
					}
				}
				break;
				
				case "flowerMid_BTN":
				{
					if(scene_mc.flowerMid_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower1.Start();
						}
						
						Particles.Q_Particles(1, 12, 420, 250, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 15, 0, 90, 1, 1);
						
						scene_mc.flowerMid_mc.play();
					}
				}
				break;
				
				case "bushLeft_BTN":
				{
					if(scene_mc.bushLeft_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBush2.Start();
						}
						
						Particles.Q_Particles(1, 12, -50, 100, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 315, 20, 5, 90, 1, 1);
						
						scene_mc.bushLeft_mc.play();
					}
				}
				break;
				
				case "littleFlowersRight_BTN":
				{
					if(scene_mc.littleFlowersRight_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower2.Start();
						}
						
						Particles.Q_Particles(1, 12, 825, 250, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 15, 10, 90, 1, 1);
						
						scene_mc.littleFlowersRight_mc.play();
					}
				}
				break;
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Particles.SB_Update(this);
				plumAnim();
				playNotes();
				
				
			}
			super.handleFrame( e );
		}		
		
		
		private function plumAnim():void
		{
			if(scene_mc.baton_mc.currentFrame > 12 && scene_mc.baton_mc.currentFrame < 22)
			{
				baton.doTap();
			}
			if(scene_mc.baton_mc.currentFrame == scene_mc.baton_mc.totalFrames)
			{
				scene_mc.SS_P07_PLUM_TOSS_BTN.mouseEnabled = true;
			}
		}
		
		
		private function playNotes():void
		{
			if(notesToPlay)
			{
				
				notesToPlay = false;
				
				var whichNoteNotPlayed:int = Math.floor(Math.random() * 4) + 1;
				
				if(whichNoteNotPlayed != 1)
				{
					scene_mc.note1_mc.x = Math.floor(Math.random() * 100) + 741;
					scene_mc.note1_mc.y = Math.floor(Math.random() * 100) + 400;
					scene_mc.note1_mc.play();
				}
				
				if(whichNoteNotPlayed != 2)
				{
					scene_mc.note2_mc.x = Math.floor(Math.random() * 100) + 741;
					scene_mc.note2_mc.y = Math.floor(Math.random() * 100) + 400;
					scene_mc.note2_mc.play();
				}
				
				if(whichNoteNotPlayed != 3)
				{
					scene_mc.note3_mc.x = Math.floor(Math.random() * 100) + 741;
					scene_mc.note3_mc.y = Math.floor(Math.random() * 100) + 400;
					scene_mc.note3_mc.play();
				}
				
				if(whichNoteNotPlayed != 4)
				{
					scene_mc.note4_mc.x = Math.floor(Math.random() * 100) + 741;
					scene_mc.note4_mc.y = Math.floor(Math.random() * 100) + 400;
					scene_mc.note4_mc.play();
				}
			}
			
			if(scene_mc.note1_mc.x < 1400)
			{
				scene_mc.note1_mc.y -= noteSpeed;
				
				if(scene_mc.note1_mc.currentFrame == scene_mc.note1_mc.totalFrames)
				{
					scene_mc.note1_mc.gotoAndStop(1);
					scene_mc.note1_mc.x = 1500;
				}
			}
			
			if(scene_mc.note2_mc.x < 1400)
			{
				scene_mc.note2_mc.y -= noteSpeed;
				
				if(scene_mc.note2_mc.currentFrame == scene_mc.note2_mc.totalFrames)
				{
					scene_mc.note2_mc.gotoAndStop(1);
					scene_mc.note2_mc.x = 1500;
				}
			}
			
			if(scene_mc.note3_mc.x < 1400)
			{
				scene_mc.note3_mc.y -= noteSpeed;
				
				if(scene_mc.note3_mc.currentFrame == scene_mc.note3_mc.totalFrames)
				{
					scene_mc.note3_mc.gotoAndStop(1);
					scene_mc.note3_mc.x = 1500;
				}
			}
			
			if(scene_mc.note4_mc.x < 1400)
			{
				scene_mc.note4_mc.y -= noteSpeed;
				
				if(scene_mc.note4_mc.currentFrame == scene_mc.note4_mc.totalFrames)
				{
					scene_mc.note4_mc.gotoAndStop(1);
					scene_mc.note4_mc.x = 1500;
				}
			}
		}
		
		
		private function stopBoombox():void
		{
			sndBoombox1.Stop();
			sndBoombox2.Stop();
			sndBoombox3.Stop();
		}
		
		
		
		
		
		
	}
}