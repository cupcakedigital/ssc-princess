﻿package 
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page12 extends WBZ_BookPage
	{
		private var eyes:TapSpritesheet;
		private var glitterOutfits:TapSpritesheet;
		private var princessDresses:TapSpritesheet;
		private var princessUL:TapSpritesheet;
		private var tiaras:TapSpritesheet;
		private var headbands:TapSpritesheet;
		private var dresses:TapSpritesheet;
		private var glovesOL:TapSpritesheet;
		private var glovesUL:TapSpritesheet;
		private var shoes:TapSpritesheet;
		
		
		public var sndVoice1:SoundEx			= null;
		public var sndVoice2:SoundEx			= null;
		public var sndVoice3:SoundEx			= null;
		public var sndMirror:SoundEx			= null;
		public var sndSwipe:SoundEx			= null;
		public var sndTap:SoundEx			= null;
		public var sndNarration:SoundEx			= null;
		
		
		
		private var NarrationOn:Boolean = true;
		private var firstTurn:Boolean = true;
		private var firstTurnOutfit:Boolean = true;
		
		private var WardrobeMove:Boolean = false;
		private var WardrobeFloat:Boolean = false;
		private var WardrobeSnap:Boolean = false;
		private var WardrobeSnapDis:int = 0;
		private var WardrobeSnapCounter:int = 0;
		private var WardrobeMouseDisX:int = 0;
		private var WardrobeFloatDestinationX:int = 0;
		private var WardrobeStartX:int = 0;
		private var WardrobeX:int = 0;
		private var WardrobeY:int = 0;
		private var WardrobeTop:int = 235;
		private var WardrobeMiddle:int = 300;
		private var WardrobeBottom:int = 340;
		private var Wardrobe2Diff:int = 0;
		private var maxMove:int = 150;
		private var WardrobeClickX:int = 0;
		
		private var headbandsStartX:int = 0;
		private var headbandsStartY:int = 0;
		private var tiarasStartX:int = 0;
		private var tiarasStartY:int = 0;
		private var headbandsMove:Boolean = false;
		private var tiarasMove:Boolean = false;
		private var headwearRange:int = 20;
		private var headwearMouseDisX:int = 0;
		private var headwearMouseDisY:int = 0;
		
		
		
		private var voiceCounter:int = 0;
		private var eyeBlinkCounter:int = 0;
		
		
		
		private var clothesArray:Array;
		private var movieClipArray:Array;
		private var movieClipArray2:Array;
		
		
		//Core.
		public function Page12(){
			super();
			
			imageFolder = CONFIG::DATA + "images/pages/page12/";
			reusableFolder = CONFIG::DATA + "images/pages/reusable/";
			
			Images.Add( "BG", { container: "BG", fromFile: true } );
			Images.Add( "tempOutfit", { container: "tempOutfit_mc", fromFile: true } );
			Images.Add( "tempShoes", { container: "tempShoes_mc", fromFile: true } );
			
			Images.Add( "Cherry_Head", { container: "head_mc", fromFile: true } );
			Images.Add( "Reusable/Undies_Top", { container: "undiesTop_mc", fromFile: true } );
			Images.Add( "Strawberry_Cherry_Body", { container: "body_mc", fromFile: true } );
			Images.Add( "Strawberry_Cherry_Arm", { container: "arm_mc", fromFile: true } );
			Images.Add( "Cherry_Hair", { container: "hair_mc", fromFile: true } );
			Images.Add( "Reusable/MIRRORSHINE", { container: scene_mc.mirrorShine_mc.mirrorShine_mc.mirrorShine_mc, fromFile: true } );
			Images.Add( "Reusable/Left_Closet_Purple_Arrow", { container: "arrowLeft_mc", fromFile: true } );
			Images.Add( "Reusable/Right_Closet_Purple_Arrow", { container: "arrowRight_mc", fromFile: true } );
			
			
			/*
			P09_CHERRY_
			sndVoice
			*/
			
			sndVoice1 = new SoundEx( "sndVoice1", CONFIG::DATA + "sounds/Animation_Sounds/P12_STRAWBERRY_1.mp3", App.soundVolume );
			sndVoice2 = new SoundEx( "sndVoice2", CONFIG::DATA + "sounds/Animation_Sounds/P12_STRAWBERRY_2.mp3", App.soundVolume );
			sndVoice3 = new SoundEx( "sndVoice3", CONFIG::DATA + "sounds/Animation_Sounds/P12_STRAWBERRY_3.mp3", App.soundVolume );
			sndMirror = new SoundEx( "sndMirror", CONFIG::DATA + "sounds/Animation_Sounds/P09_mirror.mp3", App.soundVolume );
			sndSwipe = new SoundEx( "sndSwipe", CONFIG::DATA + "sounds/Animation_Sounds/P09_swipe.mp3", App.soundVolume );
			sndTap = new SoundEx( "sndTap", CONFIG::DATA + "sounds/Animation_Sounds/P09_tap.mp3", App.soundVolume );
			sndNarration = new SoundEx( "sndNarration", CONFIG::DATA + "sounds/Animation_Sounds/DressUpNarration.mp3", App.soundVolume );
			
			
			
			eyes = spriteSheetLoader.Add( "Cherry_Blink", 60, null, null, false, false);
			
			var items1Seq:SpriteSheetSequence = new SpriteSheetSequence( "items1", SpriteSheetSequence.createRange( 0, 0, false, 0 ), false, 60 );
			var items2Seq:SpriteSheetSequence = new SpriteSheetSequence( "items2", SpriteSheetSequence.createRange( 1, 1, false, 1 ), false, 60 );
			var items3Seq:SpriteSheetSequence = new SpriteSheetSequence( "items3", SpriteSheetSequence.createRange( 2, 2, false, 2 ), false, 60 );
			var items4Seq:SpriteSheetSequence = new SpriteSheetSequence( "items4", SpriteSheetSequence.createRange( 3, 3, false, 3 ), false, 60 );
			var items5Seq:SpriteSheetSequence = new SpriteSheetSequence( "items5", SpriteSheetSequence.createRange( 4, 4, false, 4 ), false, 60 );
			var items6Seq:SpriteSheetSequence = new SpriteSheetSequence( "items6", SpriteSheetSequence.createRange( 5, 5, false, 5 ), false, 60 );
			var items7Seq:SpriteSheetSequence = new SpriteSheetSequence( "items7", SpriteSheetSequence.createRange( 6, 6, false, 6 ), false, 60 );
			var items8Seq:SpriteSheetSequence = new SpriteSheetSequence( "items8", SpriteSheetSequence.createRange( 7, 7, false, 7 ), false, 60 );
			var items9Seq:SpriteSheetSequence = new SpriteSheetSequence( "items9", SpriteSheetSequence.createRange( 8, 8, false, 8 ), false, 60 );
			var items10Seq:SpriteSheetSequence = new SpriteSheetSequence( "items10", SpriteSheetSequence.createRange( 9, 9, false, 9 ), false, 60 );
			
			glitterOutfits = spriteSheetLoader.Add( "Reusable/Glitter_Outfits", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq,items5Seq,items6Seq] );
			
			princessDresses = spriteSheetLoader.Add( "Reusable/Princess_Dresses", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq,items5Seq,items6Seq,items7Seq,items8Seq] );
			princessDresses.onComplete = princessDressesComplete;
			
			princessUL = spriteSheetLoader.Add( "Reusable/Princess_Dress_UL", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq,items5Seq] );
			
			tiaras = spriteSheetLoader.Add( "Reusable/Tiaras", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq,items5Seq,items6Seq] );
			
			headbands = spriteSheetLoader.Add( "Reusable/Headbands", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq] );
			
			dresses = spriteSheetLoader.Add( "Reusable/Dresses", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq,items5Seq,items6Seq] );
			
			glovesOL = spriteSheetLoader.Add( "Reusable/Gloves", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq] );
			
			glovesUL = spriteSheetLoader.Add( "Reusable/Gloves_UL", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq] );
			
			shoes = spriteSheetLoader.Add( "Reusable/Shoes", 60, null, null, false, false, new <SpriteSheetSequence>[items1Seq,items2Seq,items3Seq,items4Seq,items5Seq,items6Seq,items7Seq,items8Seq,items9Seq,items10Seq] );
			
			
			
			movieClipArray = new Array();
			movieClipArray2 = new Array();
			
			headbandsStartX = scene_mc.Headbands.x;
			headbandsStartY = scene_mc.Headbands.y;
			tiarasStartX = scene_mc.Tiaras.x;
			tiarasStartY = scene_mc.Tiaras.y;
			
			initPage();
			
			
			for(var X:int = 0; X < clothesArray.length; X++)
			{
				spawnWardrobe(X);
			}
			sparklers.Add(scene_mc.arrowRight_BTN,scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.arrowLeft_BTN,scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.mirrorShine_BTN,scene_mc.Sparkler4_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic5";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			NarrationOn = true;
			firstTurn = true;
			firstTurnOutfit = true;
			WardrobeMove = false;
			WardrobeFloat = false;
			WardrobeSnap = false;
			WardrobeSnapCounter = 0;
			voiceCounter = 0;
			eyeBlinkCounter = 0;
			headbandsMove = false;
			tiarasMove = false;
			initPage();
			
			stopVoices();
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();

			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			if(scene_mc.wardrobe_mc.hotzone_mc.hitTestPoint(mouseX, mouseY, true))
			{
				WardrobeMove = true;
				WardrobeFloat = false;
				WardrobeSnap = false;
				WardrobeMouseDisX = mouseX - scene_mc.wardrobe_mc.slider_mc.x;
				WardrobeX = mouseX;
				WardrobeY = mouseY;
				WardrobeClickX = scene_mc.wardrobe_mc.slider_mc.x;
			}
			
			
			switch(e.target.name)
			{
				case "mirrorShine_BTN":
				{
					if(scene_mc.mirrorShine_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndMirror.Start();
						}
						
						scene_mc.mirrorShine_mc.play();
					}
				}
				break;
				
				case "headwear_BTN":
				{
					if(scene_mc.Headbands.visible == true)
					{
						headbandsMove = true;
						headwearMouseDisX = mouseX - scene_mc.Headbands.x;
						headwearMouseDisY = mouseY - scene_mc.Headbands.y;
					}
					else if(scene_mc.Tiaras.visible == true)
					{
						tiarasMove = true;
						headwearMouseDisX = mouseX - scene_mc.Tiaras.x;
						headwearMouseDisY = mouseY - scene_mc.Tiaras.y;
					}
				}
				break;
				
				case "arrowLeft_BTN":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndSwipe.Start();
					}
					
					WardrobeMouseDisX = mouseX - scene_mc.wardrobe_mc.slider_mc.x;
					WardrobeX = mouseX;
					WardrobeY = mouseY;
					WardrobeClickX = scene_mc.wardrobe_mc.slider_mc.x;
					scene_mc.wardrobe_mc.slider_mc.x += 21;
					WardrobeMove = false;
					WardrobeFloat = true;
					WardrobeSnap = false;
					WardrobeFloatDestinationX = mouseX + 1000;
				}
				break;
				
				case "arrowRight_BTN":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndSwipe.Start();
					}
					
					WardrobeMouseDisX = mouseX - scene_mc.wardrobe_mc.slider_mc.x;
					WardrobeX = mouseX;
					WardrobeY = mouseY;
					WardrobeClickX = scene_mc.wardrobe_mc.slider_mc.x;
					scene_mc.wardrobe_mc.slider_mc.x -= 21;
					WardrobeMove = false;
					WardrobeFloat = true;
					WardrobeSnap = false;
					WardrobeFloatDestinationX = mouseX - 1000;
				}
				break;
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			if(WardrobeX >= mouseX - 20 && WardrobeX <= mouseX + 20 && 
			   WardrobeY >= mouseY - 20 && WardrobeY <= mouseY + 20 &&
			   scene_mc.wardrobe_mc.click_mc.hitTestPoint(mouseX, mouseY, true))
			{
				WardrobeMove = false;
				scene_mc.wardrobe_mc.slider_mc.x = WardrobeClickX;
				if ( this.step == BookPage.STEP_DONE )
				{
					sndTap.Start();
				}
				
				changeClothes();
			}
			if(WardrobeMove)
			{
				if ( this.step == BookPage.STEP_DONE )
				{
					sndSwipe.Start();
				}
				
				WardrobeMove = false;
				WardrobeFloat = true;
				WardrobeSnap = false;
				WardrobeFloatDestinationX = mouseX;
			}
			
			headbandsMove = false;
			tiarasMove = false;
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				
				if(firstTurn)
				{
					initClothes();
					firstTurn = false;
				}
				
				if(NarrationOn && this.step == BookPage.STEP_DONE)
				{
					sndNarration.Start();
					NarrationOn = false;
				}
				
			   moveWardrobe();
			   eyeBlink();
			   moveHeadwear();
			}
			super.handleFrame( e );
		}		
		
		
		private function initClothes():void
		{
			glitterOutfits.playSequence( "items6" );
			princessDresses.playSequence( "items1" );
			princessUL.playSequence( "items5" );
			tiaras.playSequence( "items6" );
			headbands.playSequence( "items4" );
			dresses.playSequence( "items6" );
			glovesOL.playSequence( "items4" );
			glovesUL.playSequence( "items4" );
			shoes.playSequence( "items1" );
		}
		
		private function initPage():void
		{
			scene_mc.tempOutfit_mc.visible = true;
			scene_mc.tempShoes_mc.visible = true;
			scene_mc.undiesTop_mc.visible = false;
			
			scene_mc.Glitter_Outfits.visible = false;
			scene_mc.Princess_Dresses.visible = false;
			scene_mc.Princess_Dress_UL.visible = false;
			scene_mc.Tiaras.visible = false;
			scene_mc.Headbands.visible = false;
			scene_mc.Dresses.visible = false;
			scene_mc.Gloves.visible = false;
			scene_mc.Gloves_UL.visible = false;
			scene_mc.Shoes.visible = false;
			
			WardrobeStartX = scene_mc.wardrobe_mc.slider_mc.x;
			
			clothesArray = new Array();
			clothesArray.push("item1",
							   "item2",
							   "item3",
							   "item4",
							   "item5",
							   "item6",
							   "item7",
							   "item8",
							   "item9",
							   "item10",
							   "item11",
							   "item12",
							   "item13",
							   "item14",
							   "item15",
							   "item16",
							   "item17",
							   "item18",
							   "item19",
							   "item20",
							   "item21",
							   "item22",
							   "item23",
							   "item24",
							   "item25");
							   
		}
		
		
		private function princessDressesComplete(e:TapSpritesheet):void
		{
			if(firstTurnOutfit)
			{
				firstTurnOutfit = false;
				scene_mc.tempOutfit_mc.visible = false;
				scene_mc.tempShoes_mc.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Shoes.visible = true;
				
				scene_mc.wardrobe_mc.slider2_mc.x = scene_mc.wardrobe_mc.slider_mc.x - scene_mc.wardrobe_mc.slider2_mc.width - Wardrobe2Diff;
			
				//trace("scene_mc.wardrobe_mc.slider2_mc.x: " + scene_mc.wardrobe_mc.slider2_mc.x);
				//trace("scene_mc.wardrobe_mc.slider2_mc.width: " + scene_mc.wardrobe_mc.slider2_mc.width);
			}
			
		}
		
		
		private function spawnWardrobe(number:int):void
		{
			var NewItem:MovieClip = new MovieClip();
			var NewItem2:MovieClip = new MovieClip();
			
			
			NewItem.x = 400 * number;
			NewItem2.x = 400 * number;

			if(number >= 10 && number < 16)
			{
				NewItem.y = 0;
				NewItem2.y = 0;
			}
			else if(number == 16)
			{
				NewItem.y = 20;
				NewItem2.y = 20;
			}
			else
			{
				NewItem.y = 75;
				NewItem2.y = 75;
			}
			
			Images.Add( "Reusable/" + clothesArray[number], { container: NewItem, fromFile: true } );
			Images.Add( "Reusable/" + clothesArray[number], { container: NewItem2, fromFile: true } );
			scene_mc.wardrobe_mc.slider_mc.addChildAt(NewItem, (scene_mc.wardrobe_mc.slider_mc.numChildren));
			scene_mc.wardrobe_mc.slider2_mc.addChildAt(NewItem2, (scene_mc.wardrobe_mc.slider2_mc.numChildren));
			
			movieClipArray.push(NewItem);
			movieClipArray2.push(NewItem2);
		}
		
		
		private function moveWardrobe():void
		{
			var speedX:Number = 0;
			var speedY:Number = 0;
			
			if(WardrobeMove)
			{
				speedX = (mouseX - (scene_mc.wardrobe_mc.slider_mc.x + WardrobeMouseDisX) ) / 3;
				
				scene_mc.wardrobe_mc.slider_mc.x += speedX;
				
				
				if(scene_mc.wardrobe_mc.slider_mc.x > WardrobeClickX + maxMove)
				{
					scene_mc.wardrobe_mc.slider_mc.x = WardrobeClickX + maxMove;
				}
				
				if(scene_mc.wardrobe_mc.slider_mc.x < WardrobeClickX - maxMove)
				{
					scene_mc.wardrobe_mc.slider_mc.x = WardrobeClickX - maxMove;
				}
				movePart2();
			}
			if(WardrobeFloat)
			{
				/*
				speedX = (WardrobeFloatDestinationX - (scene_mc.wardrobe_mc.slider_mc.x + WardrobeMouseDisX) ) / 5;
				
				scene_mc.wardrobe_mc.slider_mc.x += speedX;
				movePart2();
				
				
				
				if(scene_mc.wardrobe_mc.slider_mc.x + WardrobeMouseDisX >= WardrobeFloatDestinationX - 10 && scene_mc.wardrobe_mc.slider_mc.x + WardrobeMouseDisX <= WardrobeFloatDestinationX + 10)
				{
					*/
					WardrobeFloat = false;
					WardrobeSnap = true;
					var bigDressMin:int;
					var bigDressMax:int;
					
					if(WardrobeX > WardrobeFloatDestinationX)
					{
						bigDressMin = 9;
						bigDressMax = 11;
					}
					else
					{
						bigDressMin = 10;
						bigDressMax = 12;
					}
					
					//this is for all the other elements
					var clothesDirection:int;
					
					if(WardrobeX > WardrobeFloatDestinationX)
					{
						if(scene_mc.wardrobe_mc.slider_mc.x > 80)
						{
							clothesDirection = 0;
						}
						else
						{
							clothesDirection = 400;
						}
					}
					else
					{
						if(scene_mc.wardrobe_mc.slider_mc.x > 80)
						{
							clothesDirection = -400;
						}
						else
						{
							if((WardrobeStartX - scene_mc.wardrobe_mc.slider_mc.x) / 400 > -1 &&
							(WardrobeStartX - scene_mc.wardrobe_mc.slider_mc.x) / 400 < 0)
							{
								clothesDirection = -400;
							}
							else
							{
								clothesDirection = 0;
							}
							
						}
					}
					
					
					WardrobeSnapDis = (((WardrobeStartX - scene_mc.wardrobe_mc.slider_mc.x) % 400) - clothesDirection) / 8;
					/*
					trace("WardrobeStartX: " + WardrobeStartX);
					trace("WardrobeFloatDestinationX: " + WardrobeFloatDestinationX);
					trace("clothesDirection: " + clothesDirection);
					trace("WardrobeSnapDis: " + WardrobeSnapDis);
					trace("scene_mc.wardrobe_mc.slider_mc.x: " + scene_mc.wardrobe_mc.slider_mc.x);
					trace("****************************");*/
					
					
					WardrobeSnapCounter = 0;
					
				//}
				
			}
			if(WardrobeSnap)
			{
				scene_mc.wardrobe_mc.slider_mc.x += WardrobeSnapDis;
				movePart2();
				
				
				WardrobeSnapCounter++;
				
				if(WardrobeSnapCounter == 8)
				{
					//trace("scene_mc.wardrobe_mc.slider_mc.x: " + scene_mc.wardrobe_mc.slider_mc.x);
					/**/
					
					if(scene_mc.wardrobe_mc.slider_mc.x > 5700 || scene_mc.wardrobe_mc.slider_mc.x < -13600)
					{
						scene_mc.wardrobe_mc.slider_mc.x = scene_mc.wardrobe_mc.slider2_mc.x;
						scene_mc.wardrobe_mc.slider2_mc.x = scene_mc.wardrobe_mc.slider_mc.x + scene_mc.wardrobe_mc.slider_mc.width;
						
					}
					//trace("***************************");
					WardrobeSnapCounter = 0;
					WardrobeSnap = false;
				}
			}
			
			/*
			if(scene_mc.wardrobe_mc.slider_mc.x > -2000)
			{
				scene_mc.wardrobe_mc.slider2_mc.x = scene_mc.wardrobe_mc.slider_mc.x - scene_mc.wardrobe_mc.slider2_mc.width - Wardrobe2Diff;
			}
			else
			{
				scene_mc.wardrobe_mc.slider2_mc.x = scene_mc.wardrobe_mc.slider_mc.x + scene_mc.wardrobe_mc.slider_mc.width;
			}
			*/
			
			
			
			
			
		}
		
		
		private function changeClothes():void
		{
			
			if(movieClipArray[0].hitTestPoint(mouseX, mouseY, true) ||
			   movieClipArray2[0].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = true;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				dresses.playSequence( "items1" );
				voice();
			}
			else if(movieClipArray[1].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[1].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = true;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				dresses.playSequence( "items2" );
				voice();
			}
			else if(movieClipArray[2].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[2].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = true;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				dresses.playSequence( "items3" );
				voice();
			}
			else if(movieClipArray[3].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[3].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = true;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				dresses.playSequence( "items4" );
				voice();
			}
			else if(movieClipArray[4].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[4].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = true;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				dresses.playSequence( "items5" );
				voice();
			}
			
			
			else if(movieClipArray[5].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[5].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = true;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				glitterOutfits.playSequence( "items1" );
				voice();
			}
			else if(movieClipArray[6].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[6].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = true;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				glitterOutfits.playSequence( "items2" );
				voice();
			}
			else if(movieClipArray[7].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[7].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = true;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				glitterOutfits.playSequence( "items3" );
				voice();
			}
			else if(movieClipArray[8].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[8].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = true;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				glitterOutfits.playSequence( "items4" );
				voice();
			}
			else if(movieClipArray[9].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[9].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = true;
				scene_mc.Princess_Dresses.visible = false;
				scene_mc.Princess_Dress_UL.visible = false;
				glitterOutfits.playSequence( "items5" );
				voice();
			}
			
			
			else if(movieClipArray[10].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[10].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items1" );
				princessUL.playSequence( "items1" );
				voice();
			}
			else if(movieClipArray[11].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[11].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items2" );
				princessUL.playSequence( "items2" );
				voice();
			}
			else if(movieClipArray[12].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[12].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items3" );
				princessUL.playSequence( "items3" );
				voice();
			}
			else if(movieClipArray[13].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[13].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items4" );
				princessUL.playSequence( "items4" );
				voice();
			}
			else if(movieClipArray[14].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[14].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items5" );
				princessUL.playSequence( "items5" );
				voice();
			}
			else if(movieClipArray[15].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[15].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items6" );
				princessUL.playSequence( "items5" );
				voice();
			}
			else if(movieClipArray[16].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[16].hitTestPoint(mouseX, mouseY, true))
			{
				scene_mc.Dresses.visible = false;
				scene_mc.Glitter_Outfits.visible = false;
				scene_mc.Princess_Dresses.visible = true;
				scene_mc.Princess_Dress_UL.visible = true;
				princessDresses.playSequence( "items7" );
				princessUL.playSequence( "items5" );
				voice();
			}
			
			
			else if(movieClipArray[17].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[17].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeTop)
				{
					scene_mc.Gloves.visible = true;
					scene_mc.Gloves_UL.visible = true;
					glovesOL.playSequence( "items1" );
					glovesUL.playSequence( "items1" );
				}
				else if(mouseY > WardrobeBottom)
				{
					scene_mc.Gloves.visible = true;
					scene_mc.Gloves_UL.visible = true;
					glovesOL.playSequence( "items3" );
					glovesUL.playSequence( "items3" );
				}
				else
				{
					scene_mc.Gloves.visible = true;
					scene_mc.Gloves_UL.visible = true;
					glovesOL.playSequence( "items2" );
					glovesUL.playSequence( "items2" );
				}
				voice();
				
			}
			
			
			else if(movieClipArray[18].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[18].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeTop)
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items1" );
				}
				else if(mouseY > WardrobeBottom)
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items3" );
				}
				else
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items2" );
				}
				voice();
			}
			else if(movieClipArray[19].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[19].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeTop)
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items5" );
				}
				else if(mouseY > WardrobeBottom)
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items6" );
				}
				else
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items4" );
				}
				voice();
			}
			else if(movieClipArray[20].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[20].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeTop)
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items9" );
				}
				else if(mouseY > WardrobeBottom)
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items8" );
				}
				else
				{
					scene_mc.Shoes.visible = true;
					shoes.playSequence( "items7" );
				}
				voice();
			}
			
			
			
			else if(movieClipArray[21].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[21].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeMiddle)
				{
					scene_mc.Headbands.visible = true;
					scene_mc.Tiaras.visible = false;
					headbands.playSequence( "items1" );
				}
				else
				{
					scene_mc.Headbands.visible = true;
					scene_mc.Tiaras.visible = false;
					headbands.playSequence( "items2" );
				}
				voice();
				
				scene_mc.Headbands.x = headbandsStartX;
				scene_mc.Headbands.y = headbandsStartY;
				scene_mc.Tiaras.x = tiarasStartX;
				scene_mc.Tiaras.y = tiarasStartY;
			
			}
			else if(movieClipArray[22].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[22].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeMiddle)
				{
					scene_mc.Headbands.visible = true;
					scene_mc.Tiaras.visible = false;
					headbands.playSequence( "items3" );
				}
				else
				{
					scene_mc.Headbands.visible = false;
					scene_mc.Tiaras.visible = true;
					tiaras.playSequence( "items1" );
				}
				voice();
				
				scene_mc.Headbands.x = headbandsStartX;
				scene_mc.Headbands.y = headbandsStartY;
				scene_mc.Tiaras.x = tiarasStartX;
				scene_mc.Tiaras.y = tiarasStartY;
			}
			else if(movieClipArray[23].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[23].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeMiddle)
				{
					scene_mc.Headbands.visible = false;
					scene_mc.Tiaras.visible = true;
					tiaras.playSequence( "items2" );
				}
				else
				{
					scene_mc.Headbands.visible = false;
					scene_mc.Tiaras.visible = true;
					tiaras.playSequence( "items3" );
				}
				voice();
				
				scene_mc.Headbands.x = headbandsStartX;
				scene_mc.Headbands.y = headbandsStartY;
				scene_mc.Tiaras.x = tiarasStartX;
				scene_mc.Tiaras.y = tiarasStartY;
			}
			else if(movieClipArray[24].hitTestPoint(mouseX, mouseY, true) ||
					movieClipArray2[24].hitTestPoint(mouseX, mouseY, true))
			{
				if(mouseY < WardrobeMiddle)
				{
					scene_mc.Headbands.visible = false;
					scene_mc.Tiaras.visible = true;
					tiaras.playSequence( "items4" );
				}
				else
				{
					scene_mc.Headbands.visible = false;
					scene_mc.Tiaras.visible = true;
					tiaras.playSequence( "items5" );
				}
				voice();
				
				scene_mc.Headbands.x = headbandsStartX;
				scene_mc.Headbands.y = headbandsStartY;
				scene_mc.Tiaras.x = tiarasStartX;
				scene_mc.Tiaras.y = tiarasStartY;
			}
		}
		
		
		private function voice():void
		{
			if ( this.step == BookPage.STEP_DONE )
			{
				voiceCounter++;
				
				if(voiceCounter == 3)
				{
					stopVoices();
					sndVoice1.Start();
				}
				if(voiceCounter == 6)
				{
					stopVoices();
					sndVoice2.Start();
				}
				if(voiceCounter == 9)
				{
					stopVoices();
					sndVoice3.Start();
					voiceCounter = 0;
				}
				
			}
		}
		
		
		private function stopVoices():void
		{
			sndVoice1.Stop();
			sndVoice2.Stop();
			sndVoice3.Stop();
		}
		
		
		private function movePart2():void
		{
			
			if(WardrobeMove || WardrobeFloat || WardrobeSnap)
			{
				if(scene_mc.wardrobe_mc.slider_mc.x > -2000)
				{
					scene_mc.wardrobe_mc.slider2_mc.x = scene_mc.wardrobe_mc.slider_mc.x - scene_mc.wardrobe_mc.slider2_mc.width - Wardrobe2Diff;
				}
				else
				{
					scene_mc.wardrobe_mc.slider2_mc.x = scene_mc.wardrobe_mc.slider_mc.x + scene_mc.wardrobe_mc.slider_mc.width;
				}
			}
		}
		
		
		private function eyeBlink():void
		{
			eyeBlinkCounter++;
			
			if(eyeBlinkCounter >= 120)
			{
				eyes.doTap();
				eyeBlinkCounter = 0;
			}
		}
		
		
		private function moveHeadwear():void
		{
			if(headbandsMove)
			{
				scene_mc.Headbands.x = mouseX - headwearMouseDisX;
				scene_mc.Headbands.y = mouseY - headwearMouseDisY;
				
				if(scene_mc.Headbands.x > headbandsStartX + headwearRange)
				{
					scene_mc.Headbands.x = headbandsStartX + headwearRange;
				}
				if(scene_mc.Headbands.x < headbandsStartX - headwearRange)
				{
					scene_mc.Headbands.x = headbandsStartX - headwearRange;
				}
				
				if(scene_mc.Headbands.y > headbandsStartY + headwearRange)
				{
					scene_mc.Headbands.y = headbandsStartY + headwearRange;
				}
				if(scene_mc.Headbands.y < headbandsStartY - headwearRange)
				{
					scene_mc.Headbands.y = headbandsStartY - headwearRange;
				}
			}
			
			if(tiarasMove)
			{
				scene_mc.Tiaras.x = mouseX - headwearMouseDisX;
				scene_mc.Tiaras.y = mouseY - headwearMouseDisY;
				
				if(scene_mc.Tiaras.x > tiarasStartX + headwearRange)
				{
					scene_mc.Tiaras.x = tiarasStartX + headwearRange;
				}
				if(scene_mc.Tiaras.x < tiarasStartX - headwearRange)
				{
					scene_mc.Tiaras.x = tiarasStartX - headwearRange;
				}
				
				if(scene_mc.Tiaras.y > tiarasStartY + headwearRange)
				{
					scene_mc.Tiaras.y = tiarasStartY + headwearRange;
				}
				if(scene_mc.Tiaras.y < tiarasStartY - headwearRange)
				{
					scene_mc.Tiaras.y = tiarasStartY - headwearRange;
				}
			}
		}
		
		
		
		
		
		
	}
}