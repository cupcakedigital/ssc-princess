﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page04 extends WBZ_BookPage
	{
		private var cherry:TapSpritesheet;
		private var berrykins:TapSpritesheet;
		private var berrykinGoal:TapSpritesheet;
		private var pole:TapSpritesheet;
		
		
		public var cherrySounds:SoundCollection;
		public var strawberrySounds:SoundCollection;
		public var berrykinsSounds:SoundCollection;
		
		public var sndBall:SoundEx			= null;
		public var sndFlower:SoundEx			= null;
		public var sndFlower2:SoundEx			= null;
		
		
		private var Particles:SB_Particles			= null;
		
		
		
		private var strawberryCounter:int = 0;
		
		
		//Core.
		public function Page04(){
			super();
			
			Particles = new SB_Particles(false);
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page04/";
			Images.Add( "SS_P04_BG", { container: "BG", fromFile: true } );
			Images.Add( "SS_P04_OL_9HOLESIGN", { container: "sign_mc", fromFile: true } );
			Images.Add( "SS_P04_OL_HOOPS", { container: "hoops_mc", fromFile: true } );
			Images.Add( "SS_P04_BUSH_LEFT_TOP", { container: scene_mc.bush1.bush.bush, fromFile: true } );
			Images.Add( "SS_P04_BUSH_LEFT_CENTER", { container: scene_mc.bush2.bush.bush, fromFile: true } );
			Images.Add( "SS_P04_BUSH_LEFT_BOTTOM", { container: scene_mc.bush3.bush.bush, fromFile: true } );
			Images.Add( "SS_P04_BUSH_RIGHT_BOTTOM", { container: scene_mc.bush4.bush.bush, fromFile: true } );
			Images.Add( "SS_P04_BUSH_RIGHT_TOP", { container: scene_mc.bush5.bush.bush, fromFile: true } );
			Images.Add( "SS_P04_BALL", { container: scene_mc.ball_mc.ball_mc.ball_mc, fromFile: true } );
			
			
			/*
			P04_berrykins_cheer
			
			
			*/
			
			sndBall = new SoundEx( "sndBall", CONFIG::DATA + "sounds/Animation_Sounds/P4_strawberry_croquet.mp3", App.soundVolume );
			sndFlower = new SoundEx( "sndFlower", CONFIG::DATA + "sounds/Animation_Sounds/P4_flower.mp3", App.soundVolume );
			sndFlower2 = new SoundEx( "sndFlower2", CONFIG::DATA + "sounds/Animation_Sounds/P4_flower_2.mp3", App.soundVolume );
			
			
			
			cherrySounds = SoundCollection.GetAnimSounds( "P04_CHERRY", [1] );
			cherry = spriteSheetLoader.Add( "SS_P04_CHERRY", 60, cherrySounds, null, false, false);
			
			berrykinsSounds = SoundCollection.GetAnimSounds( "P04_berrykins_cheer", [1] );
			berrykins = spriteSheetLoader.Add( "SS_P04_BERRYKINS", 60, berrykinsSounds, null, false, false);
			
			berrykinGoal = spriteSheetLoader.Add( "SS_P04_BERRYKIN", 60, null, null, false, false);
			
			pole = spriteSheetLoader.Add( "SS_P04_POLE", 60, null, null, false, false);
			pole.onComplete = poleComplete;
			
			strawberrySounds = SoundCollection.GetAnimSounds( "P04_STRAWBERRY", [1] );
			spriteSheetLoader.Add( "SS_P04_STRAWBERRY", 60, strawberrySounds, null, false, false);
			
			sparklers.Add(scene_mc.SS_P04_STRAWBERRY_BTN, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.SS_P04_CHERRY_BTN, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.SS_P04_BERRYKINS_BTN, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.bush5_BTN, scene_mc.Sparkler4_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic5";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			Particles.Clear_Particles_Que();         //Clear Call.	
			Particles.Clear_All_Particles(this);	 //Clear Call.	
			
			scene_mc.SS_P04_STRAWBERRY_BTN.mouseEnabled = true;
			strawberryCounter = 0;
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			Particles.Clear_Particles_Que();        //Clear Call.
			Particles.Clear_All_Particles(this);	//Clear Call.	
			
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "bush1_BTN":
				{
					if(scene_mc.bush1.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower.Start();
						}
						
						Particles.Q_Particles(1, 12, 0, 100, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 315, 15, 20, 90, 1, 1);
						
						scene_mc.bush1.play();
					}
				}
				break;
				case "bush2_BTN":
				{
					if(scene_mc.bush2.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower2.Start();
						}
						
						Particles.Q_Particles(1, 12, -100, 450, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 315, 15, 20, 90, 1, 1);
						
						scene_mc.bush2.play();
					}
				}
				break;
				case "bush3_BTN":
				{
					if(scene_mc.bush3.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower.Start();
						}
						
						Particles.Q_Particles(1, 12, -100, 700, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 315, 15, 20, 90, 1, 1);
						
						scene_mc.bush3.play();
					}
				}
				break;
				case "bush4_BTN":
				{
					if(scene_mc.bush4.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower.Start();
						}
						
						Particles.Q_Particles(1, 12, 1150, 500, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 15, 20, 90, 1, 1);
						
						scene_mc.bush4.play();
					}
				}
				break;
				case "bush5_BTN":
				{
					if(scene_mc.bush5.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower2.Start();
						}
						
						Particles.Q_Particles(1, 12, 850, 100, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 15, 20, 90, 1, 1);
						
						scene_mc.bush5.play();
					}
				}
				break;
				
				case "SS_P04_STRAWBERRY_BTN":
				{
					if ( this.step == BookPage.STEP_DONE )
					{
						sndBall.Start();
					}
					strawberryCounter = 1;
					scene_mc.SS_P04_STRAWBERRY_BTN.mouseEnabled = false;
					
				}
				break;
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void{
		   if(isRunning && !isPaused)
		   {
			   Particles.SB_Update(this);
			   puttAnims();
			   
			   
		   }
		   super.handleFrame( e );
		}		
		
		
		
		private function puttAnims():void
		{
			if(strawberryCounter > 0)
			{
				strawberryCounter++;
			}
			if(strawberryCounter == 22)
			{
				scene_mc.ball_mc.play();
				strawberryCounter = 0;
			}
			if(scene_mc.ball_mc.currentFrame == 62)
			{
				cherry.doTap();
				berrykins.doTap();
				pole.doTap();
			}
			if(strawberryCounter == 115)
			{
				scene_mc.ball_mc.play();
				strawberryCounter = 0;
			}
			if(scene_mc.ball_mc.currentFrame == scene_mc.ball_mc.totalFrames)
			{
				scene_mc.SS_P04_STRAWBERRY_BTN.mouseEnabled = true;
			}
			
		}
		
		
		private function poleComplete(e:TapSpritesheet):void
		{
			strawberryCounter = 100;
			berrykinGoal.doTap();
		}
		
		
		
		
		
		
	}
}