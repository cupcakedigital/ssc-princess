﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page05 extends WBZ_BookPage
	{
		public var sndThrowItems:SoundEx			= null;
		public var sndBalloons:SoundEx			= null;
		
		
		public var berrykinsSounds:SoundCollection;
		public var strawberrySounds:SoundCollection;
		public var catSounds:SoundCollection;
		public var dogSounds:SoundCollection;
		public var scrollSounds:SoundCollection;
		
		
		private var tossableArray:Array;
		
		private var itemsArray:Array;
		private var itemsXArray:Array;
		private var itemsYArray:Array;
		
		
		
		//Core.
		public function Page05(){
			super();
			
			
			
			
			imageFolder = CONFIG::DATA + "images/pages/page05/";
			Images.Add( "BG", { container: "BG", fromFile: true } );
			Images.Add( "Hands_OL", { container: "hands_mc", fromFile: true } );
			Images.Add( "Ballons", { container: scene_mc.balloons_mc.balloons_mc.balloons_mc, fromFile: true } );
			
			Images.Add( "Present", { container: "tossable1_mc", align: Align.CENTER, fromFile: true } );
			Images.Add( "Wrapping_Paper", { container: "tossable2_mc", align: Align.CENTER, fromFile: true } );
			Images.Add( "Spools_02", { container: "tossable3_mc", align: Align.CENTER, fromFile: true } );
			Images.Add( "Spools_01", { container: "tossable4_mc", align: Align.CENTER, fromFile: true } );
			
			/*
			P5_cat
			P5_dog
			P5_scroll
			
			
		public var catSounds:SoundCollection;
		public var dogSounds:SoundCollection;
		public var scrollSounds:SoundCollection;
			
			*/
			
			sndThrowItems = new SoundEx( "sndThrowItems", CONFIG::DATA + "sounds/Animation_Sounds/P5_throw_item.mp3", App.soundVolume );
			sndBalloons = new SoundEx( "sndBalloons", CONFIG::DATA + "sounds/Animation_Sounds/P5_balloons.mp3", App.soundVolume );
			
			
			
			W_Loader.Add( "SS_P05_PAPER", null, null, "wobble1");
			W_Loader.Add( "SS_P05_FLOWERS", null, null, "wobble2");
			W_Loader.Add( "SS_P05_RIBBON_02", null, null, "wobble3");
			W_Loader.Add( "SS_P05_BUCKET", null, null, "wobble4");
			W_Loader.Add( "SS_P05_RIBBON_01", null, null, "wobble5");
			W_Loader.Add( "SS_P05_FEATHER_BOA", null, null, "wobble6");
			
			
			
			scrollSounds = SoundCollection.GetAnimSounds( "P5_scroll", [1] );
			TapMultiLite.Add( "scroll", { imageName: "Scroll_", sounds: scrollSounds, align: Align.CENTER, noSounds: true, langSound: true, fromFiles: true } );
			
			
			
			strawberrySounds = SoundCollection.GetAnimSounds( "P05_STRAWBERRYTAP", [1,2,3] );
			spriteSheetLoader.Add( "Strawberry", 60, strawberrySounds, null, false, false, null, null, null, scene_mc.scroll_btn );
			
			dogSounds = SoundCollection.GetAnimSounds( "P5_dog", [1] );
			var pupcake1Seq:SpriteSheetSequence = new SpriteSheetSequence( "pupcake1", SpriteSheetSequence.createRange( 0, 25, false, 25 ), false, 60 );
			var pupcake2Seq:SpriteSheetSequence = new SpriteSheetSequence( "pupcake2", SpriteSheetSequence.createRange( 26, 49, false, 0 ), false, 60 );
			spriteSheetLoader.Add( "Pupcake", 60, dogSounds, null, false, false, new <SpriteSheetSequence>[pupcake1Seq,pupcake2Seq] );
			
			catSounds = SoundCollection.GetAnimSounds( "P5_cat", [1] );
			var custard1Seq:SpriteSheetSequence = new SpriteSheetSequence( "custard1", SpriteSheetSequence.createRange( 0, 25, false, 25 ), false, 60 );
			var custard2Seq:SpriteSheetSequence = new SpriteSheetSequence( "custard2", SpriteSheetSequence.createRange( 26, 49, false, 0 ), false, 60 );
			spriteSheetLoader.Add( "Custard", 60, catSounds, null, false, false, new <SpriteSheetSequence>[custard1Seq,custard2Seq] );
			
			berrykinsSounds = SoundCollection.GetAnimSounds( "P05_BERRYKINSTAP", [1,2,3,4,5,6,7] );
			spriteSheetLoader.Add( "Berrykin_01", 60, berrykinsSounds, null, false, false);
			spriteSheetLoader.Add( "Berrykin_02", 60, berrykinsSounds, null, false, false);
			
			
			
			tossableArray = new Array();
			
			itemsArray = new Array;
			itemsArray.push(scene_mc.tossable1_mc,
						   scene_mc.tossable2_mc,
						   scene_mc.tossable3_mc,
						   scene_mc.tossable4_mc);
			
			itemsXArray = new Array;
			itemsYArray = new Array;
			for(var X:int = 0; X < itemsArray.length; X++)
			{
				itemsXArray.push(itemsArray[X].x);
				itemsYArray.push(itemsArray[X].y);
				Spawn_Tossable(itemsArray[X]);
			}
			initItems();
			
			sparklers.Add(scene_mc.scroll_btn, scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.Pupcake_BTN, scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.Custard_BTN, scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.Berrykin_02_BTN, scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.wobble5_BTN, scene_mc.Sparkler5_sprkl);
			sparklers.Add(scene_mc.balloons_BTN, scene_mc.Sparkler6_sprkl);
			sparklers.Add(scene_mc.wobble4_BTN, scene_mc.Sparkler7_sprkl);
			sparklers.Add(scene_mc.Berrykin_01_BTN, scene_mc.Sparkler8_sprkl);
			sparklers.Add(scene_mc.wobble3_BTN, scene_mc.Sparkler9_sprkl);
			sparklers.Add(scene_mc.wobble2_BTN, scene_mc.Sparkler10_sprkl);
		}			
		
		public final override function Init():void{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic2";
			
			
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void{			
			super.Reset();
			
			
			initItems();
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		public final override function Start():void{			
			super.Start();	
			cMain.addStageMouseUp( P14onUp );	
			cMain.addStageMouseDown( P01onDown );
		}	
		
		public override function Destroy():void{
			super.Destroy();
			
			
			
			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			Select_Tossable(mouseX, mouseY);
			
			
			switch(e.target.name)
			{
				case "balloons_BTN":
				{
					if(scene_mc.balloons_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBalloons.Start();
						}
						
						scene_mc.balloons_mc.play();
					}
				}
				break;
				
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			UnSelect_Tossable();			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Update_Tossable();
				
				
				
			}
			super.handleFrame( e );
		}
		
		
		private function initItems():void 
		{
			for(var X:int = 0; X < itemsArray.length; X++)
			{
				itemsArray[X].x = itemsXArray[X];
				itemsArray[X].y = itemsYArray[X];
				itemsArray[X].rotation = 0;
			}
			
		}
		
		
		private function Spawn_Tossable(clip:Placeholder):void
		{
			
			var tossable_Props:Array = new Array();
			
			
			var Phase:int = 1;			
			var Force_X:Number = ((Math.random() * 40) - 20);
			var Force_Y:Number = -40.0;
			
			tossable_Props.push(clip);
			tossable_Props.push(Phase);
			tossable_Props.push(Force_X);
			tossable_Props.push(Force_Y);
			tossable_Props.push(0);
			
			tossableArray.push(tossable_Props);
			
		}
		
		private function Select_Tossable(pX:int, pY:int):void
		{
			for(var X:int = 0; X < tossableArray.length; X++){
				if(tossableArray[X][0].hitTestPoint(mouseX, mouseY, true)){
					tossableArray[X][1] = 2;
					tossableArray[X][4] = 0;
					scene_mc.setChildIndex(tossableArray[X][0], (scene_mc.numChildren - 1));
					return;
				}
			}
		}
		private function UnSelect_Tossable():void
		{
			for(var X:int = 0; X < tossableArray.length; X++)
			{
				switch(tossableArray[X][1])
				{
					case 2:
					{
						var Angle:Number = Math.atan2((mouseY - tossableArray[X][0].y), (mouseX - tossableArray[X][0].x));
						var P1:Point = new Point(mouseX, mouseY);
						var P2:Point = new Point(tossableArray[X][0].x, tossableArray[X][0].y);
						var Speed:Number = Point.distance(P1, P2);
						if(Speed > 30){Speed = 30;}
						tossableArray[X][2] = (0 + (Speed * Math.cos(Angle)));
						tossableArray[X][3] = (0 + (Speed * Math.sin(Angle)));
						tossableArray[X][1] = 0;
					}break;
				}
			}
		}
		private function Update_Tossable():void
		{			
		
		
			for(var X:int = 0; X < tossableArray.length; X++){
				
				//scene_mc.setChildIndex(tossableArray[X][0], (scene_mc.numChildren - 1));
				
				switch(tossableArray[X][1]){
					case 0:{					
						
						tossableArray[X][0].x = (tossableArray[X][0].x + tossableArray[X][2]);
						tossableArray[X][0].y = (tossableArray[X][0].y + tossableArray[X][3]);		
						
						tossableArray[X][0].rotation = (tossableArray[X][0].rotation + tossableArray[X][2]);
						
						//Y Force.
						tossableArray[X][3] = Math.floor(tossableArray[X][3] + 3.0);
						
						
						if(tossableArray[X][0].y >= 550){
							tossableArray[X][3] = -Math.floor(tossableArray[X][3] - (tossableArray[X][3] / 2)); 							
							if(tossableArray[X][3] > -4){
								tossableArray[X][3] = 0;
							}
							tossableArray[X][0].y = 550;
							tossableArray[X][4] = (tossableArray[X][4] + 1);
							if(tossableArray[X][4] > 2){tossableArray[X][3] = 0; tossableArray[X][2] = 0;}
							if(tossableArray[X][3] != 0 && this.step == BookPage.STEP_DONE)
							{
								sndThrowItems.Start();
							}
						}		
						//X Force.										
						if(tossableArray[X][0].x >= 1024 + clsMain.offsetX){
							tossableArray[X][2] = -Math.floor(tossableArray[X][2] - (tossableArray[X][2] / 3)); 
							tossableArray[X][0].x = 1023 + clsMain.offsetX;
							if(tossableArray[X][2] != 0 && this.step == BookPage.STEP_DONE)
							{
								sndThrowItems.Start();
							}
							
						}
						if(tossableArray[X][0].x <= 0 - clsMain.offsetX){
							tossableArray[X][2] = -Math.floor(tossableArray[X][2] - (tossableArray[X][2] / 3)); 
							tossableArray[X][0].x = 1 - clsMain.offsetX;
							if(tossableArray[X][2] != 0 && this.step == BookPage.STEP_DONE)
							{
								sndThrowItems.Start();
							}
							
						}		
						if(tossableArray[X][2] < 0){
							tossableArray[X][2] = (tossableArray[X][2] + 0.1);
							if(tossableArray[X][2] >= 0){
								tossableArray[X][2] = 0;
							}
						}
						else if(tossableArray[X][2] > 0){
							tossableArray[X][2] = (tossableArray[X][2] - 0.1);
							if(tossableArray[X][2] <= 0){
								tossableArray[X][2] = 0;
							}
						}
									
					}break;
					case 1:{
						
					}break;		
					case 2:{
						tossableArray[X][0].x = mouseX;
						tossableArray[X][0].y = mouseY;
					}break;		
				}
			}
		}
		
		
		
		
		
		
	}
}