﻿package 
{
	import flash.display.MovieClip;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import com.adobe.serialization.json.JSON;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Utils;
	
	public class Upsell extends WBZ_BookPage
	{
		public static var aff_id:String			= "null";
		public static var aff_source:String		= "null";
		public static var paramsLoaded:Boolean	= false;
		
		public var doOpenMarket:Boolean			= false;
		public var ldr:URLLoader				= null;
		public var paramsLoader:URLLoader		= null;
		public var ret:Boolean					= false;
		
		public function Upsell()
		{
			super();
			imageFolder = CONFIG::DATA + "languages/en/images/pages/";
			
			var bg:MovieClip = new MovieClip;
			this.addChild( bg );
			bg.x = -171;
			
			assets.push( new ExternalImage( "upsell.jpg", bg ) );
			OverlayButton.Add( this, new Rectangle( 523, 621, 418, 96 ), BuyTapped );
			OverlayButton.Add( this, new Rectangle( 950, 696, 68, 60 ), CupcakeTapped );
		}
		
		private function CupcakeTapped( e:MouseEvent ):void { if ( isRunning && !isPaused ) { Captcha.Show( CupcakeReturn ); } }
		
		private function CupcakeReturn( Correct:Boolean, Cancel:Boolean ):void 
			{ if ( Correct && !Cancel ) { navigateToURL( new URLRequest( "http://cupcakedigital.com/" ) ); } }
		
		private function BuyTapped( e:MouseEvent ):void
		{
			Captcha.Show( CaptchaReturn );
			LoadParams();
		}
		
		private function LoadParams():void
		{
			if ( ret == false && paramsLoaded == false )
			{
				ret = true;
				var marketXML:XML = GlobalData.appXML.Upsell[0];
				var url:String = marketXML.CallURL.toString() + "user_id=" + GlobalData.userID +
								 "&site_id=" + marketXML.Markets.Market.(@name==CONFIG::MARKET)[0].LocalSiteID.toString();				
				
				paramsLoader = new URLLoader;
				Utils.addHandler( paramsLoader, Event.COMPLETE, ParamsLoaded );
				paramsLoader.load( new URLRequest( url ) );
				
				App.Log( "Loading params from: " + url );
			}
		}
		
		private function ParamsLoaded( e:Event ):void
		{
			var paramString:String = paramsLoader.data.toString();
			App.Log( "Params loaded! Return string: '" + paramString + "'" );
			
			var seqRaw:Object = com.adobe.serialization.json.JSON.decode( paramString ).data[0];
			
			try
			{
				aff_id = seqRaw.advertiser_sub_publisher.name.toString();
				aff_source = seqRaw.advertiser_sub_ad.name.toString();
				App.Log( "Affiliate id: '" + aff_id + "', affiliate source: '" + aff_source + "'" );				
			} catch ( e:Error ) { App.Log( "No tracking data for this install found." ); }
			
			paramsLoaded = true;
			ret = false;
			
			if ( doOpenMarket ) { OpenMarket(); }
		}
		
		private function CaptchaReturn(Correct:Boolean, Cancel:Boolean):void
		{
			doOpenMarket = ( Correct && !Cancel );
			if ( doOpenMarket && !ret ) { OpenMarket(); }
		}
		
		private function OpenMarket():void
		{
			var target_app_id:String = GlobalData.appXML.Upsell.Market.(@name==CONFIG::MARKET)[0].toString();
			
			if ( ret || !doOpenMarket ) { App.Log( "Can't open market link yet..." ); return; }
			
			doOpenMarket = false;
			
			if ( CONFIG::NOOK ) 
				{ clsMain.openMarket( target_app_id ); }
			else 
			{ 
				var marketXML:XML = GlobalData.appXML.Upsell.Markets.Market.(@name==CONFIG::MARKET)[0];
				if ( marketXML != null )
				{
					var affiliate_id:String = ( aff_id != "null" && aff_id != "0" ) ? aff_id : "2";
					var source_name:String = ( aff_source != "null" && aff_source != "" ) ? aff_source : "SSC_PRINCESS";
					
					var url:String = "http://cupcakedigital.go2cloud.org/aff_c?" +
									 "offer_id=" + marketXML.OfferID.toString() +
									 "&aff_id=" + affiliate_id +
									 "&source=" + source_name;
					App.Log( "Launching upsell tracking link: '" + url + "'" );
					
					navigateToURL( new URLRequest( url ) );
				} else { App.Log( "Market XML is empty" ); }
			}
		}
		
		private function URLLoaded( e:Event ):void
			{ App.Log( "Tracking link complete" ); }
	}
}