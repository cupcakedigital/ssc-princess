﻿package 
{
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.SoundEx;
	import com.cupcake.SpriteSheetSequence;
	import com.cupcake.Utils;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.MP3Loader;
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter; 
	import flash.display.DisplayObject;
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.filters.BitmapFilter;
	import flash.display.MovieClip;

	
	
	public final class Croquet extends WBZ_BookPage
	{
		//SOUNDS
		private var InstructionVO:SoundEx;
		private var AlmostVO:SoundEx;
		private var GoodVO:SoundEx;
		private var GoodVO2:SoundEx;
		
		private var LettColor1:uint = 0xA1E8FF;
		private var LettColor2:uint = 0xBFFF8E;
		
		private var RightSFX:SoundEx;
		private var WrongSFX:SoundEx;
		
		private var BallArray:Array;
		private var LettersToUseArray:Array;
		private var AlphabetArray:Array = new Array("a","b","c","d","e","f","g","h","i","j","k","l","m",
													"n","o","p","q","r","s","t","u","v","w","x","y","z");
													
		private var sndLetterArray:Array = new Array;
		private var sndWordArray:Array = new Array;
												// 3-LETTER WORDS
		private var WordArray:Array = new Array("ate", "bed", "boy", "car", "cat", "cow", "dog", "eat", "egg", "new",
												"now", "our", "pig", "saw", "say", "sun", "too", "toy", "who",
												// 4-LETTER WORDS
												"baby", "ball", "boat", "came", "coat", "doll", "door", "duck", "farm",
												"fish", "four", "game", "girl", "good", "hill", "into", "milk", "must",
												"name", "rain", "ride", "soon", "want", "well",
												// 5-LETTER WORDS
												"apple", "black", "brown", "chair", "house", "under", "white");
		
		private var _pole_berrykin:TapSpritesheet;
		private var _text_bubble:TapSpritesheet;
		private var _strawberry:TapSpritesheet;
		private var _berrykins:TapSpritesheet;
		private var _pole:TapSpritesheet;
		
		private var _currentWord:String;
		private var _currentLetter:String;
		
		private var bPauseInteractions:Boolean = true;
		private var bRoundPause:Boolean = false;
		private var bCorrect:Boolean = false;
		private var firstTurn:Boolean = true;
		
		private var nFadeTime:Number = 1;
		
		private var startGameTimer:int = 0;
		
		private var spellingCounter:int = 1000;
		private var spellingLetter:int = 0;
		private var spellingTotalTime:int = 35;
		private var winLetterTimer:int = 0;
		
		private var iRound:int = 0;
		private var iRoundMax:int = 5;
		private var iReturnLayer:int;
		
		private var Finale:clsGameFinale;
		private var EndOfLevel:clsGames_EndOfLevelSummary;
		private var sndEnd:SoundEx;
		private var Crowd:CheeringCrowd;
		private var winSound:SoundEx = new SoundEx( "win", CONFIG::DATA + "sounds/games/GameCompleted.mp3", App.soundVolume);
		
		public var Background:Rectangle;
		
		
		private var outline:GlowFilter;
		private var outline2:GlowFilter;
		
		
		public function Croquet()
		{
			super();
			
			Background = new Rectangle( 0, 0, 1024, 768 );
			
			imageFolder = CONFIG::DATA + "images/pages/Croquet/";
			assets.push( new ExternalImage( "CROQUET_BG.png", scene_mc.BG ) );
			assets.push( new ExternalImage( "CROQUET_HOOP_OL.png", scene_mc.overlay ) );
			assets.push( new ExternalImage( "CROQUET_BALL.png", scene_mc.anim_ball.actor.actor, Align.CENTER ) );
			//assets.push( new ExternalImage( "CROQUET_BALL_SHADOW.png", scene_mc.anim_ball._shadow ) );
			assets.push( new ExternalImage( "SSC_SIGHTWORDS_TITLEBAR.png", scene_mc.TitleBar_MC.TitleBarBG_MC ) );
			
			BallArray = new Array(scene_mc.ball1, scene_mc.ball2, scene_mc.ball3); 
			for(var i:int = 0; i < BallArray.length; i++)
			{
				assets.push( new ExternalImage( "CROQUET_BALL_LARGE.png", BallArray[i].ball.Ball ) );
				assets.push( new ExternalImage( "CROQUET_BALL_SHADOW.png", BallArray[i].ball.Shadow ) );
				BallArray[i].ball.BallText.txt.text = "";
			}
			
			scene_mc.anim_ball.actor.letter.txt.text = "";
			scene_mc.anim_ball.alpha = 0;
			
			var seq1:SpriteSheetSequence = new SpriteSheetSequence( "play", SpriteSheetSequence.createRange( 0, 25, false));
			var seq2:SpriteSheetSequence = new SpriteSheetSequence( "lose", SpriteSheetSequence.createRange( 26, 63, false));
			var seq3:SpriteSheetSequence = new SpriteSheetSequence( "win", SpriteSheetSequence.createRange( 64, 101, false));
			_strawberry = spriteSheetLoader.Add("strawberry", 30, null, null, false, false, new <SpriteSheetSequence>[seq1, seq2, seq3]);
			
			seq1 = new SpriteSheetSequence( "win", SpriteSheetSequence.createRange( 0, 31, false));
			seq2 = new SpriteSheetSequence( "lose", SpriteSheetSequence.createRange( 32, 67, false));
			_berrykins = spriteSheetLoader.Add("berrykins", 30, null, null, false, false, new <SpriteSheetSequence>[seq1, seq2]);
			
			seq1 = new SpriteSheetSequence( "win", SpriteSheetSequence.createRange( 0, 29, false));
			seq2 = new SpriteSheetSequence( "lose", SpriteSheetSequence.createRange( 30, 59, false));
			_pole = spriteSheetLoader.Add("pole", 30, null, null, false, false, new <SpriteSheetSequence>[seq1, seq2]);
			
			seq1 = new SpriteSheetSequence( "3", SpriteSheetSequence.createRange( 0, 0, false));
			seq2 = new SpriteSheetSequence( "4", SpriteSheetSequence.createRange( 1, 1, false));
			seq3 = new SpriteSheetSequence( "5", SpriteSheetSequence.createRange( 2, 2, false));
			_text_bubble = spriteSheetLoader.Add("text_bubble", 30, null, null, false, false, new <SpriteSheetSequence>[seq1, seq2, seq3]);
			scene_mc.text_bubble.alpha = 0;
			
			//assets.push( new ExternalImage( "EndScreen/EndOfLevel_Background.png", scene_mc.EndImage.actor ) );
			//assets.push( new ExternalImage( "EndScreen/home_teaparty.png", scene_mc.EndImage.home ) );
			//assets.push( new ExternalImage( "EndScreen/replay_teaparty.png", scene_mc.EndImage.replay ) );
			scene_mc.EndImage.visible = false;
			
			_pole_berrykin = spriteSheetLoader.Add("pole_berrykin", 60);
			
			iReturnLayer = scene_mc.getChildIndex(scene_mc.anim_ball);
			
			InstructionVO = new SoundEx( "intro", CONFIG::DATA + "languages/en/sounds/games/Croquet_Instruction.mp3", App.voiceVolume );
			AlmostVO = new SoundEx( "intro", CONFIG::DATA + "languages/en/sounds/games/Almost.mp3", App.voiceVolume );
			GoodVO = new SoundEx( "G1", CONFIG::DATA + "languages/en/sounds/games/Fruitastic.mp3", App.voiceVolume );
			GoodVO2 = new SoundEx( "G2", CONFIG::DATA + "languages/en/sounds/games/Berry_Exciting.mp3", App.voiceVolume );
			
			RightSFX = new SoundEx( "right", CONFIG::DATA + "sounds/games/croquet/right_answer2.mp3", App.soundVolume );
			WrongSFX = new SoundEx( "wrong", CONFIG::DATA + "sounds/games/croquet/wrong_answer.mp3", App.soundVolume );
			
/* *** ***** ******* ********* *********** ************* *********** ********* ******* ***** *** */
			for(var i:int = 0; i < WordArray.length; i++)
			{
				var tempString:String = CONFIG::DATA + "languages/en/sounds/sight_words/";
				tempString += WordArray[i].toUpperCase();
				tempString += ".mp3";
				var tempSound:SoundEx = new SoundEx(i.toString(), tempString, App.voiceVolume);
				sndWordArray.push(tempSound);
			}
/* *** ***** ******* ********* *********** ************* *********** ********* ******* ***** *** */
			for(i = 0; i < AlphabetArray.length; i++)
			{
				tempString = CONFIG::DATA + "languages/en/sounds/sight_words/LETTERS/";
				tempString += AlphabetArray[i].toUpperCase();
				tempString += ".mp3";
				tempSound = new SoundEx(i.toString(), tempString, App.voiceVolume);
				sndLetterArray.push(tempSound);
			}
/* *** ***** ******* ********* *********** ************* *********** ********* ******* ***** *** */
			
			Finale = new clsGameFinale(cMain, "", gc.pages_GameFinale_Text, Finale_Complete);
			Finale.x = Background.x + (Background.width/2) + gc.pages_GameFinale_Text.offset.x;
			Finale.y = Background.y + (Background.height/2) + gc.pages_GameFinale_Text.offset.y;
			Finale.Reset();
			Finale.mouseEnabled = false;
			Finale.mouseChildren = false;
			this.addChild(Finale);
			
			EndOfLevel = null;
			
			var endldr:MP3Loader = new MP3Loader( CONFIG::DATA + "languages/en/sounds/games/GreatJob.mp3", { autoPlay: false, onComplete: endLoaded } );
			endldr.load();
			
			Crowd = new CheeringCrowd( this );
			
			/*
			scene_mc.TitleBar_MC.title_mc
			scene_mc.TitleBar_MC.word_mc
			scene_mc.word.strawberryWord_mc
			scene_mc.anim_ball.actor.letter.ballWord_mc
			scene_mc.ball1.ball.BallText.choiceWord_mc
			scene_mc.ball2.ball.BallText.choiceWord_mc
			scene_mc.ball3.ball.BallText.choiceWord_mc
			
			*/
			
			outline = new GlowFilter(0x6b9849,1,2,2,20);
			outline.quality=BitmapFilterQuality.HIGH;
			outline2 = new GlowFilter(0x0174aa,1,2,2,20);
			outline2.quality=BitmapFilterQuality.HIGH;
			
			
			scene_mc.TitleBar_MC.fixedText.filters=[outline];
			
			scene_mc.TitleBar_MC.findTxt.filters=[outline2];
			//scene_mc.word.txt.filters=[outline2];
			scene_mc.anim_ball.actor.letter.txt.filters=[outline2];
			scene_mc.ball1.ball.BallText.txt.filters=[outline2];
			scene_mc.ball2.ball.BallText.txt.filters=[outline2];
			scene_mc.ball3.ball.BallText.txt.filters=[outline2];
			
			replaceDisplayObjectWithBitmap( scene_mc.TitleBar_MC.fixedText, scene_mc.TitleBar_MC.title_mc, 1, false, [outline] );
		}
		
		public final override function Init():void
		{
			if ( isInitialized ) return;
			
			musicFile = "sscmusic4";
			super.Init();
			Reset();
		}		
		
		public final override function Reset():void
		{			
			super.Reset();
			//cMain.removeStageMouseDown( OnDown );
			
			bPauseInteractions = true;
			bRoundPause = false;
			bCorrect = false;
			iRound = 0;
			scene_mc.tempEnd.visible = false;
			spellingCounter = 1000;
			startGameTimer = 0;
			
			Finale.Reset();
			if(EndOfLevel != null){EndOfLevel.destroy();}
			if(Crowd != null){Crowd.Reset();}
		}
		
		private function GameReset():void
		{
			_currentWord = "";
			_currentLetter = "";

			spellingCounter = 1000;
			spellingLetter = 0;
			spellingTotalTime = 35;
			winLetterTimer = 0;

	
			for(var i:int = 0; i < BallArray.length; i++)
			{
				BallArray[i].ball.BallText.txt.text = "";
				BallArray[i].alpha = 1;
			}
			
			scene_mc.anim_ball.actor.letter.txt.text = "";
			scene_mc.anim_ball.alpha = 0;
			
			bPauseInteractions = false;
			bRoundPause = false;
			bCorrect = false;
			iRound = 0;
			scene_mc.tempEnd.visible = false;
			startGameTimer = 110;
			
			ShuffleWordArray();
			SetTexBubble();
			SetBallLetters();
			
			scene_mc.text_bubble.alpha = 1;
			sndWordArray[iRound].Start();
			Finale.Reset();
			if(EndOfLevel != null){EndOfLevel.destroy();}
			if(Crowd != null){Crowd.Reset();}
		}
		
		public final override function Start():void
		{			
			super.Start();	
			cMain.addStageMouseDown( OnDown );
			
			InstructionVO.Start();
			
			ShuffleWordArray();
			SetTexBubble();
			SetBallLetters();
			scene_mc.text_bubble.alpha = 1;
			//bPauseInteractions = false;
			
			Finale.Reset();
		}	
		
		public override function Stop():void
		{
			super.Stop();
			InstructionVO.Stop();
			AlmostVO.Stop();
			GoodVO.Stop();
			GoodVO2.Stop();
			
			RightSFX.Stop();
			WrongSFX.Stop();
			
			if(sndEnd != null){sndEnd.Stop();}
			if(Finale != null){Finale.Stop();Finale.Reset();}
			if(sndEnd != null){sndEnd.Stop();}
			if(Finale != null){Finale.Stop();Finale.Reset();}
			
			for(var i:int = 0; i < sndWordArray.length; i++)
			{sndWordArray[i].Stop();}
		}
		
		public override function Destroy():void
		{
			super.Destroy();
			cMain.removeStageMouseDown( OnDown );
			
			TweenMax.killAll(true, true, true);
			InstructionVO.Destroy();
			AlmostVO.Destroy();
			GoodVO.Destroy();
			GoodVO2.Destroy();
			
			RightSFX.Destroy();
			WrongSFX.Destroy();
			
			for(var i:int = 0; i < sndWordArray.length; i++)
			{sndWordArray[i].Destroy();}
			
			for(var i:int = 0; i < sndLetterArray.length; i++)
			{sndLetterArray[i].Destroy();}
			
			if(winSound != null){winSound.Destroy();}
			if(sndEnd != null){sndEnd.Destroy();}
			if(Finale != null){Finale.destroy();Finale = null;}
			if(EndOfLevel != null){EndOfLevel.destroy();EndOfLevel = null;}
			if(Crowd != null){Crowd.Destroy();}
			Background = null;
		}
		
		private function OnDown( e:MouseEvent ):void
		{
			switch(e.target.name)
			{
				case "home_BTN":
				{
					cMain.PrevPage(-1);
					break;
				}
				case "replay_BTN":
				{
					trace("RESTART NOW");
				scene_mc.EndImage.visible = false;
				//TweenMax.delayedCall(nTrayTweenTime + nObjectFadeTime, ReplaceGuides);
				GameReset();
				//ResetCloth();
				//ReplaceGuides();
				}
			}
			
			if(!bPauseInteractions && !bRoundPause)
			{
				for(var i:int = 0; i < BallArray.length; i++)
				{
					if(BallArray[i].ball.Ball.hitTestPoint(mouseX, mouseY, true) && BallArray[i].alpha == 1)
					{
						if(BallArray[i].ball.BallText.txt.text == _currentLetter)
						{
							for(var x:int = 0; x < AlphabetArray.length; x++)
							{
								if(BallArray[i].ball.BallText.txt.text == AlphabetArray[x])
								{
									sndLetterArray[x].Start();
								}
							}
							
							bPauseInteractions = true;
							bCorrect = true;
							//spellingCounter = 0;
							spellingLetter = 0;
							//BallArray[i].play();
							winLetterTimer = 1;
							//sndWordArray[iRound].Start();
							//RightSFX.Start();
							
							DrawLetters();
							replaceDisplayObjectWithBitmap( scene_mc.word.txt, scene_mc.word.strawberryWord_mc, 1, false, [outline2] );
						}
						else
						{
							for(var a:int = 0; a < AlphabetArray.length; a++)
							{
								if(BallArray[i].ball.BallText.txt.text == AlphabetArray[a])
								{
									sndLetterArray[a].Start();
								}
							}
							bCorrect = false;
							TweenMax.to(BallArray[i], nFadeTime, {alpha:0});
							Wrong();
							WrongSFX.Start();
						}
					}
				}
			}
		}
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning)
			{
				spelling();
				
				if(firstTurn)
				{
					firstTurn = false;
					startGameTimer = 1;
				}
				
				if(startGameTimer > 0)
				{
					startGameTimer++;
					
					if(startGameTimer == 110)
					{
						sndWordArray[iRound].Start();
						bPauseInteractions = false;
						startGameTimer = 0;
					}
				}
				
				for(var i:int = 0; i < BallArray.length; i++)
				{
					if(BallArray[i].currentFrame == 16)
					{scene_mc.setChildIndex(BallArray[i], scene_mc.getChildIndex(scene_mc.anim_ball)+1);}
					
					if(BallArray[i].currentFrame == 26)
					{
						BallArray[i].alpha = 0;
						scene_mc.anim_ball.alpha = 1;
						scene_mc.anim_ball.actor.letter.txt.visible = true;
						scene_mc.anim_ball.actor.letter.txt.text = BallArray[i].ball.BallText.txt.text;
						replaceDisplayObjectWithBitmap( scene_mc.anim_ball.actor.letter.txt, scene_mc.anim_ball.actor.letter.ballWord_mc, 1, false, [outline2] );
						
						scene_mc.anim_ball.actor.letter.txt.visible = false;
					}
					
					if(BallArray[i].currentFrame == 27)
					{
						scene_mc.setChildIndex(BallArray[i], scene_mc.getChildIndex(scene_mc.anim_ball)+1);
						
						if(bCorrect)
						{Shoot();}
						
						else
						{Wrong();}
					}
				}
				
				if(_strawberry.currentFrame == 13)
				{scene_mc.anim_ball.play();}
				
				if(scene_mc.anim_ball.currentFrame == 55)
				{PoleHit();}
				
				if(scene_mc.anim_ball.currentFrame == 138)
				{
					bPauseInteractions = false;
					TweenMax.to(scene_mc.anim_ball, nFadeTime, {alpha:0});
				}
			}
			super.handleFrame( e );
		}
		
		private function ShuffleWordArray():void
		{
			var iLoops = WordArray.length
			var tempInt:int;
			var tempArray:Array = new Array();
			var tempSoundArray:Array = new Array();
			
			for(var i:int = 0; i < iLoops; i++)
			{
				tempInt = Math.floor(Math.random() * WordArray.length);
				tempArray.push(WordArray[tempInt]);
				WordArray.splice(tempInt, 1);
				
				tempSoundArray.push(sndWordArray[tempInt]);
				sndWordArray.splice(tempInt, 1);
			}
			WordArray = tempArray;
			sndWordArray = tempSoundArray;
		}
		
		private function SetTexBubble():void
		{
			SetWord();
			
			_text_bubble.playSequence(_currentWord.length.toString());
			
			switch(_currentWord.length)
			{
				case 3:scene_mc.word.x = 480;break;
				case 4:scene_mc.word.x = 460;break;
				case 5:scene_mc.word.x = 450;break;
			}
			
			_text_bubble.playSequence(_currentWord.length.toString());
		}
		
		private function SetWord():void
		{
			_currentWord = WordArray[iRound];
			scene_mc.TitleBar_MC.findTxt.text = _currentWord;
			SetBlank();
			replaceDisplayObjectWithBitmap( scene_mc.TitleBar_MC.findTxt, scene_mc.TitleBar_MC.word_mc, 1, false, [outline2] );
			
		}
		
		private function SetBlank():void
		{
			var tempString:String = "";
			var tempPos:int = Math.random()*(_currentWord.length-1);
			
			_currentLetter = _currentWord.charAt(tempPos);
			
			scene_mc.word.txt.txt1.textColor = LettColor1;
			scene_mc.word.txt.txt2.textColor = LettColor1;
			scene_mc.word.txt.txt3.textColor = LettColor1;
			scene_mc.word.txt.txt4.textColor = LettColor1;
			scene_mc.word.txt.txt5.textColor = LettColor1;
			
			scene_mc.word.txt.txt1.text = "";
				scene_mc.word.txt.txt2.text = "";
				scene_mc.word.txt.txt3.text = "";
				scene_mc.word.txt.txt4.text = "";
				scene_mc.word.txt.txt5.text = "";
				
			for(var i:int = 0; i < _currentWord.length; i++)
			{
					switch(i)
					{
						case 0:
						{
							if(i == tempPos)
							{
								scene_mc.word.txt.txt1.text = "_";
							}
							else
							{
								scene_mc.word.txt.txt1.text = _currentWord.charAt(i);
							}
							break;
						}
						case 1:
						{
							if(i == tempPos)
							{
								scene_mc.word.txt.txt2.text = "_";
							}
							else
							{
								scene_mc.word.txt.txt2.text = _currentWord.charAt(i);
							}
							break;
						}
						case 2:
						{
							if(i == tempPos)
							{
								scene_mc.word.txt.txt3.text = "_";
							}
							else
							{
								scene_mc.word.txt.txt3.text = _currentWord.charAt(i);
							}
							break;
						}
						case 3:
						{
							if(i == tempPos)
							{
								scene_mc.word.txt.txt4.text = "_";
							}
							else
							{
								scene_mc.word.txt.txt4.text = _currentWord.charAt(i);
							}
							break;
						}
						case 4:
						{
							if(i == tempPos)
							{
								scene_mc.word.txt.txt5.text = "_";
							}
							else
							{
								scene_mc.word.txt.txt5.text = _currentWord.charAt(i);
							}
							break;
						}
					}

			}
			replaceDisplayObjectWithBitmap( scene_mc.word.txt, scene_mc.word.strawberryWord_mc, 1, false, [outline2] );
		}
		
		private function SetBallLetters():void
		{
			LettersToUseArray = new Array();
			
			for(var i:int = 0; i < 3; i++)
			{LettersToUseArray.push(Math.floor(Math.random() * AlphabetArray.length));}
			
			BallArray[0].ball.BallText.txt.text = AlphabetArray[LettersToUseArray[0]];
			BallArray[1].ball.BallText.txt.text = AlphabetArray[LettersToUseArray[1]];
			BallArray[2].ball.BallText.txt.text = AlphabetArray[LettersToUseArray[2]];
			
			
			if(BallArray[0].ball.BallText.txt.text != _currentLetter &&
			   BallArray[1].ball.BallText.txt.text != _currentLetter &&
			   BallArray[2].ball.BallText.txt.text != _currentLetter)
			{BallArray[Math.floor(Math.random() * LettersToUseArray.length)].ball.BallText.txt.text = _currentLetter;}
			
			
			replaceDisplayObjectWithBitmap( scene_mc.ball1.ball.BallText.txt, scene_mc.ball1.ball.BallText.choiceWord_mc, 1, false, [outline2] );
			replaceDisplayObjectWithBitmap( scene_mc.ball2.ball.BallText.txt, scene_mc.ball2.ball.BallText.choiceWord_mc, 1, false, [outline2] );
			replaceDisplayObjectWithBitmap( scene_mc.ball3.ball.BallText.txt, scene_mc.ball3.ball.BallText.choiceWord_mc, 1, false, [outline2] );
			
		}
		
		private function Shoot():void
		{_strawberry.playSequence("play");}
		
		private function PoleHit():void
		{
			if(bCorrect)
			{
				_pole_berrykin.doTap();
				
				//CHOOSE ONE
				switch(Math.floor(Math.random() * 3))
				{
					case 0:GoodVO.Start();break;
					case 1:GoodVO2.Start();break;
					case 2:sndEnd.Start();break;
				}
				
				_pole.playSequence("win");
				_berrykins.playSequence("win");
				_strawberry.playSequence("win");
				FillInBlank();
				bRoundPause = true;
			}
			
			else
			{
				//AlmostVO.Start();
				_pole.playSequence("lose");
				_berrykins.playSequence("lose");
				_strawberry.playSequence("lose");
			}
		}
		
		private function FillInBlank():void
		{
			//trace("call out the word that was completed");
			DrawLetters();
			replaceDisplayObjectWithBitmap( scene_mc.word.txt, scene_mc.word.strawberryWord_mc, 1, false, [outline2] );
			
			TweenMax.delayedCall(2, FinishRound);
		}
		
		private function FinishRound():void
		{
			iRound++;
			
			if(iRound < iRoundMax)
			{
				
				for(var i:int = 0; i < BallArray.length; i++)
				{TweenMax.to(BallArray[i], nFadeTime, {alpha:0});}
				
				TweenMax.to(scene_mc.text_bubble, nFadeTime, {alpha:0});
				TweenMax.to(scene_mc.word, nFadeTime, {alpha:0, onComplete:NextRound});
			}
			
			else
			{gameOver();}
		}
		
		private function NextRound():void
		{
			SetTexBubble();
			SetBallLetters();
			
			bRoundPause = false;
			
			for(var i:int = 0; i < BallArray.length; i++)
			{TweenMax.to(BallArray[i], nFadeTime, {alpha:1});}
			
			TweenMax.to(scene_mc.text_bubble, nFadeTime, {alpha:1});
			TweenMax.to(scene_mc.word, nFadeTime, {alpha:1});
			
			sndWordArray[iRound].Start();
		}
		
		private function Wrong():void
		{PoleHit();}
		
		private function Finale_Complete():void
		{
			if ( isRunning )
			{
				var returnToPrevPage:Boolean = this.loadedFrom == 2;
				scene_mc.EndImage.visible = true;
		
				//EndOfLevel = new clsGames_EndOfLevelSummary(cMain, "BejeweledGame", returnToPrevPage, sndEnd );
				//EndOfLevel.x = Background.x;
				//EndOfLevel.y = Background.y;
				//this.addChild(EndOfLevel);
			}
		}
		
		private function endLoaded( e:LoaderEvent ):void
		{
			sndEnd = new SoundEx( "sndEnd", GlobalData.langFolder + "sounds/games/GreatJob.mp3", App.voiceVolume );
			( e.target as MP3Loader ).dispose( true );
		}
		
		private function gameOver():void
		{
			winSound.Start();
			TweenMax.delayedCall(2, Finale_Complete);
			this.setChildIndex(Crowd, this.numChildren-1);
			Crowd.Start();		
		}
		
		private function spelling():void
		{
			for(var x:int = 0; x < BallArray.length; x++)
			{
				if(BallArray[x].currentFrame == 23)
				{
					spellingCounter = 0;
				}
			}
			
			if(winLetterTimer > 0)
			{
				winLetterTimer++;
				
				if(winLetterTimer == spellingTotalTime)
				{
					winLetterTimer = 0;
					for(var s:int = 0; s < BallArray.length; s++)
					{
						if(BallArray[s].ball.BallText.txt.text == _currentLetter)
						{
							BallArray[s].play();
						}
					}
				}
			}
			
			if(spellingCounter > 0 && spellingCounter < 1000)
			{
				spellingCounter++;
			}
			if(spellingCounter == spellingTotalTime)
			{
				spellingCounter = 0;
			}
			if(spellingCounter == 0 && bCorrect)
			{
				var letterToSay:String;
				var done:Boolean = true;
				
				letterToSay = WordArray[iRound].slice(spellingLetter, spellingLetter + 1);
				
				switch(spellingLetter)
				{
					
					case 0:
					{
						trace(spellingLetter.toString());
						scene_mc.word.txt.txt1.textColor = LettColor2;
						break;
					}
					case 1:
					{
						trace(spellingLetter.toString());
						scene_mc.word.txt.txt2.textColor = LettColor2;
						break;
					}
					case 2:
					{
						trace(spellingLetter.toString());
						scene_mc.word.txt.txt3.textColor = LettColor2;
						break;
					}
					case 3:
					{
						trace(spellingLetter.toString());
						scene_mc.word.txt.txt4.textColor = LettColor2;
						break;
					}
					case 4:
					{
						trace(spellingLetter.toString());
						scene_mc.word.txt.txt5.textColor = LettColor2;
						break;
					}
				}
				replaceDisplayObjectWithBitmap( scene_mc.word.txt, scene_mc.word.strawberryWord_mc, 1, false, [outline2] );
				
				spellingLetter++;
				spellingCounter++;
				
				for(var i:int = 0; i < AlphabetArray.length; i++)
				{
					if(letterToSay == AlphabetArray[i])
					{
						done = false;
						sndLetterArray[i].Start();
						
					}
				}
				
				if(done)
				{
					for(var i:int = 0; i < BallArray.length; i++)
					{
						if(BallArray[i].currentFrame == 25)
						{
							BallArray[i].play();
						}
					}
					spellingCounter = 1000;
					sndWordArray[iRound].Start();
					RightSFX.Start();
				}
			}
		}
		
		
		private function replaceDisplayObject( src:DisplayObject, cont:MovieClip, tgt:DisplayObject, scale:Number = 1, shift:Boolean = true )
		{
			var rect:Rectangle = tgt.getBounds( tgt );
			
			src.x = 0;
			src.y = 0;
			src.name = tgt.name;
			src.alpha = tgt.alpha;
			
			if(cont.numChildren != 0)
			{
				while(cont.numChildren > 0)
				{
					cont.removeChildAt(0);
				}
			}
			cont.addChild( src );
		}
		
		private function DrawLetters():void
		{
			scene_mc.word.txt.txt1.text = "";
			scene_mc.word.txt.txt2.text = "";
			scene_mc.word.txt.txt3.text = "";
			scene_mc.word.txt.txt4.text = "";
			scene_mc.word.txt.txt5.text = "";
			
			scene_mc.word.txt.txt1.textColor = LettColor1;
			scene_mc.word.txt.txt2.textColor = LettColor1;
			scene_mc.word.txt.txt3.textColor = LettColor1;
			scene_mc.word.txt.txt4.textColor = LettColor1;
			scene_mc.word.txt.txt5.textColor = LettColor1;
			
			for(var i:int = 0; i < _currentWord.length; i++)
			{
				switch(i)
				{
					case 0:
					{
						scene_mc.word.txt.txt1.text = _currentWord.charAt(i);
						break;
					}
					case 1:
					{
						scene_mc.word.txt.txt2.text = _currentWord.charAt(i);
						break;
					}
					case 2:
					{
						scene_mc.word.txt.txt3.text = _currentWord.charAt(i);
						break;
					}
					case 3:
					{
						scene_mc.word.txt.txt4.text = _currentWord.charAt(i);
						break;
					}
					case 4:
					{
						scene_mc.word.txt.txt5.text = _currentWord.charAt(i);
						break;
					}
				}
			}
		}
		
		private function replaceDisplayObjectWithBitmap( src:DisplayObject, cont:MovieClip, scale:Number = 1, smooth:Boolean = false, filter:Array = null ):Bitmap
		{
			src.filters = filter;
			var bmp:Bitmap = createContentBitmap( src, scale, smooth );
			replaceDisplayObject( bmp, cont, src, scale );
			return bmp;
		}
		
		private function createContentBitmap( src:DisplayObject, scale:Number = 1, smooth:Boolean = false ):Bitmap
		{
			return new Bitmap( createContentBitmapData( src, scale ), "auto", smooth );
		}
		
		private function createContentBitmapData( src:DisplayObject, scale:Number = 1 ):BitmapData
		{
			var sizeRect:Rectangle = getDisplayObjectRect( src );
			var bmd:BitmapData = new BitmapData( sizeRect.width * scale, sizeRect.height * scale, true, 0 );
			var rect:Rectangle = src.getBounds( src );
			bmd.draw( src, new Matrix( scale, 0, 0, scale, Math.abs( rect.left ) * scale, Math.abs( rect.top ) * scale ) );
			return bmd;
		}
		
		private function getDisplayObjectRect( src:DisplayObject ):Rectangle
		{
			var result_rectangle:Rectangle = src.getBounds( src );
			var filterGenerater_rectangle:Rectangle = new Rectangle(0,0,result_rectangle.width, result_rectangle.height);
			var bmd:BitmapData = new BitmapData(result_rectangle.width, result_rectangle.height, true, 0x00000000);

			var filter_minimumX:Number = 0;
			var filter_minimumY:Number = 0;
			var filter_rectangle:Rectangle = result_rectangle.clone();

			var filtersLength:int = src.filters.length;
			for (var filtersIndex:int = 0; filtersIndex < filtersLength; filtersIndex++) {                                          
					var filter:BitmapFilter = src.filters[filtersIndex];

					filter_rectangle = bmd.generateFilterRect(filterGenerater_rectangle, filter);

					filter_minimumX = filter_minimumX + filter_rectangle.x;
					filter_minimumY = filter_minimumY + filter_rectangle.y;

					filterGenerater_rectangle = filter_rectangle.clone();
					filterGenerater_rectangle.x = 0;
					filterGenerater_rectangle.y = 0;

					bmd = new BitmapData(filterGenerater_rectangle.width, filterGenerater_rectangle.height, true, 0x00000000);                                              
			}

			// Reposition filter_rectangle back to global coordinates
			filter_rectangle.x = result_rectangle.x + filter_minimumX;
			filter_rectangle.y = result_rectangle.y + filter_minimumY;

			result_rectangle = filter_rectangle.clone();
			return result_rectangle;
		}
		
		
		
	}
}