﻿package  
{
	import flash.display.Bitmap;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.greensock.TweenLite;
	
	import com.cupcake.App;
	import clsMain;

	public class clsMainMenu extends WBZ_MainMenu 
	{
		//var Wubbzy:Wobbler;
		//var cherry:Wobbler;
		
		//var Main:clsMain;
		
		
		//private var A_Loader:AnimationLoader           = null;				
		
		public function clsMainMenu()
		{
			try
			{
				
				clsMain.Main.cNavBar.visible = false;		
				
				ButtonImage_ReadToMe = GlobalData.getBitmapData( "MainMenu_StarOff1" );
				ButtonImage_ReadMyself = GlobalData.getBitmapData( "MainMenu_StarOff3" );
				ButtonImage_JustABook = GlobalData.getBitmapData( "MainMenu_StarOff2" );
				ButtonImage_Games = GlobalData.getBitmapData( "MainMenu_StarOff4" );
				AnimBitmap_ReadToMe = new Bitmap( GlobalData.getBitmapData( "MainMenu_StarOn1" ) );
				AnimBitmap_ReadMyself = new Bitmap( GlobalData.getBitmapData( "MainMenu_StarOn3" ) );
				AnimBitmap_JustABook = new Bitmap( GlobalData.getBitmapData( "MainMenu_StarOn2" ) );
				AnimBitmap_Games = new Bitmap( GlobalData.getBitmapData( "MainMenu_StarOn4" ) );
				
				super();
				//lights_mc
				//scene_mc.lights_mc.addChild( new Bitmap( GlobalData.getBitmapData( "MainMenu_Lights" ) ) );					
				scene_mc.BlackScreen.addChild( new Bitmap( GlobalData.getBitmapData( "MainMenu_Blackness" ) ) );					
				/*
				TitleImages.push( scene_mc.logo_wubbzys_mc );
				TitleImages.push( scene_mc.logo_space_mc );
				TitleImages.push( scene_mc.logo_adventure_mc );
				
				TitleButtons.push( scene_mc.logo_wubbzys_hit );
				TitleButtons.push( scene_mc.logo_space_hit );
				TitleButtons.push( scene_mc.logo_adventure_hit );
				*/
				//scene_mc.Wubbzy_Mc.addChild( new Bitmap( GlobalData.getBitmapData( "MainMenu_Wubbzy" ) ) );					
				//Wubbzy = new Wobbler( scene_mc.Wubbzy_hit, scene_mc.Wubbzy_Mc );
				//Wubbzy.wobbleSounds = AppCode.wubbzySounds;
				//controls.push( Wubbzy );
				
				//scene_mc.cherry_mc.addChild( new Bitmap( GlobalData.getBitmapData( "MainMenu_Cherry" ) ) );					
				//cherry = new Wobbler( scene_mc.cherry_HIT, scene_mc.cherry_mc );
				//cherry.wobbleSounds = AppCode.wubbzySounds;
				//controls.push( cherry );
				
				//SetTitle();
			} catch ( e:Error ) { App.Log( "Error in clsMainMenu(): " + e.message + " " + e.getStackTrace() ); }			
		}
		
		public override function updateText():void
		{
			super.updateText();
			//Wubbzy.wobbleSounds = AppCode.wubbzySounds;
		}
		
		
		/**/
		public final override function Start():void{
			super.Start();
			
			
			if(clsMain.firstTurn)
			{
				TweenLite.delayedCall( 2, Go_Video );
				//btnNew.alpha = 1;
			}
			else
			{
				Start_Animation();
			}
			
		}		
		
		
		public function Go_Video():void{				
			TweenLite.killDelayedCallsTo( Go_Video );			
			clsMain.playVideo(0);								
		}	
		
		
		public function Start_Animation():void{
			TweenLite.killDelayedCallsTo( Start_Animation );		
			SetTitle();
			showAllButton();
			if(clsMain.firstTurn)
			{
				clsMain.firstTurn = false;
				//btnNew.alpha = 1;
			}
			scene_mc.BlackScreen.visible = false;
			scene_mc.stopHits_BTN.visible = false;
			clsMain.Main.cNavBar.visible = true;
			
		}		
		
		public function Go_Animation():void{
			if(clsMain.Main.cNavBar.visible){
				//clsMain.Main.cNavBar.Keep_Close();
				clsMain.Main.cNavBar.visible = false;
			}
			TweenLite.delayedCall( 0.5, Start_Animation );
		}	
		
		
		
	}
}