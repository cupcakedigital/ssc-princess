﻿package 
{
	import flash.display.Sprite;
	import flash.events.Event;	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.cupcake.BookPage;
	import com.cupcake.App;
	import com.cupcake.ExternalImage;
	import com.cupcake.Images;
	import com.cupcake.ReaderPopup;
	import com.cupcake.SoundCollection;
	import com.cupcake.Utils;
	import com.cupcake.SpriteSheetSequence;
	import com.SBLib.*;
	import com.cupcake.SoundEx;
	import com.DaveLibrary.clsSoundTransformEX;
	import com.greensock.events.LoaderEvent;
	
	
	public final class Page13 extends WBZ_BookPage
	{
		public var waveSounds:SoundCollection;
		
		public var sndFlower:SoundEx			= null;
		public var sndRainbow:SoundEx			= null;
		public var sndBush:SoundEx			= null;
		public var sndBerrykins:SoundEx			= null;
		
		
		private var Particles:SB_Particles			= null;

		//Core.
		public function Page13()
		{
			super();
			
			Particles = new SB_Particles(false);
			
			imageFolder = CONFIG::DATA + "images/pages/page13/";
			Images.Add( "BG", { container: "BG", fromFile: true } );
			Images.Add( "Sky", { container: "sky_mc", fromFile: true } );
			Images.Add( "Carriage_Girls", { container: "carriage_mc", fromFile: true } );
			Images.Add( "Blue_Flowers", { container: "flowersRight_mc", fromFile: true } );
			Images.Add( "Berrykins_Far", { container: scene_mc.berrykinsLeft_mc.berrykinsLeft_mc.berrykinsLeft_mc, fromFile: true } );
			Images.Add( "Flower", { container: scene_mc.flower_mc.flower_mc.flower_mc, fromFile: true } );
			Images.Add( "Rainbow", { container: scene_mc.rainbow_mc.rainbow_mc, fromFile: true } );
			Images.Add( "Plant_OL", { container: scene_mc.bush_mc.bush_mc.bush_mc, fromFile: true } );
			Images.Add( "Berrykins_Mid", { container: scene_mc.berrykinsRightUL_mc.berrykinsRightUL_mc.berrykinsRightUL_mc, fromFile: true } );
			Images.Add( "Berrykins_Near", { container: scene_mc.berrykinsRightOL_mc.berrykinsRightOL_mc.berrykinsRightOL_mc, fromFile: true } );
			
			sndFlower = new SoundEx( "sndFlower", CONFIG::DATA + "sounds/Animation_Sounds/P13_flower.mp3", App.soundVolume );
			sndRainbow = new SoundEx( "sndRainbow", CONFIG::DATA + "sounds/Animation_Sounds/P13_rainbow.mp3", App.soundVolume );
			sndBush = new SoundEx( "sndBush", CONFIG::DATA + "sounds/Animation_Sounds/P13_bush.mp3", App.soundVolume );
			sndBerrykins = new SoundEx( "sndBerrykins", CONFIG::DATA + "sounds/Animation_Sounds/P13_berrykins.mp3", App.soundVolume );

			waveSounds = SoundCollection.GetAnimSounds( "P13_wave", [1] );
			spriteSheetLoader.Add( "Strawberry", 60, waveSounds, null, false, false, null, null, null, scene_mc.carriage_BTN );
			spriteSheetLoader.Add( "Orange", 60, null, null, false, false, null, null, null, scene_mc.carriage_BTN );
			spriteSheetLoader.Add( "Cherry", 60, null, null, false, false, null, null, null, scene_mc.carriage_BTN );

			sparklers.Add(scene_mc.carriage_BTN,scene_mc.Sparkler1_sprkl);
			sparklers.Add(scene_mc.berrykinsRight_BTN,scene_mc.Sparkler2_sprkl);
			sparklers.Add(scene_mc.berrykinsLeft_BTN,scene_mc.Sparkler3_sprkl);
			sparklers.Add(scene_mc.flower_BTN,scene_mc.Sparkler4_sprkl);
			sparklers.Add(scene_mc.bush_BTN,scene_mc.Sparkler5_sprkl);
		}
		
		public final override function Init():void
		{
			if ( isInitialized ) return;

			musicFile = "sscmusic2";

			super.Init();
			Reset();
		}		
		
		public final override function Reset():void
		{
			super.Reset();

			Particles.Clear_Particles_Que();         //Clear Call.
			Particles.Clear_All_Particles(this);	 //Clear Call.

			cMain.removeStageMouseUp( P14onUp );	
			cMain.removeStageMouseDown( P01onDown );
		}

		public final override function Start():void
		{
			super.Start();
			cMain.addStageMouseUp( P14onUp );
			cMain.addStageMouseDown( P01onDown );
		}

		public override function Destroy():void
		{
			super.Destroy();
			
			Particles.Clear_Particles_Que();        //Clear Call.
			Particles.Clear_All_Particles(this);	//Clear Call.

			cMain.removeStageMouseUp( P14onUp );
			cMain.removeStageMouseDown( P01onDown );
		}
		
		//Updates.
		private function P01onDown( e:MouseEvent ):void
		{		
			switch(e.target.name)
			{
				case "berrykinsLeft_BTN":
				{
					if(scene_mc.berrykinsLeft_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykins.Start();
						}
						
						scene_mc.berrykinsLeft_mc.play();
					}
				}
				break;
				
				case "flower_BTN":
				{
					if(scene_mc.flower_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndFlower.Start();
						}
						
						Particles.Q_Particles(1, 12, 140, 160, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 315, 15, 0, 90, 1, 1);
						
						scene_mc.flower_mc.play();
					}
				}
				break;
				
				case "rainbow_BTN":
				{
					if(scene_mc.rainbow_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndRainbow.Start();
						}
						
						scene_mc.rainbow_mc.play();
					}
				}
				break;
				
				case "bush_BTN":
				{
					if(scene_mc.bush_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBush.Start();
						}
						
						Particles.Q_Particles(1, 12, 900, 300, "rainbow_sparkles", "flowerSparkle_mc", 0.03, 225, 20, 5, 90, 1, 1);
						
						scene_mc.bush_mc.play();
					}
				}
				break;
				
				case "berrykinsRight_BTN":
				{
					if(scene_mc.berrykinsRightOL_mc.currentFrame == 1)
					{
						if ( this.step == BookPage.STEP_DONE )
						{
							sndBerrykins.Start();
						}
						
						scene_mc.berrykinsRightOL_mc.play();
					}
				}
				break;
				
				
				
				
				
				
			}
		}				
		
		
		private function P14onUp( e:MouseEvent ):void
		{
			
			
			
		}
		
		
		protected final override function handleFrame( e:Event ):void
		{
			if(isRunning && !isPaused)
			{
				Particles.SB_Update(this);
				
				if(scene_mc.berrykinsRightOL_mc.currentFrame == 5)
				{
					
					scene_mc.berrykinsRightUL_mc.play();
				}
				
				
			}
			super.handleFrame( e );
		}		
		
		
		
		
		
		
	}
}